import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import en from './src/localization/en';

export const AVAILABLE_LANGUAGES = {
  en,
};

i18n.use(initReactI18next).init({
  lng: 'en',
  interpolation: {
    escapeValue: false,
  },
  resources: AVAILABLE_LANGUAGES,
  defaultNS: 'common',
});

export default i18n;
