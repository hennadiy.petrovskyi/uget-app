import axios from 'axios';
import uuid from 'react-native-uuid';

import { api } from '../api';
import { logger } from '../../utils/logger';
import { FileUploadEndpoints } from './endpoints';

const path = 'api/order';

export enum FileType {
  PROFILE_IMAGE = 'PROFILE_IMAGE',
  ORDER_IMAGE = 'ORDER_IMAGE',
}

export const uploadSingleFile = async (file: any, onSuccess?: () => void) => {
  const functionName = uploadSingleFile.name;

  const formData = new FormData();
  formData.append('type', FileType.PROFILE_IMAGE);
  formData.append('file', prepareImageToUpload(file));

  try {
    const response = await api.post(
      FileUploadEndpoints.UPLOAD_SINGLE,
      formData,
      {
        headers: {
          contentType: 'multipart/form-data',
        },
      },
    );

    if (response.status === 201) {
      return response;
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      // const fieldError = error.response.data;
    }

    logger.logError(path, functionName, error);
  }
};

export const uploadOrderPhotos = async (
  owner,
  photos: any,
  onSuccess: Function,
) => {
  const functionName = uploadOrderPhotos.name;

  const formData = new FormData();

  formData.append('type', FileType.ORDER_IMAGE);
  formData.append('owner', owner);
  photos.forEach((photo) => {
    const fileToUpload = prepareImageToUpload(photo.data);
    formData.append('files', fileToUpload);
  });

  try {
    const response = await api.post(
      FileUploadEndpoints.UPLOAD_MULTIPLE,
      formData,
      {
        headers: {
          'Content-Type': `multipart/form-data`,
        },
      },
    );

    if (response.status === 201) {
      onSuccess(response.data.photos);
    }
  } catch (error) {
    console.log(error);
  }
};

function prepareImageToUpload(data) {
  const { fileName, type, uri } = data;

  return {
    name: fileName,
    type,
    uri,
  };
}
