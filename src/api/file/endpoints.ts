import { API_URL } from '..';

const ENDPOINT_BASE_URL = API_URL + '/file';

export const FileUploadEndpoints = {
  UPLOAD_SINGLE: `${ENDPOINT_BASE_URL}/single`,
  UPLOAD_MULTIPLE: `${ENDPOINT_BASE_URL}/multiple`,
};
