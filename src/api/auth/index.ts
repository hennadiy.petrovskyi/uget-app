import { clearUserProfile } from './../../utils/store/profile/actions';
import axios, {
  AxiosError,
  AxiosInstance,
  AxiosInterceptorManager,
  AxiosRequestConfig,
} from 'axios';
import { api } from '../api';
import { AuthEndpoints } from './endpoint';

import { logger } from '../../utils/logger';
import { encryptedStorage } from '../../utils/encryptedStorage';
import { asyncStorage, Keys } from '../../utils/asyncStorage';
import { setAuthenticationStatus } from '../../utils/store/general/actions';
import { AppDispatch, store } from '../../utils/store';
import { publishOrder, publishRoundTrip, publishTrip } from '..';
import { hydrateUserProfile } from '../../utils/store/profile/actions';
import {
  clearOrdersState,
  ordersHydrateState,
} from '../../utils/store/orders/actions';
import {
  tripsClearState,
  tripsHydrateState,
} from '../../utils/store/trips/actions';
import {
  chatClearState,
  conversationsHydrateState,
} from '../../utils/store/chat/actions';
import { TripType } from '../../screens/TripsScreens/CreateTrip/CreateTrip';

const path = 'api/auth';

export const verifyPhoneNumber =
  (
    phoneNumber: string,
    handleSwitchToNextStep: (phoneNumber: string) => void,
    handleInputError: (type: any) => void,
  ) =>
  async (dispatch: AppDispatch) => {
    {
      const functionName = verifyPhoneNumber.name;

      const params = { phoneNumber };

      try {
        const response = await api.post(
          AuthEndpoints.VERIFY_PHONE_NUMBER(),
          params,
        );

        if (response.status === 201) {
          handleSwitchToNextStep(phoneNumber);
        }
      } catch (error) {
        if (axios.isAxiosError(error)) {
          const fieldError = error?.response?.data;

          handleInputError(fieldError.type);
          return;
        }
        console.log(error);
      }
    }
  };

export const verifyEmail =
  (token: string, onSuccess: () => void) => async (dispatch: AppDispatch) => {
    const functionName = verifyEmail.name;

    try {
      const response = await api.post(AuthEndpoints.VERIFY_EMAIL(), { token });

      if (response.status === 201) {
        handleHydrateStateWithUser(response.data.profile, dispatch);

        onSuccess();
      }
    } catch (error) {
      console.log(error);
    }
  };

export const logout = () => async (dispatch: AppDispatch) => {
  const functionName = logout.name;

  try {
    logoutStoreCleaner();

    await encryptedStorage.removeUserSession();
    await api.get(AuthEndpoints.LOGOUT());
  } catch (error) {
    logger.logError(path, functionName, error);
  }
};

export const refreshSession = async (
  refreshToken: string,
  config: AxiosRequestConfig,
): Promise<any> => {
  const functionName = refreshSession.name;

  api.defaults.headers.common['RefreshToken'] = refreshToken;

  try {
    const response = await api.get(AuthEndpoints.REFRESH_SESSION());

    if (response.status === 200) {
      store.dispatch(setAuthenticationStatus(true));

      config.headers['Authorization'] = response.data.accessToken;
      config.headers['RefreshToken'] = response.data.refreshToken;

      await encryptedStorage.storeUserSession(response.data);

      console.log('REFRESH SESSION : SUCCESS');
    }
  } catch (error) {
    delete api.defaults.headers.common['RefreshToken'];
    delete api.defaults.headers.common['Authorization'];
  }
};

export const verifySmsCode =
  (
    { phoneNumber, code }: { phoneNumber: string; code: string },
    handleNavigate: () => void,
    setFieldError: (type: any) => void,
  ) =>
  async (dispatch: AppDispatch) => {
    const functionName = verifySmsCode.name;

    const params = { phoneNumber, code };

    try {
      const response = await api.post(AuthEndpoints.VERIFY_CODE(), params);

      console.log(response);

      if (response.status === 201) {
        handleAuth(response.data, dispatch);

        publisherObserver(handleNavigate, dispatch);
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error.response && error.response.data;

        setFieldError(fieldError.type);
      }

      console.log(error);
    }
  };

export const register =
  (
    registerDto: {
      email: string;
      phoneNumber: any;
      password: string;
      firstName: string;
      lastName: string;
      recaptcha: string;
    },
    handleNavigate: () => void,
    handleInputsErrors: (errors: any[]) => void,
  ) =>
  async (dispatch: AppDispatch) => {
    const functionName = refreshSession.name;

    const params = registerDto;

    try {
      const response = await api.post(AuthEndpoints.REGISTER(), params);

      if (response?.status === 201) {
        handleAuth(response.data, dispatch).then(() => {
          publisherObserver(handleNavigate, dispatch);
        });
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const errors = error.response.data.errors;
        if (errors?.length) {
          handleInputsErrors(errors);
          return;
        }
      }

      logger.logError(path, functionName, error, params);
    }
  };

export const loginWithEmailAndPassword =
  ({
    email,
    password,
    handleNavigate,
    setFieldError,
  }: {
    email: string;
    password: string;
    handleNavigate: () => void;
    setFieldError: (inputName: string, type: any) => void;
  }) =>
  async (dispatch: AppDispatch) => {
    const functionName = loginWithEmailAndPassword.name;

    const params = { email, password };

    try {
      const response = await api.post(
        AuthEndpoints.LOGIN_WITH_CREDENTIALS(),
        params,
      );

      if (response?.status === 201) {
        handleAuth(response.data, dispatch);

        publisherObserver(handleNavigate, dispatch);
      }
    } catch (error: Error | AxiosError) {
      if (axios.isAxiosError(error)) {
        const fieldError = error.response?.data;

        console.log(functionName, error);

        setFieldError(fieldError.field, fieldError.type);
        return;
      }

      logger.logError(path, functionName, error, JSON.stringify(params));
    }
  };

export const changePhoneNumber =
  ({
    phoneNumber,
    handleNavigate,
    setFieldError,
  }: {
    phoneNumber: string;
    handleNavigate: (phoneNumber: string) => void;
    setFieldError: (inputName: string, type: any) => void;
  }) =>
  async (dispatch: AppDispatch) => {
    const functionName = changePhoneNumber.name;

    try {
      const response = await api.post(AuthEndpoints.CHANGE_PHONE_NUMBER(), {
        phoneNumber,
      });

      if (response?.status === 201) {
        handleNavigate(response.data.phoneNumber);
      }
    } catch (error: Error | AxiosError) {
      if (axios.isAxiosError(error)) {
        const fieldError = error.response?.data;
        setFieldError(fieldError.field, fieldError.type);
        return;
      }

      logger.logError(path, functionName, error);
    }
  };

export const changeEmail =
  ({
    email,
    handleNavigate,
    setFieldError,
  }: {
    email: string;
    handleNavigate: (email: string) => void;
    setFieldError: (inputName: string, type: any) => void;
  }) =>
  async (dispatch: AppDispatch) => {
    const functionName = changeEmail.name;

    try {
      const response = await api.post(AuthEndpoints.CHANGE_EMAIL(), { email });
      if (response?.status === 201) {
        handleNavigate(response.data.email);
      }
    } catch (error: Error | AxiosError) {
      if (axios.isAxiosError(error)) {
        const fieldError = error.response?.data;
        setFieldError(fieldError.field, fieldError.type);
        return;
      }

      logger.logError(path, functionName, error);
    }
  };

export const resendVerificationSmsCode =
  ({
    phoneNumber,
    handleStartCountdown,
  }: {
    phoneNumber: string;
    handleStartCountdown: () => void;
  }) =>
  async (dispatch: AppDispatch) => {
    const functionName = resendVerificationSmsCode.name;

    try {
      const response = await api.post(AuthEndpoints.RESEND_SMS_CODE(), {
        phoneNumber,
      });

      if (response?.status === 201) {
        handleStartCountdown();
      }
    } catch (error: Error | AxiosError) {
      if (axios.isAxiosError(error)) {
      }

      logger.logError(path, functionName, error);
    }
  };

export const resendEmailVerification =
  ({
    email,
    handleStartCountdown,
  }: {
    email: string;
    handleStartCountdown: () => void;
  }) =>
  async (dispatch: AppDispatch) => {
    const functionName = resendEmailVerification.name;

    try {
      const response = await api.post(
        AuthEndpoints.RESEND_EMAIL_VERIFICATION(),
        {
          email,
        },
      );

      if (response?.status === 201) {
        handleStartCountdown();
      }
    } catch (error: Error | AxiosError) {
      if (axios.isAxiosError(error)) {
      }

      logger.logError(path, functionName, error);
    }
  };

export const resetPassword =
  ({
    email,
    handleNavigate,
    setFieldError,
  }: {
    email: string;
    handleNavigate: () => void;
    setFieldError: (type: any) => void;
  }) =>
  async (dispatch: AppDispatch) => {
    const functionName = changeEmail.name;

    try {
      const response = await api.post(AuthEndpoints.RESET_PASSWORD(), {
        email,
      });

      if (response.status === 201) {
        handleNavigate();
      }
    } catch (error: Error | AxiosError) {
      if (axios.isAxiosError(error)) {
        const fieldError = error.response?.data;
        console.log(fieldError.type);

        setFieldError(fieldError.type);
        return;
      }

      logger.logError(path, functionName, error);
    }
  };

export const changePassword = async (
  changePasswordDto: { token: string; password: string },
  onSuccess: () => void,
  setFieldError?: () => void,
) => {
  const functionName = changeEmail.name;

  try {
    const response = await api.post(
      AuthEndpoints.CHANGE_PASSWORD,
      changePasswordDto,
    );

    if (response?.status === 201) {
      onSuccess();
    }
  } catch (error: Error | AxiosError) {
    if (axios.isAxiosError(error)) {
      // const fieldError = error.response?.data;
      // console.log(fieldError.type);
      // setFieldError(fieldError.type);
      // return;
    }

    logger.logError(path, functionName, error);
  }
};

export const updateProfile =
  (update: any, onSuccess: Function) => async (dispatch: AppDispatch) => {
    {
      const functionName = updateProfile.name;

      const params = update;

      try {
        const response = await api.put(AuthEndpoints.UPDATE_PROFILE, params);

        if (response.status === (200 || 201)) {
          handleHydrateStateWithUser(response.data.profile, dispatch);

          onSuccess(response.data.profile);
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

export const getUserProfile = () => async (dispatch: AppDispatch) => {
  try {
    const response = await api.get(AuthEndpoints.GET_PROFILE);

    if (response.status === 200) {
      handleHydrateStateWithUser(response.data, dispatch);

      dispatch(setAuthenticationStatus(true));
    }
  } catch (error) {
    console.log(error);
  }
};

async function publisherObserver(onSuccess, dispatch) {
  const tripToPublish = await asyncStorage.get(Keys.TRIP_TO_PUBLISH);
  const orderToPublish = await asyncStorage.get(Keys.ORDER_TO_PUBLISH);

  if (!tripToPublish && !orderToPublish) {
    onSuccess();
    return;
  }

  if (tripToPublish && !orderToPublish) {
    const deleteTripOnSuccess = async () => {
      await asyncStorage.delete(Keys.TRIP_TO_PUBLISH);

      onSuccess();
    };

    if (tripToPublish.type === TripType.ONE_WAY) {
      publishTrip(tripToPublish, deleteTripOnSuccess);
      return;
    }

    if (tripToPublish.type === TripType.ROUND) {
      publishRoundTrip(tripToPublish, deleteTripOnSuccess);
      return;
    }

    return;
  }

  if (!tripToPublish && orderToPublish) {
    const deleteOrderOnSuccess = async () => {
      await asyncStorage.delete(Keys.ORDER_TO_PUBLISH);

      onSuccess();
    };

    dispatch(publishOrder(orderToPublish, deleteOrderOnSuccess));
  }
}

async function handleAuth(
  data: {
    session: { accessToken: string; refreshToken: string };
    profile: any;
  },
  dispatch: AppDispatch,
) {
  const isSessionSaved = await encryptedStorage.storeUserSession(data.session);

  if (!isSessionSaved) {
    return;
  }

  const appSettings = await asyncStorage.get(Keys.SETTINGS);

  await asyncStorage.save(Keys.SETTINGS, {
    ...appSettings,
    isAuth: true,
  });

  handleHydrateStateWithUser(data.profile, dispatch);

  dispatch(setAuthenticationStatus(true));
}

export function handleHydrateStateWithUser(data, dispatch) {
  const { orders, trips, conversations, ...profile } = data;

  dispatch(hydrateUserProfile(profile));
  dispatch(ordersHydrateState(orders));
  dispatch(tripsHydrateState(trips));
  dispatch(conversationsHydrateState(conversations));
}

export const logoutStoreCleaner = () => {
  store.dispatch(setAuthenticationStatus(false));
  store.dispatch(clearUserProfile());
  store.dispatch(tripsClearState());
  store.dispatch(clearOrdersState());
  store.dispatch(chatClearState());
};

// export const verifyRecaptcha = async (token: string) => {
//   {
//     const functionName = verifyRecaptcha.name;

//     const params = { token };

//     try {
//       const response = await api.post(AuthEndpoints.VERIFY_RECAPTCHA, params);

//       if (response.status === 201) {
//         console.log(response.data);

//         return response.data;
//       }
//     } catch (error) {
//       console.log(error);
//     }
//   }
// };

// async function orderPublisherObserver(onSuccess, dispatch) {
//   const orderToPublish = await asyncStorage.get(Keys.ORDER_TO_PUBLISH);

//   if (!orderToPublish) {
//     onSuccess();
//     return;
//   }

//   async function modifiedOnSuccess() {
//     await asyncStorage.delete(Keys.ORDER_TO_PUBLISH);

//     onSuccess();
//   }

//   dispatch(publishOrder(orderToPublish, modifiedOnSuccess));
// }

// async function tripPublisherObserver(onSuccess) {
//   const tripToPublish = await asyncStorage.get(Keys.TRIP_TO_PUBLISH);

//   if (!tripToPublish) {
//     onSuccess();
//     return;
//   }

//   if (tripToPublish.type === TripType.ONE_WAY) {
//     publishTrip(tripToPublish, modifiedOnSuccess);
//     return;
//   }

//   if (tripToPublish.type === TripType.ROUND) {
//     publishRoundTrip(tripToPublish, modifiedOnSuccess);
//     return;
//   }

//   async function modifiedOnSuccess() {
//     await asyncStorage.delete(Keys.TRIP_TO_PUBLISH);

//     onSuccess();
//   }
// }
