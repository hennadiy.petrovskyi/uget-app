import { API_URL } from '..';

const ENDPOINT_BASE_URL = API_URL + '/auth';

export const AuthEndpoints = {
  LOGIN_WITH_CREDENTIALS: () => `${ENDPOINT_BASE_URL}/login`,
  LOGOUT: () => `${ENDPOINT_BASE_URL}/logout`,
  REFRESH_SESSION: () => `${ENDPOINT_BASE_URL}/refresh-session`,
  VERIFY_CODE: () => `${ENDPOINT_BASE_URL}/verify/code`,
  VERIFY_EMAIL: () => `${ENDPOINT_BASE_URL}/verify/email`,
  VERIFY_PHONE_NUMBER: () => `${ENDPOINT_BASE_URL}/verify/phone`,
  REGISTER: () => `${ENDPOINT_BASE_URL}/register`,
  CHANGE_PHONE_NUMBER: () => `${ENDPOINT_BASE_URL}/change/phone`,
  CHANGE_EMAIL: () => `${ENDPOINT_BASE_URL}/change/email`,
  RESEND_SMS_CODE: () => `${ENDPOINT_BASE_URL}/resend/sms-code`,
  RESEND_EMAIL_VERIFICATION: () => `${ENDPOINT_BASE_URL}/resend/email`,
  RESET_PASSWORD: () => `${ENDPOINT_BASE_URL}/reset-password`,
  CHANGE_PASSWORD: `${ENDPOINT_BASE_URL}/change/password`,
  GET_PROFILE: `${ENDPOINT_BASE_URL}/profile`,
  VERIFY_RECAPTCHA: `${ENDPOINT_BASE_URL}/verify/recaptcha`,
  UPDATE_PROFILE: `${ENDPOINT_BASE_URL}/profile`,
};
