import { SettingsEndpoints } from './settings/endpoints';
import { PricingEndpoints } from './pricing/endpoints';
import axios, { AxiosError, AxiosResponse } from 'axios';
import { Buffer } from 'buffer';
import axiosRetry from 'axios-retry';
import SocketIOClient from 'socket.io-client/dist/socket.io.js';

import { API_URL, refreshSession, WEB_SOCKET } from '.';
import { encryptedStorage } from '../utils/encryptedStorage';
import { logger } from '../utils/logger';
import { store } from '../utils/store';
import { setAuthenticationStatus } from '../utils/store/general/actions';
import { AuthEndpoints } from './auth/endpoint';
import { OrderEndpoints } from './order/endpoint';
import { FileUploadEndpoints } from './file/endpoints';
import { TripEndpoints } from './trip/endpoint';
import { ChatEndpoints } from './chat/endpoints';
import { RatingEndpoints } from './rating/endpoints';
import { DeviceTokenEndpoints } from './push-notifications/endpoints';

export const webSocket = () =>
  SocketIOClient(WEB_SOCKET, {
    withCredentials: true,
    jsonp: false,
  });

export const api = axios.create({
  baseURL: API_URL,
  responseType: 'json',
});

axiosRetry(api, { retries: 3 });

const protectedEndpoints: string[] = [
  AuthEndpoints.CHANGE_EMAIL(),
  AuthEndpoints.CHANGE_PHONE_NUMBER(),
  AuthEndpoints.LOGOUT(),
  AuthEndpoints.GET_PROFILE,
  AuthEndpoints.UPDATE_PROFILE,
  OrderEndpoints.GET_MY_ORDERS,
  OrderEndpoints.PUBLISH_ORDER,
  OrderEndpoints.UNPUBLISH_ORDER,
  OrderEndpoints.PUBLISH_INACTIVE,
  OrderEndpoints.DELETE_ORDER,
  OrderEndpoints.GET_ORDERS_BY_TRIP_PARAMS,
  OrderEndpoints.REQUEST_DELIVERY,
  OrderEndpoints.CANCEL_DELIVERY_REQUEST,
  OrderEndpoints.REQUEST_PICKUP,
  OrderEndpoints.REQUEST_FINISH_DELIVERY,
  OrderEndpoints.ACCEPT_DELIVERY,
  OrderEndpoints.VERIFY_PICKUP,
  OrderEndpoints.VERIFY_ORDER_IS_DELIVERED,
  OrderEndpoints.REQUEST_ORDER_OWNER_DETAILS,
  OrderEndpoints.GET_ONE,
  OrderEndpoints.GET_TRIP_ACTIVE_ORDERS,
  OrderEndpoints.GET_TRIP_DELIVERED_ORDERS,
  OrderEndpoints.REQUEST_PICKUP,
  OrderEndpoints.REQUEST_DELIVERY,
  TripEndpoints.GET_ALL_REQUESTS,
  TripEndpoints.GET_MY_TRIPS,
  TripEndpoints.PUBLISH_TRIP,
  TripEndpoints.PUBLISH_ROUND_TRIP,
  TripEndpoints.GET_ONE_FILTERED_BY_ORDER,
  TripEndpoints.GET_MANY_FILTERED_BY_ORDER_PARAMS,
  TripEndpoints.DELETE_TRIP,
  TripEndpoints.REQUEST_ORDER_DELIVERY,
  TripEndpoints.ACCEPT_ORDER_DELIVERY,
  TripEndpoints.CANCEL_ORDER_DELIVERY_REQUEST,
  TripEndpoints.REQUEST_TRIP_OWNER_DETAILS,
  FileUploadEndpoints.UPLOAD_SINGLE,
  FileUploadEndpoints.UPLOAD_MULTIPLE,
  ChatEndpoints.START_CHAT,
  ChatEndpoints.GET_ALL_CONVERSATIONS,
  RatingEndpoints.RATE_USER_ROLE,
  DeviceTokenEndpoints.REFRESH_DEVICE_TOKEN,
  DeviceTokenEndpoints.SAVE_DEVICE_TOKEN,
  PricingEndpoints.CALCULATE_DELIVERY_PRICE,
  SettingsEndpoints.UPDATE_SETTINGS,
];

api.interceptors.request.use(async (config) => {
  const isProtectedEndpoint = protectedEndpoints.includes(config.url);

  const serviceName = 'API';

  if (isProtectedEndpoint) {
    logger.log(`PROTECTED RESOURCE : ${config.url}`, { serviceName });
    const session = await encryptedStorage.retrieveUserSession();

    if (!session) {
      logger.log('UNAUTHORIZED : SESSION TOKENS IS NULL', {
        serviceName,
      });

      delete config.headers['Authorization'];
      delete config.headers['RefreshToken'];

      store.dispatch(setAuthenticationStatus(false));

      return config;
    }

    const accessTokenPayload = extractJwtPayload(session.accessToken);
    const refreshTokenPayload = extractJwtPayload(session.refreshToken);

    if (
      !refreshTokenPayload ||
      (accessTokenPayload && !refreshTokenPayload) ||
      (!accessTokenPayload && !refreshTokenPayload)
    ) {
      logger.log('UNAUTHORIZED : TOKENS INVALID', { serviceName });

      delete config.headers['Authorization'];
      store.dispatch(setAuthenticationStatus(false));
      await encryptedStorage.removeUserSession();

      return config;
    }

    if (!accessTokenPayload && refreshTokenPayload) {
      console.log('REFRESH FROM REQUEST');

      await refreshSession(session.refreshToken, config);

      return config;
    }

    config.headers.Authorization = session.accessToken;
    logger.log('AUTHORIZATION : VALID', { serviceName });
  }

  return config;
});

api.interceptors.response.use(
  (response: AxiosResponse) => {
    return response;
  },
  (err: AxiosError) => {
    const status = err.response?.status || 500;
    const { config } = err;

    console.log('RESPONSE ERROR :', config.url);

    switch (status) {
      // authentication (token related issues)
      case 401: {
        encryptedStorage.retrieveUserSession().then((session) => {
          if (!session) {
            return;
          }

          console.log('REFRESH FROM RESPONSE');

          refreshSession(session.refreshToken, config);
        });

        return Promise.reject(err);
      }

      // forbidden (permission related issues)
      case 403: {
        store.dispatch(setAuthenticationStatus(false));
        return Promise.reject(err);
      }

      // bad request
      case 400: {
        return Promise.reject(err);
      }

      // not found
      case 404: {
        return Promise.reject(err);
      }

      // conflict
      case 409: {
        return Promise.reject(err);
      }

      // unprocessable
      case 422: {
        return Promise.reject(err);
      }

      // generic api error (server related) unexpected
      default: {
        return Promise.reject(err);
      }
    }
  },
);

export function extractJwtPayload(jwt: string) {
  if (!jwt) {
    return;
  }
  interface IJwtPayload {
    exp: number;
    iat: number;
    role: 'user';
    sub: string;
  }
  const base64Payload = jwt.split('.')[1];
  const payload = JSON.parse(
    Buffer.from(base64Payload, 'base64').toString(),
  ) as IJwtPayload;

  // console.log('token exp :', new Date(payload.exp * 1000));

  if (Date.now() >= payload.exp * 1000) {
    return;
  }

  return payload;
}
