import { API_URL } from '..';

const ENDPOINT_BASE_URL = API_URL + '/user-details';

export const UserDetailsEndpoints = {
  GET_USER_DETAILS_BY_UUID: `${ENDPOINT_BASE_URL}/`,
};
