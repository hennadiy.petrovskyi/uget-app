import { UserDetailsEndpoints } from './endpoints';
import { api } from '../api';

export const getUserDetails = async (userUUID: string) => {
  const functionName = getUserDetails.name;

  try {
    const response = await api.post(
      UserDetailsEndpoints.GET_USER_DETAILS_BY_UUID,
      { userUUID },
    );

    if (response.status === 201) {
      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
};
