export {
  loginWithEmailAndPassword,
  logout,
  refreshSession,
  register,
  verifyPhoneNumber,
  verifySmsCode,
  changeEmail,
  changePassword,
  changePhoneNumber,
  resendEmailVerification,
  resendVerificationSmsCode,
  updateProfile,
  verifyEmail,
  getUserProfile,
} from './auth';

export {
  publishOrder,
  deletePublishedOrder,
  getOrdersByTripsParams,
  orderOwnerRequestOrderDelivery,
  publishInactiveOrder,
  orderOwnerCancelDeliveryRequest,
  unPublishedOrder,
  getMyOrders,
  requestOrderIsPickup,
  requestOrderIsDelivered,
  orderOwnerAcceptDeliveryRequest,
  getOrderByTrip,
  verifyOrderIsPickup,
  verifyOrderIsDelivered,
  getOrderOwnerDetails,
  getOneDependOnRequestedAuthRecord,
  getTravellerActiveOrders,
  getTripDeliveredOrders,
  loadDrafts,
} from './order';

export { googlePlacesFindCity, googlePlacesFindCountry } from './location';
export {
  deleteTrip,
  getMyTrips,
  publishTrip,
  publishRoundTrip,
  getTripsByOrderParams,
  travelerRequestOrderDelivery,
  travelerCancelOrderDelivery,
  travelerAcceptOrderDelivery,
  getTripOwnerDetails,
  getOrderRequests,
} from './trip';

export { rateUserRole } from './rating';
export { startConversation, getAllConversations } from './chat';
export { getUserDetails } from './user-details';

export { registerDeviceToken, refreshDeviceToken } from './push-notifications';
export { calculateDeliveryPrice } from './pricing';
export { updateNotificationSettings } from './settings';

export const IP = '192.168.31.82';
export const PORT = 3000;

// export const API_URL = `http://${IP}:${PORT}`;
// export const WEB_SOCKET = `ws://${IP}:${PORT}`;

// export const API_URL =
//   'http://ugetapi-env-1.eba-mjupdrvk.eu-central-1.elasticbeanstalk.com';
// export const WEB_SOCKET =
//   'ws://ugetapi-env-1.eba-mjupdrvk.eu-central-1.elasticbeanstalk.com:9000';

export const API_URL = 'https://api.uget.co';
export const WEB_SOCKET = `ws://api.uget.co:9000`;
