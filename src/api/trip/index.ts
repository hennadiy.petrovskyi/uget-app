import axios from 'axios';
import uuid from 'react-native-uuid';
import { getOrdersByTripsParams } from '..';
import { NotificationID } from '../../components/ModalNotifications/ModalNotificationsProvider';

import { logger } from '../../utils/logger';
import { AppDispatch, store } from '../../utils/store';
import { addLocalNotification } from '../../utils/store/general/actions';
import {
  ordersHydrateState,
  ordersUpdateOrderFilteredByTrip,
} from '../../utils/store/orders/actions';
import {
  tripsHydrateState,
  tripsSetFilteredTrips,
  tripsUpdateTrip,
  updateTripFilteredByOrder,
} from '../../utils/store/trips/actions';
import { api } from '../api';
import { uploadOrderPhotos } from '../file';
import { TripEndpoints } from './endpoint';

const path = 'api/order';

export const publishTrip = async (trip: any, onSuccess: () => void) => {
  const functionName = publishTrip.name;

  const {
    description,
    arrivalDate,
    departureDate,
    fromCountry,
    fromCity,
    fromArea,
    toCountry,
    toCity,
    toArea,
  } = trip;

  const modifiedTrip = {
    uuid: uuid.v4(),
    description,
    arrivalDate,
    departureDate,
    locations: {
      from: {
        country: fromCountry.id,
        city: fromCity.id,
        area: fromArea,
      },
      to: { country: toCountry.id, city: toCity.id, area: toArea },
    },
  };

  const params = modifiedTrip;

  // console.log(params);

  try {
    const response = await api.post(TripEndpoints.PUBLISH_TRIP, params);

    if (response.status === 201) {
      store.dispatch(tripsHydrateState(response.data));

      onSuccess();
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      // const fieldError = error.response.data;
    }

    logger.logError(path, functionName, error, params);
  }
};

export const publishRoundTrip = async (trip: any, onSuccess: () => void) => {
  const functionName = publishRoundTrip.name;

  const {
    description,
    arrivalDate,
    departureDate,
    fromCountry,
    fromCity,
    fromArea,
    toCountry,
    toCity,
    toArea,
    returnArrivalDate,
    returnDepartureDate,
  } = trip;

  const trips = [
    {
      uuid: uuid.v4(),
      description,
      arrivalDate,
      departureDate,
      locations: {
        from: {
          country: fromCountry.id,
          city: fromCity.id,
          area: fromArea,
        },
        to: { country: toCountry.id, city: toCity.id, area: toArea },
      },
    },
    {
      uuid: uuid.v4(),
      description: '',
      arrivalDate: returnArrivalDate,
      departureDate: returnDepartureDate,
      locations: {
        from: { country: toCountry.id, city: toCity.id, area: toArea },
        to: {
          country: fromCountry.id,
          city: fromCity.id,
          area: fromArea,
        },
      },
    },
  ];

  const params = { trips };

  try {
    const response = await api.post(TripEndpoints.PUBLISH_ROUND_TRIP, params);

    if (response.status === 201) {
      store.dispatch(tripsHydrateState(response.data));

      onSuccess();
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      // const fieldError = error.response.data;
    }

    logger.logError(path, functionName, error, params);
  }
};

export const getMyTrips = () => async (dispatch: AppDispatch) => {
  const functionName = getMyTrips.name;

  try {
    const response = await api.get(TripEndpoints.GET_MY_TRIPS);

    if (response.status === 200) {
      dispatch(tripsHydrateState(response.data));
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      const fieldError = error?.response?.data;

      // if (!fieldError?.statusCode) {
      //   logger.logError(path, functionName, error);
      // }
    }
    console.log(error);
  }
};

export const getTripsByOrderParams =
  (params) => async (dispatch: AppDispatch) => {
    const functionName = getTripsByOrderParams.name;

    try {
      const response = await api.get(
        TripEndpoints.GET_MANY_FILTERED_BY_ORDER_PARAMS,
        {
          params,
        },
      );

      if (response.status === 200) {
        dispatch(tripsSetFilteredTrips(response.data));
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error?.response?.data;

        // if (!fieldError?.statusCode) {
        //   logger.logError(path, functionName, error);
        // }
      }
      console.log(error);
    }
  };

export const getOneTripFilteredByOrder =
  (params) => async (dispatch: AppDispatch) => {
    const functionName = getTripsByOrderParams.name;

    try {
      const response = await api.get(TripEndpoints.GET_ONE_FILTERED_BY_ORDER, {
        params,
      });

      if (response.status === 200) {
        dispatch(updateTripFilteredByOrder(response.data));
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error?.response?.data;

        // if (!fieldError?.statusCode) {
        //   logger.logError(path, functionName, error);
        // }
      }
      console.log(error);
    }
  };

export const deleteTrip =
  (uuid: string, onSuccess: Function) => async (dispatch: AppDispatch) => {
    const functionName = deleteTrip.name;

    const params = { uuid };

    try {
      const response = await api.delete(TripEndpoints.DELETE_TRIP, {
        data: params,
      });

      console.log(response);

      if (response.status === 200) {
        dispatch(tripsHydrateState(response.data));

        onSuccess();
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error.response.data;

        if (!fieldError.statusCode) {
          logger.logError(path, functionName, error);
        }
      }
    }
  };

export const travelerRequestOrderDelivery =
  (
    tripRequestDto: { orderUUID: string; tripUUID: string },
    onSuccess: Function,
  ) =>
  async (dispatch: AppDispatch) => {
    const functionName = travelerRequestOrderDelivery.name;

    try {
      const response = await api.put(
        TripEndpoints.REQUEST_ORDER_DELIVERY,
        tripRequestDto,
      );

      if (response.status === 200) {
        dispatch(tripsUpdateTrip(response.data));

        onSuccess();
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error?.response?.data;

        // if (!fieldError?.statusCode) {
        //   logger.logError(path, functionName, error);
        // }
      }
      console.log(error);
    }
  };

export const travelerAcceptOrderDelivery =
  (
    tripRequestDto: { orderUUID: string; tripUUID: string },
    onSuccess: Function,
  ) =>
  async (dispatch: AppDispatch) => {
    const functionName = travelerAcceptOrderDelivery.name;

    try {
      const response = await api.put(
        TripEndpoints.ACCEPT_ORDER_DELIVERY,
        tripRequestDto,
      );

      if (response.status === 200) {
        dispatch(tripsUpdateTrip(response.data));

        onSuccess();
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error?.response?.data;

        // if (!fieldError?.statusCode) {
        //   logger.logError(path, functionName, error);
        // }
      }
      console.log(error);
    }
  };

export const travelerCancelOrderDelivery =
  (
    tripRequestDto: { orderUUID: string; tripUUID: string },
    onSuccess: Function,
  ) =>
  async (dispatch: AppDispatch) => {
    const functionName = travelerCancelOrderDelivery.name;

    try {
      const response = await api.put(
        TripEndpoints.CANCEL_ORDER_DELIVERY_REQUEST,
        tripRequestDto,
      );

      if (response.status === 200) {
        dispatch(tripsUpdateTrip(response.data));

        onSuccess();
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error?.response?.data;

        // if (!fieldError?.statusCode) {
        //   logger.logError(path, functionName, error);
        // }
      }
      console.log(error);
    }
  };

export const getTripOwnerDetails = async (params: any) => {
  const functionName = getTripOwnerDetails.name;

  try {
    const response = await api.post(
      TripEndpoints.REQUEST_TRIP_OWNER_DETAILS,
      params,
    );

    if (response.status === 201) {
      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
};

export const getOrderRequests = async (orderUUID: string, trips: string[]) => {
  const functionName = getOrderRequests.name;

  try {
    const response = await api.post(TripEndpoints.GET_ALL_REQUESTS, {
      orderUUID,
      trips,
    });

    if (response.status === 201) {
      return response.data;
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      const fieldError = error?.response?.data;

      // if (!fieldError?.statusCode) {
      //   logger.logError(path, functionName, error);
      // }
    }
    console.log(error);
  }
};
