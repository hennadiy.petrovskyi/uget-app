import { Platform } from 'react-native';
import { api } from '../api';
import { DeviceTokenEndpoints } from './endpoints';

export enum DeviceTokenPlatformType {
  IOS = 'APN',
  ANDROID = 'GCM',
}

export const registerDeviceToken = async (deviceToken: string) => {
  const functionName = registerDeviceToken.name;

  const type = {
    ios: DeviceTokenPlatformType.IOS,
    android: DeviceTokenPlatformType.ANDROID,
  };

  try {
    const response = await api.post(DeviceTokenEndpoints.SAVE_DEVICE_TOKEN, {
      deviceToken,
      type: type[Platform.OS],
    });

    if (response.status === 201) {
      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
};

export const refreshDeviceToken = async (refreshDeviceTokenDto: {
  newToken: string;
  oldToken: string;
}) => {
  const functionName = refreshDeviceToken.name;

  try {
    const response = await api.patch(
      DeviceTokenEndpoints.REFRESH_DEVICE_TOKEN,
      refreshDeviceTokenDto,
    );

    if (response.status === 201) {
      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
};
