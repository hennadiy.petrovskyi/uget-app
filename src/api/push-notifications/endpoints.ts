import { API_URL } from '..';

const ENDPOINT_BASE_URL = API_URL + '/device-token';

export const DeviceTokenEndpoints = {
  SAVE_DEVICE_TOKEN: `${ENDPOINT_BASE_URL}/`,
  REFRESH_DEVICE_TOKEN: `${ENDPOINT_BASE_URL}/refresh`,
};
