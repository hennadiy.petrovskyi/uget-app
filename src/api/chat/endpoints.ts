import { API_URL } from '..';

const ENDPOINT_BASE_URL = API_URL + '/chat';

export const ChatEndpoints = {
  START_CHAT: `${ENDPOINT_BASE_URL}/join`,
  GET_ALL_CONVERSATIONS: `${ENDPOINT_BASE_URL}/conversations`,
};
