import { ChatEndpoints } from './endpoints';
import { api } from '../api';
import { logger } from '../../utils/logger';
import { conversationsHydrateState } from '../../utils/store/chat/actions';

export const startConversation = async (
  params: any,
  dispatch: Function,
  onSuccess: (id: string) => void,
) => {
  const functionName = startConversation.name;

  try {
    const response = await api.post(ChatEndpoints.START_CHAT, params);

    if (response.status === 201) {
      console.log('startConversation', response);

      dispatch(conversationsHydrateState(response.data.conversations));
      onSuccess(response.data.conversationId);
    }
  } catch (error) {
    console.log(functionName, error);
  }
};

export const getAllConversations = async () => {
  const functionName = getAllConversations.name;

  try {
    const response = await api.get(ChatEndpoints.GET_ALL_CONVERSATIONS);

    if (response.status === 200) {
      return response.data;
    }
  } catch (error) {
    console.log(functionName, error);
  }
};
