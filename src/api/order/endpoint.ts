import { API_URL } from '..';

const ENDPOINT_BASE_URL = API_URL + '/order';

export const OrderEndpoints = {
  PUBLISH_ORDER: `${ENDPOINT_BASE_URL}`,
  PUBLISH_INACTIVE: `${ENDPOINT_BASE_URL}/publish/inactive`,
  UNPUBLISH_ORDER: `${ENDPOINT_BASE_URL}/unpublish`,
  GET_MY_ORDERS: `${ENDPOINT_BASE_URL}/my`,
  GET_ORDER_FOR_TRIP: `${ENDPOINT_BASE_URL}/filtered/one`,
  GET_ORDERS_BY_TRIP_PARAMS: `${ENDPOINT_BASE_URL}/filtered/many`,
  DELETE_ORDER: `${ENDPOINT_BASE_URL}`,
  CANCEL_DELIVERY_REQUEST: `${ENDPOINT_BASE_URL}/delivery/cancel`,
  REQUEST_DELIVERY: `${ENDPOINT_BASE_URL}/delivery/request`,
  ACCEPT_DELIVERY: `${ENDPOINT_BASE_URL}/delivery/accept`,
  REQUEST_PICKUP: `${ENDPOINT_BASE_URL}/delivery/request/pickup`,
  VERIFY_PICKUP: `${ENDPOINT_BASE_URL}/delivery/verify/pickup`,
  REQUEST_FINISH_DELIVERY: `${ENDPOINT_BASE_URL}/delivery/request/finish`,
  VERIFY_ORDER_IS_DELIVERED: `${ENDPOINT_BASE_URL}/delivery/verify/delivery`,
  REQUEST_ORDER_OWNER_DETAILS: `${ENDPOINT_BASE_URL}/owner/details`,
  GET_ONE: `${ENDPOINT_BASE_URL}/one`,
  GET_TRIP_ACTIVE_ORDERS: `${ENDPOINT_BASE_URL}/trip/active`,
  GET_TRIP_DELIVERED_ORDERS: `${ENDPOINT_BASE_URL}/trip/delivered`,
};
