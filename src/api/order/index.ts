import axios from 'axios';
import uuid from 'react-native-uuid';
import { NotificationID } from '../../components/ModalNotifications/ModalNotificationsProvider';
import { OwnerRole } from '../../screens/OrderScreens/CreateOrder/context/context';
import { asyncStorage, Keys } from '../../utils/asyncStorage';

import { logger } from '../../utils/logger';
import { AppDispatch, store } from '../../utils/store';
import { addLocalNotification } from '../../utils/store/general/actions';
import {
  ordersHydrateState,
  ordersSetFilteredByTrip,
  ordersUpdateOne,
  ordersUpdateOrderFilteredByTrip,
  setDrafts,
} from '../../utils/store/orders/actions';

import { api } from '../api';
import { uploadOrderPhotos } from '../file';
import { OrderEndpoints } from './endpoint';

const path = 'api/order';

export const publishOrder =
  (order: any, onSuccess: () => void) => async (dispatch: AppDispatch) => {
    const functionName = publishOrder.name;

    const pickup = { ...parseOrderLocationsFields(order.locations.pickup) };
    const endpoint = { ...parseOrderLocationsFields(order.locations.endpoint) };

    const modifiedOrder = {
      ...order,
      uuid: uuid.v4(),
      locations: {
        pickup,
        endpoint,
      },
    };

    const { photos, deliveryPrice, ...orderInformation } = modifiedOrder;

    const params = orderInformation;

    try {
      const response = await api.post(OrderEndpoints.PUBLISH_ORDER, params);

      if (response.status === 201) {
        if (!photos.length) {
          dispatch(ordersHydrateState(response.data.orders));
          onSuccess();

          return;
        }

        await uploadOrderPhotos(response.data.owner, photos, (photosUrls) => {
          dispatch(
            ordersHydrateState(
              response.data.orders.map((el) => {
                if (el.uuid === modifiedOrder.uuid) {
                  el.photos = photosUrls;
                }

                return el;
              }),
            ),
          );

          onSuccess();
        });
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        // const fieldError = error.response.data;
      }

      logger.logError(path, functionName, error, params);
    }
  };

export const getMyOrders = () => async (dispatch: AppDispatch) => {
  const functionName = getMyOrders.name;

  try {
    const response = await api.get(OrderEndpoints.GET_MY_ORDERS);

    if (response.status === 200) {
      dispatch(ordersHydrateState(response.data));
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      const fieldError = error?.response?.data;

      // if (!fieldError?.statusCode) {
      //   logger.logError(path, functionName, error);
      // }
    }
    console.log(error);
  }
};

export const getOrderByTrip =
  (queryParams: { orderUUID: string; tripUUID: string }) =>
  async (dispatch: AppDispatch) => {
    try {
      const response = await api.get(OrderEndpoints.GET_ORDER_FOR_TRIP, {
        params: queryParams,
      });

      if (response.status === 200) {
        dispatch(ordersUpdateOrderFilteredByTrip(response.data));
      }
    } catch (error) {
      console.log(error);
    }
  };

export const deletePublishedOrder = async (uuid: string) => {
  const functionName = deletePublishedOrder.name;

  const params = { uuid };

  try {
    const response = await api.delete(OrderEndpoints.DELETE_ORDER, {
      data: params,
    });

    if (response.status === 200) {
      store.dispatch(ordersHydrateState(response.data));
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      const fieldError = error.response.data;

      if (!fieldError.statusCode) {
        logger.logError(path, functionName, error);
      }
    }
  }
};

export const unPublishedOrder = async (uuid: string, onSuccess) => {
  const functionName = unPublishedOrder.name;

  const params = { uuid };

  try {
    const response = await api.post(OrderEndpoints.UNPUBLISH_ORDER, params);

    console.log(response.data);

    if (response.status === 201) {
      store.dispatch(ordersHydrateState(response.data));

      onSuccess();
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      const fieldError = error.response.data;

      if (!fieldError.statusCode) {
        logger.logError(path, functionName, error);
      }
    }
  }
};

export const getTravellerActiveOrders = async (
  uuid: string,
  onSuccess?: Function,
) => {
  const functionName = getTravellerActiveOrders.name;

  const params = { tripUUID: uuid };

  try {
    const response = await api.post(
      OrderEndpoints.GET_TRIP_ACTIVE_ORDERS,
      params,
    );

    if (response.status === 201) {
      return response.data;
    }
  } catch (error) {}
};

export const getTripDeliveredOrders = async (
  uuid: string,
  onSuccess?: Function,
) => {
  const functionName = getTripDeliveredOrders.name;

  const params = { tripUUID: uuid };

  try {
    const response = await api.post(
      OrderEndpoints.GET_TRIP_DELIVERED_ORDERS,
      params,
    );

    if (response.status === 201) {
      console.log(response);

      return response.data;
    }
  } catch (error) {}
};

export const publishInactiveOrder = async (
  uuid: string,
  onSuccess: Function,
) => {
  const functionName = publishInactiveOrder.name;

  const params = { uuid };

  try {
    const response = await api.post(OrderEndpoints.PUBLISH_INACTIVE, params);

    if (response.status === 201) {
      store.dispatch(ordersHydrateState(response.data));

      await onSuccess();
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      const fieldError = error.response.data;

      if (!fieldError.statusCode) {
        logger.logError(path, functionName, error);
      }
    }
  }
};

export const getOrdersByTripsParams =
  (params: {
    tripUUID: string;
    arrivalDate: string;
    departureDate: string;
    from: string;
    to: string;
  }) =>
  async (dispatch: AppDispatch) => {
    const functionName = getOrdersByTripsParams.name;

    console.log(params);

    try {
      const response = await api.get(OrderEndpoints.GET_ORDERS_BY_TRIP_PARAMS, {
        params,
      });

      if (response.status === 200) {
        dispatch(ordersSetFilteredByTrip(response.data));
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error?.response?.data;

        // if (!fieldError?.statusCode) {
        //   logger.logError(path, functionName, error);
        // }
      }
      console.log(error);
    }
  };

export const orderOwnerRequestOrderDelivery =
  (params, onSuccess) => async (dispatch: AppDispatch) => {
    const functionName = orderOwnerRequestOrderDelivery.name;

    try {
      const response = await api.put(OrderEndpoints.REQUEST_DELIVERY, params);

      if (response.status === 200) {
        dispatch(ordersUpdateOne(response.data));

        onSuccess();
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error?.response?.data;

        // if (!fieldError?.statusCode) {
        //   logger.logError(path, functionName, error);
        // }
      }
      console.log(error);
    }
  };

export const orderOwnerCancelDeliveryRequest =
  (params, onSuccess) => async (dispatch: AppDispatch) => {
    const functionName = orderOwnerCancelDeliveryRequest.name;

    try {
      const response = await api.put(
        OrderEndpoints.CANCEL_DELIVERY_REQUEST,
        params,
      );

      if (response.status === 200) {
        dispatch(ordersUpdateOne(response.data));

        onSuccess();
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error?.response?.data;

        // if (!fieldError?.statusCode) {
        //   logger.logError(path, functionName, error);
        // }
      }
      console.log(error);
    }
  };

export const orderOwnerAcceptDeliveryRequest =
  (params, onSuccess: Function) => async (dispatch: AppDispatch) => {
    const functionName = orderOwnerAcceptDeliveryRequest.name;

    try {
      const response = await api.put(OrderEndpoints.ACCEPT_DELIVERY, params);

      if (response.status === 200) {
        dispatch(ordersUpdateOne(response.data));

        onSuccess();
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error?.response?.data;

        // if (!fieldError?.statusCode) {
        //   logger.logError(path, functionName, error);
        // }
      }
      console.log(error);
    }
  };

export const requestOrderIsPickup = async (
  params: any,
  onSuccess: Function,
) => {
  const functionName = requestOrderIsPickup.name;

  try {
    const response = await api.post(OrderEndpoints.REQUEST_PICKUP, params);

    if (response.status === 201) {
      onSuccess(onSuccess);

      return response.data;
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      const fieldError = error?.response?.data;

      // if (!fieldError?.statusCode) {
      //   logger.logError(path, functionName, error);
      // }
    }
    console.log(error);
  }
};

export const verifyOrderIsPickup =
  (params: any, onSuccess: Function, setFieldError: Function) =>
  async (dispatch: AppDispatch) => {
    const functionName = verifyOrderIsPickup.name;

    try {
      const response = await api.post(OrderEndpoints.VERIFY_PICKUP, params);

      if (response.status === 201) {
        dispatch(ordersUpdateOrderFilteredByTrip(response.data));

        onSuccess();
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error.response && error.response.data;

        setFieldError(fieldError.type);
      }
      console.log(error);
    }
  };

export const requestOrderIsDelivered = async (
  params: any,
  onSuccess: Function,
) => {
  const functionName = requestOrderIsDelivered.name;

  try {
    const response = await api.post(
      OrderEndpoints.REQUEST_FINISH_DELIVERY,
      params,
    );

    if (response.status === 201) {
      onSuccess();
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      const fieldError = error?.response?.data;

      // if (!fieldError?.statusCode) {
      //   logger.logError(path, functionName, error);
      // }
    }
    console.log(error);
  }
};

export const verifyOrderIsDelivered =
  (params: any, onSuccess: Function) => async (dispatch: AppDispatch) => {
    const functionName = verifyOrderIsDelivered.name;

    try {
      const response = await api.post(
        OrderEndpoints.VERIFY_ORDER_IS_DELIVERED,
        params,
      );

      if (response.status === 201) {
        dispatch(ordersUpdateOrderFilteredByTrip(response.data));

        onSuccess(onSuccess);
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const fieldError = error?.response?.data;

        // if (!fieldError?.statusCode) {
        //   logger.logError(path, functionName, error);
        // }
      }
      console.log(error);
    }
  };

export const getOrderOwnerDetails = async (params: any) => {
  const functionName = getOrderOwnerDetails.name;

  try {
    const response = await api.post(
      OrderEndpoints.REQUEST_ORDER_OWNER_DETAILS,
      params,
    );

    if (response.status === 201) {
      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
};

function parseOrderLocationsFields(location) {
  const { country, type, city, area } = location;

  return { country: country.id, city: city.id, type, area };
}

export const getOneDependOnRequestedAuthRecord = async (orderUUID: string) => {
  const functionName = getOneDependOnRequestedAuthRecord.name;

  try {
    const response = await api.post(OrderEndpoints.GET_ONE, {
      orderUUID,
    });

    if (response.status === 201) {
      return response.data;
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      const fieldError = error?.response?.data;

      // if (!fieldError?.statusCode) {
      //   logger.logError(path, functionName, error);
      // }
    }
    console.log(error);
  }
};

export const loadDrafts = () => async (dispatch: AppDispatch) => {
  try {
    const drafts = await asyncStorage.get(Keys.DRAFTS);

    if (!drafts) {
      asyncStorage.save(Keys.DRAFTS, []);
      return;
    }

    if (drafts && drafts.length) {
      dispatch(setDrafts({ drafts }));
    }
  } catch (error) {
    console.log(error);
  }
};
