import { api } from '../api';
import { PricingEndpoints } from './endpoints';

export const calculateDeliveryPrice = async (calculatePriceDto: any) => {
  const functionName = calculateDeliveryPrice.name;

  try {
    const response = await api.post(
      PricingEndpoints.CALCULATE_DELIVERY_PRICE,
      calculatePriceDto,
    );

    if (response.status === 201) {
      console.log(response.data);

      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
};
