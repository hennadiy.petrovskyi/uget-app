// pricing
// calculate
import { API_URL } from '..';

const ENDPOINT_BASE_URL = API_URL + '/pricing';

export const PricingEndpoints = {
  CALCULATE_DELIVERY_PRICE: `${ENDPOINT_BASE_URL}/calculate`,
};
