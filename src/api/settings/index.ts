import { SettingsEndpoints } from './endpoints';
import { api } from '../api';
import { logger } from '../../utils/logger';
import { AppDispatch } from '../../utils/store';
import {
  setEmailNotificationStatus,
  setPushNotificationStatus,
} from '../../utils/store/profile/actions';

export const updateNotificationSettings =
  (update: { push?: boolean; email?: boolean }) =>
  async (dispatch: AppDispatch) => {
    const functionName = updateNotificationSettings.name;

    try {
      const response = await api.patch(
        SettingsEndpoints.UPDATE_SETTINGS,
        update,
      );

      if (response.status === 200) {
        const type = Object.keys(response.data)[0];
        const value = Object.values(response.data)[0] as boolean;

        if (type === 'email') {
          dispatch(setEmailNotificationStatus(value));
        }

        if (type === 'push') {
          dispatch(setPushNotificationStatus(value));
        }

        return response.data;
      }
    } catch (error) {
      console.log(functionName, error);
    }
  };
