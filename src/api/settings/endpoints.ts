import { API_URL } from '..';

const ENDPOINT_BASE_URL = API_URL + '/settings';

export const SettingsEndpoints = {
  UPDATE_SETTINGS: `${ENDPOINT_BASE_URL}`,
};
