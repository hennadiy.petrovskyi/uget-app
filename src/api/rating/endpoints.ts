import { API_URL } from '..';

const ENDPOINT_BASE_URL = API_URL + '/rating';

export const RatingEndpoints = {
  RATE_USER_ROLE: `${ENDPOINT_BASE_URL}/`,
};
