import { RatingEndpoints } from './endpoints';
import { api } from '../api';

export const rateUserRole = async (rateUserDto: any) => {
  const functionName = rateUserRole.name;

  try {
    const response = await api.put(RatingEndpoints.RATE_USER_ROLE, rateUserDto);

    if (response.status === 200) {
      return response;
    }
  } catch (error) {
    console.log(functionName, error);
  }
};
