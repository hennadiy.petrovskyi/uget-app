import axios, { AxiosError } from 'axios';
import { api } from '../api';
import { LocationEndpoints } from './endpoint';

import { logger } from '../../utils/logger';
import { encryptedStorage } from '../../utils/encryptedStorage';
import { asyncStorage, Keys } from '../../utils/asyncStorage';
import { setAuthenticationStatus } from '../../utils/store/general/actions';
import { AppDispatch } from '../../utils/store';

const path = 'api/auth';

export const googlePlacesFindCountry = async (
  { value, type }: { value: string; type: 'country' },
  setSuggestions: Function,
) => {
  const functionName = googlePlacesFindCountry.name;
  const params = { input: value, language: 'en', type };

  try {
    const response = await api.post(LocationEndpoints.FIND_COUNTRY, params);

    if (response.status === 200) {
      setSuggestions(response.data);

      return;
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      // const fieldError = error.response.data;
    }

    logger.logError(path, functionName, error, params);
  }
};

export const googlePlacesFindCity = async (
  {
    value,
    type,
    countryId,
  }: { value: string; type: 'city'; countryId: string },
  setSuggestions: Function,
) => {
  const functionName = googlePlacesFindCity.name;
  const params = { input: value, language: 'en', type, countryId };

  try {
    const response = await api.post(LocationEndpoints.FIND_CITY, params);

    if (response.status === 200) {
      setSuggestions(response.data);

      return;
    }
  } catch (error) {
    if (axios.isAxiosError(error)) {
      // const fieldError = error.response.data;
    }

    logger.logError(path, functionName, error, params);
  }
};
