import { API_URL } from '..';

const ENDPOINT_BASE_URL = API_URL + '/location';

export const LocationEndpoints = {
  FIND_COUNTRY: `${ENDPOINT_BASE_URL}/find/country`,
  FIND_CITY: `${ENDPOINT_BASE_URL}/find/city`,
};
