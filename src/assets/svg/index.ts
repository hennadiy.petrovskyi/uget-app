export { default as INCREASE } from './increase.svg';
export { default as SUBTRACT } from './subtract.svg';
export { default as ADD_PHOTO } from './addPhoto.svg';
export { default as ARROW_BOTTOM } from './arrowBottom.svg';
export { default as CALENDAR } from './calendar.svg';
export { default as SELECTED } from './selected.svg';
export { default as UN_SELECTED } from './unSelected.svg';
export { default as MY_ORDER_ICON } from './myOrderIcon.svg';
export { default as HOME_ICON } from './homeIcon.svg';
export { default as ACTIVE_ORDER_ICON } from './activeOrderIcon.svg';
export { default as ACTIVE_HOME_ICON } from './activeHomeIcon.svg';
export { default as MESSAGES_ICON } from './messages.svg';
export { default as ACTIVE_MESSAGES_ICON } from './messageActive.svg';
export { default as PROFILE_ICON } from './profile.svg';
export { default as MY_TRIPS_ICON } from './myTrips.svg';
export { default as ACTIVE_MY_TRIPS_ICON } from './myTripsActive.svg';
export { default as MASTERCARD } from './mastercard.svg';
export { default as CHECK } from './check.svg';
export { default as RATE } from './rate.svg';
export { default as SEND_MESSAGE } from './sendMessage.svg';
export { default as PATH } from './path.svg';
export { default as TRASH } from './trash.svg';
export { default as LEFT_ARROW } from './leftArrow.svg';
export { default as CLOSE } from './icon-close.svg';
export { default as DOORS } from './authorize-to-publish.svg';
export { default as CREDIT_CARD } from './credit-card.svg';
export { default as LOCATION } from './location.svg';
export { default as RATING_STAR } from './star-filled.svg';
export { default as RATING_STAR_HALF } from './star-half.svg';
export { default as RATING_STAR_OUTLINED } from './star-outlined.svg';
export { default as PLANE } from './plane.svg';
export { default as REQUEST } from './request.svg';
export { default as EARNINGS } from './earnings.svg';
export { default as ACCEPTED } from './accepted.svg';
export { default as WEIGHT } from './weight.svg';
export { default as PLANE_IN_CLOUDS } from './addd-tip-ilustration.svg';
export { default as RIGHT_ARROW } from './rightArrow.svg';
export { default as CLOUDS } from './clouds.svg';
export { default as PHONE } from './phone.svg';
export { default as EMAIL } from './email.svg';
export { default as SUCCESS } from './success.svg';
export { default as WARNING } from './warning.svg';
export { default as BIG_STAR } from './big-star.svg';
export { default as BIG_STAR_YELLOW } from './big-star-yellow.svg';
export { default as ACTIVE_PROFILE_ICON } from './active-profile-icon.svg';
export { default as ARROW_RIGHT } from './arrow-right.svg';
export { default as ARROW_LEFT } from './arrow-left.svg';
export { default as EMPTY_IMAGE } from './empty-image.svg';
