export default {
  NORMAL_400: 'FiraSans-Regular',
  NORMAL_ITALIC_400: 'FiraSans-Italic',
  MEDIUM_500: 'FiraSans-Medium',
  SEMI_BOLD_600: 'FiraSans-SemiBold',
  BOLD_700: 'FiraSans-Bold',
};
