import { Linking } from 'react-native';

const config = {
  screens: {
    TabsRoute: {
      screens: {
        HomeScreen: {
          path: 'home',
        },
        Orders: {
          path: 'orders',
          screens: {
            MyOrders: 'my',
            CreateOrder: 'add',
            TravelersList: 'travelers',
            Requests: 'requests',
            // Checkout: 'checkout',
            // CreditCardDetails: 'credit-card',
          },
        },
        Trips: {
          path: 'trips',
          screens: {
            MyTrips: 'my',
            AddTrip: 'add',
            CreateTrip: 'create',
            TripsOrders: 'orders',
            AcceptedOrders: 'to-deliver',
            DeliveredOrders: 'delivered',
          },
        },
        Messages: {
          path: 'messages',
          screens: {
            ChatScreen: 'all',
            IndividualChatScreen: 'private',
          },
        },
        Profile: {
          path: 'profile',
          screens: {
            ProfileInitScreen: 'init',
            UserDetails: 'details',
            EditProfile: 'edit',
            AppSettings: 'settings',
            InviteFriends: 'invite',
          },
        },
      },
    },
    OnBoarding: {
      path: 'onboarding',
    },
    Auth: {
      path: 'auth',
      screens: {
        SignUp: 'signup',
        LoginByPhone: 'login/phone',
        LoginVerificationCode: 'login/code',
        LoginByEmail: 'login/email',
        VerifyEmail: {
          path: 'verification/email/:id',
          parse: {
            id: (id: string) => id,
          },
        },
        VerifyPhoneNumber: 'verification/phone',
        FinishRegistration: 'finish-registration',
        ForgotPassword: 'forgot-password',
        ChangePhoneNumber: 'change/phone',
        ChangeEmail: 'change/email',
        ChangePassword: {
          path: 'change/password/:id',
          parse: {
            id: (id: string) => id,
          },
        },
      },
    },
  },
};

export const linking = {
  prefixes: ['uget://'],
  config,
};
