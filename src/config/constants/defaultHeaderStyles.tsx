import React from 'react';
import { Text } from 'react-native';

import { COLORS, NAVIGATION } from '.';
import { ButtonGoBack } from '../../components';

const {
  SIGN_UP,
  LOG_IN_VERIFICATION_CODE,
  FORGOT_PASSWORD,
  CHANGE_EMAIL,
  CHANGE_PHONE_NUMBER,
  VERIFY_PHONE_NUMBER,
  VERIFY_EMAIL,
} = NAVIGATION.AUTHENTICATION;

const { TRAVELERS_LIST, REQUESTS } = NAVIGATION.ORDER_SCREENS;
const { TRIPS_ORDERS, ACCEPTED_ORDERS, DELIVERED_ORDERS } = NAVIGATION.TRIPS;

const { APP_SETTINGS, EDIT_PROFILE, INVITE_FRIENDS, PROFILE } =
  NAVIGATION.PROFILE;

export const headerStyle = {
  headerBackTitleVisible: false,
  headerShown: true,
  title: '',
  headerTintColor: COLORS.BLACK,
  headerShadowVisible: false,
};

const screensWithBackNavigation = [
  SIGN_UP,
  LOG_IN_VERIFICATION_CODE,
  FORGOT_PASSWORD,
  CHANGE_PHONE_NUMBER,
  CHANGE_EMAIL,
  TRAVELERS_LIST,
  TRIPS_ORDERS,
  REQUESTS,
  APP_SETTINGS,
  EDIT_PROFILE,
  INVITE_FRIENDS,
  PROFILE,
  ACCEPTED_ORDERS,
  DELIVERED_ORDERS,
];

export function handleStackNavigatorTopHeaderOptions(
  routeName: string,
  handleGoBack: () => void,
) {
  return {
    ...headerStyle,
    headerLeft: () =>
      screensWithBackNavigation.includes(routeName) ? (
        <ButtonGoBack handleGoBack={handleGoBack} />
      ) : (
        <Text />
      ),
  };
}
