import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';

const { t } = useTranslation();

export const fields = {
  firstName: Yup.string()
    .min(3, t('common:inputs.firstName.errors.min'))
    .max(50, t('common:inputs.firstName.errors.max'))
    .matches(/[a-zA-Z]/, t('common:inputs.firstName.errors.onlyLatin'))
    .required(t('common:inputs.firstName.errors.required')),

  lastName: Yup.string()
    .min(3, t('common:inputs.lastName.errors.min'))
    .max(50, t('common:inputs.lastName.errors.max'))
    .matches(/[a-zA-Z]/, t('common:inputs.lastName.errors.onlyLatin'))
    .required(t('common:inputs.lastName.errors.required')),

  email: Yup.string()
    .email(t('common:inputs.email.errors.format'))
    .required(t('common:inputs.email.errors.required')),

  phoneNumber: Yup.object()
    .shape({
      code: Yup.string(),
      countryCode: Yup.string(),
      number: Yup.string().required('Phone number is required'),
      formatted: Yup.string(),
      isValid: Yup.boolean().oneOf([true], 'Invalid number format'),
    })
    .required(),

  password: Yup.string()
    .min(6, t('common:inputs.password.errors.min'))
    .max(18, t('common:inputs.password.errors.max'))
    .required(t('common:inputs.password.errors.required')),

  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password')], t('common:inputs.password.errors.match'))
    .required(t('common:inputs.password.errors.required')),
};
