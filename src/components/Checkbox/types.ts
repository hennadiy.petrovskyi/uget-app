import { StyleProp } from 'react-native';

export type PrimaryButtonProps = {
  txt: string;
  onPress?: () => void;
  children?: Element;
  style?: StyleProp<any>;
  disabled?: boolean;
};
