import React, { memo, useState } from 'react';
import { TouchableOpacity} from 'react-native';

import {
  CHECK
} from '../../assets/svg';
import styles from './styles';


const Checkbox = () => {
  const [check, setCheck] = useState(true)
  return (
    <>
      {check ?
        <TouchableOpacity
          style={styles.containerFill}
          onPress={() => {
            setCheck(!check)
          }}
        >
          <CHECK />
        </TouchableOpacity>
        :
        <TouchableOpacity
          style={styles.container}
          onPress={() => {
            setCheck(!check)
          }}
        >
        </TouchableOpacity>
      }
    </>

  );
};

export default memo(Checkbox);
