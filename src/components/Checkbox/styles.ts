import { COLORS } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  containerFill: {
    height: 20,
    width: 20,
    borderColor: COLORS.MAIN_COLOR,
    backgroundColor: COLORS.MAIN_COLOR,
    borderWidth: 2,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    height: 20,
    width: 20,
    borderColor: COLORS.MAIN_COLOR,
    borderWidth: 2,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  
});
