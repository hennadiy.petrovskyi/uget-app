import React, { memo } from 'react';
import { Image, Text, View } from 'react-native';
import { Rating } from '..';

import styles from './styles';

export default memo(
  ({
    owner,
    withMarginVertical = true,
  }: {
    owner: { profileImage: any; fullName: string; rating: any };
    withMarginVertical: boolean;
  }) => {
    const renderProfileImage = () => {
      if (owner.profileImage) {
        return (
          <Image
            style={styles.userPhoto}
            source={{
              uri: owner.profileImage.url,
            }}
          />
        );
      }

      return <View style={styles.userPhoto} />;
    };

    return (
      <View
        style={[
          styles.container,
          withMarginVertical && styles.withMarginVertical,
        ]}
      >
        {renderProfileImage()}
        <View style={styles.innerContainer}>
          <Text style={styles.name}>{owner.fullName}</Text>
          <Rating value={owner.rating.average} />
        </View>
      </View>
    );
  },
);
