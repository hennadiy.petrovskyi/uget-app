import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  containerInner: {
    height: '100%',
    justifyContent: 'space-between',
  },

  wrapper: {
    paddingHorizontal: 50,
    paddingTop: 37,
  },
});
