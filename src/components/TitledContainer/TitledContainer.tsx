import React, { FC, memo } from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import { ScreenTitle } from '..';

import styles from './styles';
import { ITitledScreenContainerProps } from './types';

const TitledContainer: FC<ITitledScreenContainerProps> = ({
  title,
  titleAlign,
  children,
  titleMarginBottom,
  withKeyboardAvoiding,
}) => {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
      style={styles.container}
    >
      <SafeAreaView style={styles.containerInner}>
        <ScrollView
          keyboardShouldPersistTaps={
            Platform.OS === 'ios' ? 'handled' : 'never'
          }
          style={styles.wrapper}
        >
          <ScreenTitle
            text={title}
            align={titleAlign}
            marginBottom={titleMarginBottom || 24}
          />
          {children}
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

export default memo(TitledContainer);
