import { ReactNode } from 'react';

export interface ITitledScreenContainerProps {
  title: string;
  titleAlign?: 'center' | 'left' | 'right';
  children: ReactNode;
  titleMarginBottom?: number;
  withKeyboardAvoiding?: boolean;
}
