import React, { FC, memo } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';

import { ButtonSkipForNowProps } from './types';
import styles from './styles';

const ButtonSkipForNow: FC<ButtonSkipForNowProps> = ({ handlePress }) => {
  const { t } = useTranslation();

  return (
    <TouchableOpacity style={styles.container} onPress={handlePress}>
      <Text style={[styles.btn]}>{t('common:buttons.skipForNow')}</Text>
    </TouchableOpacity>
  );
};

export default memo(ButtonSkipForNow);
