import { COLORS, FONT_FAMILY } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  icon: {
    marginRight: 8.59,
  },

  btn: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    flexDirection: 'column',
    fontSize: 16,
    lineHeight: 19,
    color: COLORS.MAIN_COLOR,
  },
});
