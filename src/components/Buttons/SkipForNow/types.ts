export type ButtonSkipForNowProps = {
  handlePress: () => void;
};
