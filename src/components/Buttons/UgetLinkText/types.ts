import { StyleProp } from 'react-native';

type TAlign = 'center' | 'left' | 'right';

type Size = 'default' | 'small';

export type UgetLinkTextProps = {
  text: string;
  align?: TAlign;
  marginBottom?: number;
  outerStyles?: StyleProp<any>;
  size?: Size;
  underline?: boolean;
  onPress?: Function;
  disabled?: boolean;
};
