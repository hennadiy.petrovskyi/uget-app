import React, { FC, memo } from 'react';
import { Text, TouchableOpacity } from 'react-native';

import { UgetLinkTextProps } from './types';
import styles from './styles';

const UgetLinkText: FC<UgetLinkTextProps> = ({
  text,
  size = 'default',
  marginBottom,
  outerStyles,
  underline,
  onPress,
  disabled,
}) => {
  return (
    <TouchableOpacity disabled={disabled} onPress={onPress}>
      <Text
        style={[
          styles.link,
          marginBottom && { marginBottom },
          { small: styles.small, default: styles.default }[size],
          underline && styles.underline,
          outerStyles && outerStyles,
        ]}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};

export default memo(UgetLinkText);
