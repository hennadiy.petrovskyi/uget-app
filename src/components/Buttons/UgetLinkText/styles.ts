import { COLORS, FONT_FAMILY } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  link: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    color: COLORS.MAIN_COLOR,
    alignSelf: 'auto',
  },

  underline: {
    textDecorationLine: 'underline',
    textDecorationColor: COLORS.MAIN_COLOR,
  },

  default: { fontSize: 16, lineHeight: 22, fontFamily: FONT_FAMILY.MEDIUM_500 },
  small: { fontFamily: FONT_FAMILY.NORMAL_400 },
});
