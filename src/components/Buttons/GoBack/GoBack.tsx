import React, { FC, memo } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/core';

import { LEFT_ARROW } from '../../../assets/svg';
import { ButtonGoBackPropsProps } from './types';
import styles from './styles';

const ButtonGoBack: FC<ButtonGoBackPropsProps> = ({
  handleGoBack,
}: ButtonGoBackPropsProps) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  return (
    <TouchableOpacity style={styles.container} onPress={handleGoBack}>
      <LEFT_ARROW style={styles.icon} />
      <Text style={[styles.btn]}>{t('common:buttons.back')}</Text>
    </TouchableOpacity>
  );
};

export default memo(ButtonGoBack);
