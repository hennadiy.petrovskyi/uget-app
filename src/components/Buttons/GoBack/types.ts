export type ButtonGoBackPropsProps = {
  handleGoBack: () => void;
};
