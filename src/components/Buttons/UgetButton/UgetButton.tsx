import React, { FC, memo } from 'react';
import { TouchableOpacity, Text } from 'react-native';

import { UgetButtonProps } from './types';
import styles from './styles';

const UgetButton: FC<UgetButtonProps> = ({
  label,
  type,
  disabled,
  marginBottom,
  width,
  outerContainerStyles,
  onPress = () => {},
}) => {
  function handleButtonStylesByType() {
    let buttonStyle;

    if (type === 'outlined') {
      buttonStyle = styles.containerOutlinedGreen;
    }

    if (type === 'warning') {
      buttonStyle = styles.containerOutlinedRed;
    }

    if (type === 'primary') {
      buttonStyle = styles.containerPrimary;
    }

    return [
      styles.container,
      buttonStyle,
      outerContainerStyles && outerContainerStyles,
      marginBottom && { marginBottom },
      disabled && styles.disabled,
      width && { width },
    ];
  }

  function handleTextColor() {
    let textColor;

    if (type === 'outlined') {
      textColor = styles.textOutlinedGreen;
    }

    if (type === 'warning') {
      textColor = styles.textOutlinedRed;
    }

    if (type === 'primary') {
      textColor = styles.textPrimary;
    }

    return [styles.text, textColor];
  }

  return (
    <TouchableOpacity
      disabled={disabled}
      style={handleButtonStylesByType()}
      onPress={onPress}
    >
      <Text style={handleTextColor()}>{label}</Text>
    </TouchableOpacity>
  );
};

export default memo(UgetButton);
