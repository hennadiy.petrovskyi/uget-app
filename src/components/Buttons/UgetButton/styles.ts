import { COLORS, FONT_FAMILY } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    height: 36,
    borderRadius: 6,
    borderWidth: 2,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },

  text: {
    paddingHorizontal: 10,
    fontFamily: FONT_FAMILY.MEDIUM_500,
    fontSize: 16,
    lineHeight: 20,
  },

  containerPrimary: {
    backgroundColor: COLORS.MAIN_COLOR,
    borderColor: COLORS.MAIN_COLOR,
  },

  containerOutlinedGreen: {
    backgroundColor: COLORS.GREEN_LIGHT,
    borderColor: COLORS.MAIN_COLOR,
  },

  containerOutlinedRed: {
    backgroundColor: COLORS.PINK,
    borderColor: COLORS.RED,
  },

  disabled: {
    backgroundColor: COLORS.MAIN_DISABLED,
    borderColor: COLORS.MAIN_DISABLED,
  },

  textPrimary: { color: COLORS.CLEAR_WHITE },
  textOutlinedGreen: { color: COLORS.MAIN_COLOR },
  textOutlinedRed: { color: COLORS.RED },
});
