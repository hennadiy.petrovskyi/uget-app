import { StyleProp } from 'react-native';

type TButtonTypes = 'primary' | 'outlined' | 'warning';

export type UgetButtonProps = {
  label: string;
  type: TButtonTypes;
  disabled?: boolean;
  marginBottom?: number;
  outerContainerStyles?: StyleProp<any>;
  width?: number;
  onPress: (args?: any) => any;
};
