export interface ITabs {
  label: string;
  isActive: boolean;
  filter: string[] | string;
}

export type UgetTabsProps = {
  tabs: ITabs[];
  data: any;
  renderItem: Function;
  onRefreshControl?: Function;
};
