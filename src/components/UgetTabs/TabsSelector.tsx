import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import { Animated, Text, TouchableOpacity, View } from 'react-native';
import { useSelector } from 'react-redux';

import styles from './styles';

export const TabSelector = ({ tabs, currentIndex, setIndex, setFilter }) => {
  const isAuth = useSelector((state) => state.general.isAuth);
  const [containerWidth, setWidth] = useState(0);

  const tabWidth = containerWidth / tabs.length;
  const selectorPosition = tabWidth * currentIndex;

  const selectorAnimatedValue = useRef(
    new Animated.Value(selectorPosition),
  ).current;

  useEffect(() => {
    if (tabWidth) {
      moveToSelected();
    }
  }, [currentIndex, tabWidth]);

  useEffect(() => {
    moveToSelected();
  }, [isAuth]);

  const xVal = selectorAnimatedValue.interpolate({
    inputRange: [0, containerWidth],
    outputRange: [0, containerWidth],
  });

  const animStyle = {
    transform: [
      {
        translateX: xVal,
      },
    ],
  };

  function onPageLayout(event) {
    const { width } = event.nativeEvent.layout;
    setWidth(width);
  }

  function moveToSelected() {
    Animated.timing(selectorAnimatedValue, {
      toValue: selectorPosition,
      duration: 300,
      useNativeDriver: true,
    }).start();
  }

  return (
    <View style={styles.tabsSelectorContainer} onLayout={onPageLayout}>
      <View style={[styles.tabLabelsContainer]}>
        {tabs.map((tab, index) => {
          function handleSelectTab() {
            setFilter(tab.filter);
            setIndex(index);
          }

          return (
            <TouchableOpacity
              key={index}
              style={[styles.tab, , { width: tabWidth }]}
              onPress={handleSelectTab}
              disabled={!tab.isActive}
            >
              <Text
                style={[styles.tabText, !tab.isActive && styles.inactiveLabel]}
              >
                {tab.label}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>

      <View style={styles.range}>
        <Animated.View
          style={[styles.selected, { width: tabWidth }, animStyle]}
        />
      </View>
    </View>
  );
};
