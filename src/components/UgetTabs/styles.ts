import { COLORS } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },

  tabsSelectorContainer: {
    marginBottom: 24,
    position: 'relative',
  },

  tabLabelsContainer: { flexDirection: 'row' },

  tab: { marginBottom: 6 },

  tabText: {
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '500',
    color: COLORS.BLACK,
    textAlign: 'center',
  },

  range: {
    backgroundColor: COLORS.GREY2,
    width: '100%',
    height: 4,
  },

  selected: {
    height: 4,
    backgroundColor: COLORS.MAIN_COLOR,
  },

  inactiveLabel: {
    color: COLORS.GREY,
  },
});
