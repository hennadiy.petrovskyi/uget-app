import React, { FC, memo, useEffect, useState } from 'react';
import { FlatList, View } from 'react-native';

import { TabSelector } from './TabsSelector';

import { NAVIGATION } from '../../config/constants';
import { UgetTabsProps } from './types';
import styles from './styles';
import { EmptyTab, RefreshControlScrollView } from '..';

const UgetTabs: FC<UgetTabsProps> = ({
  tabs,
  data,
  renderItem,
  onRefreshControl,
}) => {
  const [filter, setFilter] = useState<string | string[]>();
  const [currentIndex, setIndex] = useState(0);

  useEffect(() => {
    const activeTabs = tabs.filter((el) => el.isActive);

    if (tabs && tabs.length && activeTabs.length) {
      setFilter(activeTabs[0].filter);
    }
  }, [tabs]);

  useEffect(() => {
    if (Array.isArray(filter)) {
      const currentTab = tabs
        .filter((el) => Array.isArray(el.filter))
        .find((el) => String(el.filter) === String(filter));

      if (!currentTab) {
        return;
      }

      const indexSearched = tabs
        .map((tab) => tab.label)
        .indexOf(currentTab.label);

      if (indexSearched !== currentIndex) {
        setIndex(indexSearched);
      }
      return;
    }

    const currentTab = tabs.find((tab) => tab.filter === filter);

    if (!currentTab) {
      return;
    }

    const currentTabIndex = tabs
      .map((tab) => tab.label)
      .indexOf(currentTab.label);

    if (currentTabIndex !== currentIndex) {
      setIndex(currentTabIndex);
    }
  }, [filter]);

  function handleFilterData() {
    if (data && data.length && filter) {
      if (Array.isArray(filter)) {
        return data.filter((el) => filter.includes(el.status));
      }

      return data.filter((el) => el.status === filter);
    }
    return [];
  }

  const renderItems = () => {
    const items = handleFilterData();

    return items.map((item, i) => renderItem(item, i.toString(), setFilter));
  };

  return (
    <View style={styles.container}>
      <TabSelector
        tabs={tabs}
        setFilter={setFilter}
        currentIndex={currentIndex}
        setIndex={setIndex}
      />

      <RefreshControlScrollView onRefreshControl={onRefreshControl}>
        {handleFilterData()?.length ? (
          renderItems()
        ) : (
          <EmptyTab text={tabs[currentIndex].emptyText} />
        )}
      </RefreshControlScrollView>
    </View>
  );
};

export default memo(UgetTabs);
