import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  ratingContainer: {
    flexDirection: 'row',
  },

  starWrapper: {
    marginRight: 8,
  },
});
