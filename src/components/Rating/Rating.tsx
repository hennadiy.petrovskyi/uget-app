import React, { memo } from 'react';
import { View } from 'react-native';
import {
  RATING_STAR,
  RATING_STAR_HALF,
  RATING_STAR_OUTLINED,
} from '../../assets/svg';

import styles from './styles';

export default memo(
  ({
    value,
    starSize = 'medium',
  }: {
    value: number;
    isIntractable?: boolean;
    onChange?: Function;
    starSize: 'medium' | 'large';
  }) => {
    function renderRating(rate: number) {
      const stars = [];
      const isDecimal = rate % 1 !== 0;

      for (let index = 0; index < rate; index++) {
        if (isDecimal && index === Math.round(rate)) {
          stars.push(<RATING_STAR_HALF />);
          break;
        }

        stars.push(<RATING_STAR key={index} />);
      }

      const difference = Math.floor(5 - rate);

      if (difference) {
        for (let index = 0; index < difference; index++) {
          stars.push(<RATING_STAR_OUTLINED />);
        }
      }

      return stars.map((el, i) => {
        const isLastChild = i + 1 === stars.length;

        return (
          <View key={i} style={[!isLastChild && styles.starWrapper]}>
            {el}
          </View>
        );
      });
    }

    return <View style={styles.ratingContainer}>{renderRating(value)}</View>;
  },
);
