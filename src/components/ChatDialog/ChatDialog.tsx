import React, { memo, useEffect } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useState } from 'react';
import styles from './styles';
import { COLORS } from '../../config/constants';
import { useSelector } from 'react-redux';

export default memo(({ data }) => {
  const navigation = useNavigation();
  const myUUID = useSelector((state) => state.profile.uuid);

  const { message, createdAt, userId } = data;

  const self = myUUID === userId;

  const handleMessageTimestamp = () => {
    const timestamp = new Date(createdAt);
    const messageCreatedDay = new Date(
      timestamp.getFullYear(),
      timestamp.getMonth(),
      timestamp.getDate(),
    );

    const now = new Date();

    const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

    if (today.getTime() > messageCreatedDay.getTime()) {
      return new Date(createdAt).toLocaleTimeString([], {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
      });
    }

    return new Date(createdAt).toLocaleTimeString([], {
      hour: '2-digit',
      minute: '2-digit',
    });
  };

  return (
    <View
      style={[
        styles.container,
        self && {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-end',
        },
      ]}
    >
      <View
        style={[styles.innerContainer, !self && styles.contactPersonMessage]}
      >
        <Text>{message}</Text>
        <Text style={styles.timestamp}>{handleMessageTimestamp()}</Text>
      </View>
    </View>
  );
});
