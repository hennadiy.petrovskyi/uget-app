import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../config/constants';

export default StyleSheet.create({
  container: {
    width: '100%',
    marginBottom: 16,
  },

  innerContainer: {
    width: 292,
    padding: 8,
    backgroundColor: 'rgba(255, 186, 76, 0.1)',
  },

  contactPersonMessage: {
    backgroundColor: COLORS.GREEN_LIGHT,
  },

  text: {
    fontSize: 14,
    lineHeight: 22,
    fontFamily: FONT_FAMILY.NORMAL_400,
    color: COLORS.BLACK,
    marginBottom: 4,
  },

  timestamp: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    fontSize: 14,
    lineHeight: 22,
    textAlign: 'right',
    color: COLORS.GREY,
  },
});
