import { StyleProp } from 'react-native';

type TAlign = 'center' | 'left' | 'right';

export type UgetGreyTextProps = {
  text: string;
  align?: TAlign;
  marginBottom?: number;
  outerStyles?: StyleProp<any>;
  children?: string | Element;
};
