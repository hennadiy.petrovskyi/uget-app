import { COLORS, FONT_FAMILY } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  text: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    fontSize: 14,
    lineHeight: 22,
    color: COLORS.GREY,
    textAlign: 'center',
  },
});
