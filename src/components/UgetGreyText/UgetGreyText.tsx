import React, { FC, memo } from 'react';
import { Text } from 'react-native';

import { UgetGreyTextProps } from './types';
import styles from './styles';

const UgetGreyText: FC<UgetGreyTextProps> = ({
  children,
  text,
  align,
  marginBottom,
  outerStyles,
}) => {
  return (
    <Text
      style={[
        styles.text,
        marginBottom && { marginBottom },
        align && { textAlign: align },
        outerStyles && outerStyles,
      ]}
    >
      {children ? children : text}
    </Text>
  );
};

export default memo(UgetGreyText);
