import { StyleProp } from 'react-native';

type TInputType = 'password' | undefined | string;

export type TextInputComponentProps = {
  inputOptions: {
    onPress?: () => void;
    onChangeText?: () => void;
    onSubmitEditing?: () => void;
    placeholder?: string;
    value?: string;
    keyboardType?: string;
    mask?: string;
    editable?: boolean;
    multiline?: boolean;
    inputMask?: boolean;
    error?: any;
    customRef?: any;
    inputLabel: string;
    inputOuterStyles?: StyleProp<any>;
    textContentType?: TInputType;
  };

  title: string;
  description?: string;
  children: React.ReactNode;
  containerOuterStyles?: StyleProp<any>;
};
