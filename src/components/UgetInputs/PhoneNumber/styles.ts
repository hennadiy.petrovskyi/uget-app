import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';

export default StyleSheet.create({
  container: {},
  inputLabel: {
    fontFamily: 'FiraSans-Regular',
    marginBottom: 5,
  },

  phoneInputContainerStyle: {
    width: '100%',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: COLORS.GREY3,
    backgroundColor: 'white',
    height: 32,
  },

  textContainerStyle: {
    paddingVertical: 0,
    backgroundColor: 'white',
  },
  countryPickerButtonStyle: {
    borderRightWidth: 1,
    borderColor: COLORS.GREY3,
  },
  textInputStyle: {
    fontFamily: 'FiraSans-Regular',
    fontSize: 14,
    fontWeight: 'normal',
    paddingVertical: 0,
  },

  borderWithError: {
    borderColor: COLORS.RED,
  },

  errorMessage: {
    paddingTop: 4,
    color: COLORS.RED,
    fontFamily: FONT_FAMILY.NORMAL_400,
  },
});
