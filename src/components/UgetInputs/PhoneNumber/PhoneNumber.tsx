import React, { FC, memo, useEffect, useRef, useState } from 'react';
import { Text, View } from 'react-native';
import PhoneInput from 'react-native-phone-number-input';

import { PhoneTextInputComponentProps } from './types';
import styles from './styles';

const PhoneNumber: FC<PhoneTextInputComponentProps> = ({
  placeholder = '',
  onChangeText,
  value,
  defaultCode = 'AE',
  defaultValue = '',
  error,
}) => {
  const phoneInput = useRef<PhoneInput>(null);
  const [errorToShow, setErrorToShow] = useState('');

  useEffect(() => {
    if (!error) {
      return;
    }

    if (typeof error === 'string') {
      setErrorToShow(error);
      return;
    }

    const errors = Object.values(error);

    setErrorToShow(errors[0]);
  }, [error]);

  function handleChangeText(number: string) {
    if (!onChangeText) {
      return;
    }

    const code = phoneInput.current?.getCallingCode();
    const countryCode = phoneInput.current?.getCountryCode();
    const checkValid = phoneInput.current?.isValidNumber(code + number);

    onChangeText({
      code,
      countryCode,
      number,
      formatted: code + number,
      isValid: checkValid,
    });
  }

  return (
    <View style={[styles.container]}>
      <PhoneInput
        ref={phoneInput}
        containerStyle={[
          styles.phoneInputContainerStyle,
          error && styles.borderWithError,
        ]}
        textContainerStyle={styles.textContainerStyle}
        countryPickerButtonStyle={styles.countryPickerButtonStyle}
        codeTextStyle={styles.textInputStyle}
        textInputStyle={styles.textInputStyle}
        defaultCode={defaultCode}
        defaultValue={defaultValue}
        placeholder={placeholder}
        value={value}
        flagSize={20}
        layout="first"
        onChangeText={handleChangeText}
      />
      {error ? <Text style={styles.errorMessage}>{errorToShow}</Text> : null}
    </View>
  );
};

export default memo(PhoneNumber);
