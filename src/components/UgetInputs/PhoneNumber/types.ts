import { StyleProp } from 'react-native';

export type PhoneTextInputComponentProps = {
  placeholder?: string;
  onChangeText: (value: any) => void;
  error?: any;
  value?: string;
  defaultCode?: string;
  defaultValue?: string;
};
