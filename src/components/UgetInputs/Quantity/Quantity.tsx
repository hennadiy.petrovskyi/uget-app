import React, { FC, memo, useEffect, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { useTranslation } from 'react-i18next';

import { INCREASE, SUBTRACT } from '../../../assets/svg';
import styles from './styles';

const Quantity: FC<any> = ({ handleQuantity, count = 1, maxCount }) => {
  const { t } = useTranslation();

  function handleDecrease() {
    if (count - 1 >= 1) {
      handleQuantity(count - 1);
    }
  }

  function handleIncrease() {
    if (maxCount && count <= maxCount) {
      return;
    }
    handleQuantity(count + 1);
  }

  return (
    <View style={styles.quantityBtnWrap}>
      <TouchableOpacity onPress={handleDecrease}>
        <SUBTRACT />
      </TouchableOpacity>
      <Text>{count}</Text>
      <TouchableOpacity onPress={handleIncrease}>
        <INCREASE />
      </TouchableOpacity>
    </View>
  );
};

export default memo(Quantity);
