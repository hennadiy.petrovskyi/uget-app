import React, { FC, memo, useState } from 'react';
import { Text, View } from 'react-native';

import { TextInputComponentProps } from './types';
import styles from './styles';

const WithDescription: FC<any> = ({
  inputOptions,
  title,
  description,
  children,
  containerOuterStyles,
  marginBottom,
  flex,
}) => {
  const childrenWithAdjustedProps = React.Children.map(children, (child) =>
    React.cloneElement(child, { title, description }),
  );

  return (
    <View
      style={[
        styles.container,
        containerOuterStyles && containerOuterStyles,
        marginBottom && { marginBottom },
        flex && styles.flexContainer,
      ]}
    >
      {title && <Text style={styles.title}>{title}</Text>}
      {description && <Text style={styles.description}>{description}</Text>}
      <View style={styles.inputWrapper}>{childrenWithAdjustedProps}</View>
    </View>
  );
};

export default memo(WithDescription);
