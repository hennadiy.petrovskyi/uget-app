import { StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
  container: {},

  title: {
    fontFamily: 'FiraSans-Regular',
    fontWeight: 'normal',
    fontSize: 14,
    lineHeight: 20,
    color: COLORS.BLACK,
    marginBottom: 4,
  },

  description: {
    fontFamily: 'FiraSans-Regular',
    fontWeight: 'normal',
    fontSize: 12,
    lineHeight: 20,
    color: COLORS.GREY4,
    marginBottom: 4,
  },

  inputWrapper: {
    position: 'relative',
  },

  flexContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
