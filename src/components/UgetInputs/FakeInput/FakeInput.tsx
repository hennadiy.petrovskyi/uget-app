import React, { FC, memo } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { COLORS } from '../../../config/constants';

import styles from './styles';

const FakeInput: FC<any> = ({
  onPress = () => {},
  placeholder,
  value,
  error,
  disabled,
}) => {
  return (
    <View style={[styles.container]}>
      <TouchableOpacity
        style={[
          styles.input,
          { borderColor: error ? COLORS.RED : COLORS.GREY3 },
        ]}
        onPress={onPress}
        disabled={disabled}
      >
        <Text
          style={[
            styles.inputLabel,
            !value && placeholder && styles.placeholder,
          ]}
        >
          {value ? value : placeholder}
        </Text>
      </TouchableOpacity>

      {!!error && <Text style={styles.errorMessage}>{error}</Text>}
    </View>
  );
};

export default memo(FakeInput);
