import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';

export default StyleSheet.create({
  container: { overflow: 'visible' },

  input: {
    height: 32,
    borderWidth: 1,
    borderRadius: 2,
    paddingHorizontal: 8,
    borderColor: COLORS.GREY3,
    fontFamily: FONT_FAMILY.NORMAL_400,
    fontSize: 14,
    justifyContent: 'center',
  },

  inputLabel: {
    color: COLORS.BLACK,
    fontFamily: FONT_FAMILY.NORMAL_400,
    fontSize: 14,
    lineHeight: 20,
  },

  placeholder: {
    color: COLORS.GREY7,
    fontFamily: FONT_FAMILY.NORMAL_ITALIC_400,
  },

  errorMessage: {
    paddingTop: 4,
    color: COLORS.RED,
    fontFamily: FONT_FAMILY.NORMAL_400,
  },
});
