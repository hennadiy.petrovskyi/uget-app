import React, { FC, memo, useContext, useState } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { useTranslation } from 'react-i18next';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import uuid from 'react-native-uuid';

import { UgetButton } from '../..';
import { ADD_PHOTO, TRASH } from '../../../assets/svg';
import { OrderContext } from '../../../screens/OrderScreens/CreateOrder/context';
import { REMOVE_ORDER_PHOTO } from '../../../screens/OrderScreens/CreateOrder/context/actions';
import styles from './styles';
import { Actions } from './Actions';

const UploadImage: FC<any> = ({ photos = [], handlePhotos }) => {
  const { t } = useTranslation();
  const [isModalOpen, setOpenModal] = useState(false);
  const { order, setOrderFields } = useContext(OrderContext);

  function handleDeletePhoto(id: string) {
    handlePhotos(photos.filter((photo) => photo.id !== id));
    setOrderFields({
      type: REMOVE_ORDER_PHOTO,
      payload: id,
    });
  }

  function savePhoto(photo) {
    handlePhotos(photos.concat([photo]));
  }

  return (
    <>
      <View style={styles.photoWrap}>
        <View style={styles.imagesWrapp}>
          {photos.map((img: string, index: number) => {
            return (
              <TouchableOpacity
                key={index}
                style={styles.singleImageWrapp}
                onPress={() => handleDeletePhoto(img.id)}
              >
                <ADD_PHOTO />
                <Image style={styles.image} source={{ uri: img.data.uri }} />
                <View style={styles.deleteImg}>
                  <TRASH />
                </View>
              </TouchableOpacity>
            );
          })}

          <TouchableOpacity
            onPress={() => setOpenModal(true)}
            style={styles.addPhotoBtn}
          >
            <ADD_PHOTO />
          </TouchableOpacity>
        </View>
      </View>

      <Actions
        isModalOpen={isModalOpen}
        setOpenModal={setOpenModal}
        savePhoto={savePhoto}
      />
    </>
  );
};

export default memo(UploadImage);
