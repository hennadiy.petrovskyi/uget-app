import { StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
  photoWrap: {},
  photoTitleTxt: {
    fontFamily: 'FiraSans-Regular',
  },
  photoDescriptionTxt: {
    paddingTop: 2,
    fontSize: 12,
    color: COLORS.GREY4,
    fontFamily: 'FiraSans-Regular',
  },
  imagesWrapp: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  singleImageWrapp: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 24,
    marginTop: 8,
  },
  image: {
    height: 45,
    width: 45,
    marginRight: 20,
    position: 'absolute',
  },
  deleteImg: {
    height: 59,
    width: 59,
    backgroundColor: 'black',
    position: 'absolute',
    opacity: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  addPhotoBtn: {
    width: 65,
    marginTop: 8,
  },

  modalContainer: {
    justifyContent: 'flex-end',
  },

  modalContent: {
    borderRadius: 4,
  },

  wrapperActions: {
    backgroundColor: COLORS.WHITE,
    marginBottom: 16,
    paddingVertical: 24,
    paddingHorizontal: 16,
    borderRadius: 4,
  },

  createPhoto: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    paddingBottom: 7,
    borderBottomColor: COLORS.MAIN_COLOR,
    borderBottomWidth: 1,
    width: '100%',
  },

  choosePhoto: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 8,
    alignSelf: 'center',
    width: '100%',
  },

  actionText: {
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 16,
    color: COLORS.MAIN_COLOR,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.MAIN_COLOR,
  },

  errorMessage: {
    paddingTop: 4,
    color: COLORS.RED,
  },

  modalBtnWrap: {
    paddingHorizontal: 30,
    marginBottom: 24,
  },
});
