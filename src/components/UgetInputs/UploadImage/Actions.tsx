import React, { FC, memo, useContext, useState } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { useTranslation } from 'react-i18next';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import uuid from 'react-native-uuid';

import { UgetButton } from '../..';
import { OrderContext } from '../../../screens/OrderScreens/CreateOrder/context';
import { ADD_ORDER_PHOTO } from '../../../screens/OrderScreens/CreateOrder/context/actions';
import styles from './styles';

export const Actions: FC<any> = ({ isModalOpen, savePhoto, setOpenModal }) => {
  const { t } = useTranslation();
  const { order, setOrderFields } = useContext(OrderContext);

  function handleUpload() {
    launchImageLibrary(
      { mediaType: 'photo', includeBase64: true },
      (response) => {
        if (!response.assets?.length) {
          return;
        }

        const photo = response.assets[0];

        handleSavePhoto(photo);
      },
    );
  }

  function handleTakePhoto() {
    launchCamera({ mediaType: 'photo', includeBase64: true }, (response) => {
      if (!response.assets?.length) {
        return;
      }

      const photo = response.assets[0];

      handleSavePhoto(photo);
    });
  }

  function handleSavePhoto(data) {
    const photo = {
      id: uuid.v4(),
      base64: data.base64,
      type: data.type,
      data,
    };

    savePhoto(photo);

    setOrderFields({
      type: ADD_ORDER_PHOTO,
      payload: photo,
    });

    setOpenModal(false);
  }

  return (
    <Modal
      isVisible={isModalOpen}
      style={styles.modalContainer}
      onBackButtonPress={() => setOpenModal(false)}
    >
      <View style={styles.modalContent}>
        <View style={styles.wrapperActions}>
          <TouchableOpacity
            style={styles.createPhoto}
            onPress={handleTakePhoto}
          >
            <Text style={styles.actionText}>
              {t('order:itemStep.takePhoto')}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.choosePhoto} onPress={handleUpload}>
            <Text style={styles.actionText}>
              {t('order:itemStep.uploadPhoto')}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.modalBtnWrap}>
          <UgetButton
            type="outlined"
            label={t('common:buttons.cancel')}
            onPress={() => setOpenModal(false)}
          />
        </View>
      </View>
    </Modal>
  );
};
