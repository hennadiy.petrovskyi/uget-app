import { COLORS } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  dropDown: {
    borderRadius: 2,
    height: 32,
    borderColor: COLORS.GREY3,
  },
  arrowUp: {
    transform: [{ rotateZ: '180deg' }],
  },
  dropDownContainer: {
    borderColor: COLORS.GREY3,
    borderRadius: 2,
  },
});
