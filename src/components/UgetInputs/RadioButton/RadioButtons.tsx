import React, { FC, memo, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import { SELECTED, UN_SELECTED } from '../../../assets/svg';
import { RadioButtonsProps } from './types';

import styles from './style';

const RadioButtons: FC<RadioButtonsProps> = ({
  selected,
  items,
  handleSelect,
  marginBottom,
  horizontal = false,
}) => {
  return (
    <View
      style={[
        styles.container,
        marginBottom && { marginBottom },
        horizontal && styles.horizontalStyles,
      ]}
    >
      {items.map(
        ({ id, label }: { id: string; label: string }, index: number) => {
          const isSelected = id === selected;
          const isNotLastChild = index + 1 !== items.length;

          return (
            <View
              key={id}
              style={[
                isNotLastChild && styles.withMargin,
                horizontal && styles.horizontalMargin,
              ]}
            >
              <TouchableOpacity
                style={styles.radioBtn}
                onPress={() => {
                  handleSelect(id);
                }}
              >
                {isSelected ? <SELECTED /> : <UN_SELECTED />}
                <Text style={styles.label}>{label}</Text>
              </TouchableOpacity>
            </View>
          );
        },
      )}
    </View>
  );
};

export default memo(RadioButtons);
