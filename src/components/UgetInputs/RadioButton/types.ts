import { StyleProp } from 'react-native';

export type RadioButtonsProps = {
  selected: string;
  items: { id: string; label: string }[];
  handleSelect: (value: string) => void;
  marginBottom?: number;
  horizontal?: boolean;
};
