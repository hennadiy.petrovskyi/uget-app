import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {},

  radioBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  withMargin: {
    marginBottom: 13,
  },

  label: {
    paddingLeft: 12,
    fontFamily: 'FiraSans-Regular',
    fontWeight: 'normal',
    fontSize: 14,
    lineHeight: 20,
    color: COLORS.BLACK,
  },

  horizontalStyles: {
    flexDirection: 'row',
  },

  horizontalMargin: {
    marginRight: 30,
  },
});
