import React, { FC, memo, useState } from 'react';
import { SafeAreaView, Text, TouchableOpacity, View } from 'react-native';
import { useTranslation } from 'react-i18next';
import Modal from 'react-native-modal';

import { ARROW_BOTTOM, CLOSE, WEIGHT } from '../../../assets/svg';
import { TextInput, WithDescription } from '..';
import { COLORS } from '../../../config/constants';

import styles from './styles';
import { TextInputComponentProps } from '../types';

const DropDown: FC<any> = ({ placeholder, value, items, setValue, title }) => {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(false);

  const renderList = () => {
    return (
      <View style={styles.itemsList}>
        {items.map((item, i) => {
          return (
            <TouchableOpacity
              key={item}
              style={[styles.itemBorderBottom]}
              onPress={() => {
                setValue(item);
                setOpen(false);
              }}
            >
              <View style={styles.item}>
                <WEIGHT style={styles.icon} />
                <Text style={styles.text}>
                  {t(`common:inputs.size.valueKg`, {
                    weight: item,
                  })}
                </Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  return (
    <>
      <View style={styles.wrapperInput}>
        <TextInput
          value={t(`common:inputs.size.value`, {
            weight: value,
          })}
          placeholder={placeholder}
          editable={false}
          onPress={() => setOpen(true)}
        />
        <ARROW_BOTTOM style={styles.iconArrowBottomAbsolute} />
      </View>

      <Modal
        isVisible={isOpen}
        backdropOpacity={1}
        backdropColor={COLORS.CLEAR_WHITE}
        style={styles.modalContainer}
      >
        <SafeAreaView>
          <View style={styles.btnCloseContainer}>
            <TouchableOpacity onPress={() => setOpen(false)}>
              <CLOSE />
            </TouchableOpacity>
          </View>

          <WithDescription
            title={title}
            description={t('order:itemStep.inputs.size.upTo')}
            marginBottom={6}
          />

          {renderList()}
        </SafeAreaView>
      </Modal>
    </>
  );
};

export default memo(DropDown);
