import { COLORS, FONT_FAMILY } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: COLORS.CLEAR_WHITE,
  },

  btnCloseContainer: {
    paddingTop: 8,
    paddingRight: 8,
    alignItems: 'flex-end',
  },

  modalContainer: { justifyContent: 'flex-start' },

  inputContainer: {
    height: 32,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: COLORS.GREY3,
    paddingLeft: 8,
  },

  icon: {
    marginRight: 8,
  },

  itemsList: {
    backgroundColor: COLORS.CLEAR_WHITE,
    shadowColor: COLORS.BLACK,
    width: '100%',
  },

  item: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
  },

  itemBorderBottom: {
    borderBottomWidth: 1,
    borderColor: COLORS.BORDER_GREY,
  },

  wrapperInput: {
    position: 'relative',
  },

  iconArrowBottomAbsolute: {
    position: 'absolute',
    right: 16,
    top: '40%',
  },

  text: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    fontSize: 14,
    color: COLORS.BLACK,
  },
});
