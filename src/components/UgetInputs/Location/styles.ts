import { COLORS, FONT_FAMILY } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: COLORS.CLEAR_WHITE,
  },

  btnCloseContainer: {
    paddingTop: 8,
    paddingRight: 8,
    alignItems: 'flex-end',
  },

  modalContainer: { justifyContent: 'flex-start' },

  inputContainer: {
    height: 32,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: COLORS.GREY3,
    paddingLeft: 8,
  },

  icon: {
    marginRight: 8,
  },

  suggestionsList: {
    backgroundColor: COLORS.CLEAR_WHITE,
    shadowColor: COLORS.BLACK,
    width: '100%',
  },

  suggestion: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 18,
    paddingHorizontal: 15,
  },

  suggestionMarginBottom: {
    borderBottomWidth: 1,
    borderColor: COLORS.BORDER_GREY,
  },

  suggestionText: {
    fontFamily: FONT_FAMILY.SEMI_BOLD_600,
    fontSize: 14,
    lineHeight: 22,
    color: COLORS.BLACK,
  },

  label: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    fontSize: 14,
    lineHeight: 22,
    color: COLORS.BLACK,
  },
});
