export type LocationProps = {
  title?: string;
  description?: string;
  current: {
    id: string;
    label: string;
  };
  placeholder: string;
  handleValue: (value: string) => void;
  marginBottom?: number;
  type: 'country' | 'city' | 'subArea';
  params: { countryId?: string };
  error?: any;
  isDisabled: boolean;
};
