import React, { FC, memo, useEffect, useState } from 'react';
import Modal from 'react-native-modal';

import { SafeAreaView, Text, TouchableOpacity, View } from 'react-native';

import { FakeInput } from '../FakeInput';
import { TextInput, WithDescription } from '..';

import { CLOSE, LOCATION } from '../../../assets/svg';
import { COLORS } from '../../../config/constants';

import { googlePlacesFindCity, googlePlacesFindCountry } from '../../../api';

import { LocationProps } from './types';
import styles from './styles';

const Location: FC<LocationProps> = ({
  title,
  description,
  placeholder,
  type,
  current,
  isDisabled = false,
  handleValue,
  error,
  params,
}) => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [value, setValue] = useState(current ? current.label : '');
  const [isSuggestionsListVisible, setSuggestionsVisibility] = useState(false);
  const [suggestions, setSuggestions] = useState([]);
  const [search, setSearch] = useState(false);
  // const [error, setError] = useState('');

  useEffect(() => {
    if (suggestions.length && !isSuggestionsListVisible) {
      setSuggestionsVisibility(true);
    }

    if (!suggestions) {
      setSuggestionsVisibility(false);
    }
  }, [suggestions]);

  useEffect(() => {
    if (value.length >= 2 && search) {
      if (type === 'country') {
        googlePlacesFindCountry({ value, type }, setSuggestions);
      }

      if (type === 'city' && params?.countryId) {
        googlePlacesFindCity(
          { value, type, countryId: params.countryId },
          setSuggestions,
        );
      }
    }

    if (!value && isSuggestionsListVisible) {
      setSuggestionsVisibility(false);
    }
  }, [value, search]);

  function selectSuggestion(value: { id: string; label: string }) {
    setSearch(false);
    setSuggestions([]);
    handleValue(value);
    setValue(value.label);
    setModalOpen(false);
  }

  function handleChangeText(value: string) {
    if (type === 'city' && !params?.countryId) {
      // setError('Country is required to interact');

      return;
    }

    if (error) {
      // setError('');
    }

    setValue(value);
    setSearch(true);
  }

  const renderResults = () => {
    return (
      <View style={styles.suggestionsList}>
        {suggestions.map((location, i) => {
          const isLastSuggestion = suggestions.length === i + 1;

          return (
            <TouchableOpacity
              key={location.id}
              style={[!isLastSuggestion && styles.suggestionMarginBottom]}
              onPress={() => selectSuggestion(location)}
            >
              <View style={styles.suggestion}>
                <LOCATION style={styles.icon} />
                <Text style={styles.label}>{location.label}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  return (
    <>
      <FakeInput
        disabled={isDisabled}
        onPress={() => setModalOpen(true)}
        placeholder={placeholder}
        value={current ? current.label : ''}
        error={error}
      />

      <Modal
        isVisible={isModalOpen}
        backdropOpacity={1}
        backdropColor={COLORS.CLEAR_WHITE}
        style={styles.modalContainer}
      >
        <SafeAreaView>
          <View style={styles.btnCloseContainer}>
            <TouchableOpacity onPress={() => setModalOpen(false)}>
              <CLOSE />
            </TouchableOpacity>
          </View>

          <WithDescription title={title} description={description}>
            <TextInput
              type="text"
              placeholder={placeholder}
              onChangeText={handleChangeText}
              value={value}
              onFocus={() => setSearch(true)}
              onBlur={() => setSearch(false)}
            />
          </WithDescription>
          {suggestions.length && isSuggestionsListVisible
            ? renderResults()
            : null}
        </SafeAreaView>
      </Modal>
    </>
  );
};

export default memo(Location);
