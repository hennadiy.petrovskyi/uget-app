import React, { FC, memo, useState } from 'react';
import { Dimensions, Platform, View } from 'react-native';
import { Calendar as CalendarComponent } from 'react-native-calendars';
import Modal from 'react-native-modal';
import moment from 'moment';
import { useTranslation } from 'react-i18next';

import { CALENDAR } from '../../../assets/svg';
import { COLORS } from '../../../config/constants';

import { UgetLinkText } from '../..';
import { CalendarComponentProps } from './types';
import styles from './styles';
import { FakeInput } from '../FakeInput';
import { generateDateWithOffset } from '../../../utils/helpers';
import { CustomHeader } from './CustomHeader';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight =
  Platform.OS === 'ios'
    ? Dimensions.get('window').height
    : require('react-native-extra-dimensions-android').get(
        'REAL_WINDOW_HEIGHT',
      );

const Calendar: FC<CalendarComponentProps> = ({
  value,
  onPress,
  placeholder,
  error,
  minDate,
}) => {
  const { t } = useTranslation();
  const [openModal, setOpenModal] = useState(false);

  const defaultSelectedDate = moment(minDate || new Date()).format(
    'yyyy-MM-DD',
  );

  const [selectedDay, setSelectedDay] = useState(defaultSelectedDate);

  console.log(selectedDay);

  return (
    <>
      <FakeInput
        placeholder={placeholder}
        value={value && moment(value).format('MMMM DD, yyyy')}
        error={error}
        editable={false}
        onPress={() => setOpenModal(true)}
      />
      <CALENDAR style={styles.icon} />

      <Modal
        isVisible={openModal}
        deviceWidth={deviceWidth}
        deviceHeight={deviceHeight}
        onBackdropPress={() => {
          setOpenModal(false);
          setSelectedDay('');
        }}
      >
        <View style={{}}>
          <CalendarComponent
            current={selectedDay}
            minDate={minDate || moment(new Date()).format('yyyy-MM-DD')}
            maxDate={moment(generateDateWithOffset(undefined, 60)).format(
              'yyyy-MM-DD',
            )}
            enableSwipeMonths={true}
            hideDayNames={true}
            hideArrows={false}
            markingType={'custom'}
            customHeader={CustomHeader}
            markedDates={{
              [selectedDay]: {
                customStyles: {
                  container: styles.markerWrap,
                  text: styles.markerText,
                },
              },
            }}
            theme={{
              textDayFontWeight: 'normal',
              textDayFontFamily: 'FiraSans-Regular',
              todayTextColor: COLORS.MAIN_COLOR,
              textDayFontSize: 14,
            }}
            onDayPress={(day: any) => {
              setSelectedDay(day.dateString);
            }}
          />

          <View style={styles.buttonWrap}>
            <UgetLinkText
              text={t('common:buttons.cancel')}
              outerStyles={{ marginRight: 50 }}
              onPress={() => {
                setOpenModal(false);
                setSelectedDay('');
              }}
            />
            <UgetLinkText
              text={t('common:buttons.ok')}
              outerStyles={{}}
              onPress={() => {
                onPress(selectedDay);
                setOpenModal(false);
              }}
            />
          </View>
        </View>
      </Modal>
    </>
  );
};

export default memo(Calendar);
