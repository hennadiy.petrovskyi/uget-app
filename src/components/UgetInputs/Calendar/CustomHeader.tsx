import moment from 'moment';
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { ARROW_LEFT, ARROW_RIGHT } from '../../../assets/svg';

import styles from './styles';

export function CustomHeader(props) {
  console.log(props);

  return (
    <View style={styles.calendarTitleWrap}>
      <TouchableOpacity style={styles.arrow} onPress={() => props.addMonth(-1)}>
        <ARROW_LEFT />
      </TouchableOpacity>
      <Text style={styles.calendarTitle}>
        {moment(props.month[0]).format('MMMM yyyy')}
      </Text>
      <TouchableOpacity style={styles.arrow} onPress={() => props.addMonth(1)}>
        <ARROW_RIGHT />
      </TouchableOpacity>
    </View>
  );
}
