import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';

export default StyleSheet.create({
  icon: {
    position: 'absolute',
    right: 10,
    top: 5,
  },
  calendarTitleWrap: {
    width: '100%',
    justifyContent: 'flex-start',

    paddingTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  calendarTitle: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    fontSize: 16,
  },
  markerWrap: {
    backgroundColor: COLORS.MAIN_COLOR,
    alignItems: 'center',
    borderRadius: 20,
  },
  markerText: {
    color: 'white',
    fontFamily: FONT_FAMILY.NORMAL_400,
  },
  buttonWrap: {
    flexDirection: 'row',
    backgroundColor: COLORS.CLEAR_WHITE,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingVertical: 15,
  },

  arrow: {
    height: 20,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
