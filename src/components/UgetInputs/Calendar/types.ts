export type CalendarComponentProps = {
  value: string;
  minDate?: string;
  onPress: (value: string) => void;
  placeholder: string;
  error?: any;
};
