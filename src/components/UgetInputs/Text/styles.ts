import { StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
  container: { overflow: 'visible' },

  inputLabel: {
    fontFamily: 'FiraSans-Regular',
    marginBottom: 5,
  },

  inputWrapper: {
    position: 'relative',
  },

  input: {
    maxHeight: 32,
    borderWidth: 1,
    borderRadius: 2,

    borderColor: COLORS.GREY3,
    fontFamily: 'FiraSans-Regular',
    fontSize: 14,
    paddingVertical: 6,
    paddingHorizontal: 8,
    color: COLORS.BLACK,
  },

  multilineHeight: {
    height: 'auto',
    maxHeight: 100,
  },

  passwordVisibilityBtn: {
    position: 'absolute',
    alignSelf: 'flex-end',
    height: '100%',
    paddingRight: 8,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },

  passwordVisibilityTxt: {
    color: COLORS.MAIN_COLOR,
  },

  errorMessage: {
    paddingTop: 4,
    color: COLORS.RED,
  },
});
