import { StyleProp } from 'react-native';

type TInputType = 'password' | undefined | string;

export type TextInputComponentProps = {
  onPress?: () => void;
  onChangeText?: (value: string) => void;
  onSubmitEditing?: () => void;
  placeholder?: string;
  value?: string;
  keyboardType?: string;
  mask?: string;
  editable?: boolean;
  multiline?: boolean;
  inputMask?: boolean;
  error?: any;
  customRef?: any;
  inputOuterStyles?: StyleProp<any>;
  containerOuterStyles?: StyleProp<any>;
  textContentType?: TInputType;
  explanation?: string;
  onFocus?: Function;
  onBlur?: Function;
  maxLength?: number;
  isPress?: boolean;
};
