import React, { FC, memo, useState } from 'react';
import { TextInput, Text, View, TouchableOpacity } from 'react-native';
import TextInputMask from 'react-native-text-input-mask';

import { TextInputComponentProps } from './types';
import styles from './styles';
import { COLORS } from '../../../config/constants';

const TextInputComponent: FC<TextInputComponentProps> = ({
  onPress = () => {},
  onChangeText = () => {},
  onSubmitEditing = () => {},
  placeholder,
  value,
  keyboardType,
  inputOuterStyles,
  containerOuterStyles,
  maxLength,
  editable,
  multiline,
  inputMask,
  error,
  mask,
  customRef,
  textContentType,
  onFocus,
  onBlur,
  isPress,
}) => {
  const isPasswordInput = textContentType === 'password';

  const [borderColor, setBorderColor] = useState({ borderColor: COLORS.GREY3 });
  const [secureTextEntry, setSecureTextEntry] = useState(isPasswordInput);

  const renderInput = () => {
    let Input;
    let settings;

    if (!inputMask) {
      Input = TextInput;
      settings = {};
    } else {
      Input = TextInputMask;
      settings = { mask };
    }

    return (
      <Input
        {...settings}
        value={value}
        editable={editable}
        secureTextEntry={secureTextEntry}
        ref={customRef}
        onSubmitEditing={onSubmitEditing}
        style={[
          styles.input,
          error ? { borderColor: COLORS.RED } : borderColor,
          multiline && styles.multilineHeight,
          inputOuterStyles,
        ]}
        placeholder={placeholder}
        placeholderTextColor={COLORS.GREY7}
        onChangeText={onChangeText}
        keyboardType={keyboardType}
        maxLength={maxLength}
        onFocus={() => {
          setBorderColor({ borderColor: COLORS.MAIN_COLOR });

          if (onFocus) {
            onFocus();
          }
        }}
        onBlur={() => {
          setBorderColor({ borderColor: COLORS.GREY3 });

          if (onBlur) {
            onBlur();
          }
        }}
        onPressIn={onPress}
        multiline={multiline}
        returnKeyType="next"
      />
    );
  };

  return (
    <View style={[styles.container]}>
      <View style={styles.inputWrapper}>
        {renderInput()}

        {isPasswordInput && (
          <TouchableOpacity
            style={styles.passwordVisibilityBtn}
            onPress={() => setSecureTextEntry(!secureTextEntry)}
          >
            <Text style={styles.passwordVisibilityTxt}>
              {secureTextEntry ? 'Show' : 'Hide'}
            </Text>
          </TouchableOpacity>
        )}
      </View>

      {!!error && <Text style={styles.errorMessage}>{error}</Text>}
    </View>
  );
};

export default memo(TextInputComponent);
