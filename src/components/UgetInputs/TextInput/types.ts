import { StyleProp } from 'react-native';

type TInputType = 'text' | 'multiline' | 'code' | 'password' | 'email';

export type UgetTextInputProps = {
  type: TInputType;
  value: string;
  placeholder?: string;
  isEditable?: boolean;
  error?: string;
  onFocus?: Function;
  onBlur?: Function;
  inputOuterStyles?: any;
  inputWrapperOuterStyles?: any;
  onPress?: () => void;
  onChangeText: (value: string) => void;
  onSubmitEditing?: () => void;
};
