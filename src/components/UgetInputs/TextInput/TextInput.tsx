import React, { FC, memo, useState } from 'react';
import { TextInput, Text, View, TouchableOpacity } from 'react-native';

import { COLORS, FONT_FAMILY } from '../../../config/constants';
import { UgetTextInputProps } from './types';
import styles from './styles';

const UgetTextInput: FC<UgetTextInputProps> = ({
  type,
  value,
  placeholder,
  isEditable = true,
  error,
  onFocus,
  onBlur,
  onPress = () => {},
  onChangeText = () => {},
  onSubmitEditing = () => {},
  inputOuterStyles,
  inputWrapperOuterStyles,
}) => {
  const isPasswordInput = type === 'password';

  const [borderColor, setBorderColor] = useState({ borderColor: COLORS.GREY3 });
  const [secureTextEntry, setSecureTextEntry] = useState(isPasswordInput);

  const options = {
    text: {},
    multiline: {
      multiline: true,
    },
    code: {
      maxLength: 6,
      keyboardType: 'numeric',
      textAlign: 'center',
      textContentType: 'oneTimeCode',
    },
    password: {
      secureTextEntry,
    },
    email: {
      textContentType: 'emailAddress',
      keyboardType: 'email-address',
    },
  };

  function handleFocusInput() {
    setBorderColor({ borderColor: COLORS.MAIN_COLOR });

    if (onFocus) {
      onFocus();
    }
  }

  function handleBlurFunction() {
    setBorderColor({ borderColor: COLORS.GREY3 });

    if (onBlur) {
      onBlur();
    }
  }

  function handleChangeText(value: string) {
    if (type === 'code' && isNaN(+value)) {
      return;
    }

    onChangeText(value);
  }

  return (
    <View style={[styles.container]}>
      <View
        style={[
          styles.inputWrapper,
          type === 'multiline' && styles.multilineHeight,
          inputWrapperOuterStyles && inputWrapperOuterStyles,
        ]}
      >
        <TextInput
          {...options[type]}
          editable={isEditable}
          value={value}
          returnKeyType="next"
          placeholder={placeholder}
          placeholderTextColor={COLORS.GREY7}
          onSubmitEditing={onSubmitEditing}
          onChangeText={handleChangeText}
          onFocus={handleFocusInput}
          onBlur={handleBlurFunction}
          onPressOut={onPress}
          style={[
            styles.input,
            error ? styles.errorBorderColor : borderColor,
            type === 'code' && styles.letterSpacing,
            {
              fontFamily:
                value.length === 0
                  ? FONT_FAMILY.NORMAL_ITALIC_400
                  : FONT_FAMILY.NORMAL_400,
            },
            inputOuterStyles && inputOuterStyles,
          ]}
        />

        {isPasswordInput && (
          <TouchableOpacity
            style={styles.passwordVisibilityBtn}
            onPress={() => setSecureTextEntry(!secureTextEntry)}
          >
            <Text style={styles.passwordVisibilityTxt}>
              {secureTextEntry ? 'Show' : 'Hide'}
            </Text>
          </TouchableOpacity>
        )}
      </View>

      {!!error && <Text style={styles.errorMessage}>{error}</Text>}
    </View>
  );
};

export default memo(UgetTextInput);
