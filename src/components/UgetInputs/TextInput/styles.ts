import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';

export default StyleSheet.create({
  container: { overflow: 'visible' },

  inputWrapper: {
    position: 'relative',
    borderWidth: 1,
    borderRadius: 2,
    paddingHorizontal: 8,
    paddingVertical: 6,
    borderColor: COLORS.GREY3,
    maxHeight: 32,
  },

  input: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    fontSize: 14,
    color: COLORS.BLACK,
    padding: 0,
  },

  errorMessage: {
    paddingTop: 4,
    color: COLORS.RED,
  },

  letterSpacing: {
    letterSpacing: 5,
  },

  errorBorderColor: {
    borderColor: COLORS.RED,
  },

  passwordVisibilityTxt: {
    color: COLORS.MAIN_COLOR,
    lineHeight: 20,
    fontSize: 12,
    fontFamily: FONT_FAMILY.NORMAL_400,
  },

  passwordVisibilityBtn: {
    position: 'absolute',
    alignSelf: 'flex-end',
    height: '100%',
    display: 'flex',
    top: 4,
    paddingHorizontal: 8,
    color: COLORS.MAIN_COLOR,
  },

  multilineHeight: {
    height: 'auto',
    maxHeight: 100,
  },
});
