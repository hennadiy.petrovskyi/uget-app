import React, { FC, memo, useState } from 'react';
import { Text, View } from 'react-native';

import { TextInputComponentProps } from './types';
import styles from './styles';

const InputWrapper: FC<TextInputComponentProps> = ({
  // onPress = () => {},
  // onChangeText = () => {},
  // onSubmitEditing = () => {},
  // placeholder,
  // value,
  // keyboardType,
  // inputOuterStyles,

  // editable,
  // multiline,
  // inputMask,
  // mask,
  // customRef,
  // textContentType,
  // error,
  inputOptions,
  title,
  description,
  children,
  containerOuterStyles,
}) => {
  return (
    <View
      style={[styles.container, containerOuterStyles && containerOuterStyles]}
    >
      <Text style={styles.inputLabel}>{title}</Text>
      <Text>{description}</Text>
      <View style={styles.inputWrapper}>{children}</View>
    </View>
  );
};

export default memo(InputWrapper);
