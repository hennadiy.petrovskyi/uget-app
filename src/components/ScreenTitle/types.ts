import { StyleProp } from 'react-native';

type TAlign = 'center' | 'left' | 'right';

export type ScreenTitleProps = {
  text: string;
  align?: TAlign;
  marginBottom?: number;
  outerStyles?: StyleProp<any>;
};
