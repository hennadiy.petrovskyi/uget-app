import { COLORS, FONT_FAMILY } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  title: {
    fontFamily: FONT_FAMILY.BOLD_700,
    fontSize: 32,
    lineHeight: 38,
    color: COLORS.BLACK,
    textAlign: 'center',
  },
});
