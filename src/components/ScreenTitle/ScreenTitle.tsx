import React, { FC, memo } from 'react';
import { Text } from 'react-native';

import { ScreenTitleProps } from './types';
import styles from './styles';

const ScreenTitle: FC<ScreenTitleProps> = ({
  text,
  align,
  marginBottom,
  outerStyles,
}) => {
  return (
    <Text
      style={[
        styles.title,
        outerStyles && outerStyles,
        marginBottom && { marginBottom },
        align && { textAlign: align },
      ]}
    >
      {text}
    </Text>
  );
};

export default memo(ScreenTitle);
