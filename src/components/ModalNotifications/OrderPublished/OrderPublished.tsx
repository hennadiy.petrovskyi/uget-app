import React, { memo } from 'react';
import { View } from 'react-native';
import { useTranslation } from 'react-i18next';

import { ScreenTitle, UgetGreyText, UgetButton } from '../..';

import styles from './styles';

const OrderPublished = ({ removeNotification, options }) => {
  const { t } = useTranslation();

  console.log(options);

  return (
    <View style={styles.container}>
      <ScreenTitle
        text={t('notifications:itemPublished.title')}
        marginBottom={24}
      />

      <UgetGreyText
        text={t(
          `notifications:itemPublished.${options?.type || 'order'}.subtitle`,
        )}
        outerStyles={styles.subTittle}
        marginBottom={40}
      />

      <UgetButton
        type="primary"
        label={t('notifications:itemPublished.btn')}
        marginBottom={32}
        onPress={removeNotification}
      />
    </View>
  );
};

export default memo(OrderPublished);
