import React, { memo, useState } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { ScreenTitle, UgetGreyText, UgetButton } from '../..';

import styles from './styles';

const OrderSenderRequestAccepted = ({ cancelModal }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const orderId = 42284;

  return (
    <View style={styles.container}>
      <ScreenTitle
        text={t('notifications:orderSenderRequestAccepted.title')}
        marginBottom={24}
      />

      <UgetGreyText
        text={t('notifications:orderSenderRequestAccepted.subtitle', {
          orderId,
        })}
        outerStyles={styles.subTittle}
        marginBottom={62}
      />

      <UgetButton
        type="primary"
        label={t('notifications:orderSenderRequestAccepted.btnView')}
        marginBottom={32}
        onPress={() => {}}
      />
    </View>
  );
};

export default memo(OrderSenderRequestAccepted);
