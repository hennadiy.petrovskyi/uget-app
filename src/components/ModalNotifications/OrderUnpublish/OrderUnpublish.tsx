import React, { memo, useState } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { ScreenTitle, UgetGreyText, UgetButton } from '../..';
import { NAVIGATION } from '../../../config/constants';

import styles from './styles';
import { unPublishedOrder } from '../../../api/order';

const OrderUnpublish = ({ removeNotification, options }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  function unpublish() {
    const onSuccess = () => {
      removeNotification();

      if (options.setFilter) {
        options.setFilter(['draft', 'inactive']);
      }
    };

    unPublishedOrder(options.uuid, onSuccess);
  }

  return (
    <View style={styles.container}>
      <ScreenTitle
        text={t('notifications:orderUnpublished.title')}
        marginBottom={24}
      />

      <UgetGreyText
        text={t('notifications:orderUnpublished.subtitle')}
        outerStyles={styles.subTittle}
        marginBottom={23}
      />

      <View style={styles.buttonsWrapper}>
        <UgetButton
          type="warning"
          label={t('notifications:orderUnpublished.btnUnpublish')}
          outerContainerStyles={[styles.button, styles.withMarginRight]}
          onPress={unpublish}
        />

        <UgetButton
          type="outlined"
          label={t('notifications:orderUnpublished.btnCancel')}
          outerContainerStyles={styles.button}
          onPress={removeNotification}
        />
      </View>
    </View>
  );
};

export default memo(OrderUnpublish);
