import React, { memo, useRef, useState } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useDispatch } from 'react-redux';
import Recaptcha, { RecaptchaHandles } from 'react-native-recaptcha-that-works';

import {
  ScreenTitle,
  UgetGreyText,
  UgetButton,
  WithDescription,
  LinkWithCountdown,
} from '../..';

import { TextInput } from '../../UgetInputs';
import { verifyOrderIsDelivered, verifyOrderIsPickup } from '../../../api';
import styles from './styles';
import { setLoaderStatus } from '../../../utils/store/general/actions';

const VerifyTravelerActions = ({ removeNotification, options }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [isTimeoutActive, activateTimeout] = useState(false);

  const signUpSchema = Yup.object().shape({
    code: Yup.string()
      .min(6, t('common:inputs.sms.errors.length'))
      .max(6, t('common:inputs.sms.errors.length'))
      .required(t('common:inputs.sms.errors.required')),
  });

  async function onSubmit(formData: { code: string }, { setFieldError }) {
    const verificationDto = { code: formData.code, ...options.dto };

    const onSuccess = () => {
      dispatch(setLoaderStatus(false));

      removeNotification();

      if (options.setFilter && options.type === 'delivery') {
        setTimeout(() => {
          options.setFilter('delivered');
        }, 200);
      }
    };

    dispatch(setLoaderStatus(true));

    if (options.type === 'pickup') {
      dispatch(
        verifyOrderIsPickup(verificationDto, onSuccess, handleInputError),
      );
      return;
    }

    if (options.type === 'delivery') {
      dispatch(verifyOrderIsDelivered(verificationDto, onSuccess));
      return;
    }

    function handleInputError(type: any) {
      dispatch(setLoaderStatus(false));

      setFieldError('code', t(`common:inputs.sms.errors.${type}`));
    }
  }

  return (
    <View style={styles.container}>
      <Formik
        initialValues={{
          code: '',
        }}
        validationSchema={signUpSchema}
        validateOnChange={false}
        onSubmit={onSubmit}
      >
        {(props) => {
          const { values, handleSubmit, setFieldValue, setFieldError, errors } =
            props;

          function handleInputValue(value: string) {
            setFieldValue('code', value);
            setFieldError('code', '');
          }

          return (
            <View>
              <ScreenTitle
                text={t(
                  `notifications:verifyTravelerActions.${options.type}.title`,
                )}
                marginBottom={16}
              />

              <UgetGreyText
                text={t(
                  `notifications:verifyTravelerActions.${options.type}.description`,
                )}
                marginBottom={30}
              />

              <UgetGreyText
                text={t(`notifications:verifyTravelerActions.helper`)}
                marginBottom={23}
              />
              <WithDescription
                title={t('common:inputs.sms.label')}
                marginBottom={28}
              >
                <TextInput
                  type="code"
                  value={values.code}
                  error={errors.code}
                  placeholder={t('common:inputs.sms.placeholder')}
                  onChangeText={handleInputValue}
                />
              </WithDescription>

              <UgetButton
                type="primary"
                label={t(`notifications:verifyTravelerActions.confirm`)}
                disabled={values.code.length !== 6}
                onPress={handleSubmit}
                marginBottom={24}
              />

              <UgetButton
                type="warning"
                label={t(`notifications:verifyTravelerActions.cancel`)}
                onPress={() => removeNotification()}
                marginBottom={54}
              />
            </View>
          );
        }}
      </Formik>

      <LinkWithCountdown
        label={t('auth:login.resend')}
        isTimeoutActive={isTimeoutActive}
        activateTimeout={activateTimeout}
        type="sms-verification"
        params={{ phoneNumber: '' }}
        marginBottom={33}
      />
    </View>
  );
};

export default memo(VerifyTravelerActions);
