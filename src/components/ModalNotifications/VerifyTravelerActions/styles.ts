import { StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 35,
  },

  subTittle: {
    color: COLORS.BLACK,
    maxWidth: 268,
    alignSelf: 'center',
  },

  button: {
    width: 140,
  },
  withMarginRight: {
    marginRight: 15,
  },

  buttonsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    marginBottom: 40,
  },
});
