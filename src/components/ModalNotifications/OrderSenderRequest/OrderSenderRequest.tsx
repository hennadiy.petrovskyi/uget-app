import React, { memo, useState } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { ScreenTitle, UgetGreyText, UgetButton } from '../..';

import styles from './styles';

const OrderSenderRequest = ({ cancelModal }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <ScreenTitle
        text={t('notifications:orderSenderRequest.title')}
        marginBottom={24}
      />

      <UgetGreyText
        text={t('notifications:orderSenderRequest.subtitle')}
        outerStyles={styles.subTittle}
        marginBottom={54}
      />

      <UgetButton
        type="primary"
        label={t('notifications:orderSenderRequest.btnTrack')}
        marginBottom={16}
        onPress={() => {}}
      />

      <UgetButton
        type="outlined"
        label={t('notifications:orderSenderRequest.btnCreate')}
        marginBottom={50}
        onPress={() => {}}
      />
    </View>
  );
};

export default memo(OrderSenderRequest);
