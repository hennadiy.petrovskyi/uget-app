import React, { memo, useState } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { ScreenTitle, UgetGreyText, UgetButton } from '../..';
import { NAVIGATION } from '../../../config/constants';

import styles from './styles';

const OrderCancel = ({ removeNotification, options }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  const { isOrderFilled, saveAsDraft, saveForm, resetForm } = options;

  function cancelOrder() {
    resetForm();
    removeNotification();
  }

  return (
    <View style={styles.container}>
      <ScreenTitle
        text={t('notifications:cancelOrder.title')}
        marginBottom={24}
      />

      <UgetGreyText
        text={t(
          `notifications:cancelOrder.${
            isOrderFilled ? 'subtitle' : 'subtitleNoDraft'
          }`,
        )}
        outerStyles={styles.subTittle}
        marginBottom={23}
      />

      <UgetButton
        type="primary"
        label={t('notifications:cancelOrder.btnContinue')}
        marginBottom={16}
        onPress={removeNotification}
      />

      {isOrderFilled ? (
        <UgetButton
          type="outlined"
          label={t('notifications:cancelOrder.btnSaveAsDraft')}
          marginBottom={16}
          onPress={() => {
            saveAsDraft();

            removeNotification();
          }}
        />
      ) : (
        <UgetButton
          type="outlined"
          label={t('notifications:cancelOrder.btnSaveForm')}
          marginBottom={16}
          onPress={() => {
            saveForm();

            removeNotification();
          }}
        />
      )}

      <UgetButton
        type="warning"
        label={t('notifications:cancelOrder.btnCancel')}
        marginBottom={32}
        onPress={cancelOrder}
      />
    </View>
  );
};

export default memo(OrderCancel);
