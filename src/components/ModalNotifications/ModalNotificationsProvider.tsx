import React, { memo, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { NotificationContainer } from './NotificationContainer';
import { OrderCancel } from './OrderCancel';
import { OrderPublished } from './OrderPublished';
import { OrderSenderRequest } from './OrderSenderRequest';
import { OrderSenderRequestAccepted } from './OrderSenderRequestAccepted';
import { AuthorizeToPublish } from './AuthorizeToPublish';
import { EditCreditCard } from './EditCreditCard';
import { OrderUnpublish } from './OrderUnpublish';
import { VerifyTravelerActions } from './VerifyTravelerActions';

import { removeLocalNotification } from '../../utils/store/general/actions';
import { RateUser } from './RateUser';
import { ResetPasswordEmailSent } from './ResetPasswordEmailSent';
import { PasswordUpdated } from './PasswordUpdated';
import { EmailVerified } from './EmailVerified';

export enum NotificationID {
  ORDER_CANCEL = 'order.cancel',
  ORDER_PUBLISHED = 'item.publish',
  ORDER_UNPUBLISH = 'order.unpublish',
  ORDER_REQUEST_TO_SENDER = 'order.sender.request',
  ORDER_ACCEPTED_TRAVELER_REQUEST = 'order.sender.request.accepted',
  ORDER_SHOW_DETAILS = 'orders.open.details',
  ORDER_VERIFY_TRAVELER_ACTIONS = 'order.verify.traveler.actions',
  ORDER_RATE_USER = 'order.rate.by-role',
  AUTHORIZE_TO_PUBLISH = 'authorize.publish',
  CREDIT_CARD_ADD = 'credit-card.add',
  RESET_PASSWORD_REQUEST_SUCCESS = 'password.request.success',
  PASSWORD_UPDATE_SUCCESS = 'password.update.success',
  EMAIL_VERIFIED = 'email.verified.success',
}

const ModalNotificationsProvider = () => {
  const [isActive, setActive] = useState(false);
  const dispatch = useDispatch();

  const notifications = useSelector(
    (state) => state.general.localNotifications,
  );

  useEffect(() => {
    if (notifications.length) {
      setActive(true);
    }

    if (isActive && !notifications.length) {
      setActive(false);
    }
  }, [notifications]);

  function closeModal() {
    dispatch(removeLocalNotification());
    setActive(false);
  }

  const content = [
    { id: NotificationID.ORDER_CANCEL, component: OrderCancel },
    { id: NotificationID.ORDER_PUBLISHED, component: OrderPublished },
    {
      id: NotificationID.ORDER_REQUEST_TO_SENDER,
      component: OrderSenderRequest,
    },
    {
      id: NotificationID.ORDER_ACCEPTED_TRAVELER_REQUEST,
      component: OrderSenderRequestAccepted,
    },
    {
      id: NotificationID.AUTHORIZE_TO_PUBLISH,
      component: AuthorizeToPublish,
    },
    {
      id: NotificationID.CREDIT_CARD_ADD,
      component: EditCreditCard,
    },
    {
      id: NotificationID.ORDER_UNPUBLISH,
      component: OrderUnpublish,
    },
    {
      id: NotificationID.ORDER_VERIFY_TRAVELER_ACTIONS,
      component: VerifyTravelerActions,
    },
    { id: NotificationID.ORDER_RATE_USER, component: RateUser },
    {
      id: NotificationID.RESET_PASSWORD_REQUEST_SUCCESS,
      component: ResetPasswordEmailSent,
    },
    {
      id: NotificationID.PASSWORD_UPDATE_SUCCESS,
      component: PasswordUpdated,
    },
    {
      id: NotificationID.EMAIL_VERIFIED,
      component: EmailVerified,
    },
  ];

  const render = () => {
    return notifications.map(({ id, options }, i) => {
      const result = content.find((content) => content.id === id);

      if (!result) {
        setActive(false);
        return;
      }

      const removeNotification = () => {
        dispatch(removeLocalNotification({ id: result.id }));
      };

      const { component: Content } = result;
      const params = { removeNotification, options };

      return (
        i === 0 && (
          <NotificationContainer
            key={result.id}
            isVisible={isActive}
            removeNotification={removeNotification}
          >
            <Content {...params} />
          </NotificationContainer>
        )
      );
    });
  };

  return render();
};

export default memo(ModalNotificationsProvider);
