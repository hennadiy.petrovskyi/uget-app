import { StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 35,
  },
  ratingContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginBottom: 56,
  },
  starWrapper: {
    marginRight: 8,
  },
});
