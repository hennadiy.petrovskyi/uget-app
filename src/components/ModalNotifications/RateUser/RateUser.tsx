import React, { memo, useEffect, useRef, useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { useDispatch } from 'react-redux';
import Recaptcha, { RecaptchaHandles } from 'react-native-recaptcha-that-works';

import { ScreenTitle, UgetGreyText, UgetButton } from '../..';
import { NAVIGATION } from '../../../config/constants';

import { getUserProfile, rateUserRole } from '../../../api';
import { BIG_STAR, BIG_STAR_YELLOW } from '../../../assets/svg';

import styles from './styles';

const RateUser = ({ removeNotification, options }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [selected, setSelected] = useState(null);
  const [stars, setStars] = useState([]);

  console.log(options);

  useEffect(() => {
    const items = [];

    for (let index = 0; index <= 4; index++) {
      if (index + 1 <= selected && selected !== null) {
        items.push(<BIG_STAR_YELLOW key={index} />);
      } else {
        items.push(<BIG_STAR key={index} />);
      }
    }

    setStars(items);
  }, [selected]);

  function renderRating() {
    return stars.map((el, i) => {
      const isLastChild = i + 1 === stars.length;
      return (
        <TouchableOpacity
          key={i}
          style={[!isLastChild && styles.starWrapper]}
          onPress={() => setSelected(i + 1)}
        >
          {el}
        </TouchableOpacity>
      );
    });
  }

  function handleSubmit() {
    rateUserRole({ id: options.id, stars: selected }).then((response) => {
      dispatch(getUserProfile());
      removeNotification();
    });
  }

  return (
    <View style={styles.container}>
      <ScreenTitle
        text={t(`notifications:rateUser.title`, {
          role: options.role.toLowerCase(),
        })}
        marginBottom={16}
      />

      <UgetGreyText
        text={t(`notifications:rateUser.subTitle`)}
        marginBottom={30}
      />

      <View style={styles.ratingContainer}>{renderRating()}</View>

      <UgetButton
        type="primary"
        label={t(`notifications:verifyTravelerActions.confirm`)}
        onPress={handleSubmit}
        marginBottom={16}
      />

      <UgetButton
        type="outlined"
        label={t(`notifications:verifyTravelerActions.cancel`)}
        onPress={removeNotification}
        marginBottom={54}
      />
    </View>
  );
};

export default memo(RateUser);
