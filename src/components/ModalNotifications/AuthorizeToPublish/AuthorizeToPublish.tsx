import React, { memo, useState } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { ScreenTitle, UgetGreyText, UgetButton } from '../..';
import { DOORS } from '../../../assets/svg';

import { NAVIGATION } from '../../../config/constants';
import { setNavigationSession } from '../../../utils/store/general/actions';

import styles from './styles';
import { useDispatch } from 'react-redux';
import { NotificationID } from '../ModalNotificationsProvider';

const AuthorizeToPublish = ({ removeNotification, options }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  console.log(options);

  function startNavigationSession() {
    removeNotification();

    let navigationSession;

    if (options.type === 'order') {
      navigationSession = {
        from: {
          stack: NAVIGATION.TABS_ROUTE.ORDER_SCREENS,
          screen: NAVIGATION.ORDER_SCREENS.CREATE_ORDER,
        },
        to: {
          stack: NAVIGATION.TABS_ROUTE.ORDER_SCREENS,
          screen: NAVIGATION.ORDER_SCREENS.MY_ORDERS,
        },
        options: {
          withNotification: true,
          notificationId: NotificationID.ORDER_PUBLISHED,
          notificationOption: { type: options.type },
        },
      };
    }

    if (options.type === 'trip') {
      navigationSession = {
        from: {
          stack: NAVIGATION.TABS_ROUTE.MY_TRIPS,
          screen: NAVIGATION.TRIPS.CREATE_TRIP,
        },
        to: {
          stack: NAVIGATION.TABS_ROUTE.MY_TRIPS,
          screen: NAVIGATION.TRIPS.MY_TRIPS,
        },
        options: {
          withNotification: true,
          notificationId: NotificationID.ORDER_PUBLISHED,
          notificationOption: { type: options.type },
        },
      };
    }

    dispatch(setNavigationSession(navigationSession));
  }

  function navigateToSignUp() {
    startNavigationSession();

    navigation.navigate(NAVIGATION.UGET_ROUTE.AUTHENTICATION as never, {
      screen: NAVIGATION.AUTHENTICATION.SIGN_UP,
    });
  }

  function navigateToLogin() {
    startNavigationSession();

    navigation.navigate(NAVIGATION.UGET_ROUTE.AUTHENTICATION as never, {
      screen: NAVIGATION.AUTHENTICATION.LOG_IN_BY_PHONE,
    });
  }

  return (
    <View style={styles.container}>
      <ScreenTitle
        text={t('notifications:authorizeToPublish.title', {
          type: options.type,
        })}
        marginBottom={24}
      />

      <UgetGreyText
        text={t('notifications:authorizeToPublish.subtitle', {
          type: options.type,
        })}
        outerStyles={styles.subTittle}
        marginBottom={22}
      />

      <View style={styles.icon}>
        <DOORS />
      </View>

      <UgetButton
        type="primary"
        label={t('notifications:authorizeToPublish.btnSignUp')}
        marginBottom={16}
        onPress={navigateToSignUp}
      />

      <UgetButton
        type="outlined"
        label={t('notifications:authorizeToPublish.btnLogin')}
        marginBottom={40}
        onPress={navigateToLogin}
      />
    </View>
  );
};

export default memo(AuthorizeToPublish);
