import React, { memo, useState } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { ScreenTitle, UgetGreyText, UgetButton } from '../..';

import styles from './styles';
import { NAVIGATION } from '../../../config/constants';

export default memo(({ removeNotification }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <ScreenTitle
        text={t('notifications:passwordUpdated.title')}
        marginBottom={24}
      />

      <UgetGreyText
        text={t('notifications:passwordUpdated.subTitle')}
        outerStyles={styles.subTittle}
        marginBottom={40}
      />

      <UgetButton
        type="primary"
        label={t('notifications:passwordUpdated.button')}
        marginBottom={32}
        onPress={() => {
          navigation.navigate(NAVIGATION.UGET_ROUTE.AUTHENTICATION as never, {
            screen: NAVIGATION.AUTHENTICATION.LOG_IN_BY_EMAIL,
          });

          removeNotification();
        }}
      />
    </View>
  );
});
