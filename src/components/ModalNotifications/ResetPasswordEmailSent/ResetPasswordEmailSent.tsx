import React, { memo, useState } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { ScreenTitle, UgetGreyText, UgetButton } from '../..';

import styles from './styles';

export default memo(({ removeNotification }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <ScreenTitle
        text={t('notifications:resetPasswordRequestSent.title')}
        marginBottom={24}
      />

      <UgetGreyText
        text={t('notifications:resetPasswordRequestSent.subTitle')}
        outerStyles={styles.subTittle}
        marginBottom={40}
      />

      <UgetButton
        type="primary"
        label={t('notifications:resetPasswordRequestSent.button')}
        marginBottom={32}
        onPress={removeNotification}
      />
    </View>
  );
});
