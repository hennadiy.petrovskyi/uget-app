import React, { memo, useState } from 'react';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { ScreenTitle, UgetGreyText, UgetButton } from '../..';
import { CREDIT_CARD } from '../../../assets/svg';

import { setNavigationSession } from '../../../utils/store/general/actions';
import { NotificationID } from '../ModalNotificationsProvider';
import { NAVIGATION } from '../../../config/constants';

import styles from './styles';

const EditCreditCard = ({ removeNotification }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  function startNavigationSession() {
    removeNotification();

    // dispatch(
    //   setNavigationSession({
    //     from: {
    //       stack: NAVIGATION.TABS_ROUTE.ORDER_SCREENS,
    //       screen: NAVIGATION.ORDER_SCREENS.CREATE_ORDER,
    //     },
    //     to: {
    //       stack: NAVIGATION.TABS_ROUTE.ORDER_SCREENS,
    //       screen: NAVIGATION.ORDER_SCREENS.MY_ORDERS,
    //     },
    //     options: {
    //       withNotification: true,
    //       notificationId: NotificationID.ORDER_PUBLISHED,
    //     },
    //   }),
    // );
  }

  function navigateTo() {
    startNavigationSession();
  }

  return (
    <View style={styles.container}>
      <ScreenTitle
        text={t('notifications:editCreditCard.title')}
        marginBottom={24}
      />

      <UgetGreyText
        text={t('notifications:editCreditCard.subtitle')}
        outerStyles={styles.subTittle}
        marginBottom={22}
      />

      <View style={styles.icon}>
        <CREDIT_CARD />
      </View>

      <UgetButton
        type="primary"
        label={t('notifications:editCreditCard.btnProceed')}
        marginBottom={16}
        onPress={navigateTo}
      />

      <UgetButton
        type="outlined"
        label={t('notifications:editCreditCard.btnCancel')}
        marginBottom={40}
        onPress={navigateTo}
      />
    </View>
  );
};

export default memo(EditCreditCard);
