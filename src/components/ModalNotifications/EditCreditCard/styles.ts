import { StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 35,
  },

  subTittle: {
    color: COLORS.BLACK,
    maxWidth: 268,
    alignSelf: 'center',
  },

  icon: {
    alignSelf: 'center',
    marginBottom: 40,
  },
});
