import { StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
  modalContainer: {
    backgroundColor: 'white',
    borderRadius: 4,
  },
  btnCloseContainer: {
    paddingTop: 8,
    paddingRight: 8,
    alignItems: 'flex-end',
  },
  cross: {
    fontSize: 42,
    transform: [{ rotateZ: '45deg' }],
    color: COLORS.GREY5,
  },
  childrenContainer: {},
});
