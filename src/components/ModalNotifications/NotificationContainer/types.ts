export type NotificationContainerProps = {
  removeNotification: () => void;
  isVisible: boolean;
  children: React.ReactNode;
};
