import React, { FC, memo } from 'react';
import { View, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';

import { CLOSE } from '../../../assets/svg';
import { NotificationContainerProps } from './types';
import styles from './styles';

const NotificationContainer: FC<NotificationContainerProps> = ({
  isVisible,
  children,
  removeNotification,
}) => {
  return (
    <Modal isVisible={isVisible} onBackdropPress={removeNotification}>
      <View style={[styles.modalContainer]}>
        <View style={styles.btnCloseContainer}>
          <TouchableOpacity onPress={removeNotification}>
            <CLOSE />
          </TouchableOpacity>
        </View>
        <View style={styles.childrenContainer}>{children}</View>
      </View>
    </Modal>
  );
};

export default memo(NotificationContainer);
