import React, { memo, ReactNode, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { ordersHydrateState } from '../../utils/store/orders/actions';
import { encryptedStorage } from '../../utils/encryptedStorage';
import { api } from '../../api/api';
import { AuthEndpoints } from '../../api/auth/endpoint';
import {
  addLocalNotification,
  setAuthenticationStatus,
} from '../../utils/store/general/actions';
import { hydrateUserProfile } from '../../utils/store/profile/actions';

import { NotificationID } from '../ModalNotifications/ModalNotificationsProvider';
import { tripsHydrateState } from '../../utils/store/trips/actions';
import { getAllConversations, loadDrafts } from '../../api';
import { getUserProfile, logoutStoreCleaner } from '../../api/auth';
import { fcmService } from '../../utils/pushNotifications/FCMservice';
import { conversationsHydrateState } from '../../utils/store/chat/actions';

export default memo(({ children }: { children: ReactNode }) => {
  const dispatch = useDispatch();
  const profile = useSelector((state) => state.profile);
  const isAuth = useSelector((state) => state.general.isAuth);

  useEffect(() => {
    loadDrafts();
    loadAuthStatus().then(() => dispatch(getUserProfile()));
  }, []);

  useEffect(() => {
    fcmService.getToken();
  }, [isAuth]);

  useEffect(() => {
    if (profile && profile?.rating) {
      const { rating } = profile;

      if (rating.toRate.length) {
        dispatch(
          addLocalNotification({
            id: NotificationID.ORDER_RATE_USER,
            options: profile.rating.toRate[0],
          }),
        );
      }
    }
  }, [profile]);

  async function loadAuthStatus() {
    const functionName = loadAuthStatus.name;
    const session = await encryptedStorage.retrieveUserSession();
    if (!session) {
      return;
    }

    try {
      const response = await api.get(AuthEndpoints.GET_PROFILE);

      if (response.status === 200) {
        const { orders, trips, conversations, ...userProfile } = response.data;

        dispatch(hydrateUserProfile(userProfile));
        dispatch(ordersHydrateState(orders));
        dispatch(tripsHydrateState(trips));
        dispatch(setAuthenticationStatus(true));
        dispatch(conversationsHydrateState(conversations));
      }
    } catch (error) {
      console.log(error);
    }
  }

  return <>{children}</>;
});
