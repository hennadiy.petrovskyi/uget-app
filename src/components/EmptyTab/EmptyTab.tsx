import React, { memo } from 'react';
import { View } from 'react-native';

import { UgetGreyText } from '..';

import { CLOUDS } from '../../assets/svg';

import styles from './styles';

export default memo(({ text, marginBottom }) => {
  return (
    <View style={[styles.container, marginBottom && { marginBottom }]}>
      <CLOUDS style={styles.icon} />
      <UgetGreyText text={text} align="center" />
    </View>
  );
});
