import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    paddingTop: 50,
  },

  icon: {
    marginBottom: 54,
    alignSelf: 'center',
  },
});
