import { COLORS, FONT_FAMILY } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  orderProgress: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 6,
  },
  circleWrapp: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  elementContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 8,
  },

  circle: {
    width: 12,
    height: 12,
    borderWidth: 2,
    borderRadius: 30,
    borderColor: COLORS.ORANGE,
  },
  circleFill: {
    backgroundColor: COLORS.ORANGE,
  },
  arrow: {
    height: 2,
    width: 40,
    backgroundColor: COLORS.ORANGE_LIGHT,
    marginHorizontal: 13,
  },

  labelWrapper: {
    flexDirection: 'row',
  },

  itemTxt: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    color: COLORS.BLACK,
    fontSize: 12,
  },

  pickUpTxt: {
    paddingLeft: 43,
  },
  deliveryTxt: {
    paddingLeft: 37,
  },
  summaryTxt: {
    paddingLeft: 30,
  },
});
