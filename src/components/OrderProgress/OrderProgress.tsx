import React, { FC, memo } from 'react';
import { View, Text } from 'react-native';
import { useTranslation } from 'react-i18next';

import { OrderProgressProps } from './types';
import styles from './styles';

const OrderProgress: FC<OrderProgressProps> = ({ list, active }) => {
  const { t } = useTranslation();
  const [, ...steps] = list;

  return (
    <View>
      <View style={styles.orderProgress}>
        {steps.map((el, i) => {
          const isSelected = i === active - 1;
          const isLastChild = i + 1 === steps.length;

          return (
            <View key={i.toString()} style={styles.elementContainer}>
              <View style={[styles.circle, isSelected && styles.circleFill]} />
              {!isLastChild && <View style={styles.arrow} />}
            </View>
          );
        })}
      </View>

      <View style={styles.labelWrapper}>
        <Text style={[styles.itemTxt]}>{t('order:steps.item')}</Text>
        <Text style={[styles.itemTxt, styles.pickUpTxt]}>
          {t('order:steps.pickUp')}
        </Text>
        <Text style={[styles.itemTxt, styles.deliveryTxt]}>
          {t('order:steps.delivery')}
        </Text>
        <Text style={[styles.itemTxt, styles.summaryTxt]}>
          {t('order:steps.summary')}
        </Text>
      </View>
    </View>
  );
};

export default memo(OrderProgress);
