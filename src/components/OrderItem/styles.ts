import { COLORS } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  statusContainer: {
    borderBottomWidth: 1,
    borderColor: COLORS.GREY6,
  },

  withPadding: { paddingHorizontal: 16 },
});
