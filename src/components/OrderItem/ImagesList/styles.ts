import { Dimensions, StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  container: { height: 262, marginBottom: 30, position: 'relative' },
  child: {
    width: width - 38,
    justifyContent: 'center',
    backgroundColor: COLORS.GREY6,
    borderRadius: 6,
  },
  text: { fontSize: width * 0.5, textAlign: 'center' },
  paginationItem: { width: 8, height: 8 },
  topPagination: {
    borderRadius: 6,
    backgroundColor: COLORS.CLEAR_WHITE,
    position: 'absolute',
    top: 14,
    right: 14,
    zIndex: 2,
    width: 54,
  },

  topPaginationText: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    fontSize: 12,
    lineHeight: 20,
    color: COLORS.BLACK,
    textAlign: 'center',
  },

  emptyContainer: {
    height: 262,
    justifyContent: 'center',
    backgroundColor: COLORS.GREY6,
    alignItems: 'center',
  },
});
