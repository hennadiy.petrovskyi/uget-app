import React, { memo, useState } from 'react';
import { Image, Text, View } from 'react-native';
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import { EMPTY_IMAGE } from '../../../assets/svg';
import { COLORS } from '../../../config/constants';

import styles from './styles';

export default memo(({ images }) => {
  const [slideIndex, setSlideIndex] = useState(1);

  return (
    <View style={styles.container}>
      <View style={styles.topPagination}>
        <Text
          style={styles.topPaginationText}
        >{`${slideIndex} of ${images.length}`}</Text>
      </View>

      {images.length ? (
        <>
          <SwiperFlatList
            showPagination={images.length > 1}
            data={images}
            centerContent
            paginationStyle={{
              position: 'absolute',
              bottom: -34,
            }}
            paginationStyleItem={{ width: 8, height: 8 }}
            paginationActiveColor={COLORS.GREY4}
            paginationDefaultColor={COLORS.GRAY8}
            onChangeIndex={({ index }) => {
              setSlideIndex(index + 1);
            }}
            renderItem={(props) => {
              const { index, item } = props;

              return (
                <Image
                  style={styles.child}
                  source={{
                    uri: item.url,
                  }}
                />
              );
            }}
          />
        </>
      ) : (
        <View style={styles.emptyContainer}>
          <EMPTY_IMAGE />
        </View>
      )}
    </View>
  );
});
