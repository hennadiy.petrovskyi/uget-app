import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: 10,
  },

  imgWrapper: {
    height: 66,
    width: 66,
    backgroundColor: COLORS.GREY6,
  },

  img: {
    height: 66,
    width: 66,
    resizeMode: 'cover',
  },

  textContainer: {
    paddingLeft: 10,
    flexDirection: 'column',
    justifyContent: 'center',
  },

  itemName: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    color: COLORS.BLACK,
    fontSize: 16,
    lineHeight: 22,
    marginBottom: 5,
  },
  itemDetailText: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    color: COLORS.BLACK,
    fontSize: 14,
  },
});
