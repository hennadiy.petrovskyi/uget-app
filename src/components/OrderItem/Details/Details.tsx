import React from 'react';
import { useTranslation } from 'react-i18next';
import { Image, Text, View } from 'react-native';
import { OrderViewerType } from '../../../types';
import styles from './styles';

export const Details = ({ order, viewer }) => {
  const { t } = useTranslation();

  return (
    <View
      style={[
        styles.container,
        viewer === OrderViewerType.ORDER_OWNER && { paddingTop: 16 },
      ]}
    >
      <View style={styles.imgWrapper}>
        {order.photos && order.photos.length ? (
          <Image
            style={styles.img}
            source={{
              // uri: `data:${item.photos[0].type};base64,${item.photos[0].base64}`,
              uri: order.photos[0].url
                ? order.photos[0].url
                : order.photos[0].data.uri,
            }}
          />
        ) : null}
      </View>

      <View style={styles.textContainer}>
        <Text style={styles.itemName}>{order.name}</Text>
        <Text style={styles.itemDetailText}>
          {t(`common:inputs.size.value`, {
            weight: order.size,
          })}
        </Text>
      </View>
    </View>
  );
};
