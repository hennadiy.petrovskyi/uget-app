import React, { memo, useState } from 'react';
import Modal from 'react-native-modal';

import { OrderExpanded, OrderMinimized, OrderPreview } from './Views';
import { COLORS } from '../../config/constants';
import {
  OrderActionsViewType,
  OrderViewerType,
  OrderViewType,
} from '../../types';
import { ModalLoader } from '../ModalLoader';

const OrderItem = ({
  order,
  viewer,
  tripUUID,
  setFilter,
  view = OrderViewType.PREVIEW,
}: {
  order: any;
  viewer: OrderViewerType;
  view: OrderViewType;
  tripUUID?: string;
  setFilter?: Function;
}) => {
  const [isModalOpen, setOpenModal] = useState(false);

  function handleViewOrderDetails() {
    setOpenModal(true);
  }

  return (
    <>
      <ModalLoader />
      {view === OrderViewType.MINIMIZED && (
        <OrderMinimized order={order} onPress={handleViewOrderDetails} />
      )}

      {view === OrderViewType.PREVIEW && (
        <OrderPreview
          order={order}
          tripUUID={tripUUID}
          setOpenModal={setOpenModal}
          setFilter={setFilter}
          viewer={viewer}
          handleViewOrderDetails={handleViewOrderDetails}
        />
      )}

      <Modal
        isVisible={isModalOpen}
        backdropColor={COLORS.CLEAR_WHITE}
        backdropOpacity={1}
      >
        <OrderExpanded
          order={order}
          tripUUID={tripUUID}
          view={isModalOpen ? OrderActionsViewType.EXPANDED : view}
          viewer={viewer}
          setOpenModal={setOpenModal}
          setFilter={setFilter}
        />
        <ModalLoader />
      </Modal>
    </>
  );
};

export default memo(OrderItem);
