import React, { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { View, Text } from 'react-native';
import { OrderOwnerStatus } from '../../../types';

import styles from './styles';

export const Status = ({ status }) => {
  const { t } = useTranslation();

  function handleStatusLabel() {
    if (status === OrderOwnerStatus.REQUEST_INCOMING) {
      return 'incoming';
    }

    if (status === OrderOwnerStatus.REQUEST_OUTGOING) {
      return 'outgoing';
    }

    if (status === OrderOwnerStatus.PICKED_UP) {
      return 'progress';
    }

    console.log(status);

    return status;
  }

  const handleDotColor = () => {
    if (['accepted', 'delivered', 'picked-up'].includes(status)) {
      return styles.success;
    }
    return styles.warning;
  };

  return (
    <View style={styles.container}>
      <View style={[styles.dot, handleDotColor()]} />
      <Text style={styles.status}>
        {t(`order:status.${handleStatusLabel()}`)}
      </Text>
    </View>
  );
};
