import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';

export default StyleSheet.create({
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: COLORS.GREY6,
  },

  status: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    fontSize: 14,
    lineHeight: 22,
    color: COLORS.GREY,
    textTransform: 'capitalize',
    marginBottom: 3,
  },

  dot: {
    width: 8,
    height: 8,
    borderRadius: 50,
    marginRight: 4,
  },

  warning: {
    backgroundColor: COLORS.YELLOW,
  },

  success: {
    backgroundColor: COLORS.MAIN_COLOR,
  },
});
