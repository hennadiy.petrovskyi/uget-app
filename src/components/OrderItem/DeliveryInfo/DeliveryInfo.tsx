import React from 'react';
import { useTranslation } from 'react-i18next';
import { Text, View } from 'react-native';
import styles from './styles';

export const DeliveryInfo = ({ fields }) => {
  const { t } = useTranslation();

  return (
    <View style={styles.deliveryInfo}>
      {fields.map((info) => {
        return (
          <View key={info.id} style={styles.deliveryField}>
            <Text style={styles.firstField}>
              {t(`order:orderPreview.${info.id}`)}
            </Text>
            <Text style={styles.secondField}>{info.value}</Text>
          </View>
        );
      })}
    </View>
  );
};
