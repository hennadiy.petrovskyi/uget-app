import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';

export default StyleSheet.create({
  deliveryInfo: {
    marginBottom: 16,
  },

  deliveryField: {
    flexDirection: 'row',
  },

  firstField: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    color: COLORS.GREY,
    width: 34,
    fontSize: 14,
    lineHeight: 22,
    marginRight: 12,
  },
  secondField: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    color: COLORS.BLACK,
    fontSize: 14,
    lineHeight: 22,
  },
});
