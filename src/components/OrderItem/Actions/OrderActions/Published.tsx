import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { UgetButton } from '../../..';
import { deletePublishedOrder, getOrderRequests } from '../../../../api';
import { NAVIGATION } from '../../../../config/constants';
import { addLocalNotification } from '../../../../utils/store/general/actions';
import { NotificationID } from '../../../ModalNotifications/ModalNotificationsProvider';
import { OrderActionsViewType } from '../../../../types';

export const PublishedActions = ({ order, view, closeModal, setFilter }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);

  function findTravelers() {
    navigation.navigate(NAVIGATION.ORDER_SCREENS.TRAVELERS_LIST, {
      locations: order.locations,
      deliveryDate: order.deliveryDate,
      uuid: order.uuid,
    });
  }

  async function viewOrderRequests() {
    const requests = []
      .concat(order.incomingRequests)
      .concat(order.outgoingRequests);

    const trips = await getOrderRequests(order.uuid, requests);

    if (!trips) {
      return;
    }

    navigation.navigate(NAVIGATION.ORDER_SCREENS.REQUESTS, {
      trips: trips.map((el) => el.uuid),
      orderUUID: order.uuid,
    });
  }

  function unpublish() {
    closeModal();

    setTimeout(() => {
      dispatch(
        addLocalNotification({
          id: NotificationID.ORDER_UNPUBLISH,
          options: { uuid: order.uuid, setFilter },
        }),
      );
    }, 1000);
  }

  const requestsExist =
    (order.incomingRequests.length || order.outgoingRequests.length) > 0;

  console.log(view);

  if (view === OrderActionsViewType.MINIMIZED) {
    return (
      <>
        <UgetButton
          type="primary"
          label={t('order:orderPreview.actions.viewTravelers')}
          onPress={findTravelers}
          marginBottom={10}
        />

        {requestsExist && (
          <UgetButton
            type="outlined"
            label={t('order:orderPreview.actions.viewRequests')}
            onPress={viewOrderRequests}
            marginBottom={10}
          />
        )}
      </>
    );
  }

  if (view === OrderActionsViewType.EXPANDED) {
    return (
      <>
        <UgetButton
          type="primary"
          label={t('order:orderPreview.actions.viewTravelers')}
          onPress={findTravelers}
          marginBottom={10}
        />
        <UgetButton
          type="warning"
          label={t('order:orderPreview.actions.unpublish')}
          onPress={unpublish}
          marginBottom={10}
        />
      </>
    );
  }

  return <></>;
};
