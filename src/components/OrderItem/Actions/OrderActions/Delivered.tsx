import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { NAVIGATION } from '../../../../config/constants';
import { addLocalNotification } from '../../../../utils/store/general/actions';
import { NotificationID } from '../../../ModalNotifications/ModalNotificationsProvider';
import { UgetButton } from '../../..';

export const OrderOwnerDeliveredActions = ({
  order,
  view,
  tripUUID,
  cancel,
}) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);

  const dto = {
    tripUUID,
    orderUUID: order.uuid,
  };

  return <></>;
};
