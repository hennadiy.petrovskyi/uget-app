import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import {
  deletePublishedOrder,
  publishInactiveOrder,
} from '../../../../api/order';
import { NAVIGATION } from '../../../../config/constants';
import { addLocalNotification } from '../../../../utils/store/general/actions';
import { NotificationID } from '../../../ModalNotifications/ModalNotificationsProvider';
import { UgetButton } from '../../..';
import { OrderActionsViewType } from '../../../../types';

export const InactiveActions = ({ order, view }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);

  async function handlePublishOrder() {
    await publishInactiveOrder(order.uuid, onSuccess);

    async function onSuccess() {
      dispatch(addLocalNotification({ id: NotificationID.ORDER_PUBLISHED }));

      navigation.reset({
        index: 0,
        routes: [{ name: NAVIGATION.ORDER_SCREENS.MY_ORDERS }],
      });
    }
  }

  if (view === OrderActionsViewType.MINIMIZED) {
    return (
      <UgetButton
        type="primary"
        label={t('order:orderPreview.actions.publish')}
        onPress={handlePublishOrder}
      />
    );
  }

  if (view === OrderActionsViewType.EXPANDED) {
    return (
      <>
        <UgetButton
          type="primary"
          label={t('order:orderPreview.actions.publish')}
          onPress={handlePublishOrder}
          marginBottom={10}
        />

        <UgetButton
          type="warning"
          label={t('order:orderPreview.actions.delete')}
          onPress={() => deletePublishedOrder(order.uuid)}
        />
      </>
    );
  }
};
