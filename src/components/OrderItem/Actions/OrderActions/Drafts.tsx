import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { UgetButton } from '../../..';
import { deletePublishedOrder, publishOrder } from '../../../../api';
import { NAVIGATION } from '../../../../config/constants';
import { asyncStorage, Keys } from '../../../../utils/asyncStorage';
import {
  addLocalNotification,
  setLoaderStatus,
} from '../../../../utils/store/general/actions';
import { removeDraft } from '../../../../utils/store/orders/actions';
import { NotificationID } from '../../../ModalNotifications/ModalNotificationsProvider';
import { ActionsViewType } from '../../types.d';
import { OrderActionsViewType } from '../../../../types';

export const DraftsActions = ({ order, view }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);

  function handlePublish() {
    if (!isAuth) {
      dispatch(
        addLocalNotification({
          id: NotificationID.AUTHORIZE_TO_PUBLISH,
          options: { type: 'order' },
        }),
      );
      return;
    }

    dispatch(publishOrder(order, onSuccess));
    async function onSuccess() {
      await handleDelete();
      navigation.reset({
        index: 0,
        routes: [{ name: NAVIGATION.ORDER_SCREENS.MY_ORDERS }],
      });
    }
  }

  async function handleDelete() {
    dispatch(setLoaderStatus(true));

    if (order.status === 'draft') {
      const drafts = await asyncStorage.get(Keys.DRAFTS);

      await asyncStorage.save(
        Keys.DRAFTS,
        drafts.filter((draft) => draft.uuid !== order.uuid),
      );

      dispatch(setLoaderStatus(false));

      dispatch(removeDraft({ uuid: order.uuid }));
      return;
    }

    if (order.status === 'inactive') {
      deletePublishedOrder(order.uuid);
    }
  }

  if (view === OrderActionsViewType.MINIMIZED) {
    return (
      <UgetButton
        type="primary"
        label={t('order:orderPreview.actions.publish')}
        onPress={handlePublish}
        marginBottom={10}
      />
    );
  }

  if (view === OrderActionsViewType.EXPANDED) {
    return (
      <>
        <UgetButton
          type="primary"
          label={t('order:orderPreview.actions.publish')}
          onPress={handlePublish}
          marginBottom={10}
        />

        <UgetButton
          type="warning"
          label={t('order:orderPreview.actions.delete')}
          onPress={handleDelete}
        />
      </>
    );
  }

  return <></>;
};
