import React from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { UgetButton } from '../../..';
import { OrderActionsViewType } from '../../../../types';

import { getOrder, travelerRequestOrderDelivery } from '../../../../api';

export const AvailableActions = ({
  order,
  view,
  tripUUID,
  update,
  setFilter,
  handleChat,
}) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);

  async function handleRequestDeliveryToOrderOwner() {
    const onSuccess = () => {
      update();

      setFilter(['request.incoming', 'request.outgoing']);
    };

    dispatch(
      travelerRequestOrderDelivery(
        { tripUUID, orderUUID: order.uuid },
        onSuccess,
      ),
    );
  }

  if (view === OrderActionsViewType.MINIMIZED) {
    return (
      <View>
        <UgetButton
          type="primary"
          label={'Request to deliver'}
          onPress={handleRequestDeliveryToOrderOwner}
          marginBottom={10}
        />

        <UgetButton type="outlined" label={'Chat'} onPress={handleChat} />
      </View>
    );
  }

  if (view === OrderActionsViewType.EXPANDED) {
    return (
      <>
        <UgetButton
          type="primary"
          label={'Request to deliver'}
          onPress={handleRequestDeliveryToOrderOwner}
          marginBottom={10}
        />

        <UgetButton type="outlined" label={'Chat'} onPress={handleChat} />
      </>
    );
  }
};
