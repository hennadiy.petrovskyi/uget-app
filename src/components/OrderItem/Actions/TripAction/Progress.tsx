import React, { useRef } from 'react';
import { useNavigation } from '@react-navigation/core';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import Recaptcha, { RecaptchaHandles } from 'react-native-recaptcha-that-works';

import {
  deletePublishedOrder,
  publishInactiveOrder,
  requestOrderIsDelivered,
} from '../../../../api/order';
import { NAVIGATION } from '../../../../config/constants';
import { addLocalNotification } from '../../../../utils/store/general/actions';
import { NotificationID } from '../../../ModalNotifications/ModalNotificationsProvider';
import { UgetButton } from '../../..';
import { OrderActionsViewType } from '../../../../types';

export const InProgressActions = ({
  order,
  view,
  tripUUID,
  setFilter,
  handleChat,
}) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);
  const recaptcha = useSelector((state) => state.general.recaptcha.token);

  const dto = {
    tripUUID,
    orderUUID: order.uuid,
  };

  const sendReCaptcha = () => {
    recaptcha.current?.open();
  };

  const onReCaptchaVerify = () => {
    const onSuccess = () =>
      dispatch(
        addLocalNotification({
          id: NotificationID.ORDER_VERIFY_TRAVELER_ACTIONS,
          options: { type: 'delivery', dto, setFilter },
        }),
      );

    requestOrderIsDelivered(dto, onSuccess);
  };

  if (view === OrderActionsViewType.MINIMIZED) {
    return (
      <>
        <UgetButton
          type="primary"
          label={'Mark as delivered'}
          onPress={onReCaptchaVerify}
          marginBottom={16}
        />
        <UgetButton
          type="outlined"
          label={'Chat'}
          onPress={handleChat}
          marginBottom={16}
        />
      </>
    );
  }

  if (view === OrderActionsViewType.EXPANDED) {
    return <></>;
  }
};
