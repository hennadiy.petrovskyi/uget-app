import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { UgetButton } from '../../..';
import { OrderActionsViewType } from '../../../../types';

export const DeliveredActions = ({
  order,
  view,
  tripUUID,
  update,
  cancel,
  handleChat,
}) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);

  const dto = {
    tripUUID,
    orderUUID: order.uuid,
  };

  if (view === OrderActionsViewType.MINIMIZED) {
    return (
      <UgetButton
        type="outlined"
        label={'Chat'}
        onPress={handleChat}
        marginBottom={16}
      />
    );
  }
};
