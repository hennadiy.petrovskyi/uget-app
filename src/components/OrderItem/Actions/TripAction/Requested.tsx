import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { UgetButton } from '../../..';

import { travelerAcceptOrderDelivery } from '../../../../api';
import { OrderActionsViewType, OrderOwnerStatus } from '../../../../types';
import { View } from 'react-native';

export const RequestedActions = ({
  order,
  view,
  tripUUID,
  update,
  cancel,
  setFilter,
  handleChat,
}) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);

  function handleAccept() {
    const onSuccess = () => {
      update();
      setFilter(['accepted', 'progress']);
    };

    dispatch(
      travelerAcceptOrderDelivery(
        { tripUUID, orderUUID: order.uuid },
        onSuccess,
      ),
    );
  }

  if (view === OrderActionsViewType.MINIMIZED) {
    if (order.status === OrderOwnerStatus.REQUEST_INCOMING) {
      return (
        <>
          <UgetButton
            type="primary"
            label={'Accept request'}
            onPress={handleAccept}
            marginBottom={16}
          />
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <UgetButton
              type="outlined"
              label={'Chat'}
              onPress={handleChat}
              width={137}
            />
            <UgetButton
              type="warning"
              label={'Decline'}
              onPress={cancel}
              width={137}
            />
          </View>
        </>
      );
    }

    if (order.status === OrderOwnerStatus.REQUEST_OUTGOING) {
      return (
        <>
          <UgetButton
            type="primary"
            label={'Chat'}
            onPress={handleChat}
            marginBottom={16}
          />
          <UgetButton type="warning" label={'Decline'} onPress={cancel} />
        </>
      );
    }
  }
};
