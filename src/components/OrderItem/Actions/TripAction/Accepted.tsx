import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { View, TouchableOpacity, Text } from 'react-native';
import { useDispatch } from 'react-redux';

import { requestOrderIsPickup } from '../../../../api/order';

import { addLocalNotification } from '../../../../utils/store/general/actions';
import { NotificationID } from '../../../ModalNotifications/ModalNotificationsProvider';
import { UgetButton } from '../../..';
import { OrderActionsViewType } from '../../../../types';
import style from '../../../UgetInputs/RadioButton/style';

export const AcceptedActions = ({
  order,
  view,
  tripUUID,
  cancel,
  handleChat,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const dto = {
    tripUUID,
    orderUUID: order.uuid,
  };

  const onReCaptchaVerify = (token: string) => {
    const onSuccess = () =>
      dispatch(
        addLocalNotification({
          id: NotificationID.ORDER_VERIFY_TRAVELER_ACTIONS,
          options: { type: 'pickup', dto: { ...dto } },
        }),
      );

    requestOrderIsPickup({ ...dto }, onSuccess);
  };

  if (view === OrderActionsViewType.MINIMIZED) {
    return (
      <>
        <UgetButton
          type="primary"
          label={'Mark as picked-up'}
          onPress={onReCaptchaVerify}
          marginBottom={16}
        />

        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          <UgetButton
            type="outlined"
            label={'Chat'}
            onPress={handleChat}
            width={137}
          />
          <UgetButton
            type="warning"
            label={'Cancel'}
            onPress={cancel}
            width={137}
          />
        </View>
      </>
    );
  }

  if (view === OrderActionsViewType.EXPANDED) {
    return <></>;
  }
};
