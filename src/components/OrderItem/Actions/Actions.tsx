import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { View } from 'react-native';

import { DraftsActions } from './OrderActions/Drafts';
import { InactiveActions } from './OrderActions/Inactive';
import { PublishedActions } from './OrderActions/Published';
import { OrderOwnerAcceptedActions } from './OrderActions/Accepted';
import { InProgress } from './OrderActions/inProgres';
import {
  OrderActionsViewType,
  OrderOwnerStatus,
  OrderViewerType,
} from '../../../types';
import styles from './styles';
import { AvailableActions } from './TripAction/Available';
import { RequestedActions } from './TripAction/Requested';
import { AcceptedActions } from './TripAction/Accepted';
import { InProgressActions } from './TripAction/Progress';
import { useDispatch } from 'react-redux';

import {
  getOrderByTrip,
  getOrderOwnerDetails,
  orderOwnerCancelDeliveryRequest,
  startConversation,
  travelerCancelOrderDelivery,
} from '../../../api';
import { getOneTripFilteredByOrder } from '../../../api/trip';
import { DeliveredActions } from './TripAction/Delivered';
import { OrderOwnerDeliveredActions } from './OrderActions/Delivered';
import { NAVIGATION } from '../../../config/constants';
import { setNavigationSession } from '../../../utils/store/general/actions';

export const Actions = ({
  order,
  view,
  closeModal,
  viewer,
  tripUUID,
  setFilter,
}: {
  order: any;
  view: OrderActionsViewType;
  closeModal: Function;
  viewer: OrderViewerType;
  tripUUID?: string;
  setFilter?: Function;
}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const statuses: Record<string, any> = {
    owner: {
      draft: DraftsActions,
      published: PublishedActions,
      inactive: InactiveActions,
      accepted: OrderOwnerAcceptedActions,
      delivered: OrderOwnerDeliveredActions,
      inProgress: InProgress,
    },
    traveler: {
      available: AvailableActions,
      requested: RequestedActions,
      accepted: AcceptedActions,
      progress: InProgressActions,
      delivered: DeliveredActions,
    },
  };

  function parseStatus(status: any) {
    if (
      status === OrderOwnerStatus.REQUEST_INCOMING ||
      status === OrderOwnerStatus.REQUEST_OUTGOING
    ) {
      return 'requested';
    }

    if (status === 'picked-up') {
      return 'inProgress';
    }

    return status;
  }

  const parsedStatus = parseStatus(order.status);

  const apiRequestDto = {
    orderUUID: order.uuid,
    tripUUID: tripUUID || order.tripId,
  };

  async function handleChat() {
    const onSuccess = (conversationId: any) => {
      dispatch(
        setNavigationSession({
          from: {
            stack:
              viewer === OrderViewerType.ORDER_OWNER
                ? NAVIGATION.TABS_ROUTE.ORDER_SCREENS
                : NAVIGATION.TABS_ROUTE.MY_TRIPS,
            screen:
              viewer === OrderViewerType.ORDER_OWNER
                ? NAVIGATION.ORDER_SCREENS.MY_ORDERS
                : NAVIGATION.TRIPS.MY_TRIPS,
          },
          to: {
            stack: NAVIGATION.TABS_ROUTE.MESSAGES,
            screen: NAVIGATION.MESSAGE_SCREENS.CHAT,
          },
        }),
      );

      navigation.navigate(NAVIGATION.TABS_ROUTE.MESSAGES, {
        screen: NAVIGATION.MESSAGE_SCREENS.CHAT,
        params: { conversationId, initMessage: order.details.description },
      });
    };

    const contactPersonUUID = await getOrderOwnerDetails({ uuid: order.uuid });

    startConversation(
      { contactPersonUUID, orderUUID: order.uuid },
      dispatch,
      onSuccess,
    );
  }

  const renderActions = () => {
    const ActionsUi = statuses[viewer] ? statuses[viewer][parsedStatus] : null;

    if (!ActionsUi) {
      console.log('ActionsUi', ActionsUi);
    }

    if (viewer === OrderViewerType.ORDER_OWNER) {
      function orderOwnerUpdateFilteredTrip() {
        dispatch(getOneTripFilteredByOrder(apiRequestDto));

        if (setFilter) {
          setFilter('published');
        }
      }

      function ownerCancelRequest() {
        dispatch(
          orderOwnerCancelDeliveryRequest(
            apiRequestDto,
            orderOwnerUpdateFilteredTrip,
          ),
        );
      }

      return (
        <ActionsUi
          order={order}
          view={view}
          closeModal={closeModal}
          tripUUID={tripUUID}
          update={orderOwnerUpdateFilteredTrip}
          cancel={ownerCancelRequest}
          setFilter={setFilter}
          handleChat={handleChat}
        />
      );
    }

    if (viewer === OrderViewerType.TRAVELER) {
      function travelerUpdateFilteredOrder() {
        dispatch(getOrderByTrip(apiRequestDto));

        if (setFilter) {
          setFilter('available');
        }
      }

      function travelerCancelRequest() {
        dispatch(
          travelerCancelOrderDelivery(
            apiRequestDto,
            travelerUpdateFilteredOrder,
          ),
        );
      }

      return (
        <ActionsUi
          order={order}
          view={view}
          tripUUID={tripUUID}
          update={travelerUpdateFilteredOrder}
          cancel={travelerCancelRequest}
          setFilter={setFilter}
          handleChat={handleChat}
        />
      );
    }

    if (viewer === OrderViewerType.DEFAULT) {
      return <></>;
    }
  };

  return <View style={styles.container}>{renderActions()}</View>;
};
