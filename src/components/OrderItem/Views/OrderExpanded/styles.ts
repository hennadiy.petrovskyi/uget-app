import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../../config/constants';

export default StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
    backgroundColor: 'white',
  },

  closeButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 24,
  },

  buttonLabel: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    color: COLORS.BLACK,
    fontSize: 16,
  },
});
