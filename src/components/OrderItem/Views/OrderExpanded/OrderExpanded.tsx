import React, { memo, useEffect, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import { SafeAreaView } from 'react-native-safe-area-context';
import { OrderDetails, Rating, UserPreview } from '../../..';

import { CLOSE } from '../../../../assets/svg';
import {
  OrderActionsViewType,
  OrderViewerType,
  OrderViewType,
} from '../../../../types';
import { Actions } from '../../Actions';
import { ImagesList } from '../../ImagesList';

import styles from './styles';

export default memo(
  ({ order, view, viewer, setOpenModal, setFilter, tripUUID }) => {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={() => setOpenModal(false)}
          >
            <CLOSE />
            <Text style={styles.buttonLabel}>Close</Text>
          </TouchableOpacity>

          <ImagesList images={order.photos} />

          <View>
            <OrderDetails order={order} view={view} />

            {view !== OrderViewType.MINIMIZED && (
              <Actions
                order={order}
                view={view}
                viewer={viewer}
                closeModal={() => setOpenModal(false)}
                tripUUID={tripUUID}
                setFilter={setFilter}
              />
            )}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  },
);
