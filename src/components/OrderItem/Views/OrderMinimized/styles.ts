import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../../config/constants';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 8,
    backgroundColor: COLORS.GREY2,
  },
  innerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  itemPhoto: {
    width: 31,
    height: 31,
    backgroundColor: COLORS.GREY6,
    marginRight: 12,
  },
  ratingContainer: {
    flexDirection: 'row',
  },
  name: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    fontSize: 16,
    lineHeight: 22,
  },
});
