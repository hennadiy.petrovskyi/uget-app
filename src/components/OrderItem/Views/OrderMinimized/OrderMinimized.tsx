import React, { memo } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { RIGHT_ARROW } from '../../../../assets/svg';

import styles from './styles';

export default memo(({ order, onPress }) => {
  const { photos } = order;

  const orderImage =
    photos && photos?.length ? (
      <Image
        style={styles.itemPhoto}
        source={{
          uri: order.photos[0]?.url,
        }}
      />
    ) : (
      <View style={styles.itemPhoto} />
    );

  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.innerContainer}>
        {orderImage}
        <Text style={styles.name}>{order.details.name}</Text>
      </View>

      <RIGHT_ARROW />
    </TouchableOpacity>
  );
});
