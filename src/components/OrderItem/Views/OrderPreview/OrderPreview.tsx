import React, { memo } from 'react';
import { TouchableOpacity, View } from 'react-native';

import { UserPreview } from '../../..';
import {
  OrderActionsViewType,
  OrderOwnerStatus,
  OrderViewerType,
} from '../../../../types';
import { Actions } from '../../Actions';
import { DeliveryInfo } from '../../DeliveryInfo';
import { Details } from '../../Details';
import { Price } from '../../Price';
import { Status } from '../../Status';

import styles from './styles';

export default memo(
  ({
    order,
    tripUUID,
    setOpenModal,
    setFilter,
    viewer,
    handleViewOrderDetails,
  }: {
    order: any;
    viewer: OrderViewerType;
    tripUUID?: string;
    setFilter?: Function;
    setOpenModal: Function;
    handleViewOrderDetails: Function;
  }) => {
    const details = {
      size: order.details.size,
      name: order.details.name,
      photos: order.photos,
    };

    const deliveryInformation = [
      {
        id: 'from',
        value: `${order.locations.pickup.city.label}, ${order.locations.pickup.country.label}`,
      },
      {
        id: 'to',
        value: `${order.locations.endpoint.city.label}, ${order.locations.endpoint.country.label}`,
      },
      {
        id: 'by',
        value: new Date(order.deliveryDate).toLocaleDateString(),
      },
    ];

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={handleViewOrderDetails}
      >
        <View style={[styles.withPadding]}>
          {order.status !== OrderOwnerStatus.AVAILABLE && (
            <Status status={order.status} />
          )}

          {viewer === OrderViewerType.TRAVELER && (
            <UserPreview owner={order.owner} />
          )}
        </View>

        <View style={styles.withPadding}>
          <Details order={details} viewer={viewer} />
          <DeliveryInfo fields={deliveryInformation} />
        </View>
        {order.status !== OrderOwnerStatus.DELIVERED && (
          <Price
            value={order.deliveryPrice.value}
            type={viewer}
            currency={order.deliveryPrice.currency}
          />
        )}
        <Actions
          order={order}
          view={OrderActionsViewType.MINIMIZED}
          viewer={viewer}
          closeModal={() => setOpenModal(false)}
          tripUUID={tripUUID}
          setFilter={setFilter}
        />
      </TouchableOpacity>
    );
  },
);
