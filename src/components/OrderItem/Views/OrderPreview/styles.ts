import { StyleSheet } from 'react-native';
import { COLORS } from '../../../../config/constants';

export default StyleSheet.create({
  container: {
    backgroundColor: COLORS.CLEAR_WHITE,
    borderWidth: 1.5,
    borderColor: COLORS.MAIN_COLOR,
    borderStyle: 'dashed',
    borderRadius: 0.5,
    paddingTop: 5,
    paddingBottom: 16,
    marginBottom: 20,
  },

  statusContainer: {
    borderBottomWidth: 1,
    borderColor: COLORS.GREY6,
  },

  withPadding: { paddingHorizontal: 16 },
});
