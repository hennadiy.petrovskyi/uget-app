import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';

export default StyleSheet.create({
  totalPrice: {
    backgroundColor: COLORS.GREEN_LIGHT,
    paddingHorizontal: 16,
    marginBottom: 16,
  },
  priceTxtWrapp: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 30,
  },
  priceTxt: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    color: COLORS.BLACK,
    fontSize: 14,
  },
});
