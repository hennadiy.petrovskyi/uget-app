import React from 'react';
import { Text, View } from 'react-native';
import { useTranslation } from 'react-i18next';

import styles from './styles';

export const Price = ({ value, type, currency }) => {
  const { t } = useTranslation();

  return (
    <View style={styles.totalPrice}>
      <View style={styles.priceTxtWrapp}>
        <Text style={styles.priceTxt}>
          {t(`order:orderPreview.price.label.${type}`)}
        </Text>
        <Text style={styles.priceTxt}>{value + ' ' + currency}</Text>
      </View>
    </View>
  );
};
