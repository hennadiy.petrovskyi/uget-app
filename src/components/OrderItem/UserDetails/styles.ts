import { StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
  container: {
    marginBottom: 13,
    flexDirection: 'row',
    alignItems: 'center',
  },
  userPhoto: {
    width: 48,
    height: 48,
    borderRadius: 50,
    backgroundColor: COLORS.GREY6,
    marginRight: 16,
  },
  ratingContainer: {
    flexDirection: 'row',
  },
  name: {
    fontFamily: 'FiraSans-Regular',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 22,
  },
  innerContainer: {},
});
