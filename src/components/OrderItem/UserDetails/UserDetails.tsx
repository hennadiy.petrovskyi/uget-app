import React from 'react';
import { Image, Text, View } from 'react-native';
import {
  RATING_STAR,
  RATING_STAR_HALF,
  RATING_STAR_OUTLINED,
} from '../../../assets/svg';
import styles from './styles';

export const UserDetails = ({}) => {
  const user = {
    name: 'Hennadii Petrovskyi',
    photo: '',
    rate: 4.5,
  };

  function renderRating(rate: number) {
    const stars = [];
    const isDecimal = rate % 1 !== 0;

    for (let index = 0; index < rate; index++) {
      if (isDecimal && index === Math.round(rate)) {
        stars.push(<RATING_STAR_HALF />);
        break;
      }

      stars.push(<RATING_STAR key={index} />);
    }

    const difference = Math.floor(5 - rate);

    if (difference) {
      for (let index = 0; index < difference; index++) {
        stars.push(<RATING_STAR_OUTLINED />);
      }
    }

    return <View style={styles.ratingContainer}>{stars.map((el) => el)}</View>;
  }

  return (
    <View style={styles.container}>
      <Image
        style={styles.userPhoto}
        source={
          {
            //   uri: `data:${item.photos[0].type};base64,${item.photos[0].base64}`,
          }
        }
      />
      <View style={styles.innerContainer}>
        <Text style={styles.name}>{user.name}</Text>
        {renderRating(user.rate)}
      </View>
    </View>
  );
};
