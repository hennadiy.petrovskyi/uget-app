import React, { memo, useEffect, useState } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/core';

import moment from 'moment';

import { UserPreview } from '..';
import { NAVIGATION } from '../../config/constants';
import { getOneDependOnRequestedAuthRecord, getUserDetails } from '../../api';
import styles from './styles';

const Test = ({ conversation }) => {
  const navigation = useNavigation();
  const [contactPerson, setContactPerson] = useState();
  const [order, setOrder] = useState();

  useEffect(() => {
    getUserDetails(conversation.contactPerson).then((response) =>
      setContactPerson(response),
    );

    getOneDependOnRequestedAuthRecord(conversation.orderUUID).then((response) =>
      setOrder(response),
    );
  }, []);

  console.log(conversation.lastMessage);

  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate(NAVIGATION.MESSAGE_SCREENS.CHAT, {
          conversationId: conversation.id,
        })
      }
    >
      <View style={styles.item}>
        {/* {contactPerson && <UserPreview owner={contactPerson} />} */}

        <Text style={styles.date}>
          {moment(conversation.lastMessageTimestamp).format('lll')}
        </Text>

        {order && <Text style={styles.orderName}>{order.details.name}</Text>}

        <View>
          <Text style={styles.text} numberOfLines={2}>
            {conversation.lastMessage}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Test;
