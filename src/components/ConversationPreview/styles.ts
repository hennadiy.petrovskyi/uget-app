import { COLORS, FONT_FAMILY } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  item: {
    backgroundColor: COLORS.GREY2,
    marginBottom: 2,
    paddingVertical: 10,
    paddingHorizontal: 24,
  },

  top: {
    marginBottom: 18,
  },

  date: {
    color: COLORS.GREY,
    fontSize: 12,
    fontFamily: FONT_FAMILY.NORMAL_400,
    marginBottom: 8,
  },

  text: {
    fontSize: 12,
    fontFamily: FONT_FAMILY.NORMAL_400,
    color: COLORS.BLACK,
  },

  orderName: {
    fontSize: 16,
    fontFamily: FONT_FAMILY.MEDIUM_500,
    color: COLORS.BLACK,
    marginBottom: 8,
  },
});
