import { COLORS } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 35,
    borderRadius: 4,
    alignSelf: 'center',
  },

  flexContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },

  marginRight: {
    paddingTop: 1,
    marginRight: 3,
  },

  disabled: {
    color: COLORS.MAIN_DISABLED,
  },
});
