export type LinkWithCountdownProps = {
  isTimeoutActive: boolean;
  activateTimeout: Function;
  marginBottom?: number;
  type: 'sms-verification' | 'email-verification';
  params: any;
  label: string;
};
