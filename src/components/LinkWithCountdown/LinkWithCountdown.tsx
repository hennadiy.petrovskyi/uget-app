import React, { FC, memo, useEffect, useRef, useState } from 'react';
import { View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Recaptcha, { RecaptchaHandles } from 'react-native-recaptcha-that-works';

import { UgetGreyText, UgetLinkText } from '..';

import {
  resendEmailVerification,
  resendVerificationSmsCode,
  verifyRecaptcha,
} from '../../api';

import { LinkWithCountdownProps } from './types';
import styles from './styles';
import { setRecaptcha } from '../../utils/store/general/actions';

const initialTimeoutTime = 15;

const LinkWithCountdown: FC<LinkWithCountdownProps> = ({
  label,
  isTimeoutActive,
  activateTimeout,
  marginBottom = 0,
  type,
  params,
}) => {
  const dispatch = useDispatch();
  const [timeout, setTimeoutDuration] = useState(initialTimeoutTime);
  const recaptcha = useSelector((state) => state.general.recaptcha);
  const recaptchaRef = useRef<RecaptchaHandles>();

  useEffect(() => {
    activateTimeout(true);
  }, []);

  useEffect(() => {
    if (!isTimeoutActive) {
      return;
    }

    if (!timeout && isTimeoutActive) {
      activateTimeout(false);
      setTimeoutDuration(initialTimeoutTime);
    }

    const timeoutId = setTimeout(() => {
      setTimeoutDuration((time) => {
        const result = time - 1;
        return result;
      });
    }, 1000);

    return () => {
      clearTimeout(timeoutId);
    };
  }, [isTimeoutActive, timeout]);

  function onPress() {
    function handleStartCountdown() {
      activateTimeout(true);
    }
    const payload = { ...params, handleStartCountdown };
    if (type === 'sms-verification') {
      dispatch(resendVerificationSmsCode(payload));
      return;
    }
    if (type === 'email-verification') {
      dispatch(resendEmailVerification(payload));
      return;
    }
    if (type === 'order-pick-up-verification') {
    }
    if (type === 'order-delivery-verification') {
    }
  }

  return (
    <View style={[styles.container, { marginBottom }]}>
      <UgetGreyText
        text={isTimeoutActive ? `0:${timeout}` : ''}
        marginBottom={16}
      />
      <UgetLinkText
        text={label}
        onPress={onPress}
        disabled={isTimeoutActive}
        outerStyles={isTimeoutActive && styles.disabled}
      />
    </View>
  );
};

export default memo(LinkWithCountdown);
