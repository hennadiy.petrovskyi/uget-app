import React, { memo, useContext, useState } from 'react';
import { Text, View } from 'react-native';
import { useTranslation } from 'react-i18next';

import styles from './style';
import { OwnerRole } from '../../screens/OrderScreens/CreateOrder/context/context';
import { OrderViewerType, OrderViewType } from '../../types';

const OrderDetails = ({ order, view }: { order: any; view: OrderViewType }) => {
  const { t } = useTranslation();

  const generalDetails = {
    name: {
      label: t('order:summaryStep.itemName'),
      value: order.details.name,
    },
    quantity: {
      label: t('order:summaryStep.quantity'),
      value: order.details.quantity,
    },
    size: {
      label: t('order:summaryStep.packageWeight.key'),
      value: t(`common:inputs.size.value`, {
        weight: order.details.size,
      }),
    },
    type: {
      label: t('order:summaryStep.orderType.label'),
      value: t(`order:summaryStep.orderType.${order.details.type}`),
    },
  };

  const pickUpDetails = {
    country: {
      label: t('order:summaryStep.pickupCountry'),
      value: order.locations.pickup.country.label,
    },
    city: {
      label: t('order:summaryStep.pickupCity'),
      value: order.locations.pickup.city.label,
    },
    area: {
      label: t('order:summaryStep.pickupArea'),
      value: order.locations.pickup.area,
    },
  };

  const endpointDetails = {
    country: {
      label: t('order:summaryStep.deliveryCountry'),
      value: order.locations.endpoint.country.label,
    },
    city: {
      label: t('order:summaryStep.deliveryCity'),
      value: order.locations.endpoint.city.label,
    },
    area: {
      label: t('order:summaryStep.deliveryArea'),
      value: order.locations.endpoint.area,
    },
    deliverBy: {
      label: t('order:summaryStep.deliveryDate'),
      value: new Date(order.deliveryDate).toLocaleDateString(),
    },
  };

  if (order.ownerRole === OwnerRole.RECEIVER) {
    pickUpDetails.contactPhoneNumber = {
      label: t('order:summaryStep.receiverContactNumber'),
      value: order.thirdPartyPhoneNumber.formatted,
    };
  }

  if (order.ownerRole === OwnerRole.SENDER) {
    endpointDetails.contactPhoneNumber = {
      label: t('order:summaryStep.receiverContactNumber'),
      value: order.thirdPartyPhoneNumber.formatted,
    };
  }

  const orderDetails = [
    Object.values(generalDetails),
    Object.values(pickUpDetails),
    Object.values(endpointDetails),
  ];

  return (
    <View style={styles.wrapper}>
      {orderDetails.map((details, sectionIndex) => {
        const isLastSection = sectionIndex + 1 !== orderDetails.length;

        return (
          <View
            key={sectionIndex}
            style={[styles.section, isLastSection && styles.sectionWithBorder]}
          >
            {details.map(({ label, value }, rowIndex) => {
              const isLastRow = rowIndex + 1 !== details.length;

              return (
                <View
                  key={label}
                  style={[styles.row, isLastRow && styles.rowMarginBottom]}
                >
                  <Text style={styles.rowLabel}>{label}</Text>
                  <Text style={styles.rowValue}>{value}</Text>
                </View>
              );
            })}
          </View>
        );
      })}
      {view !== OrderViewType.MINIMIZED && (
        <View style={styles.price}>
          <Text style={styles.rowLabel}>
            {t('order:summaryStep.deliveryPrice')}
          </Text>
          <Text style={styles.rowLabel}>
            {order.deliveryPrice.value + ' ' + order.deliveryPrice.currency}
          </Text>
        </View>
      )}
    </View>
  );
};

export default memo(OrderDetails);
