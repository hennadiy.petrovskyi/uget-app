import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../config/constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.CLEAR_WHITE,
  },
  wrapper: {
    flexDirection: 'column',
  },

  section: {
    paddingVertical: 15,
  },

  sectionWithBorder: {
    borderBottomWidth: 1,
    borderBottomColor: COLORS.GREY3,
  },

  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  rowMarginBottom: {
    marginBottom: 8,
  },

  rowLabel: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    fontSize: 14,
    lineHeight: 22,
    color: COLORS.BLACK,
  },

  rowValue: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    fontSize: 14,
    lineHeight: 20,
    color: COLORS.BLACK,
  },

  price: {
    height: 30,
    backgroundColor: COLORS.GREEN_LIGHT,
    marginBottom: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
});
