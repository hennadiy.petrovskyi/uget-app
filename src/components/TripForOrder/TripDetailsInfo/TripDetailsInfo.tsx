import React, { memo } from 'react';

import { Text, View } from 'react-native';
import styles from './styles';

const ItemDeliveryInfo = ({ fields, marginBottom }) => {
  return (
    <View style={styles.deliveryInfo}>
      {fields.map((info, i) => {
        const isLastChild = i + 1 === fields.length;

        return (
          <View
            key={i}
            style={[
              styles.deliveryField,
              !isLastChild && styles.withMargin,
              marginBottom && { marginBottom },
            ]}
          >
            <Text style={styles.firstField}>{info.key}</Text>
            <Text style={styles.secondField}>{info.value}</Text>
          </View>
        );
      })}
    </View>
  );
};

export default memo(ItemDeliveryInfo);
