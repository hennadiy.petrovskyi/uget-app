import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';

export default StyleSheet.create({
  deliveryInfo: {
    marginBottom: 16,
  },

  deliveryField: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  firstField: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    color: COLORS.BLACK,
    fontSize: 14,
    lineHeight: 22,
  },

  secondField: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    fontSize: 14,
    lineHeight: 22,
    color: COLORS.BLACK,
  },

  withMargin: { marginBottom: 10 },
});
