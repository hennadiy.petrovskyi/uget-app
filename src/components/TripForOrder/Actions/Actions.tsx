import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';
import { UgetButton } from '../..';
import {
  orderOwnerAcceptDeliveryRequest,
  orderOwnerCancelDeliveryRequest,
  orderOwnerRequestOrderDelivery,
  startConversation,
} from '../../../api';
import {
  getOneTripFilteredByOrder,
  getTripOwnerDetails,
} from '../../../api/trip';
import { NAVIGATION } from '../../../config/constants';
import { setNavigationSession } from '../../../utils/store/general/actions';

export const Actions = ({ status, requestDto, description }) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  function updateTrip() {
    dispatch(getOneTripFilteredByOrder(requestDto));
  }

  function acceptIncomingRequest() {
    dispatch(orderOwnerAcceptDeliveryRequest(requestDto, updateTrip));
  }

  async function handleRequestDelivery() {
    dispatch(orderOwnerRequestOrderDelivery(requestDto, updateTrip));
  }

  async function handleCancel() {
    dispatch(orderOwnerCancelDeliveryRequest(requestDto, updateTrip));
  }

  async function handleChat() {
    const onSuccess = (conversationId: any) => {
      dispatch(
        setNavigationSession({
          from: {
            stack: NAVIGATION.TABS_ROUTE.ORDER_SCREENS,
            screen: NAVIGATION.ORDER_SCREENS.TRAVELERS_LIST,
          },
          to: {
            stack: NAVIGATION.TABS_ROUTE.MESSAGES,
            screen: NAVIGATION.MESSAGE_SCREENS.CHAT,
          },
        }),
      );

      navigation.navigate(NAVIGATION.TABS_ROUTE.MESSAGES, {
        screen: NAVIGATION.MESSAGE_SCREENS.CHAT,
        params: {
          conversationId,
          initMessage: description,
        },
      });
    };

    const contactPersonUUID = await getTripOwnerDetails({
      uuid: requestDto.tripUUID,
    });

    console.log('handleChat', requestDto.tripUUID);

    startConversation(
      { contactPersonUUID, orderUUID: requestDto.orderUUID },
      dispatch,
      onSuccess,
    );
  }

  const actions = {
    incoming: (
      <>
        <UgetButton
          type="primary"
          label="Accept"
          onPress={acceptIncomingRequest}
          marginBottom={10}
        />
        <UgetButton
          type="outlined"
          label="Chat"
          onPress={handleChat}
          marginBottom={10}
        />
        <UgetButton type="warning" label="Decline" onPress={handleCancel} />
      </>
    ),
    outgoing: (
      <>
        <UgetButton
          type="primary"
          label="Chat"
          onPress={handleChat}
          marginBottom={10}
        />
        <UgetButton type="warning" label="Cancel" onPress={handleCancel} />
      </>
    ),
    available: (
      <>
        <UgetButton
          type="primary"
          label="Request to deliver"
          onPress={handleRequestDelivery}
          marginBottom={10}
        />
        <UgetButton type="outlined" label="Chat" onPress={handleChat} />
      </>
    ),
    accepted: (
      <>
        <UgetButton
          type="primary"
          label="Chat"
          onPress={handleChat}
          marginBottom={10}
        />
        <UgetButton type="warning" label="Cancel" onPress={handleCancel} />
      </>
    ),
  };

  return <View>{actions[status]}</View>;
};
