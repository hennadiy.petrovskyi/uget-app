import React, { FC, memo } from 'react';
import { View } from 'react-native';
import { useTranslation } from 'react-i18next';

import { TripDetailsInfo } from './TripDetailsInfo';
import { Status } from './Status';
import { Actions } from './Actions';
import { UserPreview } from '..';

import { TravelerCartComponentProps } from './types';
import { TripForOrderStatus } from '../../types';
import styles from './styles';

const TripForOrder: FC<TravelerCartComponentProps> = ({
  trip,
  marginBottom,
  orderUUID,
}) => {
  const { t } = useTranslation();

  const deliveryInformation = [
    {
      key: t('order:trips.details.from'),
      value: `${trip.from.city.label}, ${trip.from.country.label}`,
    },
    {
      key: t('order:trips.details.to'),
      value: `${trip.to.city.label}, ${trip.to.country.label}`,
    },
    {
      key: t('order:trips.details.departure'),
      value: new Date(trip.departureDate).toLocaleDateString(),
    },
    {
      key: t('order:trips.details.arrival'),
      value: new Date(trip.arrivalDate).toLocaleDateString(),
    },
  ];

  return (
    <View style={[styles.container, marginBottom && { marginBottom }]}>
      {trip.status !== TripForOrderStatus.AVAILABLE && (
        <Status status={trip.status} />
      )}

      <View style={styles.deliveryInfo}>
        <UserPreview owner={trip.owner} />

        <TripDetailsInfo fields={deliveryInformation} marginBottom={10} />

        <Actions
          status={trip.status}
          description={trip.description}
          requestDto={{ tripUUID: trip.uuid, orderUUID }}
        />
      </View>
    </View>
  );
};

export default memo(TripForOrder);
