import { StyleProp } from 'react-native';

export type TravelerCartComponentProps = {
  trip: any;
  marginBottom?: number;
  setTrips: Function;
  orderUUID: string;
};
