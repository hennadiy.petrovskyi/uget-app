import { COLORS } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: COLORS.GREY2,
    paddingBottom: 22,
  },
  header: {
    flexDirection: 'row',
  },

  deliveryInfo: {
    paddingHorizontal: 16,
  },
});
