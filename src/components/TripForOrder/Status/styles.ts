import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';

export default StyleSheet.create({
  container: {
    paddingVertical: 8,
  },

  text: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    color: COLORS.BLACK,
    textAlign: 'center',
    fontSize: 14,
    lineHeight: 18,
  },
});
