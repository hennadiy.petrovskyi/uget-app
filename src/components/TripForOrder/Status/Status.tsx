import React, { FC, memo } from 'react';
import { View, Text } from 'react-native';
import { useTranslation } from 'react-i18next';

import { COLORS } from '../../../config/constants';
import styles from './styles';

export const Status: FC<any> = ({ status }) => {
  const { t } = useTranslation();

  const statusStyles = {
    incoming: COLORS.NUDE,
    outgoing: COLORS.NUDE,
    accepted: COLORS.GREEN_LIGHT,
    declined: 'red',
  };

  return (
    <View style={[styles.container, { backgroundColor: statusStyles[status] }]}>
      <Text style={styles.text}>{t(`order:trips.status.${status}`)}</Text>
    </View>
  );
};
