import React, { FC, memo } from 'react';
import { useSelector } from 'react-redux';
import { View, ActivityIndicator } from 'react-native';
import Modal from 'react-native-modal';

import { ModalLoaderProps } from './types';
import { COLORS } from '../../config/constants';

import styles from './styles';

const ModalLoader: FC<ModalLoaderProps> = () => {
  const isActive = useSelector((state) => state.general.loader.isActive);

  return (
    <Modal isVisible={isActive}>
      <View style={[styles.modalContainer]}>
        <ActivityIndicator size="large" color={COLORS.CLEAR_WHITE} />
      </View>
    </Modal>
  );
};

export default memo(ModalLoader);
