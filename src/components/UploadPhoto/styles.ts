import { StyleSheet } from 'react-native';
import { COLORS } from '../../config/constants';

export default StyleSheet.create({
  modalContainer: {
    justifyContent: 'flex-end',
  },

  modalContent: {
    borderRadius: 4,
  },

  wrapperActions: {
    backgroundColor: COLORS.WHITE,
    marginBottom: 16,
    paddingVertical: 24,
    paddingHorizontal: 16,
    borderRadius: 4,
  },

  createPhoto: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    paddingBottom: 7,
    borderBottomColor: COLORS.MAIN_COLOR,
    borderBottomWidth: 1,
    width: '100%',
  },

  choosePhoto: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 8,
    alignSelf: 'center',
    width: '100%',
  },

  actionText: {
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 16,
    color: COLORS.MAIN_COLOR,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.MAIN_COLOR,
  },

  errorMessage: {
    paddingTop: 4,
    color: COLORS.RED,
  },

  modalBtnWrap: {
    paddingHorizontal: 30,
    marginBottom: 24,
  },
});
