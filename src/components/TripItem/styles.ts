import { COLORS, FONT_FAMILY } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    marginBottom: 16,
    backgroundColor: COLORS.CLEAR_WHITE,
    borderWidth: 1.5,
    borderColor: COLORS.BORDER_GREY,
    borderStyle: 'dashed',
    borderRadius: 0.5,
  },

  notify: {
    flex: 1,
    marginTop: 16,
  },

  notifyRow: {
    flexDirection: 'row',
    flex: 1,

    position: 'relative',
  },

  leftContainer: {
    flexDirection: 'row',
    width: '50%',
    alignItems: 'center',
  },

  rightContainer: {
    flexDirection: 'row',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  notifyText: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    color: COLORS.GREY5,
    fontSize: 14,
    lineHeight: 16,
  },

  valuesText: {
    backgroundColor: COLORS.CLEAR_WHITE,
    paddingLeft: 20,
  },

  wrapLine: {
    overflow: 'hidden',
    height: 1,
    width: '100%',
    position: 'absolute',
  },

  notifyLine: {
    height: 1,
    borderColor: COLORS.BORDER_GREY,
    borderStyle: 'dashed',
    borderRadius: 0.1,
    borderWidth: 3,
    marginBottom: 0,
  },

  notifyIcon: {
    marginRight: 10,
  },
});
