import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';
import { UgetButton } from '../..';
import { deleteTrip } from '../../../api';

import { NAVIGATION } from '../../../config/constants';
import {
  setLoaderStatus,
  setNavigationSession,
} from '../../../utils/store/general/actions';

export const Actions = ({ trip }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const navigation = useNavigation();

  const { uuid, locations, arrivalDate, departureDate } = trip;

  const incomingTripRoutesParams = {
    tripUUID: uuid,
    departureDate,
    arrivalDate,
    locations: trip.locations,
    earnings: trip.earnings,
  } as never;

  const activeAndPastRouteParams = {
    tripUUID: uuid,
    from: locations.from.city.label,
    to: locations.to.city.label,
    tripDates: `${new Date(departureDate).toLocaleDateString()} - ${new Date(
      arrivalDate,
    ).toLocaleDateString()}`,
  } as never;

  const actions = {
    upcoming: (
      <>
        <UgetButton
          type="primary"
          marginBottom={10}
          label={t('trips:tripItem.buttons.viewOrders')}
          onPress={() =>
            navigation.navigate(
              NAVIGATION.TRIPS.TRIPS_ORDERS as never,
              incomingTripRoutesParams,
            )
          }
        />

        <UgetButton
          type="warning"
          label={t('trips:tripItem.buttons.delete')}
          onPress={() => {
            dispatch(setLoaderStatus(true));

            dispatch(
              deleteTrip(trip.uuid, () => dispatch(setLoaderStatus(false))),
            );
          }}
        />
      </>
    ),
    active: (
      <UgetButton
        type="primary"
        label={t('trips:tripItem.buttons.viewOrders')}
        onPress={() =>
          navigation.navigate(
            NAVIGATION.TRIPS.ACCEPTED_ORDERS as never,
            activeAndPastRouteParams,
          )
        }
      />
    ),
    past: (
      <>
        {trip.delivered.length ? (
          <UgetButton
            type="primary"
            label={t('trips:tripItem.buttons.viewOrders')}
            onPress={() =>
              navigation.navigate(
                NAVIGATION.TRIPS.DELIVERED_ORDERS as never,
                activeAndPastRouteParams,
              )
            }
          />
        ) : null}
      </>
    ),
  };

  return <View style={{ marginTop: 18 }}>{actions[trip.status]}</View>;
};
