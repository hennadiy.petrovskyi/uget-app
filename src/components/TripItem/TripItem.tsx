import React, { FC, memo, ReactElement } from 'react';
import { Text, View } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import { EARNINGS, ACCEPTED, REQUEST } from '../../assets/svg';
import { RoutePreview, UgetButton } from '..';
import { NAVIGATION } from '../../config/constants';
import { Actions } from './Actions/Actions';
import { TripItemProps } from './types';

import styles from './styles';

const TripItem: FC<TripItemProps> = ({ trip }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  const informationRows: {
    id: string;
    icon: ReactElement;
    value: number;
  }[] = [
    {
      id: 'requests',
      icon: REQUEST,
      value: trip.incomingRequests.length,
      isVisible: trip.status === 'upcoming',
    },
    {
      id: 'accepted',
      icon: ACCEPTED,
      value: trip.accepted.length,
      isVisible: trip.status === 'active' || trip.status === 'upcoming',
    },
    {
      id: 'transit',
      icon: ACCEPTED,
      value: trip.inProgress.length,
      isVisible: trip.status === 'active' && trip.inProgress.length,
    },
    {
      id: 'delivered',
      icon: ACCEPTED,
      value: trip.delivered.length,
      isVisible: trip.status === 'active' || trip.status === 'past',
    },
    {
      id: 'earnings',
      icon: EARNINGS,
      value: trip.earnings.value + ' ' + trip.earnings.currency,
      isVisible: true,
    },
  ];

  const renderInformationRow = (
    information: {
      id: 'requests' | 'accepted' | 'earnings';
      icon: ReactElement;
      value: number;
    },
    isLastChild,
  ) => {
    const { id, icon: Icon, value } = information;
    const label = t(`trips:tripItem.${id}`);

    return (
      <View
        key={id}
        style={[styles.notifyRow, !isLastChild && { marginBottom: 10 }]}
      >
        <View style={styles.leftContainer}>
          <Icon style={styles.notifyIcon} />
          <Text style={styles.notifyText}>{label}</Text>
        </View>

        <View style={[styles.rightContainer]}>
          <View style={styles.wrapLine}>
            <View style={styles.notifyLine} />
          </View>
          <Text
            style={[styles.notifyText, styles.valuesText]}
          >{`(${value})`}</Text>
        </View>
      </View>
    );
  };

  const filteredByVisibility = informationRows.filter((row) => row.isVisible);

  return (
    <View style={styles.container}>
      <RoutePreview
        from={trip.locations.from.city.label}
        to={trip.locations.to.city.label}
        date={
          new Date(trip.departureDate).toLocaleDateString() +
          ' - ' +
          new Date(trip.arrivalDate).toLocaleDateString()
        }
      />

      <View style={styles.notify}>
        {filteredByVisibility.map((row, i) => {
          const isLastChild = i + 1 === filteredByVisibility.length;

          return renderInformationRow(row, isLastChild);
        })}
      </View>

      <Actions trip={trip} />
    </View>
  );
};

export default memo(TripItem);
