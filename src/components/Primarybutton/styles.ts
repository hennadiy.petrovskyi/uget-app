import { COLORS } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    height: 44,
    backgroundColor: COLORS.MAIN_COLOR,
    borderRadius: 6,
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  childrenWrap: {
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: 10
  },
  txtButton: {
    paddingHorizontal: 10,
    color: 'white',
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 16
  }
});
