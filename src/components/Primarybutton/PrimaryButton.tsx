import React, { FC, memo } from 'react';
import { TouchableOpacity, View, Text } from 'react-native';

import { PrimaryButtonProps } from './types';
import styles from './styles';

const PrimaryButton: FC<PrimaryButtonProps> = ({
  txt,
  onPress = () => {},
  children,
  style = {},
  disabled
}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      style={[styles.container, style]}
      onPress={() => {
        onPress();
      }}
    >
      <View style={styles.childrenWrap}>
        {children}
        <Text style={styles.txtButton}>{`${txt}`}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default memo(PrimaryButton);
