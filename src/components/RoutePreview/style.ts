import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../config/constants';

export default StyleSheet.create({
  directionWrapper: {},

  direction: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 8,
  },

  iconPlane: {
    marginHorizontal: 21,
  },

  directionTxt: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    color: COLORS.BLACK,
    fontSize: 16,
    lineHeight: 19,
  },

  dateTxt: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    color: COLORS.GREY,
    fontSize: 14,
    lineHeight: 17,
  },
});
