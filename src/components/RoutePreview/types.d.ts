export interface ITripDestinationProps {
  from: string;
  to: string;
  date: string;
  marginBottom?: number;
}
