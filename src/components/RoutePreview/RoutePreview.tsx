import React, { FC, memo } from 'react';
import { Text, View } from 'react-native';

import { useTranslation } from 'react-i18next';

import { PLANE } from '../../assets/svg';

import { ITripDestinationProps } from './types';
import styles from './style';

const RoutePreview: FC<ITripDestinationProps> = ({
  from,
  to,
  date,
  marginBottom,
}) => {
  const { t } = useTranslation();

  return (
    <View style={[styles.directionWrapper, marginBottom && { marginBottom }]}>
      <View style={styles.direction}>
        <Text style={styles.directionTxt}>{from}</Text>
        <PLANE style={styles.iconPlane} />
        <Text style={styles.directionTxt}>{to}</Text>
      </View>

      <Text style={styles.dateTxt}>{date}</Text>
    </View>
  );
};

export default memo(RoutePreview);
