import { COLORS } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  imgWrapp: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '70%',
  },
  image: {
    width: '100%',
    height: '80%',
  },
  txtWrap: {
    height: '25%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 26,
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 14,
    lineHeight: 21,
    color: COLORS.GREY,
    paddingTop: 16,
    textAlign: 'center',
  },
});
