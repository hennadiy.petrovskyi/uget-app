export type OnBoardingComponentProps = {
  img: string;
  title: string;
  subTitle: string;
};
