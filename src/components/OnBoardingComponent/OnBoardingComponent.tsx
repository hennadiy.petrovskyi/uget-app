import React, { FC, memo } from 'react';
import { Text, View, Image } from 'react-native';

import { OnBoardingComponentProps } from './types';
import styles from './styles';

const OnBoardingComponent: FC<OnBoardingComponentProps> = ({
  img,
  title,
  subTitle
}) => {
  return (
    <>
      <View style={styles.imgWrapp}>
        <Image style={styles.image} resizeMode="contain" source={img} />
      </View>
      <View style={styles.txtWrap}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.subTitle}>{subTitle}</Text>
      </View>
    </>
  );
};

export default memo(OnBoardingComponent);
