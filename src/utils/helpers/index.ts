export function generateDateWithOffset(date: any | undefined, offset: number) {
  const dateToParse = date || new Date();

  return new Date(
    dateToParse.getFullYear(),
    dateToParse.getMonth(),
    dateToParse.getDate() + offset,
  );
}
