import EncryptedStorage from 'react-native-encrypted-storage';
import { logger } from '../logger';

interface ISession {
  accessToken: string;
  refreshToken: string;
}

class DeviceEncryptedStorage {
  SERVICE_PATH = 'utils/encryptedStorage';

  async storeUserSession(session: ISession) {
    const functionName = this.storeUserSession.name;

    try {
      await EncryptedStorage.setItem('user_session', JSON.stringify(session));

      return {
        status: 'success',
        text: 'user_session was saved to encrypted storage',
      };
    } catch (error) {
      logger.logError(this.SERVICE_PATH, functionName, error);
    }
  }

  async retrieveUserSession(): Promise<ISession | undefined> {
    const functionName = this.retrieveUserSession.name;

    try {
      const session = await EncryptedStorage.getItem('user_session');

      if (!session) {
        return;
      }

      return JSON.parse(session);
    } catch (error) {
      logger.logError(this.SERVICE_PATH, functionName, error);
    }
  }

  async removeUserSession() {
    const functionName = this.removeUserSession.name;

    try {
      await EncryptedStorage.removeItem('user_session');
    } catch (error) {
      logger.logError(this.SERVICE_PATH, functionName, error);
    }
  }

  async clearStorage() {
    const functionName = this.removeUserSession.name;

    try {
      await EncryptedStorage.clear();
    } catch (error) {
      logger.logError(this.SERVICE_PATH, functionName, error);
    }
  }
}

export const encryptedStorage = new DeviceEncryptedStorage();
