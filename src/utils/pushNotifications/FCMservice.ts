import { Keys } from './../asyncStorage/keys';
import { Linking, Platform } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import { asyncStorage } from '../asyncStorage';
import { refreshDeviceToken, registerDeviceToken } from '../../api';
/* eslint-disable class-methods-use-this */

export class FCMService {
  /** Service initialization
   IOS - to have permissions
  Android - to create after app get installed a chanel with settings
   Will be created only once */
  public async register() {
    if (Platform.OS === 'ios') {
      this.checkPermission();
    }

    if (Platform.OS === 'android') {
      // await notifee.createChannel({
      //   id: 'AlmaHealth',
      //   name: 'AlmaHealth Notifications',
      //   importance: AndroidImportance.HIGH,
      //   visibility: 1,
      //   sound: 'default',
      // });
      await this.getToken();
      await this.createNotificationListeners();
    }
  }

  /** ONLY IOS - Verify if user  permissions */
  private async checkPermission() {
    try {
      const authorizationStatus = await messaging().requestPermission();
      const hasPermission =
        authorizationStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authorizationStatus === messaging.AuthorizationStatus.PROVISIONAL;

      if (hasPermission) {
        this.getToken();
        this.createNotificationListeners();
      }
    } catch (error) {
      console.log(error);
    }
  }

  /**  Get token from FCM and add it in to the DB */
  async getToken() {
    try {
      const fcmToken = await messaging().getToken();

      console.log(`[FCMservice] TOKEN' : ${fcmToken}`);
      const deviceToken = await asyncStorage.get(Keys.DEVICE_TOKEN);

      if (!deviceToken) {
        await asyncStorage.save(Keys.DEVICE_TOKEN, fcmToken);
      }

      await registerDeviceToken(fcmToken);
    } catch (error) {
      console.log(`[FCMservice] getToken was rejected`);
    }
  }

  /**  Listeners for quit / background / foreground / token refresh */
  createNotificationListeners() {
    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        console.log(
          '[FCMservice] Notification caused app to open from quit state:',
        );
        this.onNotification(remoteMessage);

        // notifeeHelper.handlePressAction(remoteMessage);
      });

    messaging().onNotificationOpenedApp((remoteMessage) => {
      console.log(
        '[FCMservice] Notification caused app to open from background state:',
        remoteMessage,
      );
      this.onNotification(remoteMessage);

      // notifeeHelper.handlePressAction(remoteMessage);
    });

    messaging().onMessage((remoteMessage) => {
      if (remoteMessage) {
        console.log('[FCMservice] A new FCM message arrived!', remoteMessage);

        enum NotificationType {
          MY_ORDERS = 'order.my',
          ORDER_RELATED_TRIPS = 'order.related.trips',
          ORDERS_SEARCH_TRIPS = 'order.search.trips',
          MY_TRIPS = 'trip.my',
          TRIP_RELATED_ORDERS = 'trip.related.orders',
          TRIP_SEARCH_ORDERS = 'trip.search.orders',
        }

        const handleRefetchData = (key: NotificationType) => {
          switch (key) {
            case NotificationType.MY_ORDERS:
              break;
            case NotificationType.MY_TRIPS:
              break;
            case NotificationType.ORDERS_SEARCH_TRIPS:
              break;
            case NotificationType.ORDER_RELATED_TRIPS:
              break;
            case NotificationType.TRIP_RELATED_ORDERS:
              break;
            case NotificationType.TRIP_SEARCH_ORDERS:
              break;

            default:
              break;
          }
        };
      }
    });

    /**  Firebase can update token by itself .
     In this case only we will refresh existing one
     Else we will register new one in DB */
    messaging().onTokenRefresh(async (newToken) => {
      console.log(`[FCMservice] Token was refreshed : ${newToken}`);
      const oldToken = await asyncStorage.get(Keys.DEVICE_TOKEN);

      if (!oldToken) {
        await registerDeviceToken(newToken);
      } else {
        await refreshDeviceToken({ newToken, oldToken });
      }

      await asyncStorage
        .save(Keys.DEVICE_TOKEN, newToken)
        .catch((err) => console.log(err));
    });
  }

  /** Notification handler from all types of states
    Receive raw notification and prepared it to show via NOTIFEE
    Depend on platform */

  async onNotification(remoteMessage: any) {
    if (!remoteMessage) {
      return;
    }

    const { data } = remoteMessage;

    if (data && data.link) {
      console.log('LINKING !!!');

      Linking.openURL(data.link);
    }
  }

  /** Unregister listeners on APP quit , to prevent memory leaks */
  unRegister() {
    this.createNotificationListeners();
  }
}

export const fcmService = new FCMService();
