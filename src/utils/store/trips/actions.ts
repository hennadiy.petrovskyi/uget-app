import { Order } from '../../../screens/OrderScreens/CreateOrder/context/context';
import { makeAction, ACTIONS } from '../actions';

export const tripsHydrateState = makeAction<ACTIONS.TRIPS_HYDRATE_STATE, any[]>(
  ACTIONS.TRIPS_HYDRATE_STATE,
);

export const tripsUpdateTrip = makeAction<ACTIONS.TRIPS_UPDATE_TRIP, any>(
  ACTIONS.TRIPS_UPDATE_TRIP,
);

export const tripsSetFilteredTrips = makeAction<
  ACTIONS.TRIPS_SET_FILTERED_BY_ORDER,
  any
>(ACTIONS.TRIPS_SET_FILTERED_BY_ORDER);

export const updateTripFilteredByOrder = makeAction<
  ACTIONS.TRIPS_UPDATE_FILTERED_BY_ORDER_TRIP,
  any
>(ACTIONS.TRIPS_UPDATE_FILTERED_BY_ORDER_TRIP);

export const tripsClearState = makeAction<ACTIONS.TRIPS_CLEAR_STATE>(
  ACTIONS.TRIPS_CLEAR_STATE,
);
