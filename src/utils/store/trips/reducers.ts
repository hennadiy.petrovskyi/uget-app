import order from '../../../localization/en/order';
import { IAction, ACTIONS } from '../actions';

export class GeneralState {
  uploaded: any[];
  filteredByOrder: any[];

  constructor() {
    this.uploaded = [];
    this.filteredByOrder = [];
  }
}

const initialState = new GeneralState();

export function tripsReducer(
  state = initialState,
  { type, payload }: IAction,
): GeneralState {
  switch (type) {
    case ACTIONS.TRIPS_HYDRATE_STATE:
      return {
        ...state,
        uploaded: payload,
      };

    case ACTIONS.TRIPS_UPDATE_TRIP:
      return {
        ...state,
        uploaded: state.uploaded
          .filter((trip) => trip.uuid !== payload.uuid)
          .concat([payload]),
      };

    case ACTIONS.TRIPS_SET_FILTERED_BY_ORDER:
      return {
        ...state,
        filteredByOrder: payload,
      };

    case ACTIONS.TRIPS_UPDATE_FILTERED_BY_ORDER_TRIP:
      return {
        ...state,
        filteredByOrder: state.filteredByOrder
          .filter((trip) => trip.uuid !== payload.uuid)
          .concat([payload]),
      };

    case ACTIONS.TRIPS_CLEAR_STATE:
      return {
        ...state,
        uploaded: [],
        filteredByOrder: [],
      };

    default:
      return state;
  }
}
