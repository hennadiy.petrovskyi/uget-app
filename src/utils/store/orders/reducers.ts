import { Order } from '../../../screens/OrderScreens/CreateOrder/context';
import { IAction, ACTIONS } from '../actions';

export class GeneralState {
  selected: null | string;
  uploaded: Order[];
  filteredByTrip: any[];
  drafts: Order[];

  constructor() {
    this.selected = null;
    this.uploaded = [];
    this.filteredByTrip = [];
    this.drafts = [];
  }
}

const initialState = new GeneralState();

export function orderReducer(
  state = initialState,
  { type, payload }: IAction,
): GeneralState {
  switch (type) {
    case ACTIONS.ORDERS_ADD_DRAFT:
      return {
        ...state,
        drafts: state.drafts.concat([payload]),
      };

    case ACTIONS.ORDERS_DELETE_DRAFT:
      return {
        ...state,
        drafts: state.drafts.filter((order) => order.uuid !== payload.uuid),
      };

    case ACTIONS.ORDERS_SET_DRAFTS:
      return {
        ...state,
        drafts: payload.drafts,
      };

    case ACTIONS.ORDERS_HYDRATE_STATE:
      return {
        ...state,
        uploaded: payload,
      };

    case ACTIONS.ORDERS_UPDATE_ONE:
      return {
        ...state,
        uploaded: state.uploaded
          .filter((order) => order.uuid !== payload.uuid)
          .concat([payload]),
      };

    case ACTIONS.ORDERS_SET_FILTERED_BY_TRIP:
      return {
        ...state,
        filteredByTrip: payload,
      };

    case ACTIONS.ORDERS_UPDATE_FILTERED_BY_TRIP_ORDER:
      return {
        ...state,
        filteredByTrip: state.filteredByTrip
          .filter((order) => order.uuid !== payload.uuid)
          .concat([payload]),
      };

    case ACTIONS.ORDERS_CLEAR_STATE:
      return {
        ...state,
        uploaded: [],
        filteredByTrip: [],
      };

    default:
      return state;
  }
}
