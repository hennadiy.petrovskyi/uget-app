import { Order } from '../../../screens/OrderScreens/CreateOrder/context/context';
import { makeAction, ACTIONS } from '../actions';

export const addDraft = makeAction<ACTIONS.ORDERS_ADD_DRAFT, Order>(
  ACTIONS.ORDERS_ADD_DRAFT,
);

export const removeDraft = makeAction<
  ACTIONS.ORDERS_DELETE_DRAFT,
  { uuid: string }
>(ACTIONS.ORDERS_DELETE_DRAFT);

export const setDrafts = makeAction<
  ACTIONS.ORDERS_SET_DRAFTS,
  { drafts: Order[] }
>(ACTIONS.ORDERS_SET_DRAFTS);

export const ordersHydrateState = makeAction<
  ACTIONS.ORDERS_HYDRATE_STATE,
  Order[]
>(ACTIONS.ORDERS_HYDRATE_STATE);

export const ordersUpdateOne = makeAction<ACTIONS.ORDERS_UPDATE_ONE, Order>(
  ACTIONS.ORDERS_UPDATE_ONE,
);

export const ordersSetFilteredByTrip = makeAction<
  ACTIONS.ORDERS_SET_FILTERED_BY_TRIP,
  Order[]
>(ACTIONS.ORDERS_SET_FILTERED_BY_TRIP);

export const ordersUpdateOrderFilteredByTrip = makeAction<
  ACTIONS.ORDERS_UPDATE_FILTERED_BY_TRIP_ORDER,
  Order
>(ACTIONS.ORDERS_UPDATE_FILTERED_BY_TRIP_ORDER);

export const clearOrdersState = makeAction<ACTIONS.ORDERS_CLEAR_STATE>(
  ACTIONS.ORDERS_CLEAR_STATE,
);
