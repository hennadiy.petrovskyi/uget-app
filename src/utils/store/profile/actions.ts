import { makeAction, ACTIONS } from '../actions';

export const hydrateUserProfile = makeAction<ACTIONS.USER_HYDRATE_PROFILE, any>(
  ACTIONS.USER_HYDRATE_PROFILE,
);

export const clearUserProfile = makeAction<ACTIONS.USER_CLEAR_PROFILE>(
  ACTIONS.USER_CLEAR_PROFILE,
);

export const setPushNotificationStatus = makeAction<
  ACTIONS.SET_PUSH_NOTIFICATIONS_STATUS,
  boolean
>(ACTIONS.SET_PUSH_NOTIFICATIONS_STATUS);

export const setEmailNotificationStatus = makeAction<
  ACTIONS.SET_EMAIL_NOTIFICATIONS_STATUS,
  boolean
>(ACTIONS.SET_EMAIL_NOTIFICATIONS_STATUS);
