import { IAction, ACTIONS } from '../actions';

interface IEmailPhoneNumber {
  value: string;
  isVerified: boolean;
}

export class GeneralState {
  firstName: string | null;
  lastName: string | null;
  phoneNumber: IEmailPhoneNumber | null;
  email: IEmailPhoneNumber | null;
  rating: number | null;
  profileImage: string | null;
  uuid: string | null;
  notificationSettings: { push: boolean; email: boolean };

  constructor() {
    this.firstName = null;
    this.lastName = null;
    this.phoneNumber = null;
    this.email = null;
    this.rating = null;
    this.profileImage = null;
    this.uuid = null;
    this.notificationSettings = {
      push: false,
      email: false,
    };
  }
}

const initialState = new GeneralState();

export function profileReducer(
  state = initialState,
  { type, payload }: IAction,
): GeneralState {
  switch (type) {
    case ACTIONS.USER_HYDRATE_PROFILE:
      return {
        ...state,
        ...payload,
      };

    case ACTIONS.USER_CLEAR_PROFILE:
      return {
        ...state,
        firstName: null,
        lastName: null,
        phoneNumber: null,
        email: null,
        rating: null,
        profileImage: null,
        uuid: null,
      };

    case ACTIONS.SET_PUSH_NOTIFICATIONS_STATUS:
      return {
        ...state,
        notificationSettings: {
          ...state.notificationSettings,
          push: payload,
        },
      };

    case ACTIONS.SET_EMAIL_NOTIFICATIONS_STATUS:
      return {
        ...state,
        notificationSettings: {
          ...state.notificationSettings,
          email: payload,
        },
      };

    default:
      return state;
  }
}
