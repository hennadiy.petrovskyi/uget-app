import { IAction, ACTIONS } from '../actions';

export class GeneralState {
  conversations: any[];

  constructor() {
    this.conversations = [];
  }
}

const initialState = new GeneralState();

export function ChatReducer(
  state = initialState,
  { type, payload }: IAction,
): GeneralState {
  switch (type) {
    case ACTIONS.CHAT_HYDRATE_CONVERSATIONS:
      return {
        ...state,
        conversations: payload.map((el) => {
          if (el.messages.length) {
            const lastMessage = el.messages[el.messages.length - 1];

            el.lastMessage = lastMessage.message;
            el.lastMessageTimestamp = lastMessage.createdAt;
          }

          return el;
        }),
      };
    case ACTIONS.CHAT_ADD_NEW_MESSAGE:
      return {
        ...state,
        conversations: state.conversations.map((conversation) => {
          if (conversation.id === payload.conversationId) {
            const isMessageAlreadyExist = conversation.messages.find(
              (el) => el.id === payload.id,
            );

            if (isMessageAlreadyExist) {
              return conversation;
            }

            conversation.messages = conversation.messages.concat(payload);
            conversation.lastMessage = payload.message;
            conversation.lastMessageTimestamp = payload.createdAt;

            return conversation;
          }

          return conversation;
        }),
      };
    case ACTIONS.CHAT_CLEAR_STATE:
      return {
        ...state,
        conversations: [],
      };
    default:
      return state;
  }
}
