import { makeAction, ACTIONS } from '../actions';

export const addNewConversationMessage = makeAction<
  ACTIONS.CHAT_ADD_NEW_MESSAGE,
  any
>(ACTIONS.CHAT_ADD_NEW_MESSAGE);

export const conversationsHydrateState = makeAction<
  ACTIONS.CHAT_HYDRATE_CONVERSATIONS,
  any[]
>(ACTIONS.CHAT_HYDRATE_CONVERSATIONS);

export const chatClearState = makeAction<ACTIONS.CHAT_CLEAR_STATE>(
  ACTIONS.CHAT_CLEAR_STATE,
);
