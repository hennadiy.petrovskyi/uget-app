import { NotificationID } from '../../../components/ModalNotifications/ModalNotificationsProvider';
import { IAction, ACTIONS } from '../actions';

export class GeneralState {
  isAuth: boolean;
  navigationSession: {
    from: {
      stack: string;
      screen: string;
    };
    to: {
      stack: string;
      screen: string;
    };
    options?: {
      withNotification: boolean;
      notificationId: NotificationID;
      notificationOption: any;
    };
  } | null;

  loader: {
    isActive: boolean;
    text: string | undefined;
  };

  localNotifications: { id: NotificationID; options: any }[];
  recaptcha: {
    refresh: boolean;
    token: string | null;
    created: number | null;
  };
  constructor() {
    this.isAuth = false;
    this.navigationSession = null;
    this.loader = { isActive: false, text: '' };
    this.localNotifications = [];
    this.recaptcha = {
      refresh: false,
      token: null,
      created: null,
    };
  }
}

const initialState = new GeneralState();

export function generalReducer(
  state = initialState,
  { type, payload }: IAction,
): GeneralState {
  switch (type) {
    case ACTIONS.AUTH_HANDLE_AUTHENTICATION_STATUS:
      return {
        ...state,
        isAuth: payload,
      };

    case ACTIONS.LOCAL_NOTIFICATIONS_ADD_NOTIFICATION:
      return {
        ...state,
        localNotifications: state.localNotifications.concat([payload]),
      };

    case ACTIONS.LOCAL_NOTIFICATIONS_DELETE_NOTIFICATION:
      return {
        ...state,
        localNotifications: state.localNotifications.filter(
          (notification) => notification.id !== payload.id,
        ),
      };

    case ACTIONS.NAVIGATION_SET_NAVIGATION_SESSION:
      return {
        ...state,
        navigationSession: payload,
      };

    case ACTIONS.NAVIGATION_CLEAR_NAVIGATION_SESSION:
      return {
        ...state,
        navigationSession: null,
      };

    case ACTIONS.RECAPTCHA_SET:
      return {
        ...state,
        recaptcha: { ...state.recaptcha, ...payload },
      };

    case ACTIONS.RECAPTCHA_DELETE:
      return {
        ...state,
        recaptcha: {
          ...state.recaptcha,
          token: null,
          created: null,
        },
      };

    case ACTIONS.RECAPTCHA_REFRESH:
      return {
        ...state,
        recaptcha: {
          ...state.recaptcha,
          refresh: payload.status,
        },
      };

    case ACTIONS.SET_LOADER:
      return {
        ...state,
        loader: {
          ...state.loader,
          isActive: payload,
        },
      };

    default:
      return state;
  }
}
