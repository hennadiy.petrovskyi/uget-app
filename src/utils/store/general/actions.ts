import { NotificationID } from '../../../components/ModalNotifications/ModalNotificationsProvider';
import { makeAction, ACTIONS } from '../actions';

export const setAuthenticationStatus = makeAction<
  ACTIONS.AUTH_HANDLE_AUTHENTICATION_STATUS,
  boolean
>(ACTIONS.AUTH_HANDLE_AUTHENTICATION_STATUS);

export const addLocalNotification = makeAction<
  ACTIONS.LOCAL_NOTIFICATIONS_ADD_NOTIFICATION,
  { id: string; options?: any }
>(ACTIONS.LOCAL_NOTIFICATIONS_ADD_NOTIFICATION);

export const removeLocalNotification = makeAction<
  ACTIONS.LOCAL_NOTIFICATIONS_DELETE_NOTIFICATION,
  { id: string }
>(ACTIONS.LOCAL_NOTIFICATIONS_DELETE_NOTIFICATION);

export const setNavigationSession = makeAction<
  ACTIONS.NAVIGATION_SET_NAVIGATION_SESSION,
  {
    from: {
      stack: string;
      screen: string;
    };
    to: {
      stack: string;
      screen: string;
    };
    options?: {
      withNotification: boolean;
      notificationId: NotificationID;
      notificationOption: any;
    };
  }
>(ACTIONS.NAVIGATION_SET_NAVIGATION_SESSION);

export const setRecaptcha = makeAction<
  ACTIONS.RECAPTCHA_SET,
  { token: string; created: number }
>(ACTIONS.RECAPTCHA_SET);

export const removeRecaptcha = makeAction<ACTIONS.RECAPTCHA_DELETE>(
  ACTIONS.RECAPTCHA_DELETE,
);

export const refreshRecaptcha = makeAction<
  ACTIONS.RECAPTCHA_REFRESH,
  { status: boolean }
>(ACTIONS.RECAPTCHA_REFRESH);

export const clearNavigationSession =
  makeAction<ACTIONS.NAVIGATION_CLEAR_NAVIGATION_SESSION>(
    ACTIONS.NAVIGATION_CLEAR_NAVIGATION_SESSION,
  );

export const setLoaderStatus = makeAction<ACTIONS.SET_LOADER, boolean>(
  ACTIONS.SET_LOADER,
);
