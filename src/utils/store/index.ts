import {
  applyMiddleware,
  createStore,
  combineReducers,
  compose,
  Store,
} from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { generalReducer } from './general/reducers';
import { orderReducer } from './orders/reducers';
import { profileReducer } from './profile/reducers';
import { tripsReducer } from './trips/reducers';
import { ChatReducer } from './chat/reducers';

const middlewares = [thunk];

const composeEnhancer =
  (window.__REDUX_DEVTOOLS_EXTENSION__ &&
    window.__REDUX_DEVTOOLS_EXTENSION__()) ||
  compose;

const enhancers = __DEV__
  ? composeWithDevTools(applyMiddleware(...middlewares))
  : composeEnhancer(applyMiddleware(...middlewares));

export const rootReducer = combineReducers({
  general: generalReducer,
  profile: profileReducer,
  orders: orderReducer,
  trips: tripsReducer,
  chat: ChatReducer,
});

export type IAppState = ReturnType<typeof rootReducer>;

const store: Store<IAppState> = createStore(rootReducer, enhancers);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export { store };
