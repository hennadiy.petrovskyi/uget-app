import AsyncStorage from '@react-native-async-storage/async-storage';
import { logger } from '../logger';
import { Keys } from './keys';
export { Keys } from './keys';

class DeviceAsyncStorage {
  APP_STORAGE_KEY = 'UGET/';
  SERVICE_PATH = 'utils/asyncStorage';

  async save(key: Keys, value: any) {
    const functionName = this.save.name;

    try {
      // const jsonValue = JSON.stringify(value);
      // await AsyncStorage.setItem(this.APP_STORAGE_KEY + key, jsonValue);

      const store = JSON.stringify(value).match(/.{1,1000000}/g);
      store.forEach((part, index) => {
        AsyncStorage.setItem(key + index, part);
      });

      AsyncStorage.setItem(this.APP_STORAGE_KEY + key, '' + store.length);
    } catch (e) {
      logger.logError(this.SERVICE_PATH, functionName, e, { key, value });
    }
  }

  async get(key: Keys) {
    const functionName = this.get.name;

    try {
      // const jsonValue = await AsyncStorage.getItem(this.APP_STORAGE_KEY + key);
      // return jsonValue != null ? JSON.parse(jsonValue) : null;

      let store = '';
      let numberOfParts = await AsyncStorage.getItem(
        this.APP_STORAGE_KEY + key,
      );
      if (typeof numberOfParts === 'undefined' || numberOfParts === null) {
        return null;
      } else numberOfParts = parseInt(numberOfParts);
      for (let i = 0; i < numberOfParts; i++) {
        store += await AsyncStorage.getItem(key + i);
      }
      if (store === '') return null;
      return JSON.parse(store);
    } catch (e) {
      logger.logError(this.SERVICE_PATH, functionName, e, { key });
    }
  }

  async delete(key: Keys) {
    const functionName = this.delete.name;

    try {
      await AsyncStorage.removeItem(this.APP_STORAGE_KEY + key);
    } catch (e) {
      logger.logError(this.SERVICE_PATH, functionName, e, { key });
    }

    logger.log(`AsyncStorage -> ${key} : successfully deleted`, functionName);
  }
}

export const asyncStorage = new DeviceAsyncStorage();
