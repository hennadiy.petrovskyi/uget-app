class Logger {
  private serviceName = '[LOGGER] ';

  log(
    text: string,
    logOptions: {
      serviceName?: string;
      functionName?: string;
      params?: any;
    },
  ) {
    let informationToLog;

    informationToLog = this.serviceName;

    if (!logOptions) {
      console.log(informationToLog + text);

      return;
    }

    if (logOptions.serviceName) {
      informationToLog = `[${logOptions.serviceName}] ${text}`;
    }

    if (logOptions.functionName) {
      informationToLog + `${logOptions.functionName} -> ${text}`;
    }

    if (logOptions.params) {
      console.log(informationToLog, logOptions.params);
      return;
    }

    console.log(informationToLog);
  }

  logError(path: string, functionName: string, error: any, params?: any) {
    console.error(
      `${this.serviceName}${path} -> ${functionName}\n
   ${
     params
       ? `params : ${params}\n 
    error : ${error}`
       : `error : ${error}`
   }
      `,
    );
  }
}

export const logger = new Logger();
