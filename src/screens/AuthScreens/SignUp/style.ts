import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrapper: {
    paddingHorizontal: 50,
  },

  inputWrapper: {
    paddingTop: 20,
    marginBottom: 44,
  },

  inputContainer: {
    marginBottom: 12,
  },

  inputPassword: {
    paddingRight: 50,
  },

  alreadyWrap: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 16,
  },

  termsWrap: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginBottom: 61,
  },

  fonSizeSmall: {
    fontSize: 14,
  },

  and: {
    marginHorizontal: 4,
  },
});
