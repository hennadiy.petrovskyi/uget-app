export interface IFormState {
  email: string;
  phoneNumber: any;
  password: string;
  firstName: string;
  lastName: string;
  confirmPassword: string;
  recaptcha?: string;
}

export type TInputs =
  | 'firstName'
  | 'lastName'
  | 'email'
  | 'phoneNumber'
  | 'password'
  | 'confirmPassword';
