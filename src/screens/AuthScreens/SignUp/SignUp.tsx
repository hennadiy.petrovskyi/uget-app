import React, { memo, useEffect, useRef, useState } from 'react';
import {
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Formik } from 'formik';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';

import {
  ScreenTitle,
  UgetGreyText,
  UgetLinkText,
  WithDescription,
  UgetButton,
} from '../../../components';
import { PhoneNumber, TextInput } from '../../../components/UgetInputs';

import { NAVIGATION } from '../../../config/constants';
import { register, verifyRecaptcha } from '../../../api/auth';
import { IFormState, TInputs } from './types';

import styles from './style';
import {
  addLocalNotification,
  clearNavigationSession,
  setLoaderStatus,
} from '../../../utils/store/general/actions';

export default memo(() => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const navigationSession = useSelector(
    (state) => state.general.navigationSession,
  );

  const initFormValues: IFormState = {
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: {
      code: '',
      countryCode: '',
      number: '',
      formatted: '',
      isValid: false,
    },
    password: '',
    confirmPassword: '',
  };

  const inputs: TInputs[] = Object.keys(initFormValues) as TInputs[];

  const signUpSchema = Yup.object().shape({
    firstName: Yup.string()
      .min(3, t('common:inputs.firstName.errors.min'))
      .max(50, t('common:inputs.firstName.errors.max'))
      .matches(/[a-zA-Z]/, t('common:inputs.firstName.errors.onlyLatin'))
      .required(t('common:inputs.firstName.errors.required')),

    lastName: Yup.string()
      .min(3, t('common:inputs.lastName.errors.min'))
      .max(50, t('common:inputs.lastName.errors.max'))
      .matches(/[a-zA-Z]/, t('common:inputs.lastName.errors.onlyLatin'))
      .required(t('common:inputs.lastName.errors.required')),

    email: Yup.string()
      .email(t('common:inputs.email.errors.format'))
      .required(t('common:inputs.email.errors.required')),

    phoneNumber: Yup.object()
      .shape({
        code: Yup.string(),
        countryCode: Yup.string(),
        number: Yup.string().required('Phone number is required'),
        formatted: Yup.string(),
        isValid: Yup.boolean().oneOf([true], 'Invalid number format'),
      })
      .required(),

    password: Yup.string()
      .min(6, t('common:inputs.password.errors.min'))
      .max(18, t('common:inputs.password.errors.max'))
      .required(t('common:inputs.password.errors.required')),

    confirmPassword: Yup.string()
      .oneOf([Yup.ref('password')], t('common:inputs.password.errors.match'))
      .required(t('common:inputs.password.errors.required')),
  });

  async function onSubmit(formData: IFormState, { setErrors }) {
    const { confirmPassword, ...registerDTO } = formData;

    const handleInputsErrors = (errors: any) => {
      type TField = 'email' | 'phoneNumber';

      if (errors.length) {
        const errorsToShow: {
          email?: string;
          phoneNumber?: string;
        } = {};

        dispatch(setLoaderStatus(false));

        errors.forEach(
          ({ field, type }: { field: TField; type: string }) =>
            (errorsToShow[field] = t(`common:inputs.${field}.errors.${type}`)),
        );

        setErrors(errorsToShow);
      }
    };

    dispatch(setLoaderStatus(true));

    dispatch(
      register({ ...registerDTO }, handleNavigateOnSuccess, handleInputsErrors),
    );
  }

  function handleNavigateOnSuccess() {
    if (!navigationSession) {
      navigation.navigate(
        NAVIGATION.AUTHENTICATION.VERIFY_PHONE_NUMBER as never,
      );

      dispatch(setLoaderStatus(false));
      return;
    }

    if (
      navigationSession.options &&
      navigationSession.options.withNotification
    ) {
      dispatch(
        addLocalNotification({
          id: navigationSession.options.notificationId,
          options: navigationSession.options.notificationOption,
        }),
      );
    }

    setTimeout(() => {
      navigation.navigate(navigationSession.to.stack, {
        screen: navigationSession.to.screen,
      });

      dispatch(clearNavigationSession());

      dispatch(setLoaderStatus(false));
    }, 200);
  }

  function handleNavigateLoginScreen() {
    navigation.navigate(NAVIGATION.AUTHENTICATION.LOG_IN_BY_PHONE as never);
  }

  return (
    <Formik
      initialValues={initFormValues}
      validationSchema={signUpSchema}
      validateOnChange={false}
      validateOnBlur={true}
      onSubmit={onSubmit}
    >
      {(props) => {
        const { values, handleSubmit, setFieldError, setFieldValue, errors } =
          props;

        const renderInputs = () =>
          inputs.map((inputId) => {
            const inputTitle = t(`common:inputs.${inputId}.label`);
            const placeholder = t(`common:inputs.${inputId}.placeholder`);

            let inputComponent;

            if (inputId === 'phoneNumber') {
              inputComponent = (
                <PhoneNumber
                  placeholder={placeholder}
                  error={errors.phoneNumber}
                  onChangeText={(value) => {
                    setFieldValue(inputId, value);

                    if (errors.phoneNumber) {
                      setFieldError('phoneNumber', '');
                    }
                  }}
                  value={values.phoneNumber.value}
                />
              );
            } else {
              inputComponent = (
                <TextInput
                  type={
                    inputId === 'password' || inputId === 'confirmPassword'
                      ? 'password'
                      : 'text'
                  }
                  placeholder={placeholder}
                  value={values[inputId]}
                  error={errors[inputId]}
                  onChangeText={(value: string) => {
                    setFieldValue(
                      inputId,
                      inputId === 'email' ? value.toLowerCase() : value,
                    );
                    setFieldError(inputId, '');
                  }}
                />
              );
            }

            return (
              <WithDescription
                key={inputId}
                title={inputTitle}
                marginBottom={20}
              >
                {inputComponent}
              </WithDescription>
            );
          });

        return (
          <SafeAreaView style={styles.container}>
            <KeyboardAvoidingView
              behavior="padding"
              enabled={Platform.OS === 'ios'}
              style={styles.container}
            >
              <ScrollView
                keyboardShouldPersistTaps={
                  Platform.OS === 'ios' ? 'handled' : 'never'
                }
                style={styles.wrapper}
              >
                <ScreenTitle text={t('auth:signUp.title')} marginBottom={16} />
                <UgetGreyText text={t('auth:signUp.subTitle')} />

                <View style={styles.inputWrapper}>{renderInputs()}</View>

                <UgetButton
                  type="primary"
                  label={t('auth:signUp.btnSubmit')}
                  onPress={handleSubmit}
                  marginBottom={40}
                />

                <View style={styles.alreadyWrap}>
                  <UgetGreyText text={t('auth:signUp.haveAccount')} />
                  <UgetLinkText
                    underline={true}
                    text={t('auth:signUp.linkLogin')}
                    outerStyles={styles.fonSizeSmall}
                    onPress={handleNavigateLoginScreen}
                  />
                </View>

                <UgetGreyText text={t('auth:signUp.agreement')} />

                <View style={styles.termsWrap}>
                  <UgetLinkText
                    text={t('auth:signUp.terms')}
                    outerStyles={styles.fonSizeSmall}
                  />
                  <UgetGreyText
                    text={t('auth:signUp.and')}
                    outerStyles={styles.and}
                  />
                  <UgetLinkText
                    text={t('auth:signUp.policy')}
                    outerStyles={styles.fonSizeSmall}
                  />
                </View>
              </ScrollView>
            </KeyboardAvoidingView>
          </SafeAreaView>
        );
      }}
    </Formik>
  );
});
