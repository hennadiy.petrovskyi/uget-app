import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  wrapper: {
    paddingHorizontal: 50,
  },

  inputContainer: {
    marginBottom: 44,
  },

  alignSelfCenter: {
    alignSelf: 'center',
  },
});
