import React, { memo, useEffect, useState } from 'react';
import {
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as Yup from 'yup';
import { useTranslation } from 'react-i18next';
import { Formik } from 'formik';

import {
  ScreenTitle,
  UgetButton,
  UgetGreyText,
  UgetLinkText,
  TextInput,
  WithDescription,
} from '../../../components';

import { NAVIGATION } from '../../../config/constants';
import { changeEmail } from '../../../api';

import styles from './style';
import { useDispatch, useSelector } from 'react-redux';

export default memo(() => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const profile = useSelector((state) => state.profile);

  useEffect(() => {
    if (profile && profile.email) {
      setEmail(profile.email.value);
    }
  }, [profile]);

  const ChangeEmailSchema = Yup.object().shape({
    email: Yup.string()
      .email(t('common:inputs.email.errors.format'))
      .required(t('common:inputs.email.errors.required')),
  });

  async function onSubmit(formData: { email: string }, { setFieldError }) {
    const handleNavigate = (newEmail: string) => {
      navigation.navigate(
        NAVIGATION.AUTHENTICATION.VERIFY_EMAIL as never,
        {
          newEmail,
        } as never,
      );
    };

    const handleInputsErrors = (inputName: string, type: any) => {
      setFieldError(inputName, t(`common:inputs.email.errors.${type}`));
    };

    dispatch(
      changeEmail({
        email: formData.email,
        handleNavigate,
        setFieldError: handleInputsErrors,
      }),
    );
  }

  return (
    <Formik
      initialValues={{
        email: '',
      }}
      validationSchema={ChangeEmailSchema}
      validateOnChange={false}
      onSubmit={onSubmit}
    >
      {(props) => {
        const { values, handleSubmit, setFieldError, setFieldValue, errors } =
          props;

        function handleValue(value: string) {
          setFieldValue('email', value.toLowerCase());
          setFieldError('email', '');
        }

        return (
          <SafeAreaView style={styles.container}>
            <KeyboardAvoidingView
              behavior="padding"
              enabled={Platform.OS === 'ios'}
              style={styles.container}
            >
              <View style={styles.wrapper}>
                <ScreenTitle
                  text={t('auth:changeEmail.title')}
                  marginBottom={16}
                />

                <UgetGreyText
                  text={t('auth:changeEmail.subTitle')}
                  marginBottom={8}
                />

                <UgetLinkText
                  disabled={true}
                  text={email}
                  outerStyles={styles.alignSelfCenter}
                  marginBottom={32}
                />

                <WithDescription
                  title={t('common:inputs.email.label')}
                  marginBottom={44}
                >
                  <TextInput
                    type="text"
                    value={values.email}
                    error={errors.email}
                    placeholder={t('common:inputs.email.placeholder')}
                    onChangeText={handleValue}
                  />
                </WithDescription>

                <UgetButton
                  type="primary"
                  label={t('auth:changeEmail.btnSubmit')}
                  onPress={handleSubmit}
                />
              </View>
            </KeyboardAvoidingView>
          </SafeAreaView>
        );
      }}
    </Formik>
  );
});
