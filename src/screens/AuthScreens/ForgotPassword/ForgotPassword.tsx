import React, { memo } from 'react';
import {
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { Formik } from 'formik';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';

import {
  ScreenTitle,
  UgetButton,
  UgetGreyText,
  TextInput,
  WithDescription,
} from '../../../components';

import { resetPassword } from '../../../api/auth';

import styles from './style';
import { NAVIGATION } from '../../../config/constants';
import { addLocalNotification } from '../../../utils/store/general/actions';
import { NotificationID } from '../../../components/ModalNotifications/ModalNotificationsProvider';

export default memo(() => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const signUpSchema = Yup.object().shape({
    email: Yup.string()
      .email(t('common:inputs.email.errors.format'))
      .required(t('common:inputs.email.errors.required')),
  });

  async function onSubmit(formData: { email: string }, { setFieldError }) {
    const handleNavigate = () => {
      dispatch(
        addLocalNotification({
          id: NotificationID.RESET_PASSWORD_REQUEST_SUCCESS,
        }),
      );

      navigation.navigate(NAVIGATION.TABS_ROUTE.HOME_SCREEN);
    };

    const handleInputsErrors = (type: any) => {
      setFieldError('email', t(`common:inputs.email.errors.${type}`));
    };

    dispatch(
      resetPassword({
        email: formData.email,
        handleNavigate,
        setFieldError: handleInputsErrors,
      }),
    );
  }

  return (
    <Formik
      initialValues={{
        email: '',
      }}
      validationSchema={signUpSchema}
      validateOnChange={false}
      onSubmit={onSubmit}
    >
      {(props) => {
        const { values, handleSubmit, setFieldError, setFieldValue, errors } =
          props;

        function handleInputValue(value: string) {
          setFieldValue('email', value.toLowerCase());
          setFieldError('email', '');
        }

        return (
          <KeyboardAvoidingView
            behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
            style={styles.container}
          >
            <SafeAreaView>
              <View style={styles.wrapper}>
                <ScreenTitle
                  text={t('auth:forgotPassword.title')}
                  marginBottom={16}
                />

                <UgetGreyText
                  text={t('auth:forgotPassword.subTitle')}
                  outerStyles={styles.subTitle}
                  marginBottom={30}
                />

                <WithDescription
                  title={t('common:inputs.email.label')}
                  marginBottom={44}
                >
                  <TextInput
                    type="email"
                    value={values.email}
                    error={errors.email}
                    placeholder={t('common:inputs.email.placeholder')}
                    onChangeText={handleInputValue}
                  />
                </WithDescription>

                <UgetButton
                  type="primary"
                  label={t('auth:forgotPassword.btnSubmit')}
                  onPress={handleSubmit}
                />
              </View>
            </SafeAreaView>
          </KeyboardAvoidingView>
        );
      }}
    </Formik>
  );
});
