import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  wrapper: {
    paddingHorizontal: 50,
    paddingTop: 19,
  },

  subTitle: {
    maxWidth: 268,
    alignSelf: 'center',
  },
});
