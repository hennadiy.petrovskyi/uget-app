import React, { memo, useEffect, useState } from 'react';
import {
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import {
  PhoneNumber,
  ScreenTitle,
  UgetButton,
  UgetGreyText,
  WithDescription,
} from '../../../components';

import { NAVIGATION } from '../../../config/constants';
import { changePhoneNumber } from '../../../api';
import styles from './style';

export default memo(() => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [phoneNumber, setPhoneNumber] = useState('');
  const profile = useSelector((state) => state.profile);

  useEffect(() => {
    if (profile && profile.phoneNumber) {
      setPhoneNumber(profile.phoneNumber.formatted);
    }
  }, [profile]);

  const ChangePhoneNumberSchema = Yup.object().shape({
    phoneNumber: Yup.object()
      .shape({
        code: Yup.string(),
        countryCode: Yup.string(),
        number: Yup.string().required('Phone number is required'),
        formatted: Yup.string(),
        isValid: Yup.boolean().oneOf([true], 'Invalid number format'),
      })
      .required(),
  });

  async function onSubmit(
    formData: { phoneNumber: string },
    { setFieldError },
  ) {
    const handleNavigate = (newPhoneNumber: string) => {
      navigation.navigate(
        NAVIGATION.AUTHENTICATION.VERIFY_PHONE_NUMBER as never,
        {
          newPhoneNumber,
        } as never,
      );
    };

    const handleInputsErrors = (inputName: string, type: any) => {
      setFieldError(inputName, t(`common:inputs.phone.errors.${type}`));
    };

    dispatch(
      changePhoneNumber({
        phoneNumber: formData.phoneNumber,
        handleNavigate,
        setFieldError: handleInputsErrors,
      }),
    );
  }

  return (
    <Formik
      initialValues={{
        phoneNumber: '',
      }}
      validationSchema={ChangePhoneNumberSchema}
      validateOnChange={false}
      onSubmit={onSubmit}
    >
      {(props) => {
        const { values, handleSubmit, setFieldError, setFieldValue, errors } =
          props;

        function handleValue(value: string) {
          setFieldValue('phoneNumber', value);

          setFieldError('phoneNumber', '');
        }

        return (
          <SafeAreaView style={styles.container}>
            <KeyboardAvoidingView
              behavior="padding"
              enabled={Platform.OS === 'ios'}
              style={styles.container}
            >
              <View style={styles.wrapper}>
                <ScreenTitle
                  text={t('auth:changePhoneNumber.title')}
                  marginBottom={16}
                />

                <UgetGreyText
                  text={t('auth:changePhoneNumber.subTitle')}
                  marginBottom={8}
                />

                <UgetGreyText
                  text={'Current is ' + phoneNumber}
                  outerStyles={styles.alignSelfCenter}
                  marginBottom={32}
                />

                <WithDescription
                  title={t('common:inputs.sms.label')}
                  marginBottom={44}
                >
                  <PhoneNumber
                    placeholder={t(`common:inputs.phoneNumber.placeholder`)}
                    onChangeText={handleValue}
                    error={errors.phoneNumber}
                  />
                </WithDescription>

                <UgetButton
                  type="primary"
                  label={t('auth:changePhoneNumber.btnSubmit')}
                  onPress={handleSubmit}
                />
              </View>
            </KeyboardAvoidingView>
          </SafeAreaView>
        );
      }}
    </Formik>
  );
});
