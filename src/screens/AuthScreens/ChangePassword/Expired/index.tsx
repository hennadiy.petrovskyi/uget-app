import React from 'react';
import { View } from 'react-native';
import { useLinkTo, useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { TitledContainer } from '../../../../components/TitledContainer';

import { UgetButton, UgetGreyText } from '../../../../components';
import styles from './styles';
import { NAVIGATION } from '../../../../config/constants';

export const LinkExpired = () => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  function redirectToForgotPassword() {
    navigation.reset({
      index: 0,
      routes: [{ name: NAVIGATION.AUTHENTICATION.FORGOT_PASSWORD }],
    });
  }

  return (
    <TitledContainer title={t('auth:resetPassword.expired.title')}>
      <UgetGreyText
        text={t('auth:resetPassword.expired.description')}
        marginBottom={40}
      />

      <UgetButton
        type="primary"
        label={t('auth:resetPassword.expired.btnSubmit')}
        onPress={redirectToForgotPassword}
      />
    </TitledContainer>
  );
};
