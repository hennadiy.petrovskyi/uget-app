import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    paddingTop: 100,
    justifyContent: 'space-between',
  },
  article: {
    fontWeight: '400',
    maxWidth: 320,
    letterSpacing: -0.24,
    marginBottom: 40,
    alignSelf: 'center',
  },
  icon: {
    width: 180,
    height: 180,
    alignSelf: 'center',
    marginBottom: 34,
  },
});
