import React, { memo } from 'react';
import { View } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { LinkExpired } from './Expired';
import {
  TitledContainer,
  UgetButton,
  TextInput,
  WithDescription,
  UgetLinkText,
} from '../../../components';

import { extractJwtPayload } from '../../../api/api';
import { changePassword } from '../../../api';
import { COLORS, NAVIGATION } from '../../../config/constants';

import { addLocalNotification } from '../../../utils/store/general/actions';
import { NotificationID } from '../../../components/ModalNotifications/ModalNotificationsProvider';
import styles from './styles';

export const ChangePassword = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const route = useRoute();

  const { params } = route;

  const isTokenExpired = params && params.id && extractJwtPayload(params.id);

  const changePasswordSchema = Yup.object().shape({
    password: Yup.string()
      .min(6, t('common:inputs.password.errors.min'))
      .max(18, t('common:inputs.password.errors.max'))
      .required(t('common:inputs.password.errors.required')),

    confirmPassword: Yup.string()
      .oneOf([Yup.ref('password')], t('common:inputs.password.errors.match'))
      .required(t('common:inputs.password.errors.required')),
  });

  React.useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <UgetLinkText
          text="Close"
          onPress={() => navigation.navigate(NAVIGATION.TABS_ROUTE.HOME_SCREEN)}
          outerStyles={{ color: COLORS.BLACK }}
        />
      ),
    });
  });

  function onSubmit({ password }: { password: string }, { setFieldError }) {
    const onSuccess = () => {
      dispatch(
        addLocalNotification({
          id: NotificationID.PASSWORD_UPDATE_SUCCESS,
        }),
      );

      navigation.navigate(NAVIGATION.TABS_ROUTE.HOME_SCREEN);
    };

    changePassword({ token: params.id, password }, onSuccess);
  }

  const inputs = ['password', 'confirmPassword'];

  return isTokenExpired ? (
    <TitledContainer title={t('auth:resetPassword.title')}>
      <Formik
        initialValues={{
          password: '',
          confirmPassword: '',
        }}
        validationSchema={changePasswordSchema}
        validateOnChange={false}
        validateOnBlur={true}
        onSubmit={onSubmit}
      >
        {(props) => {
          const { values, handleSubmit, setFieldError, setFieldValue, errors } =
            props;

          const renderInputs = () =>
            inputs.map((inputId) => {
              const inputTitle = t(`common:inputs.${inputId}.label`);
              const placeholder = t(`common:inputs.${inputId}.placeholder`);

              return (
                <WithDescription
                  key={inputId}
                  title={inputTitle}
                  marginBottom={20}
                >
                  <TextInput
                    type={'password'}
                    placeholder={placeholder}
                    value={values[inputId]}
                    error={errors[inputId]}
                    onChangeText={(value: string) => {
                      setFieldValue(inputId, value);
                      setFieldError(inputId, '');
                    }}
                  />
                </WithDescription>
              );
            });

          return (
            <View>
              {renderInputs()}

              <UgetButton
                type="primary"
                label={t('auth:resetPassword.btnSubmit')}
                onPress={handleSubmit}
                marginBottom={40}
              />
            </View>
          );
        }}
      </Formik>
    </TitledContainer>
  ) : (
    <LinkExpired />
  );
};

export default memo(ChangePassword);
