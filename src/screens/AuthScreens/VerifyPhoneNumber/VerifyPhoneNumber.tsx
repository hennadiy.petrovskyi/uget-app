import React, { memo, useEffect, useState } from 'react';
import {
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Formik } from 'formik';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';

import {
  LinkWithCountdown,
  ScreenTitle,
  UgetGreyText,
  UgetLinkText,
  ButtonSkipForNow,
  WithDescription,
  TextInput,
  UgetButton,
} from '../../../components';

import { NAVIGATION } from '../../../config/constants';
import { verifySmsCode } from '../../../api';
import styles from './style';
import { useDispatch, useSelector } from 'react-redux';
import { clearNavigationSession } from '../../../utils/store/general/actions';

export default memo(() => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const route = useRoute();
  const navigation = useNavigation();
  const [isTimeoutActive, activateTimeout] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState('');
  const profile = useSelector((state) => state.profile);
  const navigationSession = useSelector(
    (state) => state.general.navigationSession,
  );

  const verificationCodeSchema = Yup.object().shape({
    code: Yup.string()
      .min(6, t('common:inputs.sms.errors.length'))
      .max(6, t('common:inputs.sms.errors.length'))
      .required(t('common:inputs.sms.errors.required')),
  });

  const { params } = route as any;

  console.log(params);

  useEffect(() => {
    if (profile && profile.phoneNumber) {
      setPhoneNumber(profile.phoneNumber.formatted);
    }
  }, [profile]);

  useEffect(() => {
    if (params && params.newPhoneNumber) {
      setPhoneNumber(params.newPhoneNumber);
    }
  }, [params]);

  React.useLayoutEffect(() => {
    function handleSkipVerification() {
      navigation.navigate(navigationSession.to.stack, {
        screen: navigationSession.to.screen,
      });

      dispatch(clearNavigationSession());
    }

    navigation.setOptions({
      headerRight: () => (
        <ButtonSkipForNow handlePress={handleSkipVerification} />
      ),
    });
  }, []);

  async function onSubmit(formData: { code: string }, { setFieldError }) {
    const handleNavigate = () => {
      navigation.navigate(NAVIGATION.AUTHENTICATION.VERIFY_EMAIL as never);
    };

    const handleInputsErrors = (type: any) => {
      setFieldError('sms', t(`common:inputs.sms.errors.${type}`));
    };

    dispatch(
      verifySmsCode(
        {
          phoneNumber,
          code: formData.code,
        },
        !params?.isReconfirmation
          ? handleNavigate
          : () => {
              navigation.navigate(navigationSession.to.stack, {
                screen: navigationSession.to.screen,
              });
            },
        handleInputsErrors,
      ),
    );
  }

  function handleChangePhoneNumber() {
    navigation.navigate(NAVIGATION.AUTHENTICATION.CHANGE_PHONE_NUMBER as never);
  }

  return (
    <Formik
      initialValues={{
        phoneNumber: '',
        code: '',
      }}
      validationSchema={verificationCodeSchema}
      validateOnChange={false}
      onSubmit={onSubmit}
    >
      {(props) => {
        const { values, handleSubmit, setFieldError, setFieldValue, errors } =
          props;

        function handleInputValue(value: string) {
          setFieldValue('code', value);
          setFieldError('code', '');
        }

        return (
          <SafeAreaView style={styles.container}>
            <KeyboardAvoidingView
              behavior="padding"
              enabled={Platform.OS === 'ios'}
              style={styles.container}
            >
              <View style={styles.wrapper}>
                <ScreenTitle
                  text={t('auth:verifyPhoneNumber.title')}
                  marginBottom={16}
                />

                <UgetGreyText
                  text={t('auth:verifyPhoneNumber.subTitle')}
                  marginBottom={8}
                />

                <UgetGreyText
                  text={phoneNumber}
                  outerStyles={styles.alignSelfCenter}
                  marginBottom={30}
                />

                <WithDescription
                  title={t('common:inputs.sms.label')}
                  marginBottom={44}
                >
                  <TextInput
                    type="code"
                    value={values.code}
                    error={errors.code}
                    placeholder={t('common:inputs.sms.placeholder')}
                    onChangeText={handleInputValue}
                  />
                </WithDescription>

                <UgetButton
                  type="primary"
                  label={t('auth:verifyPhoneNumber.btnSubmit')}
                  onPress={handleSubmit}
                  marginBottom={40}
                />

                <UgetGreyText
                  text={t('auth:verifyPhoneNumber.wrongNumber')}
                  marginBottom={8}
                />

                <UgetLinkText
                  text={t('auth:verifyPhoneNumber.linkChangeNumber')}
                  onPress={handleChangePhoneNumber}
                  outerStyles={styles.alignSelfCenter}
                  underline={true}
                />
              </View>

              <LinkWithCountdown
                label={t('auth:verifyPhoneNumber.resend')}
                isTimeoutActive={isTimeoutActive}
                activateTimeout={activateTimeout}
                marginBottom={40}
                params={{ phoneNumber }}
                type="sms-verification"
              />
            </KeyboardAvoidingView>
          </SafeAreaView>
        );
      }}
    </Formik>
  );
});
