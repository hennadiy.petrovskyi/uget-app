import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  wrapper: {
    paddingHorizontal: 50,
    paddingTop: 37,
  },

  phoneNumber: {
    fontFamily: 'FiraSans-Regular',
    color: COLORS.MAIN_COLOR,
  },

  input: { textAlign: 'center', letterSpacing: 10 },

  inputContainer: {
    marginBottom: 44,
  },

  alignSelfCenter: {
    alignSelf: 'center',
  },
});
