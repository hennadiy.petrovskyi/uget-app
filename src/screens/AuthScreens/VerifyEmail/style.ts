import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  wrapper: {
    paddingHorizontal: 50,
    paddingTop: 16,
  },

  alignSelfCenter: {
    alignSelf: 'center',
    fontWeight: 'bold',
  },
});
