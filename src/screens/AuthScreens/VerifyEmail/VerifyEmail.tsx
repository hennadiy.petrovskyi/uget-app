import React, { memo, useEffect, useState } from 'react';
import {
  View,
  SafeAreaView,
  Platform,
  NativeModules,
  Linking,
} from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import {
  ButtonSkipForNow,
  LinkWithCountdown,
  ScreenTitle,
  UgetButton,
  UgetGreyText,
  UgetLinkText,
} from '../../../components';

import {
  addLocalNotification,
  clearNavigationSession,
} from '../../../utils/store/general/actions';

import { NAVIGATION } from '../../../config/constants';

import styles from './style';
import { extractJwtPayload } from '../../../api/api';
import { LinkExpired } from './Expired';
import { NotificationID } from '../../../components/ModalNotifications/ModalNotificationsProvider';
import { verifyEmail } from '../../../api';

export default memo(() => {
  const { t } = useTranslation();
  const route = useRoute();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const navigationSession = useSelector(
    (state) => state.general.navigationSession,
  );
  const [email, setEmail] = useState('');
  const [isTimeoutActive, activateTimeout] = useState(false);
  const profile = useSelector((state) => state.profile);
  const { params } = route;

  const isVerificationTokenExist = params?.id;
  const isTokenValid =
    isVerificationTokenExist && extractJwtPayload(params?.id);

  useEffect(() => {
    if (profile && profile.email) {
      setEmail(profile.email.value);
    }
  }, [profile]);

  useEffect(() => {
    if (params && params.newEmail) {
      setEmail(params.newEmail);
    }
  }, [params]);

  useEffect(() => {
    console.log(isVerificationTokenExist);

    if (isTokenValid) {
      const onSuccess = () => {
        dispatch(
          addLocalNotification({
            id: NotificationID.EMAIL_VERIFIED,
          }),
        );

        navigation.navigate(NAVIGATION.UGET_ROUTE.TABS_ROUTE, {
          screen: NAVIGATION.PROFILE.PROFILE,
        });
      };

      dispatch(verifyEmail(params?.id, onSuccess));

      return;
    }

    return;
  }, [isVerificationTokenExist]);

  React.useLayoutEffect(() => {
    function handleSkipVerification() {
      if (navigationSession) {
        navigation.navigate(navigationSession.to.stack, {
          screen: navigationSession.to.screen,
        });

        dispatch(clearNavigationSession());
      }

      navigation.navigate(NAVIGATION.UGET_ROUTE.TABS_ROUTE, {
        screen: NAVIGATION.TABS_ROUTE.HOME_SCREEN,
      });
    }

    navigation.setOptions({
      headerRight: () => (
        <ButtonSkipForNow handlePress={handleSkipVerification} />
      ),
    });
  }, []);

  function handleChangeEmail() {
    navigation.navigate(NAVIGATION.AUTHENTICATION.CHANGE_EMAIL as never);
  }

  function openMailApp() {
    // if (Platform.OS === 'android') {
    //   openMailClientAndroid();
    //   return;
    // }
    // openMailClientIOS();
    // function openMailClientIOS() {
    //   Linking.canOpenURL('message:0')
    //     .then((supported) => {
    //       if (!supported) {
    //         console.log('Cant handle url');
    //       } else {
    //         return Linking.openURL('message:0').catch((err) =>
    //           console.log(err),
    //         );
    //       }
    //     })
    //     .catch((err) => console.log(err));
    // }
    // function openMailClientAndroid() {
    //   const activityAction = 'android.intent.action.MAIN'; // Intent.ACTION_MAIN
    //   const intentParams: IntentLauncher.IntentLauncherParams = {
    //     flags: 268435456, // Intent.FLAG_ACTIVITY_NEW_TASK
    //     category: 'android.intent.category.APP_EMAIL', // Intent.CATEGORY_APP_EMAIL
    //   };
    //   IntentLauncher.startActivityAsync(activityAction, intentParams).catch(
    //     (err) => console.log(err),
    //   );
    // }
    // }
    // Linking.openURL('message:0'); // iOS
    // return;
  }

  return (
    <>
      {!isVerificationTokenExist && (
        <SafeAreaView style={styles.container}>
          <View style={styles.wrapper}>
            <ScreenTitle text={t('auth:verifyEmail.title')} marginBottom={24} />

            <UgetGreyText
              text={t('auth:verifyEmail.subTitle')}
              marginBottom={8}
            />

            <UgetGreyText
              text={email}
              outerStyles={styles.alignSelfCenter}
              marginBottom={8}
            />

            <UgetGreyText
              text={t('auth:verifyEmail.openEmail')}
              marginBottom={40}
            />

            <UgetGreyText
              text={t('auth:verifyEmail.wrongEmail')}
              marginBottom={8}
            />

            <UgetLinkText
              underline={true}
              text={t('auth:verifyEmail.changeEmail')}
              outerStyles={styles.alignSelfCenter}
              onPress={handleChangeEmail}
            />

            {/* <UgetButton type="primary" onPress={openMailApp} /> */}
          </View>

          <LinkWithCountdown
            label={t('auth:verifyEmail.resend')}
            isTimeoutActive={isTimeoutActive}
            activateTimeout={activateTimeout}
            marginBottom={40}
            params={{ email }}
            type="email-verification"
          />
        </SafeAreaView>
      )}
      {isVerificationTokenExist && !isTokenValid && <LinkExpired />}
    </>
  );
});
