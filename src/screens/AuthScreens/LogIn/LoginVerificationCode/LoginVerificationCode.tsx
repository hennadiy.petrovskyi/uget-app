import React, { memo, useEffect, useState } from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  View,
} from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';

import {
  LinkWithCountdown,
  ScreenTitle,
  UgetButton,
  WithDescription,
} from '../../../../components';

import {
  addLocalNotification,
  clearNavigationSession,
  setLoaderStatus,
} from '../../../../utils/store/general/actions';

import styles from '../style';
import { RedirectToSignUp } from '../RedirectToSignUp';
import { LoginTypeSelector } from '../LoginTypeSelector';
import { TextInput } from '../../../../components/UgetInputs';
import { verifySmsCode } from '../../../../api';

export const LoginVerificationCode = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const route = useRoute();
  const [isTimeoutActive, activateTimeout] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState('');
  const navigationSession = useSelector(
    (state) => state.general.navigationSession,
  );

  const { params } = route as any;

  useEffect(() => {
    if (params && params.phoneNumber) {
      setPhoneNumber(params.phoneNumber);
    }
  }, [params]);

  const signUpSchema = Yup.object().shape({
    code: Yup.string()
      .min(6, t('common:inputs.sms.errors.length'))
      .max(6, t('common:inputs.sms.errors.length'))
      .required(t('common:inputs.sms.errors.required')),
  });

  async function onSubmit(formData: { code: string }, { setFieldError }) {
    dispatch(setLoaderStatus(true));

    dispatch(
      verifySmsCode(
        {
          phoneNumber,
          code: formData.code,
        },
        handleNavigate,
        handleInputError,
      ),
    );

    function handleInputError(type: any) {
      dispatch(setLoaderStatus(false));

      setFieldError('code', t(`common:inputs.sms.errors.${type}`));
    }
  }

  function handleNavigate() {
    if (
      navigationSession.options &&
      navigationSession.options.withNotification
    ) {
      dispatch(
        addLocalNotification({
          id: navigationSession.options.notificationId,
          options: navigationSession.options.notificationOption,
        }),
      );
    }

    setTimeout(() => {
      navigation.navigate(navigationSession.to.stack, {
        screen: navigationSession.to.screen,
      });

      dispatch(clearNavigationSession());

      dispatch(setLoaderStatus(false));
    }, 200);
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
      style={styles.container}
    >
      <SafeAreaView style={styles.containerInner}>
        <Formik
          initialValues={{
            code: '',
          }}
          validationSchema={signUpSchema}
          validateOnChange={false}
          onSubmit={onSubmit}
        >
          {(props) => {
            const {
              values,
              handleSubmit,
              setFieldValue,
              setFieldError,
              errors,
            } = props;

            function handleInputValue(value: string) {
              setFieldValue('code', value);
              setFieldError('code', '');
            }

            return (
              <View style={styles.wrapper}>
                <ScreenTitle
                  text={t('auth:login.title.phoneNumber')}
                  marginBottom={40}
                  outerStyles={styles.titlePhoneNumber}
                />

                <WithDescription
                  title={t('common:inputs.sms.label')}
                  marginBottom={68}
                >
                  <TextInput
                    type="code"
                    value={values.code}
                    error={errors.code}
                    placeholder={t('common:inputs.sms.placeholder')}
                    onChangeText={handleInputValue}
                  />
                </WithDescription>

                <UgetButton
                  type="primary"
                  label={t('auth:login.btnSubmit')}
                  disabled={values.code.length !== 6}
                  onPress={handleSubmit}
                  marginBottom={24}
                />
                <LoginTypeSelector type="phone" />
              </View>
            );
          }}
        </Formik>

        <View>
          <LinkWithCountdown
            label={t('auth:login.resend')}
            isTimeoutActive={isTimeoutActive}
            activateTimeout={activateTimeout}
            type="sms-verification"
            params={{ phoneNumber }}
            marginBottom={40}
          />
          <RedirectToSignUp />
        </View>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

export default memo(LoginVerificationCode);
