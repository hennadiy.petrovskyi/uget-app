import React, { memo, useState } from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  View,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';

import {
  UgetButton,
  WithDescription,
  PhoneNumber,
  ScreenTitle,
  ButtonGoBack,
} from '../../../../components';

import { verifyPhoneNumber } from '../../../../api';
import styles from '../style';
import { RedirectToSignUp } from '../RedirectToSignUp';
import { NAVIGATION } from '../../../../config/constants';
import { useNavigation } from '@react-navigation/core';
import { LoginTypeSelector } from '../LoginTypeSelector';
import { setLoaderStatus } from '../../../../utils/store/general/actions';

const LoginByPhone = ({}) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const signUpSchema = Yup.object().shape({
    phoneNumber: Yup.object()
      .shape({
        code: Yup.string(),
        countryCode: Yup.string(),
        number: Yup.string().required('Phone number is required'),
        formatted: Yup.string(),
        isValid: Yup.boolean().oneOf([true], 'Invalid number format'),
      })
      .required(),
  });

  async function onSubmit(
    formData: { phoneNumber: string },
    { setFieldError },
  ) {
    dispatch(setLoaderStatus(true));

    dispatch(
      verifyPhoneNumber(
        formData.phoneNumber.formatted,
        handleNavigateNext,
        handleInputError,
      ),
    );

    function handleNavigateNext() {
      navigation.navigate(NAVIGATION.AUTHENTICATION.LOG_IN_VERIFICATION_CODE, {
        phoneNumber: formData.phoneNumber.formatted,
      });

      dispatch(setLoaderStatus(false));
    }

    function handleInputError(type: any) {
      dispatch(setLoaderStatus(false));

      setFieldError(
        'phoneNumber',
        t(`common:inputs.phoneNumber.errors.${type}`),
      );
    }
  }

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <ButtonGoBack
          handleGoBack={() => {
            navigation.navigate(NAVIGATION.TABS_ROUTE.HOME_SCREEN);
          }}
        />
      ),
    });
  }, []);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
      style={styles.container}
    >
      <SafeAreaView style={styles.containerInner}>
        <Formik
          initialValues={{
            phoneNumber: '',
          }}
          validationSchema={signUpSchema}
          validateOnChange={false}
          onSubmit={onSubmit}
        >
          {(props) => {
            const {
              values,
              handleSubmit,
              setFieldValue,
              setFieldError,
              errors,
            } = props;

            function setPhoneNumber(value: string) {
              setFieldValue('phoneNumber', value);

              setFieldError('phoneNumber', '');
            }

            return (
              <View style={styles.wrapper}>
                <ScreenTitle
                  text={t('auth:login.title.phoneNumber')}
                  marginBottom={40}
                  outerStyles={styles.titlePhoneNumber}
                />

                <WithDescription
                  title={t('common:inputs.phoneNumber.label')}
                  marginBottom={68}
                  error={errors.phoneNumber}
                >
                  <PhoneNumber
                    onChangeText={setPhoneNumber}
                    error={errors.phoneNumber}
                  />
                </WithDescription>

                <UgetButton
                  type="primary"
                  // disabled={values.phoneNumber.length < 11}
                  label={t('auth:login.btnNext')}
                  onPress={handleSubmit}
                  marginBottom={32}
                />
                <LoginTypeSelector type="phone" />
              </View>
            );
          }}
        </Formik>

        <RedirectToSignUp />
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

export default memo(LoginByPhone);
