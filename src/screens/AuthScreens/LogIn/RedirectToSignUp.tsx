import React, { memo } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { UgetGreyText, UgetLinkText } from '../../../components';

import styles from './style';
import { NAVIGATION } from '../../../config/constants';

export const RedirectToSignUp = ({}) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  function handleNavigateSignUp() {
    navigation.navigate(NAVIGATION.AUTHENTICATION.SIGN_UP);
  }

  return (
    <View style={styles.signUpContainer}>
      <UgetGreyText
        text={t('auth:login.account')}
        marginBottom={24}
        outerStyles={styles.accountText}
      />
      <UgetLinkText
        underline={true}
        text={t('auth:login.signUp')}
        marginBottom={64}
        onPress={handleNavigateSignUp}
      />
    </View>
  );
};
