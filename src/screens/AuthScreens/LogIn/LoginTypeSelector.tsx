import React from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { UgetButton, UgetGreyText } from '../../../components';

import { NAVIGATION } from '../../../config/constants';

export const LoginTypeSelector = ({ type }: { type: 'email' | 'phone' }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  console.log(type);

  function handleNavigateToLoginType() {
    navigation.navigate(
      type === 'email'
        ? NAVIGATION.AUTHENTICATION.LOG_IN_BY_PHONE
        : NAVIGATION.AUTHENTICATION.LOG_IN_BY_EMAIL,
    );
  }

  return (
    <View>
      <UgetGreyText text={t('auth:login.or')} marginBottom={24} />

      <UgetButton
        type="outlined"
        label={t(`auth:login.bntLoginWith.${type}`)}
        onPress={handleNavigateToLoginType}
      />
    </View>
  );
};
