import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  containerInner: {
    height: '100%',
    justifyContent: 'space-between',
  },

  wrapper: {
    paddingHorizontal: 50,
  },

  inputContainer: {
    marginBottom: 16,
  },

  signUpContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },

  accountText: {
    marginRight: 4,
  },

  inputPhoneNumberContainer: {
    marginBottom: 68,
  },

  titlePhoneNumber: {
    maxWidth: 250,
    alignSelf: 'center',
  },

  inputCode: { textAlign: 'center', letterSpacing: 10 },
});
