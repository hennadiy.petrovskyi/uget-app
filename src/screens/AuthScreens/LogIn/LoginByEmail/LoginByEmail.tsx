import React, { memo } from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  View,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Formik } from 'formik';
import * as Yup from 'yup';

import {
  ScreenTitle,
  UgetGreyText,
  UgetLinkText,
  UgetButton,
  TextInput,
  WithDescription,
  ButtonGoBack,
} from '../../../../components';
import { RedirectToSignUp } from '../RedirectToSignUp';

import { NAVIGATION } from '../../../../config/constants';
import { loginWithEmailAndPassword } from '../../../../api';
import {
  addLocalNotification,
  clearNavigationSession,
  setLoaderStatus,
} from '../../../../utils/store/general/actions';

import styles from '../style';
import { asyncStorage, Keys } from '../../../../utils/asyncStorage';
import { LoginTypeSelector } from '../LoginTypeSelector';

export default memo(({}) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const navigationSession = useSelector(
    (state) => state.general.navigationSession,
  );

  const signUpSchema = Yup.object().shape({
    email: Yup.string()
      .email(t('common:inputs.email.errors.format'))
      .required(t('common:inputs.email.errors.required')),

    password: Yup.string()
      .min(6, t('common:inputs.password.errors.min'))
      .required(t('common:inputs.password.errors.required')),
  });

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <ButtonGoBack
          handleGoBack={() => {
            navigation.navigate(NAVIGATION.TABS_ROUTE.HOME_SCREEN);
          }}
        />
      ),
    });
  }, []);

  async function onSubmit(
    formData: { email: string; password: string },
    { setFieldError },
  ) {
    dispatch(setLoaderStatus(true));

    dispatch(
      loginWithEmailAndPassword({
        ...formData,
        handleNavigate,
        setFieldError: handleInputsErrors,
      }),
    );
    function handleInputsErrors(inputName: string, type: any) {
      dispatch(setLoaderStatus(false));

      setFieldError(inputName, t(`common:inputs.${inputName}.errors.${type}`));
    }
  }

  function handleNavigate() {
    if (
      navigationSession.options &&
      navigationSession.options.withNotification
    ) {
      dispatch(
        addLocalNotification({
          id: navigationSession.options.notificationId,
          options: navigationSession.options.notificationOption,
        }),
      );
    }

    setTimeout(() => {
      navigation.navigate(navigationSession.to.stack, {
        screen: navigationSession.to.screen,
      });

      dispatch(clearNavigationSession());

      dispatch(setLoaderStatus(false));
    }, 200);
  }

  function handleNavigateForgotPassword() {
    navigation.navigate(NAVIGATION.AUTHENTICATION.FORGOT_PASSWORD as never);
  }

  const inputs = ['email', 'password'];

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
      style={styles.container}
    >
      <SafeAreaView style={styles.containerInner}>
        <Formik
          initialValues={{
            email: '',
            password: '',
          }}
          validationSchema={signUpSchema}
          validateOnChange={false}
          onSubmit={onSubmit}
        >
          {(props) => {
            const {
              values,
              handleSubmit,
              setFieldValue,
              setFieldError,
              errors,
            } = props;

            return (
              <View style={styles.wrapper}>
                <ScreenTitle
                  text={t('auth:login.title.email')}
                  marginBottom={40}
                />

                {inputs.map((input) => {
                  function handleInputValue(value: string) {
                    setFieldValue(
                      input,
                      input === 'email' ? value.toLowerCase() : value,
                    );
                    setFieldError(input, '');
                  }

                  return (
                    <WithDescription
                      key={input}
                      title={t(`common:inputs.${input}.label`)}
                      marginBottom={20}
                    >
                      <TextInput
                        type={input !== 'password' ? input : 'password'}
                        placeholder={t(`common:inputs.${input}.placeholder`)}
                        value={values[input]}
                        error={errors[input]}
                        onChangeText={handleInputValue}
                      />
                    </WithDescription>
                  );
                })}

                <UgetLinkText
                  text={t('auth:login.forgotPassword')}
                  marginBottom={64}
                  onPress={handleNavigateForgotPassword}
                />

                <UgetButton
                  type="primary"
                  label={t('auth:login.btnSubmit')}
                  disabled={false}
                  onPress={handleSubmit}
                  marginBottom={24}
                />

                <LoginTypeSelector type="email" />
              </View>
            );
          }}
        </Formik>
        <RedirectToSignUp />
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
});
