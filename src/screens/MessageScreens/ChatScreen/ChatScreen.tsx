import React, { useEffect, useState } from 'react';
import { Text, SafeAreaView, View } from 'react-native';

import {
  ConversationPreview,
  EmptyTab,
  ScreenTitle,
  UgetButton,
} from '../../../components';
import { useSelector } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';
import { useTranslation } from 'react-i18next';

import styles from './style';
import navigation from '../../../config/constants/navigation';
import { useNavigation } from '@react-navigation/native';
import { NAVIGATION } from '../../../config/constants';

const ChatScreen = () => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const isAuth = useSelector((state) => state.general.isAuth);
  const conversations = useSelector((state) => state.chat.conversations);
  const trips = useSelector((state) => state.trips.uploaded);
  const orders = useSelector((state) => state.orders.uploaded);

  const conversationsWithMessages = conversations.filter(
    (el) => el.messages.length,
  );

  console.log(conversationsWithMessages);

  return (
    <SafeAreaView style={styles.container}>
      {isAuth && (
        <View style={styles.innerWrapper}>
          <ScreenTitle
            text={t('chat:title')}
            align="left"
            marginBottom={16}
            outerStyles={styles.title}
          />

          {conversations.length ? (
            <ScrollView>
              {conversationsWithMessages.map((el) => (
                <ConversationPreview key={el.id} conversation={el} />
              ))}
            </ScrollView>
          ) : (
            <View style={styles.emptyTabContainer}>
              <EmptyTab text={t('chat:emptyText')} marginBottom={60} />
              {!orders.length ? (
                <UgetButton
                  type="primary"
                  label={t('chat:buttons.createOrder')}
                  marginBottom={10}
                  onPress={() => {
                    navigation.navigate(NAVIGATION.TABS_ROUTE.ORDER_SCREENS, {
                      screen: NAVIGATION.ORDER_SCREENS.CREATE_ORDER,
                    });
                  }}
                />
              ) : (
                <UgetButton
                  type="primary"
                  label={t('chat:buttons.myOrders')}
                  marginBottom={10}
                  onPress={() => {
                    navigation.navigate(NAVIGATION.TABS_ROUTE.ORDER_SCREENS, {
                      screen: NAVIGATION.ORDER_SCREENS.MY_ORDERS,
                    });
                  }}
                />
              )}
              {!trips.length ? (
                <UgetButton
                  type="primary"
                  label={t('chat:buttons.addTrip')}
                  onPress={() => {
                    navigation.navigate(NAVIGATION.TABS_ROUTE.MY_TRIPS, {
                      screen: NAVIGATION.TRIPS.CREATE_TRIP,
                    });
                  }}
                />
              ) : (
                <UgetButton
                  type="primary"
                  label={t('chat:buttons.myTrips')}
                  onPress={() => {
                    navigation.navigate(NAVIGATION.TABS_ROUTE.MY_TRIPS, {
                      screen: NAVIGATION.TRIPS.MY_TRIPS,
                    });
                  }}
                />
              )}
            </View>
          )}
        </View>
      )}
    </SafeAreaView>
  );
};

export default ChatScreen;
