import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  innerWrapper: {
    paddingTop: 25,
  },

  title: {
    paddingHorizontal: 24,
  },

  item: {
    backgroundColor: '#F8F8F8',
  },

  emptyTabContainer: {
    paddingHorizontal: 40,
  },
});
