import React, { useEffect } from 'react';
import { View, SafeAreaView, TouchableOpacity, Text } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useState } from 'react';
import { ScrollView } from 'react-native-gesture-handler';

import { useDispatch, useSelector } from 'react-redux';

import {
  ChatMessage,
  OrderItem,
  TextInput,
  UgetButton,
  UserPreview,
} from '../../../components';
import { SEND_MESSAGE, PATH } from '../../../assets/svg';

import { webSocket } from '../../../api/api';
import GoBack from '../../../components/Buttons/GoBack/GoBack';
import { NAVIGATION } from '../../../config/constants';
import { clearNavigationSession } from '../../../utils/store/general/actions';
import { OrderViewerType, OrderViewType } from '../../../types';
import {
  getAllConversations,
  getOneDependOnRequestedAuthRecord,
  getUserDetails,
} from '../../../api';

import styles from './style';
import { conversationsHydrateState } from '../../../utils/store/chat/actions';

const socket = webSocket();

function sendMessage(payload) {
  socket.emit('send_message', payload);
}

const IndividualChatScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [message, setMessage] = useState('');
  const routeOptions = useRoute();
  const scrollRef = React.createRef();

  const [order, setOrder] = useState();
  const [contactPerson, setContactPerson] = useState();
  const conversations = useSelector((state) => state.chat.conversations);
  const myUUID = useSelector((state) => state.profile.uuid);
  const navigationSession = useSelector(
    (state) => state.general.navigationSession,
  );

  const { params } = routeOptions;
  const { initMessage } = params;

  const currentConversationDetails = conversations.find(
    (el) => el.id === params.conversationId,
  );

  useEffect(() => {
    getAllConversations().then((data) => {
      dispatch(conversationsHydrateState(data));
    });
  }, []);

  useEffect(() => {
    if (!currentConversationDetails) {
      return;
    }

    getUserDetails(currentConversationDetails.contactPerson).then((response) =>
      setContactPerson(response),
    );

    getOneDependOnRequestedAuthRecord(
      currentConversationDetails.orderUUID,
    ).then((responseData) => setOrder(responseData));
  }, [currentConversationDetails]);

  React.useLayoutEffect(() => {
    let handleGoBack: () => void;

    if (navigationSession) {
      handleGoBack = () => {
        navigation.navigate(navigationSession.from.stack, {
          screen: navigationSession.from.screen,
        });

        dispatch(clearNavigationSession());
      };
    } else {
      handleGoBack = () => {
        navigation.navigate(NAVIGATION.TABS_ROUTE.MESSAGES, {
          screen: NAVIGATION.MESSAGE_SCREENS.MESSAGES,
        });
      };
    }

    navigation.setOptions({
      headerLeft: () => {
        return <GoBack handleGoBack={handleGoBack} />;
      },
    });
  }, [navigationSession]);

  return (
    <SafeAreaView style={styles.container}>
      {contactPerson && (
        <View style={styles.contactPersonWrapper}>
          <UserPreview owner={contactPerson} />
        </View>
      )}

      {order && (
        <View style={styles.contactPersonWrapper}>
          <OrderItem
            order={order}
            viewer={OrderViewerType.DEFAULT}
            view={OrderViewType.MINIMIZED}
          />
        </View>
      )}

      <ScrollView
        style={styles.messagesList}
        ref={scrollRef}
        onContentSizeChange={() => {
          scrollRef.current.scrollToEnd();
        }}
      >
        {initMessage ? (
          <View style={styles.initMessageWrapper}>
            <Text style={styles.initMessage}>{initMessage}</Text>
          </View>
        ) : null}

        {currentConversationDetails?.messages.map((el, i) => (
          <ChatMessage key={i} data={el} />
        ))}
      </ScrollView>

      <View style={styles.messageInput}>
        <View style={styles.messageView}>
          <TextInput
            value={message}
            type="multiline"
            placeholder="Enter your message here..."
            onChangeText={setMessage}
            inputWrapperOuterStyles={styles.inputOuterStyles}
            error={''}
          />
        </View>

        <TouchableOpacity
          style={styles.sendMessageIcon}
          onPress={() => {
            sendMessage({
              ...params,
              message,
              userId: myUUID,
            });

            setMessage('');
          }}
        >
          <SEND_MESSAGE />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default IndividualChatScreen;
