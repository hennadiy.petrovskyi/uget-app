import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-end',
    paddingHorizontal: 24,
  },

  messageInput: {
    borderWidth: 1,
    borderColor: COLORS.GREY3,
    padding: 10,
    paddingTop: 0,
    paddingRight: 40,
    flexDirection: 'row',
    position: 'relative',
    marginBottom: 24,
    color: COLORS.BLACK,
  },

  messageView: { maxHeight: 100 },
  sendMessageIcon: {
    position: 'absolute',
    top: 10,
    right: 10,
  },

  contactPersonWrapper: {
    paddingHorizontal: 8,
    backgroundColor: COLORS.GREY2,
    marginTop: 10,
  },

  messagesList: {
    paddingTop: 16,
    marginBottom: 16,
  },

  inputOuterStyles: {
    borderColor: 'transparent',
    padding: 0,
  },

  initMessageWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 16,
  },

  initMessage: {
    width: 292,
    borderWidth: 1,
    borderColor: COLORS.GOLD,
    padding: 8,
    paddingBottom: 16,
    fontFamily: 'FiraSans-Italic',
  },
});
