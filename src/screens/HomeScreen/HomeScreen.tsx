import React, { memo, useState } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Image,
  SafeAreaView,
  Alert,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { PrimaryButton } from '../../components';
import { IMAGES } from '../../assets';
import { NAVIGATION } from '../../config/constants';
import styles from './style';

const RowComponent = ({
  img,
  header,
  description,
  buttonTxt,
  onTap,
  imgStyle,
}: {
  img: string;
  header: string;
  description: string;
  buttonTxt: string;
  onTap: any;
  imgStyle: any;
}) => {
  return (
    <View style={styles.categoryRowWrapp}>
      <View style={styles.wrapp}>
        <View style={styles.rowHeader}>
          <Text style={styles.headerTxt}>{header}</Text>
          <Text style={styles.subTitleRow}>{description}</Text>
        </View>
        <View style={styles.btnWrapp}>
          <PrimaryButton
            onPress={onTap}
            style={styles.rowBtn}
            txt={buttonTxt}
          />
        </View>
      </View>
      <View style={[styles.imgWrap, imgStyle]}>
        <Image style={styles.img} resizeMode="contain" source={img} />
      </View>
    </View>
  );
};

export default memo(() => {
  const navigation = useNavigation();
  const { t } = useTranslation();

  const [tab, setTab] = useState(true);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <RowComponent
          img={IMAGES.ON_BOARD_BOX}
          header={t('home:intro.senderTitle')}
          description={t('home:intro.senderSubTitle')}
          buttonTxt={t('home:intro.senderBtn')}
          onTap={() =>
            navigation.navigate(NAVIGATION.UGET_ROUTE.ON_BOARDING, {
              isSender: true,
            })
          }
          imgStyle={styles.senderImg}
        />
        <RowComponent
          img={IMAGES.HOME_SCREEN_PLANE}
          header={t('home:intro.travelerTitle')}
          description={t('home:intro.travelerSubTitle')}
          buttonTxt={t('home:intro.travelerBtn')}
          onTap={() =>
            navigation.navigate(NAVIGATION.UGET_ROUTE.ON_BOARDING, {
              isSender: false,
            })
          }
          imgStyle={styles.travelerImg}
        />
        <View style={styles.description}>
          <Text style={styles.title}>{t('home:intro.tabs.title')}</Text>
          <View style={styles.tabs}>
            <View style={styles.tabsTxtWrapp}>
              <TouchableOpacity onPress={() => setTab(true)}>
                <Text style={styles.tabsTxt}>
                  {t('home:intro.tabs.receiver.title')}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => setTab(false)}>
                <Text style={styles.tabsTxt}>
                  {t('home:intro.tabs.traveler.title')}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.toggleBar}>
              <View
                style={
                  tab ? styles.activeToggleBarLeft : styles.activeToggleBarRight
                }
              ></View>
            </View>
          </View>
          {tab ? (
            <>
              <>
                <Text style={styles.titleRow}>
                  {t('home:intro.tabs.receiver.p1.title')}
                </Text>
                <Text style={styles.subTitle}>
                  {t('home:intro.tabs.receiver.p1.text')}
                </Text>
              </>
              <>
                <Text style={styles.titleRow}>
                  {t('home:intro.tabs.receiver.p2.title')}
                </Text>
                <Text style={styles.subTitle}>
                  {t('home:intro.tabs.receiver.p2.text')}
                </Text>
              </>
              <>
                <Text style={styles.titleRow}>
                  {t('home:intro.tabs.receiver.p3.title')}
                </Text>
                <Text style={styles.subTitle}>
                  {t('home:intro.tabs.receiver.p3.text')}
                </Text>
              </>
            </>
          ) : (
            <>
              <>
                <Text style={styles.titleRow}>
                  {t('home:intro.tabs.traveler.p1.title')}
                </Text>
                <Text style={styles.subTitle}>
                  {t('home:intro.tabs.traveler.p1.text')}
                </Text>
              </>
              <>
                <Text style={styles.titleRow}>
                  {t('home:intro.tabs.traveler.p2.title')}
                </Text>
                <Text style={styles.subTitle}>
                  {t('home:intro.tabs.traveler.p2.text')}
                </Text>
              </>
              <>
                <Text style={styles.titleRow}>
                  {t('home:intro.tabs.traveler.p3.title')}
                </Text>
                <Text style={styles.subTitle}>
                  {t('home:intro.tabs.traveler.p3.text')}
                </Text>
              </>
              <>
                <Text style={styles.titleRow}>
                  {t('home:intro.tabs.traveler.p4.title')}
                </Text>
                <Text style={styles.subTitle}>
                  {t('home:intro.tabs.traveler.p4.text')}
                </Text>
              </>
            </>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
});
