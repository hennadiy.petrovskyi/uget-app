import { StyleSheet } from 'react-native';

import { COLORS } from '../../config/constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  wrapp: {
    flexDirection: 'column',
    paddingHorizontal: 24
  },
  categoryRowWrapp: {
    height: 375,
    backgroundColor: COLORS.GREY2,
    marginBottom: 4
  },
  rowHeader: {
    paddingTop: 40
  },
  headerTxt: {
    fontSize: 26,
    fontFamily: 'FiraSans-SemiBold'
  },
  subTitleRow: {
    color: COLORS.GREY,
    paddingTop: 15,
    lineHeight: 21
  },
  btnWrapp: {
    width: '60%'
  },
  rowBtn: {
    marginTop: 24
  },
  imgWrap: {
    position: 'absolute',
    bottom: 20,
    right: 0,
    zIndex: -1
  },
  img: {
    height: '100%',
    width: '100%'
  },
  senderImg: {
    height: 180,
    width: 200
  },
  travelerImg: {
    height: 180,
    width: 400,
    right: -50,
    bottom: -20
  },
  tabs: {
    marginTop: 24
  },
  tabsTxtWrapp: {
    flexDirection: 'row'
  },
  tabsTxt: {
    fontFamily: 'FiraSans-SemiBold',
    paddingLeft: 12
  },
  toggleBar: {
    height: 4,
    width: 180,
    backgroundColor: COLORS.GREY2,
    marginTop: 5
  },
  activeToggleBarLeft: {
    position: 'absolute',
    height: 4,
    width: 91,
    backgroundColor: COLORS.MAIN_COLOR
  },
  activeToggleBarRight: {
    position: 'absolute',
    height: 4,
    width: 90,
    backgroundColor: COLORS.MAIN_COLOR,
    marginLeft: 90
  },
  title: {
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 26,
    paddingTop: 40
  },
  description: {
    paddingHorizontal: 24,
    paddingBottom: 15
  },
  titleRow: {
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 16,
    paddingTop: 24
  },
  subTitle: {
    color: COLORS.GREY,
    paddingTop: 8,
    lineHeight: 21
  }
});
