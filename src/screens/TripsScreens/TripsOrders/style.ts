import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY } from '../../../config/constants';
import colors from '../../../config/constants/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 24,
  },
  titleWrap: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 25,
    paddingBottom: 20,
  },
  title: {
    fontSize: 32,
    fontFamily: 'FiraSans-SemiBold',
  },

  topContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 32,
    paddingTop: 20,
  },

  earningsContainer: {},
  earningsValue: {
    fontFamily: FONT_FAMILY.MEDIUM_500,
    color: COLORS.BLACK,
    marginBottom: 8,
    textAlign: 'right',
    fontSize: 16,
  },
  earningsLabel: {
    fontFamily: FONT_FAMILY.NORMAL_400,
    color: COLORS.GREY5,
    fontSize: 14,
  },
});
