import { useFocusEffect, useRoute } from '@react-navigation/native';
import React, { memo, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { SafeAreaView, View, Text } from 'react-native';

import { useDispatch, useSelector } from 'react-redux';
import { getOrdersByTripsParams } from '../../../api';

import { OrderItem, RoutePreview, UgetTabs } from '../../../components';
import { OrderViewerType, OrderViewType } from '../../../types';
import { ordersSetFilteredByTrip } from '../../../utils/store/orders/actions';

import styles from './style';

export default memo(() => {
  const route = useRoute();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const tripOrders = useSelector((state) => state.orders.filteredByTrip);
  const [screenParams, setParams] = useState({
    tripUUID: '',
    from: '',
    to: '',
    tripDates: '',
  });

  const { params } = route;

  useEffect(() => {
    if (params) {
      const { locations, arrivalDate, departureDate, tripUUID } = params;

      setParams({
        tripUUID,
        from: locations.from.city.label,
        to: locations.to.city.label,
        tripDates: `${new Date(
          departureDate,
        ).toLocaleDateString()} - ${new Date(
          arrivalDate,
        ).toLocaleDateString()}`,
      });

      getTripRelatedOrders();
    }
  }, []);

  const tabs = [
    {
      label: t('trips:tripItem.available'),
      filter: 'available',
      isActive: true,
      emptyText: '',
    },
    {
      label: t('trips:tripItem.requests'),
      filter: ['request.incoming', 'request.outgoing'],
      isActive: true,
      emptyText: t('trips:tripItem.emptyList.requests'),
    },
    {
      label: t('trips:tripItem.accepted'),
      filter: ['accepted', 'progress'],
      isActive: true,
      emptyText: '',
    },
    {
      label: t('trips:tripItem.delivered'),
      filter: 'delivered',
      isActive: true,
      emptyText: '',
    },
  ];

  function getTripRelatedOrders() {
    const { locations, arrivalDate, departureDate, tripUUID } = params;

    dispatch(
      getOrdersByTripsParams({
        tripUUID,
        arrivalDate,
        departureDate,
        from: locations.from.city.id,
        to: locations.to.city.id,
      }),
    );
  }

  const renderItem = (item, key, setFilter) => {
    return (
      <OrderItem
        key={key}
        order={item}
        view={OrderViewType.PREVIEW}
        viewer={OrderViewerType.TRAVELER}
        tripUUID={screenParams.tripUUID}
        setFilter={setFilter}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.topContainer}>
        <RoutePreview
          from={screenParams.from}
          to={screenParams.to}
          date={screenParams.tripDates}
        />

        <View style={styles.earningsContainer}>
          <Text style={styles.earningsValue}>
            {params.earnings
              ? (() => {
                  const { value, currency } = params.earnings;

                  return `${currency} ${value}`;
                })()
              : '$0.00'}
          </Text>
          <Text>{t('trips:tripOrders.earnings')}</Text>
        </View>
      </View>

      <UgetTabs
        tabs={tabs}
        data={tripOrders}
        renderItem={renderItem}
        onRefreshControl={getTripRelatedOrders}
      />
    </SafeAreaView>
  );
});
