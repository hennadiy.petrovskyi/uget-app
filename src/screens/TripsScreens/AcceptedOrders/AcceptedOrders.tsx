import { useRoute } from '@react-navigation/native';
import React, { memo, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { SafeAreaView } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { getOrdersByTripsParams, getTravellerActiveOrders } from '../../../api';

import { OrderItem, RoutePreview, UgetTabs } from '../../../components';
import { OrderViewerType, OrderViewType } from '../../../types';

import styles from './style';

export default memo(() => {
  const route = useRoute();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [orders, setOrders] = useState([]);
  const [screenParams, setParams] = useState({
    tripUUID: '',
    from: '',
    to: '',
    tripDates: '',
  });

  const { params } = route;

  useEffect(() => {
    if (params) {
      setParams(params);

      getOrders();
    }
  }, []);

  const tabs = [
    {
      label: handleLabel('available'),
      filter: 'available',
      isActive: true,
    },
    {
      label: handleLabel('progress'),
      filter: 'progress',
      isActive: true,
    },
    {
      label: handleLabel('delivered'),
      filter: 'delivered',
      isActive: true,
    },
  ];

  function handleLabel(filterParam: string) {
    const result = orders.filter((el) => el.status === filterParam);

    let amount;

    if (!result.length) {
      amount = '';
    } else {
      amount = `(${result.length})`;
    }

    console.log(result.length);

    return t(
      `trips:tripItem.${filterParam === 'progress' ? 'transit' : filterParam}`,
      { amount },
    );
  }

  function getOrders() {
    const { tripUUID } = params;

    getTravellerActiveOrders(tripUUID).then((response) => setOrders(response));
  }

  return (
    <SafeAreaView style={styles.container}>
      <RoutePreview
        from={screenParams.from}
        to={screenParams.to}
        date={screenParams.tripDates}
        marginBottom={32}
      />

      <UgetTabs
        tabs={tabs}
        data={orders}
        renderItem={(item, key) => {
          return (
            <OrderItem
              key={key}
              order={item}
              view={OrderViewType.PREVIEW}
              viewer={OrderViewerType.TRAVELER}
              tripUUID={screenParams.tripUUID}
            />
          );
        }}
        onRefreshControl={getOrders}
      />
    </SafeAreaView>
  );
});
