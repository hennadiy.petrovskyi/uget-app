import React, { memo, useEffect } from 'react';
import { SafeAreaView, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import {
  ScreenTitle,
  TripItem,
  UgetLinkText,
  UgetTabs,
} from '../../../components';
import { AddTrip } from './AddTrip';
import { NAVIGATION } from '../../../config/constants';
import { getMyTrips } from '../../../api';

import styles from './style';

export default memo(() => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);
  const tripsUploaded = useSelector((state) => state.trips.uploaded);
  const navigation = useNavigation();

  const tabs = [
    {
      label: t('trips:tabs.active.label'),
      isActive: isAuth,
      filter: 'upcoming',
      emptyText: t('trips:tabs.active.emptyText'),
    },
    {
      label: t('trips:tabs.upcoming.label'),
      isActive: isAuth,
      filter: 'active',
      emptyText: t('trips:tabs.upcoming.emptyText'),
    },
    {
      label: t('trips:tabs.past.label'),
      isActive: isAuth,
      filter: 'past',
      emptyText: t('trips:tabs.past.emptyText'),
    },
  ];

  useEffect(() => {
    dispatch(getMyTrips());
  }, []);

  function renderItem(item, key) {
    return <TripItem key={key} trip={item} />;
  }

  return (
    <SafeAreaView style={styles.container}>
      {!tripsUploaded.length || !isAuth ? (
        <AddTrip />
      ) : (
        <>
          <View style={styles.titleWrap}>
            <ScreenTitle text={t('trips:myTrips.title')} />

            <UgetLinkText
              text={`+ ${t('trips:myTrips.addNewTrip')}`}
              onPress={() => navigation.navigate(NAVIGATION.TRIPS.CREATE_TRIP)}
            />
          </View>

          <UgetTabs
            tabs={tabs}
            data={tripsUploaded}
            renderItem={renderItem}
            onRefreshControl={() => {
              dispatch(getMyTrips());
            }}
          />
        </>
      )}
    </SafeAreaView>
  );
});
