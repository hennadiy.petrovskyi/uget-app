import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 25,
  },
  description: {
    maxWidth: 300,
    alignSelf: 'center',
  },
});
