import React, { memo } from 'react';
import { View, SafeAreaView } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { ScreenTitle, UgetButton, UgetGreyText } from '../../../components';

import styles from './style';
import { PLANE_IN_CLOUDS } from '../../../assets/svg';
import { NAVIGATION } from '../../../config/constants';

export default memo(() => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  return (
    <SafeAreaView style={styles.container}>
      <ScreenTitle
        text={t('trips:addTrip.title')}
        align="left"
        marginBottom={94}
      />

      <PLANE_IN_CLOUDS style={{ marginBottom: 54.05, alignSelf: 'center' }} />
      <UgetGreyText
        text={t('trips:addTrip.description')}
        marginBottom={24}
        outerStyles={styles.description}
      />
      <UgetButton
        label={t('trips:addTrip.button')}
        type="primary"
        onPress={() => navigation.navigate(NAVIGATION.TRIPS.CREATE_TRIP)}
      />
    </SafeAreaView>
  );
});
