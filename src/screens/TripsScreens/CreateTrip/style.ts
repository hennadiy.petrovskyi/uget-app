import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingBottom: 61,
  },
  titleWrap: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 25,
    paddingBottom: 20,
  },
  cancelBtn: {
    color: COLORS.BLACK,
  },
  formTitle: {
    fontFamily: 'FiraSans-Bold',
    fontSize: 18,
    lineHeight: 21,
    color: COLORS.BLACK,
    marginTop: 20,
    marginBottom: 15,
  },
});
