import React, { memo, useEffect } from 'react';
import {
  View,
  SafeAreaView,
  Text,
  KeyboardAvoidingView,
  ScrollView,
  Platform,
} from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

import {
  ScreenTitle,
  UgetButton,
  UgetLinkText,
  WithDescription,
} from '../../../components';
import {
  Calendar,
  Location,
  RadioButtons,
  TextInput,
} from '../../../components/UgetInputs';
import { publishRoundTrip, publishTrip } from '../../../api';
import { NAVIGATION } from '../../../config/constants';

import styles from './style';
import { generateDateWithOffset } from '../../../utils/helpers';
import { useDispatch, useSelector } from 'react-redux';
import { asyncStorage, Keys } from '../../../utils/asyncStorage';
import {
  addLocalNotification,
  setLoaderStatus,
} from '../../../utils/store/general/actions';
import { NotificationID } from '../../../components/ModalNotifications/ModalNotificationsProvider';

export enum TripType {
  ROUND = 'round',
  ONE_WAY = 'one',
}

export default memo(() => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);

  const initFormValues = {
    type: TripType.ONE_WAY,
    fromCountry: null,
    fromCity: null,
    fromArea: '',
    toCountry: { id: 'ChIJvRKrsd9IXj4RpwoIwFYv0zM', label: 'UAE' },
    toCity: { id: 'ChIJRcbZaklDXz4RYlEphFBu5r0', label: 'Dubai' },
    toArea: '',
    arrivalDate: generateDateWithOffset(undefined, 2),
    departureDate: generateDateWithOffset(undefined, 2),
    returnArrivalDate: generateDateWithOffset(undefined, 3),
    returnDepartureDate: generateDateWithOffset(undefined, 3),
    description: '',
  };

  const travelerSchema = Yup.lazy((values) => {
    return Yup.object().shape({
      fromCountry: Yup.object().required(
        t('trips:createTrip.inputs.fromCountry.errors.required'),
      ),
      fromCity: Yup.object().required(
        t('trips:createTrip.inputs.fromCity.errors.required'),
      ),
      fromArea: Yup.string().required(
        t('trips:createTrip.inputs.fromArea.errors.required'),
      ),
      toCountry: Yup.object().required(
        t('trips:createTrip.inputs.toCountry.errors.required'),
      ),
      toCity: Yup.object().required(
        t('trips:createTrip.inputs.toCity.errors.required'),
      ),
      toArea: Yup.string().required(
        t('trips:createTrip.inputs.toArea.errors.required'),
      ),
      departureDate: Yup.string().required(
        t('trips:createTrip.inputs.departureDate.errors.required'),
      ),
      arrivalDate: Yup.string().required(
        t('trips:createTrip.inputs.arrivalDate.errors.required'),
      ),
      description: Yup.string(),
      // returnArrivalDate:
      //   values.typeOfTrip === TripType.ONE_WAY
      //     ? Yup.mixed().notRequired()
      //     : Yup.string().required(
      //         t('trips:createTrip.inputs.returnDate.errors.required'),
      //       ),
      // returnDepartureDate:
      //   values.typeOfTrip === TripType.ONE_WAY
      //     ? Yup.mixed().notRequired()
      //     : Yup.string().required(
      //         t('trips:createTrip.inputs.returnDate.errors.required'),
      //       ),
    });
  });

  useEffect(() => {}, []);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <UgetLinkText
          text={t('common:buttons.cancel')}
          outerStyles={styles.cancelBtn}
          onPress={() =>
            navigation.navigate(NAVIGATION.TABS_ROUTE.MY_TRIPS, {
              screen: NAVIGATION.TRIPS.MY_TRIPS,
            })
          }
        />
      ),
    });
  }, []);

  const typeOfTrip = [
    {
      id: TripType.ONE_WAY,
      label: t('trips:createTrip.oneWay'),
    },
    {
      id: TripType.ROUND,
      label: t('trips:createTrip.roundTrip'),
    },
  ];

  async function onSubmit(formData: any, { setErrors }) {
    const dataToPublish = prepareFormDataToSubmit(formData, formData.type);

    if (!isAuth) {
      await asyncStorage.save(Keys.TRIP_TO_PUBLISH, dataToPublish);

      dispatch(
        addLocalNotification({
          id: NotificationID.AUTHORIZE_TO_PUBLISH,
          options: { type: 'trip' },
        }),
      );
      return;
    }

    dispatch(setLoaderStatus(true));

    if (formData.type === TripType.ONE_WAY) {
      publishTrip(dataToPublish, onSuccess);
      return;
    }

    if (formData.type === TripType.ROUND) {
      publishRoundTrip(dataToPublish, onSuccess);
      return;
    }
  }

  function onSuccess() {
    dispatch(setLoaderStatus(false));

    navigation.navigate(NAVIGATION.TRIPS.MY_TRIPS);
  }

  function prepareFormDataToSubmit(data: any, type: TripType) {
    if (type === TripType.ONE_WAY) {
      delete data.returnArrivalDate;
      delete data.returnDepartureDate;

      return data;
    }

    if (type === TripType.ROUND) {
      return data;
    }
  }

  return (
    <SafeAreaView>
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.container}
        style={{ height: '100%' }}
      >
        <View style={styles.titleWrap}>
          <ScreenTitle text={t('trips:createTrip.title')} />
        </View>
        <Formik
          initialValues={initFormValues}
          validationSchema={travelerSchema}
          validateOnChange={false}
          onSubmit={onSubmit}
        >
          {(props) => {
            const {
              handleSubmit,
              errors,
              values,
              setFieldError,
              setFieldValue,
            } = props;

            function setTypeOfTrip(value: any) {
              setFieldValue('type', value);
            }

            function setFromCountry(value: string) {
              setFieldValue('fromCountry', value);
            }

            function setFromCity(value: string) {
              setFieldValue('fromCity', value);
            }

            function setFromArea(value: string) {
              setFieldValue('fromArea', value);
              setFieldError('fromArea', '');
            }

            function setToCountry(value: string) {
              setFieldValue('toCountry', value);
            }

            function setToCity(value: string) {
              setFieldValue('toCity', value);
            }

            function setToArea(value: string) {
              setFieldValue('toArea', value);
              setFieldError('toArea', '');
            }

            function setDepartureDate(value: string) {
              const parsedDate = new Date(value).toISOString();

              setFieldValue('departureDate', parsedDate);
              setFieldValue('arrivalDate', parsedDate);

              setFieldError('departureDate', '');
            }

            function setArrivalDate(value: string) {
              const parsedDate = new Date(value).toISOString();

              setFieldValue('arrivalDate', parsedDate);
              setFieldError('arrivalDate', '');
            }

            function setReturnDepartureDate(value: string) {
              const parsedDate = new Date(value).toISOString();

              setFieldValue('returnDepartureDate', parsedDate);
              setFieldValue('returnArrivalDate', parsedDate);

              setFieldError('returnDepartureDate', '');
            }
            function setReturnArrivalDate(value: string) {
              const parsedDate = new Date(value).toISOString();

              setFieldValue('returnArrivalDate', parsedDate);
              setFieldError('returnArrivalDate', '');
            }

            function setTripDetails(value: string) {
              setFieldValue('description', value);
            }

            const isRoundTrip = values.type === TripType.ROUND;

            return (
              <View>
                <View>
                  <RadioButtons
                    horizontal
                    items={typeOfTrip}
                    handleSelect={setTypeOfTrip}
                    selected={values.type}
                  />

                  <Text style={styles.formTitle}>
                    {t('trips:createTrip.location')}
                  </Text>

                  <WithDescription
                    title={t('trips:createTrip.inputs.fromCountry.label')}
                    marginBottom={20}
                  >
                    <Location
                      placeholder={t(
                        'trips:createTrip.inputs.fromCountry.placeholder',
                      )}
                      current={values.fromCountry}
                      type="country"
                      handleValue={setFromCountry}
                      error={errors.fromCountry}
                    />
                  </WithDescription>

                  <WithDescription
                    title={t('trips:createTrip.inputs.fromCity.label')}
                    marginBottom={20}
                  >
                    <Location
                      placeholder={t(
                        'trips:createTrip.inputs.fromCity.placeholder',
                      )}
                      type="city"
                      current={values.fromCity}
                      handleValue={setFromCity}
                      params={{
                        countryId: values.fromCountry
                          ? values.fromCountry.id
                          : '',
                      }}
                      error={errors.fromCity}
                    />
                  </WithDescription>

                  <WithDescription
                    title={t('trips:createTrip.inputs.fromArea.label')}
                    marginBottom={20}
                  >
                    <TextInput
                      type="text"
                      placeholder={t(
                        'trips:createTrip.inputs.fromArea.placeholder',
                      )}
                      onChangeText={setFromArea}
                      value={values.fromArea}
                      error={errors.fromArea}
                    />
                  </WithDescription>

                  <WithDescription
                    title={t('trips:createTrip.inputs.toCountry.label')}
                    marginBottom={20}
                  >
                    <Location
                      placeholder={t(
                        'trips:createTrip.inputs.toCountry.placeholder',
                      )}
                      isDisabled={false}
                      current={values.toCountry}
                      type="country"
                      handleValue={setToCountry}
                      error={errors.toCountry}
                    />
                  </WithDescription>

                  <WithDescription
                    title={t('trips:createTrip.inputs.toCity.label')}
                    marginBottom={20}
                  >
                    <Location
                      placeholder={t(
                        'trips:createTrip.inputs.toCity.placeholder',
                      )}
                      isDisabled={false}
                      type="city"
                      current={values.toCity}
                      handleValue={setToCity}
                      params={{
                        countryId: values.toCountry ? values.toCountry.id : '',
                      }}
                      error={errors.toCity}
                    />
                  </WithDescription>

                  <WithDescription
                    title={t('trips:createTrip.inputs.toArea.label')}
                    marginBottom={20}
                  >
                    <TextInput
                      type="text"
                      placeholder={t(
                        'trips:createTrip.inputs.toArea.placeholder',
                      )}
                      onChangeText={setToArea}
                      value={values.toArea}
                      error={errors.toArea}
                    />
                  </WithDescription>

                  <Text style={styles.formTitle}>
                    {t('trips:createTrip.datesAndDetails')}
                  </Text>

                  <WithDescription
                    title={t('trips:createTrip.inputs.departureDate.label')}
                    marginBottom={20}
                  >
                    <Calendar
                      minDate={moment(
                        generateDateWithOffset(undefined, 2),
                      ).format('yyyy-MM-DD')}
                      placeholder={t(
                        'trips:createTrip.inputs.departureDate.placeholder',
                      )}
                      onPress={setDepartureDate}
                      value={values.departureDate}
                      error={errors.departureDate}
                    />
                  </WithDescription>

                  <WithDescription
                    title={t('trips:createTrip.inputs.arrivalDate.label')}
                    marginBottom={20}
                  >
                    <Calendar
                      minDate={moment(
                        generateDateWithOffset(undefined, 2),
                      ).format('yyyy-MM-DD')}
                      placeholder={t(
                        'trips:createTrip.inputs.arrivalDate.placeholder',
                      )}
                      onPress={setArrivalDate}
                      value={values.arrivalDate}
                      error={errors.arrivalDate}
                    />
                  </WithDescription>

                  {isRoundTrip && (
                    <>
                      <WithDescription
                        title={t(
                          'trips:createTrip.inputs.returnArrivalDate.label',
                        )}
                        marginBottom={20}
                      >
                        <Calendar
                          minDate={moment(
                            generateDateWithOffset(undefined, 2),
                          ).format('yyyy-MM-DD')}
                          placeholder={t(
                            'trips:createTrip.inputs.returnArrivalDate.placeholder',
                          )}
                          onPress={setReturnDepartureDate}
                          value={values.returnDepartureDate}
                          error={errors.returnDepartureDate}
                        />
                      </WithDescription>

                      <WithDescription
                        title={t(
                          'trips:createTrip.inputs.returnDepartureDate.label',
                        )}
                        marginBottom={20}
                      >
                        <Calendar
                          minDate={moment(
                            generateDateWithOffset(undefined, 2),
                          ).format('yyyy-MM-DD')}
                          placeholder={t(
                            'trips:createTrip.inputs.returnDepartureDate.placeholder',
                          )}
                          onPress={setReturnArrivalDate}
                          value={values.returnArrivalDate}
                          error={errors.returnArrivalDate}
                        />
                      </WithDescription>
                    </>
                  )}

                  <WithDescription
                    title={t('trips:createTrip.inputs.tripDetails.label')}
                    marginBottom={20}
                  >
                    <TextInput
                      value={values.description}
                      placeholder={t(
                        'trips:createTrip.inputs.tripDetails.placeholder',
                      )}
                      type="multiline"
                      onChangeText={setTripDetails}
                      error={errors.description}
                    />
                  </WithDescription>
                </View>

                <UgetButton
                  type="primary"
                  label={t('trips:createTrip.buttons.addTrip')}
                  onPress={handleSubmit}
                />
              </View>
            );
          }}
        </Formik>
      </ScrollView>
    </SafeAreaView>
  );
});
