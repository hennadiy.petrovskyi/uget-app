import { useRoute } from '@react-navigation/native';
import React, { memo, useEffect, useState } from 'react';
import { SafeAreaView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { getTripDeliveredOrders } from '../../../api';

import {
  OrderItem,
  RefreshControlScrollView,
  RoutePreview,
} from '../../../components';
import { OrderViewerType, OrderViewType } from '../../../types';

import styles from './style';

export default memo(() => {
  const route = useRoute();
  const dispatch = useDispatch();
  const [orders, setOrders] = useState([]);
  const [screenParams, setParams] = useState({
    tripUUID: '',
    from: '',
    to: '',
    tripDates: '',
  });

  const { params } = route;

  useEffect(() => {
    if (params) {
      setParams(params);
      getOrders();
    }
  }, []);

  function getOrders() {
    const { tripUUID } = params;
    getTripDeliveredOrders(tripUUID).then((response) => setOrders(response));
  }

  return (
    <SafeAreaView style={styles.container}>
      <RoutePreview
        from={screenParams.from}
        to={screenParams.to}
        date={screenParams.tripDates}
        marginBottom={32}
      />
      <RefreshControlScrollView onRefreshControl={getOrders}>
        {orders.map((item, i) => (
          <OrderItem
            key={i.toString()}
            order={item}
            view={OrderViewType.PREVIEW}
            viewer={OrderViewerType.TRAVELER}
            tripUUID={screenParams.tripUUID}
          />
        ))}
      </RefreshControlScrollView>
    </SafeAreaView>
  );
});
