import React, { memo, useState, useRef } from 'react';
import { View, SafeAreaView } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import Swiper from 'react-native-swiper';
import { useTranslation } from 'react-i18next';

import {
  OnBoardingComponent,
  UgetButton,
  UgetLinkText,
} from '../../components';
import { NAVIGATION } from '../../config/constants';
import { IMAGES } from '../../assets';
import styles from './style';

export default memo(() => {
  const navigation = useNavigation();
  const route = useRoute();
  const { t } = useTranslation();
  const swipe = useRef(null);

  const { isSender } = route.params;
  const [nextNavigate, setNextNavigate] = useState(false);

  const onBoardingSenderData = [
    {
      img: IMAGES.ON_BOARD_CITY,
      title: t('home:onBoarding.sender.p1.title'),
      subTitle: t('home:onBoarding.sender.p1.text'),
    },
    {
      img: IMAGES.ON_BOARD_HAND,
      title: t('home:onBoarding.sender.p2.title'),
      subTitle: t('home:onBoarding.sender.p2.text'),
    },
    {
      img: IMAGES.ON_BOARD_FACES,
      title: t('home:onBoarding.sender.p3.title'),
      subTitle: t('home:onBoarding.sender.p3.text'),
    },
    {
      img: IMAGES.ON_BOARD_BOX,
      title: t('home:onBoarding.sender.p4.title'),
      subTitle: t('home:onBoarding.sender.p4.text'),
    },
  ];

  const onBoardingTravelerData = [
    {
      img: IMAGES.ON_BOARD_CITY,
      title: t('home:onBoarding.traveler.p1.title'),
      subTitle: t('home:onBoarding.traveler.p1.text'),
    },
    {
      img: IMAGES.ON_BOARD_HAND,
      title: t('home:onBoarding.traveler.p2.title'),
      subTitle: t('home:onBoarding.traveler.p2.text'),
    },
    {
      img: IMAGES.ON_BOARD_FACES,
      title: t('home:onBoarding.traveler.p3.title'),
      subTitle: t('home:onBoarding.traveler.p3.text'),
    },
    {
      img: IMAGES.ON_BOARD_BOX,
      title: t('home:onBoarding.traveler.p4.title'),
      subTitle: t('home:onBoarding.traveler.p4.text'),
    },
  ];

  const setIndex = (index: any) => {
    if (index === 3) {
      setNextNavigate(true);
    } else {
      setNextNavigate(false);
    }
  };

  const swiperData = isSender ? onBoardingSenderData : onBoardingTravelerData;

  const handleNavigate = () => {
    const navigationStack = isSender
      ? NAVIGATION.TABS_ROUTE.ORDER_SCREENS
      : NAVIGATION.TABS_ROUTE.MY_TRIPS;

    const navigationRoute = isSender
      ? NAVIGATION.ORDER_SCREENS.CREATE_ORDER
      : NAVIGATION.TRIPS.CREATE_TRIP;

    navigation.navigate(navigationStack, { screen: navigationRoute });
  };

  React.useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <UgetLinkText
          text={t('common:buttons.skip')}
          onPress={handleNavigate}
        />
      ),
    });
  });

  return (
    <>
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapp}>
          <View style={styles.swiper}>
            <Swiper
              ref={swipe}
              loop={false}
              showsButtons={false}
              loadMinimal={true}
              onIndexChanged={(index: number) => setIndex(index)}
              activeDot={<View style={styles.dots} />}
            >
              {swiperData.map((item, index) => (
                <View key={index}>
                  <OnBoardingComponent
                    img={item.img}
                    title={item.title}
                    subTitle={item.subTitle}
                  />
                </View>
              ))}
            </Swiper>
          </View>

          <UgetButton
            type="primary"
            label={t('common:buttons.next')}
            onPress={() => {
              nextNavigate ? handleNavigate() : swipe.current!.scrollBy(1);
            }}
          />
        </View>
      </SafeAreaView>
    </>
  );
});
