import { COLORS } from '../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrapp: {
    flexDirection: 'column',
    height: '90%',
    marginHorizontal: '5%',
  },
  skipBtn: {
    paddingTop: 42,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: 24,
  },
  txtButton: {
    paddingHorizontal: 10,
    color: 'white',
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 18,
  },
  dots: {
    backgroundColor: COLORS.GREY,
    width: 8,
    height: 8,
    borderRadius: 10,
    marginStart: 3,
    marginEnd: 3,
    marginTop: 3,
    marginBottom: 3,
  },
  swiper: {
    height: '80%',
  },
});
