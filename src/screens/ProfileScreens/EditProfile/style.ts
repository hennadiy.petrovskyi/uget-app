import { StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingTop: 20,
  },

  inputsWrapper: {
    marginBottom: 16,
  },

  buttonsWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  imageWrapper: {
    backgroundColor: COLORS.GRAY8,
    borderRadius: 50,
    overflow: 'hidden',
    marginBottom: 32,
    alignSelf: 'center',
  },

  imagePreview: {
    width: 100,
    height: 100,
  },
});
