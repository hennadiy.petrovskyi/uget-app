import React, { memo, useEffect, useState } from 'react';
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  View,
} from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Formik } from 'formik';

import { TouchableOpacity } from 'react-native';
import * as Yup from 'yup';

import {
  PhoneNumber,
  UgetButton,
  TextInput,
  WithDescription,
} from '../../../components';

import { UploadPhoto } from '../../../components/UploadPhoto';
import { getUserProfile, updateProfile } from '../../../api';
import { hydrateUserProfile } from '../../../utils/store/profile/actions';
import { uploadSingleFile } from '../../../api/file';

import styles from './style';
import { setLoaderStatus } from '../../../utils/store/general/actions';

const EditProfile = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const profileDetails = useSelector((state) => state.profile);
  const [isModalOpen, setOpenModal] = useState(false);
  const [isChangesExist, setChangesExistStatus] = useState(false);
  const [outerFormState, setOuterFromState] = useState({});

  const { firstName, lastName, email, phoneNumber, profileImage } =
    profileDetails;

  const initFormValues = {
    firstName,
    lastName,
    email: email.value,
    phoneNumber,
  };

  const inputs = Object.keys(initFormValues);

  const signUpSchema = Yup.object().shape({
    firstName: Yup.string()
      .min(3, t('common:inputs.firstName.errors.min'))
      .max(50, t('common:inputs.firstName.errors.max'))
      .matches(/[a-zA-Z]/, t('common:inputs.firstName.errors.onlyLatin')),
    lastName: Yup.string()
      .min(3, t('common:inputs.lastName.errors.min'))
      .max(50, t('common:inputs.lastName.errors.max'))
      .matches(/[a-zA-Z]/, t('common:inputs.lastName.errors.onlyLatin')),
    email: Yup.string().email(t('common:inputs.email.errors.format')),
    phoneNumber: Yup.object().shape({
      code: Yup.string(),
      countryCode: Yup.string(),
      number: Yup.string(),
      formatted: Yup.string(),
      isValid: Yup.boolean().oneOf([true], 'Invalid number format'),
    }),
  });

  useEffect(() => {
    const profileChanges = Object.keys(
      changesStateTracker(outerFormState),
    ).length;

    setChangesExistStatus(profileChanges ? true : false);
  }, [outerFormState]);

  async function onSubmit(formData, { setErrors }) {
    const profileUpdates = changesStateTracker(formData);

    const { profileImage, ...update } = profileUpdates;

    const goBackAndCloseLoader = () => {
      dispatch(setLoaderStatus(false));
      navigation.goBack();
    };

    const updateUserProfileAndGoBack = () => {
      if (Object.keys(update).length) {
        dispatch(updateProfile(update, goBackAndCloseLoader));
        return;
      }

      goBackAndCloseLoader();

      dispatch(getUserProfile());
    };

    dispatch(setLoaderStatus(true));

    if (!profileImage) {
      updateUserProfileAndGoBack();

      return;
    }

    uploadSingleFile(profileImage.data).then((resp) =>
      updateUserProfileAndGoBack(),
    );
  }

  function handleSetOuterFormState(key, value) {
    setOuterFromState((prev) => ({ ...prev, [key]: value }));
  }

  function changesStateTracker(toCompare: any) {
    let changes = {};

    if (toCompare.email && initFormValues.email !== toCompare.email) {
      changes.email = toCompare.email;
    }

    if (
      toCompare.firstName &&
      initFormValues.firstName !== toCompare.firstName
    ) {
      changes.firstName = toCompare.firstName;
    }

    if (toCompare.lastName && initFormValues.lastName !== toCompare.lastName) {
      changes.lastName = toCompare.lastName;
    }

    if (
      toCompare.phoneNumber &&
      initFormValues.phoneNumber.formatted !== toCompare.phoneNumber.formatted
    ) {
      changes.phoneNumber = toCompare.phoneNumber;
    }

    if (outerFormState.profileImage) {
      changes.profileImage = outerFormState.profileImage;
    }

    return changes;
  }

  function handleInputType(id: string) {
    if (id === 'firstName' || id === 'lastName') {
      return 'text';
    }

    return id;
  }

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView
        behavior="padding"
        enabled={Platform.OS === 'ios'}
        style={styles.container}
      >
        <Formik
          initialValues={{ ...initFormValues, profileImage: null }}
          validationSchema={signUpSchema}
          validateOnChange={false}
          validateOnBlur={true}
          onSubmit={onSubmit}
        >
          {(props) => {
            const {
              values,
              handleSubmit,
              setFieldError,
              setFieldValue,
              errors,
            } = props;

            function savePhoto(photo) {
              handleSetOuterFormState('profileImage', photo);
              setFieldValue('profileImage', photo);
            }

            const renderInputs = () =>
              inputs.map((inputId) => {
                const inputTitle = t(`common:inputs.${inputId}.label`);
                const placeholder = t(`common:inputs.${inputId}.placeholder`);

                let inputComponent;

                if (inputId === 'phoneNumber') {
                  inputComponent = (
                    <PhoneNumber
                      placeholder={placeholder}
                      onChangeText={(value: string) => {
                        setFieldValue(inputId, value);
                        handleSetOuterFormState(inputId, value);

                        if (errors.phoneNumber) {
                          setFieldError('phoneNumber', '');
                        }
                      }}
                      defaultCode={phoneNumber.countryCode}
                      defaultValue={phoneNumber.number}
                      error={errors[inputId]}
                      value={values[inputId].number}
                    />
                  );
                } else {
                  inputComponent = (
                    <TextInput
                      type={
                        handleInputType(inputId)
                        // inputId === 'password' || inputId === 'confirmPassword'
                        //   ? 'password'
                        //   : 'text'
                      }
                      placeholder={placeholder}
                      value={values[inputId]}
                      error={errors[inputId]}
                      onChangeText={(value: string) => {
                        setFieldValue(
                          inputId,
                          inputId === 'email' ? value.toLowerCase() : value,
                        );
                        handleSetOuterFormState(inputId, value);

                        setFieldError(inputId, '');
                      }}
                    />
                  );
                }

                return (
                  <WithDescription
                    key={inputId}
                    title={inputTitle}
                    marginBottom={20}
                  >
                    {inputComponent}
                  </WithDescription>
                );
              });

            const handleShowProfileImage = () => {
              const handleUrl = () => {
                if (values.profileImage) {
                  return values.profileImage.data.uri;
                }

                if (profileImage) {
                  return profileImage.url;
                }
              };

              const uri = handleUrl();

              if (!uri) {
                return <View style={styles.imagePreview} />;
              }

              return (
                <Image
                  style={styles.imagePreview}
                  source={{
                    uri: uri,
                  }}
                />
              );
            };

            return (
              <View>
                <View style={styles.inputsWrapper}>
                  <TouchableOpacity
                    style={styles.imageWrapper}
                    onPress={() => setOpenModal(true)}
                  >
                    {handleShowProfileImage()}
                  </TouchableOpacity>
                  {renderInputs()}
                </View>

                <View style={styles.buttonsWrapper}>
                  <UgetButton
                    disabled={!isChangesExist}
                    type="primary"
                    label={t('profile:edit.save')}
                    onPress={handleSubmit}
                    width={130}
                  />
                  <UgetButton
                    type="outlined"
                    label={t('profile:edit.cancel')}
                    onPress={() => navigation.goBack()}
                    width={130}
                  />
                </View>
                <UploadPhoto
                  isModalOpen={isModalOpen}
                  savePhoto={savePhoto}
                  setOpenModal={setOpenModal}
                />
              </View>
            );
          }}
        </Formik>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default memo(EditProfile);
