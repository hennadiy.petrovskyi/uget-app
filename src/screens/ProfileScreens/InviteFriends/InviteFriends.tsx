import React, { memo } from 'react';
import { Share, Switch, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { ScreenTitle, UgetButton, UgetGreyText } from '../../../components';

import styles from './styles';

export default memo(() => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const userProfile = useSelector((state) => state.profile);

  const onShare = async () => {
    let infoToSHare: { message: string; url?: string } = {
      message: `Cross Border, Uget Delivers.

Uget is a unique delivery platform that allows people to send personal items with travelers already heading in the right direction, and create opportunities for cabin crew to make extra money. 

https://uget.co
      `,
    };

    try {
      const result = await Share.share(infoToSHare);

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <View style={styles.container}>
      <View>
        <ScreenTitle
          text={t('profile:invite.title')}
          align="left"
          marginBottom={8}
        />
        <UgetGreyText text={t('profile:invite.subTitle')} align="left" />
      </View>

      <UgetButton
        label={t('profile:invite.button')}
        type="primary"
        onPress={onShare}
      />
    </View>
  );
});
function alert(message: any) {
  throw new Error('Function not implemented.');
}
