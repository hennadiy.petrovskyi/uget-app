import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingTop: 20,
  },
  userPhoto: {
    width: 64,
    height: 64,
    borderRadius: 50,
    backgroundColor: COLORS.GREY6,
  },
  flexContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  name: {
    fontSize: 26,
    lineHeight: 27,
    fontFamily: 'FiraSans-Bold',
    marginBottom: 8,
  },

  joined: {
    fontFamily: 'FiraSans-Regular',
    fontSize: 14,
    lineHeight: 22,
    color: COLORS.GREY,
  },
  sectionTitle: {
    backgroundColor: COLORS.GREY2,
    fontFamily: 'FiraSans-Regular',
    fontSize: 12,
    lineHeight: 22,
    textTransform: 'uppercase',
    paddingVertical: 4,
    paddingLeft: 45,
    marginBottom: 16,
  },

  verify: {
    fontSize: 14,
    lineHeight: 20,
    textTransform: 'capitalize',
    marginRight: 5,
    fontWeight: '400',
  },

  informationContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  ratingInnerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  flex: { flexDirection: 'row', alignItems: 'center' },
});
