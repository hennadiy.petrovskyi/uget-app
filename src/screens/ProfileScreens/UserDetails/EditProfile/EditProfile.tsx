import React, { memo, useState } from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  View,
} from 'react-native';
import Modal from 'react-native-modal';
import { TouchableOpacity } from 'react-native';
import * as Yup from 'yup';

import { COLORS } from '../../../../config/constants';
import styles from './styles';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Formik } from 'formik';
import {
  PhoneNumber,
  UgetTextInput,
  WithDescription,
} from '../../../../components/UgetInputs';
import { UgetButton } from '../../../../components';

const EditProfile = ({ isModalOpen, setOpenModal }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const initFormValues = {
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: '',
  };

  const inputs = Object.keys(initFormValues);

  const signUpSchema = Yup.object().shape({
    firstName: Yup.string()
      .min(3, t('common:inputs.firstName.errors.min'))
      .max(50, t('common:inputs.firstName.errors.max'))
      .matches(/[a-zA-Z]/, t('common:inputs.firstName.errors.onlyLatin'))
      .required(t('common:inputs.firstName.errors.required')),

    lastName: Yup.string()
      .min(3, t('common:inputs.lastName.errors.min'))
      .max(50, t('common:inputs.lastName.errors.max'))
      .matches(/[a-zA-Z]/, t('common:inputs.lastName.errors.onlyLatin'))
      .required(t('common:inputs.lastName.errors.required')),

    email: Yup.string()
      .email(t('common:inputs.email.errors.format'))
      .required(t('common:inputs.email.errors.required')),

    phoneNumber: Yup.string().required(
      t('common:inputs.phoneNumber.errors.required'),
    ),

    password: Yup.string()
      .min(6, t('common:inputs.password.errors.min'))
      .max(18, t('common:inputs.password.errors.max'))
      .required(t('common:inputs.password.errors.required')),

    confirmPassword: Yup.string()
      .oneOf([Yup.ref('password')], t('common:inputs.password.errors.match'))
      .required(t('common:inputs.password.errors.required')),
  });

  async function onSubmit(formData, { setErrors }) {}

  return (
    <Modal
      isVisible={isModalOpen}
      backdropColor={COLORS.CLEAR_WHITE}
      backdropOpacity={1}
      onBackdropPress={() => setOpenModal(false)}
    >
      <SafeAreaView style={styles.container}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled={Platform.OS === 'ios'}
          style={styles.container}
        >
          <Formik
            initialValues={initFormValues}
            validationSchema={signUpSchema}
            validateOnChange={false}
            validateOnBlur={true}
            onSubmit={onSubmit}
          >
            {(props) => {
              const {
                values,
                handleSubmit,
                setFieldError,
                setFieldValue,
                errors,
              } = props;

              const renderInputs = () =>
                inputs.map((inputId) => {
                  const inputTitle = t(`common:inputs.${inputId}.label`);
                  const placeholder = t(`common:inputs.${inputId}.placeholder`);

                  let inputComponent;

                  if (inputId === 'phoneNumber') {
                    inputComponent = (
                      <PhoneNumber
                        placeholder={placeholder}
                        handleValue={(value: string) => {
                          setFieldValue(inputId, value);

                          if (errors.phoneNumber) {
                            setFieldError('phoneNumber', '');
                          }
                        }}
                        error={errors[inputId]}
                      />
                    );
                  } else {
                    inputComponent = (
                      <UgetTextInput
                        type={
                          inputId === 'password' ||
                          inputId === 'confirmPassword'
                            ? 'password'
                            : 'text'
                        }
                        placeholder={placeholder}
                        value={values[inputId]}
                        error={errors[inputId]}
                        onChangeText={(value: string) => {
                          setFieldValue(
                            inputId,
                            inputId === 'email' ? value.toLowerCase() : value,
                          );
                          setFieldError(inputId, '');
                        }}
                      />
                    );
                  }

                  return (
                    <WithDescription
                      key={inputId}
                      title={inputTitle}
                      marginBottom={20}
                    >
                      {inputComponent}
                    </WithDescription>
                  );
                });

              return (
                <View>
                  {renderInputs()}
                  <UgetButton
                    type="primary"
                    label={t('auth:signUp.btnSubmit')}
                    onPress={handleSubmit}
                    marginBottom={40}
                  />
                </View>
              );
            }}
          </Formik>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </Modal>
  );
};

export default memo(EditProfile);
