import React, { memo, useState } from 'react';
import { Image, Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import {
  Rating,
  RefreshControlScrollView,
  ScreenTitle,
  UgetLinkText,
} from '../../../components';
import { EditProfile } from './EditProfile';

import styles from './styles';
import { EMAIL, PHONE, SUCCESS, WARNING } from '../../../assets/svg';
import { NAVIGATION } from '../../../config/constants';
import { UploadPhoto } from '../../../components/UploadPhoto';
import {
  addLocalNotification,
  setNavigationSession,
} from '../../../utils/store/general/actions';
import { NotificationID } from '../../../components/ModalNotifications/ModalNotificationsProvider';
import {
  getUserProfile,
  resendEmailVerification,
  resendVerificationSmsCode,
  verifyPhoneNumber,
} from '../../../api';

export default memo(() => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const userProfile = useSelector((state) => state.profile);

  const { rating } = userProfile;

  const handleVerificationIcon = (
    isVerified: boolean,
    type: 'phone' | 'email',
  ) => {
    if (isVerified) {
      return <SUCCESS />;
    }

    const handleVerifyEmail = () => {
      const onSuccess = () => {
        dispatch(
          setNavigationSession({
            from: {
              stack: NAVIGATION.UGET_ROUTE.AUTHENTICATION,
              screen: NAVIGATION.PROFILE.PROFILE,
            },
            to: {
              stack: NAVIGATION.UGET_ROUTE.TABS_ROUTE,
              screen: NAVIGATION.PROFILE.PROFILE,
            },
          }),
        );

        navigation.navigate(NAVIGATION.UGET_ROUTE.AUTHENTICATION, {
          screen: NAVIGATION.AUTHENTICATION.VERIFY_EMAIL,
          params: { email: userProfile.email },
        });
      };

      dispatch(
        resendEmailVerification({
          email: userProfile.email.value,
          handleStartCountdown: onSuccess,
        }),
      );
    };

    const handleVerifyPhoneNumber = () => {
      const onSuccess = () => {
        dispatch(
          setNavigationSession({
            from: {
              stack: NAVIGATION.UGET_ROUTE.AUTHENTICATION,
              screen: NAVIGATION.PROFILE.PROFILE,
            },
            to: {
              stack: NAVIGATION.UGET_ROUTE.TABS_ROUTE,
              screen: NAVIGATION.PROFILE.PROFILE,
            },
          }),
        );

        navigation.navigate(NAVIGATION.UGET_ROUTE.AUTHENTICATION, {
          screen: NAVIGATION.AUTHENTICATION.VERIFY_PHONE_NUMBER,
          params: {
            phoneNumber: userProfile.phoneNumber.formatted,
            isReconfirmation: true,
          },
        });
      };

      dispatch(
        resendVerificationSmsCode({
          phoneNumber: userProfile.phoneNumber.formatted,
          handleStartCountdown: onSuccess,
        }),
      );
    };

    const handlers = {
      email: handleVerifyEmail,
      phone: handleVerifyPhoneNumber,
    };

    return (
      <View style={styles.flexContainer}>
        <UgetLinkText
          text={t('profile:profile.sections.information.button')}
          onPress={handlers[type]}
          outerStyles={styles.verify}
        />

        <WARNING />
      </View>
    );
  };

  const ratings = [
    {
      id: 'traveler',
      ...rating.traveler,
    },
    {
      id: 'sender',
      ...rating.sender,
    },
  ];

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return (
          <UgetLinkText
            text={t('profile:profile.edit')}
            onPress={() => navigation.navigate(NAVIGATION.PROFILE.EDIT_PROFILE)}
          />
        );
      },
    });
  }, []);

  const renderRating = ({ id, amount, value }) => {
    const label = t(`profile:profile.sections.rating.${id}`);

    return (
      <View key={id} style={[styles.ratingContainer, { marginBottom: 24 }]}>
        <Text>{label}</Text>

        <View style={styles.ratingInnerContainer}>
          <View style={{ marginRight: 14 }}>
            <Rating value={value} />
          </View>

          <Text>{`(${amount})`}</Text>
        </View>
      </View>
    );
  };

  return (
    <RefreshControlScrollView
      onRefreshControl={() => dispatch(getUserProfile())}
    >
      <View style={styles.container}>
        <View style={[styles.flexContainer, { marginBottom: 24 }]}>
          <View>
            <Text style={styles.name}>
              {userProfile.firstName + ' ' + userProfile.lastName}
            </Text>
            <Text style={styles.joined}>
              {t('profile:profile.joined', {
                date: new Date(userProfile.createdAt).toLocaleDateString(),
              })}
            </Text>
          </View>

          {userProfile.profileImage ? (
            <Image
              style={styles.userPhoto}
              source={{
                uri: userProfile.profileImage.url,
              }}
            />
          ) : (
            <View style={styles.userPhoto} />
          )}
        </View>

        <View>
          <Text style={styles.sectionTitle}>
            {t('profile:profile.sections.information.title')}
          </Text>

          <View style={[styles.informationContainer, { marginBottom: 16 }]}>
            <View style={styles.flex}>
              <PHONE style={{ marginRight: 8 }} />
              <Text>{userProfile.phoneNumber.formatted}</Text>
            </View>

            {handleVerificationIcon(
              userProfile.phoneNumber.isVerified,
              'phone',
            )}
          </View>

          <View style={[styles.informationContainer, { marginBottom: 24 }]}>
            <View style={styles.flex}>
              <EMAIL style={{ marginRight: 8 }} />
              <Text>{userProfile.email.value}</Text>
            </View>
            {handleVerificationIcon(userProfile.email.isVerified, 'email')}
          </View>
        </View>

        <View>
          <Text style={styles.sectionTitle}>
            {t('profile:profile.sections.rating.title')}
          </Text>

          {ratings.map((el) => renderRating(el))}
        </View>
      </View>
    </RefreshControlScrollView>
  );
});
