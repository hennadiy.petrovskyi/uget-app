import React, { memo } from 'react';
import { Switch, Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { ScreenTitle, UgetGreyText } from '../../../components';
import { COLORS } from '../../../config/constants';

import styles from './styles';

import { updateNotificationSettings } from '../../../api';

export default memo(() => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const userProfile = useSelector((state) => state.profile);
  const notificationSettings = useSelector(
    (state) => state.profile.notificationSettings,
  );

  const settings = [
    {
      id: 'email',
      status: notificationSettings.email,
      onChange: () => {
        dispatch(
          updateNotificationSettings({ email: !notificationSettings.email }),
        );
      },
    },
    {
      id: 'push',
      status: notificationSettings.push,
      onChange: () => {
        dispatch(
          updateNotificationSettings({
            push: !notificationSettings.push,
          }),
        );
      },
    },
  ];

  const renderNotificationSettings = ({ id, status, onChange }) => {
    return (
      <View key={id} style={styles.settingContainer}>
        <Text style={styles.settingsLabel}>{t(`profile:settings.${id}`)}</Text>

        <Switch
          trackColor={{ false: COLORS.GRAY8, true: COLORS.MAIN_COLOR }}
          thumbColor={COLORS.CLEAR_WHITE}
          ios_backgroundColor={COLORS.GRAY8}
          onValueChange={onChange}
          value={status}
          style={{ transform: [{ scaleX: 0.8 }, { scaleY: 0.8 }] }}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <ScreenTitle
        text={t('profile:settings.title')}
        align="left"
        marginBottom={8}
      />
      <UgetGreyText
        text={t('profile:settings.subTitle')}
        align="left"
        marginBottom={24}
      />

      {settings.map((el) => renderNotificationSettings(el))}
    </View>
  );
});
