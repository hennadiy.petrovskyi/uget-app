import React, { memo } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { useDispatch, useSelector } from 'react-redux';
import { ScreenTitle, UgetButton, WithDescription } from '../../components';

import { logout } from '../../api';

const Profile = () => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const userProfile = useSelector((state) => state.profile);
  const isAuth = useSelector((state) => state.general.isAuth);

  return (
    <View style={{ flex: 1, backgroundColor: 'white', paddingHorizontal: 50 }}>
      <ScreenTitle text={'Profile page'} marginBottom={24} />
      {isAuth && (
        <>
          {/* <WithDescription
            title={'First Name'}
            description={userProfile.firstName}
            marginBottom={10}
          />

          <WithDescription
            title={'Last name'}
            description={userProfile.lastName}
            marginBottom={20}
          />

          <WithDescription
            title={'Email'}
            description={`${userProfile.email.value} - ${
              userProfile.email.isVerified ? 'verified' : 'not verified'
            }`}
            marginBottom={20}
          />

          <WithDescription
            title={'Phone number'}
            description={`${userProfile.phoneNumber.value} - ${
              userProfile.phoneNumber.isVerified ? 'verified' : 'not verified'
            }`}
            marginBottom={50}
          /> */}

          <UgetButton
            type="warning"
            label={'Logout'}
            onPress={() => dispatch(logout())}
          />
        </>
      )}
    </View>
  );
};

export default memo(Profile);
