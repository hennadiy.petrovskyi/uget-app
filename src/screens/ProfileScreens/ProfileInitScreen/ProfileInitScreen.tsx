import React, { memo, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import {
  ScreenTitle,
  UgetButton,
  UgetLinkText,
  UserPreview,
} from '../../../components';

import styles from './styles';
import { RIGHT_ARROW } from '../../../assets/svg';
import { NAVIGATION } from '../../../config/constants';
import { logout } from '../../../api';
import { ScrollView } from 'react-native-gesture-handler';

export default memo(() => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);
  const userProfile = useSelector((state) => state.profile);

  const profileActionPages = [
    {
      id: 'settings',
      onPress: () => navigation.navigate(NAVIGATION.PROFILE.APP_SETTINGS),
    },
    {
      id: 'help',
      onPress: () => {},
    },
    {
      id: 'invite',
      onPress: () => navigation.navigate(NAVIGATION.PROFILE.INVITE_FRIENDS),
    },
    {
      id: 'rate',
      onPress: () => {},
    },
  ];

  const informationPages = [
    {
      id: 'faq',
      onPress: () => {},
    },
    {
      id: 'policy',
      onPress: () => {},
    },
    {
      id: 'terms',
      onPress: () => {},
    },
    {
      id: 'safety',
      onPress: () => {},
    },
  ];

  const renderField = ({ id, onPress }, path) => {
    const label = t(path + id);

    return (
      <TouchableOpacity
        key={id}
        style={styles.fieldContainer}
        onPress={onPress}
      >
        <Text style={styles.fieldText}>{label}</Text>
        <RIGHT_ARROW />
      </TouchableOpacity>
    );
  };

  return (
    <ScrollView style={styles.container}>
      {isAuth && (
        <>
          <ScreenTitle
            text={t('profile:init.title')}
            align="left"
            marginBottom={21}
          />

          <TouchableOpacity
            style={styles.userPreviewContainer}
            onPress={() => navigation.navigate(NAVIGATION.PROFILE.PROFILE)}
          >
            <UserPreview
              owner={{
                fullName: userProfile.firstName + ' ' + userProfile.lastName,
                rating: userProfile.rating,
                profileImage: userProfile.profileImage,
              }}
            />
            <RIGHT_ARROW />
          </TouchableOpacity>

          <View style={styles.actionScreensContainer}>
            {profileActionPages.map((el) =>
              renderField(el, 'profile:init.actions.'),
            )}
          </View>

          <Text style={styles.sectionTitle}>
            {t('profile:init.information.title')}
          </Text>

          <View style={styles.actionScreensContainer}>
            {informationPages.map((el) =>
              renderField(el, 'profile:init.information.'),
            )}
          </View>

          <UgetButton
            type="warning"
            label="Logout"
            onPress={() => dispatch(logout())}
          />
        </>
      )}
    </ScrollView>
  );
});
