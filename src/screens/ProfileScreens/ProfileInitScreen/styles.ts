import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 24,
  },

  userPreviewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: COLORS.GREY2,
    paddingHorizontal: 16,
    marginBottom: 16,
  },

  fieldContainer: {
    borderBottomWidth: 1,
    borderColor: COLORS.GREY6,
    paddingLeft: 44,
    paddingRight: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  fieldText: {
    fontFamily: 'FiraSans-Medium',
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 22,
    paddingVertical: 8,
  },

  actionScreensContainer: {
    marginBottom: 24,
  },

  sectionTitle: {
    backgroundColor: COLORS.GREY2,
    fontFamily: 'FiraSans-Regular',
    fontSize: 12,
    lineHeight: 22,
    textTransform: 'uppercase',
    paddingVertical: 4,
    paddingLeft: 45,
    marginBottom: 16,
  },
});
