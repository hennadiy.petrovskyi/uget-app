import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrapp: {
    paddingHorizontal: 25,
  },
  title: {
    paddingTop: 20,
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 32,
  },
  orderTitlewrap: {
    paddingTop: 25,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  orderTitle: {
    fontFamily: 'FiraSans-SemiBold',
  },
  orderChange: {
    color: COLORS.MAIN_COLOR,
    fontFamily: 'FiraSans-SemiBold',
  },
  orderContainer: {
    marginTop: 8,
    height: 214,
    backgroundColor: COLORS.GREY2,
  },
  orderHeader: {
    flexDirection: 'row',
    padding: 13,
  },
  img: {
    height: 80,
    width: 80,
    backgroundColor: 'white',
  },
  headerTxtWrap: {
    paddingLeft: 15,
  },
  headerTitle: {
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 16,
  },
  hederSubTitle: {
    fontFamily: 'FiraSans-Regular',
    paddingTop: 8,
  },
  deliveryInfo: {
    paddingLeft: 13,
  },
  deliveryField: {
    flexDirection: 'row',
    paddingTop: 10,
  },
  firstField: {
    fontFamily: 'FiraSans-Regular',
    color: COLORS.GREY,
    width: 45,
  },
  secondField: {
    fontFamily: 'FiraSans-Regular',
  },
  paymentItem: {
    marginTop: 8,
    height: 137,
    backgroundColor: COLORS.GREY2,
  },
  headerPaymentTitle: {
    fontFamily: 'FiraSans-Regular',
  },
  totalPrice: {
    backgroundColor: COLORS.GREEN_LIGHT,
    height: 30,
    marginTop: 18,
  },
  priceTxtWrapp: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 30,
    paddingHorizontal: 8,
  },
  priceTxt: {
    fontFamily: 'FiraSans-SemiBold',
  },
  btn: {
    marginTop: 37,
  },
});
