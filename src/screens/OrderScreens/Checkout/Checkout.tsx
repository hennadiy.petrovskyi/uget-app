import React, { memo, useState } from 'react';
import { Text, View, SafeAreaView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { PrimaryButton, UgetModal } from '../../../components';
import { NAVIGATION } from '../../../config/constants';
import { MASTERCARD } from '../../../assets/svg';
import styles from './style';

export default memo(() => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  const [openModal, setOpenModal] = useState(false);

  const toggleModal = () => {
    setOpenModal(!openModal);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapp}>
        <Text style={styles.title}>{t('order:checkout.title')}</Text>
        <View style={styles.orderTitlewrap}>
          <Text style={styles.orderTitle}>{t('order:checkout.yourOrder')}</Text>
          <Text
            style={styles.orderChange}
            onPress={() =>
              navigation.navigate(NAVIGATION.ORDER_SCREENS.MY_ORDERS)
            }
          >
            {t('common:buttons.change')}
          </Text>
        </View>

        <View style={styles.orderContainer}>
          <View style={styles.orderHeader}>
            <View style={styles.img} />
            <View style={styles.headerTxtWrap}>
              <Text style={styles.headerTitle}>Creme (50ml)</Text>
              <Text style={styles.hederSubTitle}>Cosmetics — 1 item</Text>
              <Text style={styles.hederSubTitle}>Up to 0.5 kg</Text>
            </View>
          </View>
          <View style={styles.deliveryInfo}>
            <View style={styles.deliveryField}>
              <Text style={styles.firstField}>From</Text>
              <Text style={styles.secondField}>Berlin, Germany</Text>
            </View>
            <View style={styles.deliveryField}>
              <Text style={styles.firstField}>To</Text>
              <Text style={styles.secondField}>Dubai, UAE</Text>
            </View>
            <View style={styles.deliveryField}>
              <Text style={styles.firstField}>By</Text>
              <Text style={styles.secondField}>Jul 20, 2021</Text>
            </View>
          </View>
        </View>
        <View style={styles.orderTitlewrap}>
          <Text style={styles.orderTitle}>
            {t('order:checkout.paymentDetails')}
          </Text>
          <Text style={styles.orderChange} onPress={() => navigation.goBack()}>
            {t('common:buttons.change')}
          </Text>
        </View>

        <View style={styles.paymentItem}>
          <View style={styles.orderHeader}>
            <MASTERCARD />
            <View style={styles.headerTxtWrap}>
              <Text style={styles.headerPaymentTitle}>
                Debit Mastercard (2454)
              </Text>
              <Text style={styles.hederSubTitle}>Exp. 09/24</Text>
              <Text style={styles.hederSubTitle}>Max Anders</Text>
            </View>
          </View>
          <View style={styles.totalPrice}>
            <View style={styles.priceTxtWrapp}>
              <Text style={styles.priceTxt}>{t('order:checkout.price')}</Text>
              <Text style={styles.priceTxt}>40 USD</Text>
            </View>
          </View>
        </View>
        <PrimaryButton
          txt={t('order:buttons.placeOrder')}
          style={styles.btn}
          onPress={toggleModal}
        />
      </View>

      {/* <UgetModal
        isVisible={openModal}
        toggleModal={toggleModal}
        title={`${t('order:orderModal.title')} 🎉`}
        textContent={`${t('order:orderModal.text1')} #42284 ${t(
          'order:orderModal.text2',
        )}`}
        btnTxt={t('order:orderModal.button')}
        btnOnPress={() => {
          navigation.navigate(NAVIGATION.ORDER_SCREENS.MY_ORDERS);
        }}
      /> */}
    </SafeAreaView>
  );
});
