import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 24,
    width: '100%',
  },
  wrapper: {
    paddingTop: 25,
  },
  cartWrap: {},

  title: {
    fontSize: 32,
    fontFamily: 'FiraSans-SemiBold',
  },

  directionWrapper: {
    marginBottom: 24,
  },

  direction: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 8,
  },

  iconPlane: {
    marginHorizontal: 21,
  },

  directionTxt: {
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 16,
    lineHeight: 19,
  },

  dateTxt: {
    fontFamily: 'FiraSans-Regular',
    color: COLORS.GREY,
    fontSize: 14,
    lineHeight: 17,
  },

  tripsListContainer: {},
});
