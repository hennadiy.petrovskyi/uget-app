import React, { memo, useEffect, useState } from 'react';
import { View, SafeAreaView, ScrollView, RefreshControl } from 'react-native';
import { useRoute } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import {
  OrderItem,
  ScreenTitle,
  TripForOrder,
  UgetTabs,
} from '../../../components';
import { getOrderRequests, getTripsByOrderParams } from '../../../api';
import { OrderViewerType, OrderViewType } from '../../../types';
import styles from './style';
import { tripsSetFilteredTrips } from '../../../utils/store/trips/actions';

export default memo(() => {
  const { t } = useTranslation();
  const route = useRoute();
  const dispatch = useDispatch();
  const requests = useSelector((state) => state.trips.filteredByOrder);
  const { params } = route;

  useEffect(() => {
    const { orderUUID, trips } = params;

    if (!orderUUID || !trips) {
      return;
    }

    getOrderRequests(orderUUID, trips).then((data) => {
      dispatch(tripsSetFilteredTrips(data));
    });
  }, []);

  const tabs = [
    {
      label: t('order:tabs.incoming.label', {
        amount: requests.filter((el) => el.status === 'incoming').length,
      }),
      isActive: true,
      filter: 'incoming',
      emptyText: t('order:tabs.incoming.emptyText'),
    },
    {
      label: t('order:tabs.outgoing.label', {
        amount: requests.filter((el) => el.status === 'outgoing').length,
      }),
      isActive: true,
      filter: 'outgoing',
      emptyText: t('order:tabs.incoming.emptyText'),
    },
  ];

  console.log(requests);

  const renderItem = (item, key, setFilter) => {
    return <TripForOrder key={key} trip={item} orderUUID={params.orderUUID} />;
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapper}>
        <ScreenTitle
          text={t('order:requests.title')}
          align="left"
          marginBottom={24}
        />
      </View>
      <UgetTabs tabs={tabs} data={requests} renderItem={renderItem} />
    </SafeAreaView>
  );
});
