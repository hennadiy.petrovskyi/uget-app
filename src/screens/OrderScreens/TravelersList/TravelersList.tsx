import React, { memo, useEffect, useState } from 'react';
import { View, SafeAreaView, ScrollView, RefreshControl } from 'react-native';
import { useFocusEffect, useRoute } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import { EmptyTab, RoutePreview, TripForOrder } from '../../../components';
import { getTripsByOrderParams } from '../../../api';
import styles from './style';
import { useDispatch, useSelector } from 'react-redux';
import { tripsSetFilteredTrips } from '../../../utils/store/trips/actions';

export default memo(() => {
  const { t } = useTranslation();
  const route = useRoute();
  const dispatch = useDispatch();
  const trips = useSelector((state) => state.trips.filteredByOrder);

  const [screenParams, setParams] = useState({
    orderUUID: '',
    from: '',
    to: '',
    deliveryDate: '',
  });

  const { params } = route;

  // useEffect(() => {}, []);

  useFocusEffect(
    React.useCallback(() => {
      if (params) {
        const { locations, deliveryDate, uuid } = params;

        setParams({
          from: locations.pickup.city.label,
          to: locations.endpoint.city.label,
          deliveryDate,
          orderUUID: uuid,
        });

        dispatch(
          getTripsByOrderParams({
            deliveryDate,
            from: locations.pickup.city.id,
            to: locations.endpoint.city.id,
            uuid,
          }),
        );
      }

      return () => {
        dispatch(tripsSetFilteredTrips([]));
      };
    }, []),
  );

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapp}>
        {params && (
          <RoutePreview
            from={screenParams.from}
            to={screenParams.to}
            date={new Date(screenParams.deliveryDate).toDateString()}
            marginBottom={24}
          />
        )}

        <ScrollView style={styles.tripsListContainer}>
          {trips.length ? (
            trips.map((trip, i) => {
              const isLastChild = i + 1 === trips.length;
              return (
                <TripForOrder
                  key={trip.uuid}
                  trip={trip}
                  orderUUID={screenParams.orderUUID}
                  marginBottom={isLastChild ? 50 : 10}
                />
              );
            })
          ) : (
            <EmptyTab text={t('order:travelersList.emptyText')} />
          )}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
});
