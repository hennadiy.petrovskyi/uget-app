import React, { memo, ReactNode, useEffect, useReducer, useState } from 'react';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import uuid from 'react-native-uuid';

import { ButtonGoBack, OrderProgress, ScreenTitle } from '../../../components';
import { PickUp } from './PickUp';
import { DeliveryOrder } from './Delivery';
import { SummaryOrder } from './Summary';
import { ItemOrder } from './Item';
import { SelectType } from './Type';

import { useDispatch } from 'react-redux';
import {
  addLocalNotification,
  setLoaderStatus,
} from '../../../utils/store/general/actions';
import { NotificationID } from '../../../components/ModalNotifications/ModalNotificationsProvider';

import {
  defaultOrder,
  orderContextHandler,
  OrderContext,
  Order,
} from './context';

import styles from './style';
import { OrderStatus } from './context/context';
import { asyncStorage, Keys } from '../../../utils/asyncStorage';
import { NAVIGATION } from '../../../config/constants';
import { addDraft } from '../../../utils/store/orders/actions';
import { SET_STATE_FROM_SAVED_DATA } from './context/actions';

const screenList = ['type', 'item', 'pickUp', 'delivery', 'summary'];

export default memo(() => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [screenCount, setScreenCount] = useState(0);
  const [order, setOrderFields] = useReducer(orderContextHandler, defaultOrder);

  const nextScreen = () => {
    setScreenCount(screenCount + 1);
  };

  const previousScreen = () => {
    setScreenCount(screenCount - 1);
  };

  const providerValue: {
    order: Order;
    setOrderFields: (type: any, payload: any) => void | any;
  } = { order, setOrderFields };

  const Screens = (key: string) => {
    const screens: Record<string, ReactNode> = {
      type: <SelectType nextScreen={nextScreen} />,
      item: <ItemOrder nextScreen={nextScreen} />,
      pickUp: <PickUp nextScreen={nextScreen} />,
      delivery: <DeliveryOrder nextScreen={nextScreen} />,
      summary: <SummaryOrder saveAsDraft={saveAsDraft} />,
    };
    return screens[key] || <SummaryOrder />;
  };

  const isSummaryScreen = screenCount === 4;

  useEffect(() => {
    asyncStorage.get(Keys.SAVED_FORM).then((savedForm) => {
      if (!savedForm) {
        return;
      }

      setOrderFields({
        type: SET_STATE_FROM_SAVED_DATA,
        payload: savedForm.order,
      });

      setScreenCount(savedForm.lastScreen);
    });
  }, []);

  React.useEffect(() => {
    if (screenCount > 0) {
      navigation.setOptions({
        headerLeft: () => (
          <ButtonGoBack handleGoBack={() => previousScreen()} />
        ),
        headerRight: handleSaveFormForFuture,
      });
    } else {
      navigation.setOptions({
        headerBackVisible: false,
        headerLeft: () => (
          <ButtonGoBack
            handleGoBack={() =>
              navigation.navigate(NAVIGATION.TABS_ROUTE.ORDER_SCREENS, {
                screen: NAVIGATION.ORDER_SCREENS.MY_ORDERS,
              })
            }
          />
        ),
        headerRight: () => null,
      });
    }
  });

  async function saveAsDraft() {
    const orderToSave = {
      ...order,
      uuid: uuid.v4(),
      status: OrderStatus.DRAFT,
    };

    dispatch(setLoaderStatus(true));

    const drafts = (await asyncStorage.get(Keys.DRAFTS)) || [];

    drafts.push(orderToSave);

    await asyncStorage.save(Keys.DRAFTS, drafts);

    await asyncStorage.delete(Keys.SAVED_FORM);

    dispatch(addDraft(orderToSave));

    dispatch(setLoaderStatus(false));

    navigation.reset({
      index: 0,
      routes: [{ name: NAVIGATION.ORDER_SCREENS.MY_ORDERS }],
    });
  }

  async function saveFormForFuture() {
    await asyncStorage.save(Keys.SAVED_FORM, {
      order,
      lastScreen: screenCount,
    });

    navigation.reset({
      index: 0,
      routes: [{ name: NAVIGATION.ORDER_SCREENS.MY_ORDERS }],
    });
  }

  function handleSaveFormForFuture() {
    return (
      <TouchableOpacity
        onPress={() =>
          dispatch(
            addLocalNotification({
              id: NotificationID.ORDER_CANCEL,
              options: {
                isOrderFilled: isSummaryScreen,
                lastScreen: screenCount,
                saveAsDraft: isSummaryScreen ? saveAsDraft : null,
                saveForm: !isSummaryScreen ? saveFormForFuture : null,
                resetForm: handleResetForm,
              },
            }),
          )
        }
      >
        <Text style={styles.headerTxt}>{t('common:buttons.cancel')}</Text>
      </TouchableOpacity>
    );
  }

  async function handleResetForm() {
    setOrderFields({
      type: SET_STATE_FROM_SAVED_DATA,
    });

    const savedForm = await asyncStorage.get(Keys.SAVED_FORM);

    if (savedForm) {
      await asyncStorage.delete(Keys.SAVED_FORM);
    }

    navigation.reset({
      index: 0,
      routes: [{ name: NAVIGATION.ORDER_SCREENS.MY_ORDERS }],
    });
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
      style={styles.container}
    >
      <SafeAreaView>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          contentContainerStyle={{ paddingBottom: 60 }}
          style={{ height: '100%' }}
        >
          <View
            style={[styles.wrapper, screenCount !== 0 && styles.withPadding]}
          >
            <ScreenTitle
              text={t('order:title')}
              align={screenCount > 0 ? 'left' : 'center'}
              marginBottom={18}
            />
            {screenCount > 0 && (
              <OrderProgress list={screenList} active={screenCount} />
            )}
            <OrderContext.Provider value={providerValue}>
              {Screens(screenList[screenCount])}
            </OrderContext.Provider>
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
});
