import React, { memo, useContext } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageSourcePropType,
} from 'react-native';

import { useTranslation } from 'react-i18next';

import { UgetButton, UgetGreyText } from '../../../../components';

import { IMAGES } from '../../../../assets';
import styles from './style';
import { OrderContext } from '../context';
import { SET_ORDER_TYPE } from '../context/actions';

export default memo(({ nextScreen }) => {
  const { t } = useTranslation();
  const { order, setOrderFields } = useContext(OrderContext);

  const options: {
    id: 'parcel' | 'document';
    image: ImageSourcePropType;
    label: string;
  }[] = [
    {
      id: 'parcel',
      image: IMAGES.PARCEL,
      label: t('order:typeOfOrder.parcel'),
    },
    {
      id: 'document',
      image: IMAGES.DOCUMENTS,
      label: t('order:typeOfOrder.documents'),
    },
  ];

  function selectType(id: 'parcel' | 'document') {
    setOrderFields({
      type: SET_ORDER_TYPE,
      payload: id,
    });
  }

  const renderOptions = () => {
    return options.map((option) => (
      <TouchableOpacity
        key={option.id}
        onPress={() => selectType(option.id)}
        style={[
          styles.imageContainer,
          order.details.type === option.id && styles.optionSelected,
        ]}
      >
        <Image
          style={styles.image}
          resizeMode="contain"
          source={option.image}
        />
        <Text style={styles.selectText}>{option.label}</Text>
      </TouchableOpacity>
    ));
  };

  return (
    <View style={styles.innerContainer}>
      <UgetGreyText text={t('order:typeOfOrder.text')} marginBottom={88} />

      <View style={styles.optionsContainer}>{renderOptions()}</View>

      <UgetButton
        type="primary"
        label={t('common:buttons.next')}
        onPress={() => nextScreen()}
      />
    </View>
  );
});
