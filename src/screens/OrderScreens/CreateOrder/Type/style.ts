import { COLORS } from '../../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  innerContainer: {
    paddingHorizontal: 24,
  },
  optionsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 90,
  },

  imageContainer: {
    width: '45%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 180,
    backgroundColor: COLORS.GREY2,
    borderRadius: 6,
    borderWidth: 3,
    borderColor: 'transparent',
  },

  optionSelected: {
    borderColor: COLORS.SELECTED_GREEN,
  },

  image: {
    height: 60,
  },

  selectText: {
    fontSize: 16,
    fontFamily: 'FiraSans-SemiBold',
    color: COLORS.BLACK,
    marginTop: 20,
  },
  btn: {
    marginTop: 90,
  },
});
