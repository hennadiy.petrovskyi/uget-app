import React, { memo, useContext } from 'react';
import { View } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useTranslation } from 'react-i18next';

import { UgetButton } from '../../../../components';

import {
  PhoneNumber,
  TextInput,
  WithDescription,
  Calendar,
  Location,
} from '../../../../components/UgetInputs';

import {
  SET_DELIVERY_AREA,
  SET_DELIVERY_CITY,
  SET_DELIVERY_COUNTRY,
  SET_DELIVERY_DATE,
  SET_DELIVERY_PRICE,
  SET_THIRD_PARTY_PHONE_NUMBER,
} from '../context/actions';
import { OrderContext } from '../context';

import styles from './style';
import { defaultDeliveryDate, OwnerRole } from '../context/context';
import { calculateDeliveryPrice } from '../../../../api';
import moment from 'moment';
import { generateDateWithOffset } from '../../../../utils/helpers';

export default memo(({ nextScreen }: { nextScreen: any }) => {
  const { t } = useTranslation();
  const { order, setOrderFields } = useContext(OrderContext);

  const createOrderSchema =
    order.ownerRole === OwnerRole.SENDER
      ? Yup.object().shape({
          deliveryDate: Yup.string().required(
            t('order:deliveryStep.inputs.country.errors.required'),
          ),
          area: Yup.string().required(
            t('order:deliveryStep.inputs.area.errors.required'),
          ),
          contactPhoneNumber: Yup.object()
            .shape({
              code: Yup.string(),
              countryCode: Yup.string(),
              number: Yup.string().required('Phone number is required'),
              formatted: Yup.string(),
              isValid: Yup.boolean().oneOf([true], 'Invalid number format'),
            })
            .required(),
        })
      : Yup.object().shape({
          deliveryDate: Yup.string().required(
            t('order:deliveryStep.inputs.country.errors.required'),
          ),
          area: Yup.string().required(
            t('order:deliveryStep.inputs.area.errors.required'),
          ),
          contactPhoneNumber: Yup.object().nullable(true),
        });

  function onSubmit() {
    const { endpoint, pickup, details } = order.locations;

    calculateDeliveryPrice({
      from: pickup.country.id,
      to: endpoint.country.id,
      weight: order.details.size,
    }).then((response) => {
      if (!response) {
        return;
      }

      setOrderFields({
        type: SET_DELIVERY_PRICE,
        payload: response,
      });

      nextScreen();
    });
  }

  return (
    <Formik
      initialValues={{
        ...order.locations.endpoint,
        deliveryDate: order.deliveryDate,
        contactPhoneNumber: order.thirdPartyPhoneNumber,
      }}
      validationSchema={createOrderSchema}
      validateOnChange={false}
      onSubmit={onSubmit}
    >
      {(props) => {
        const {
          handleSubmit,
          handleChange,
          errors,
          setFieldValue,
          setFieldError,
          values,
        } = props;

        function setCountry(value: string) {
          console.log(value);

          setFieldValue('country', value);
          setOrderFields({
            type: SET_DELIVERY_COUNTRY,
            payload: value,
          });
        }

        function setCity(value: string) {
          setFieldValue('city', value);
          setOrderFields({
            type: SET_DELIVERY_CITY,
            payload: value,
          });
        }

        function setArea(value: string) {
          setFieldValue('area', value);
          setFieldError('area', '');

          setOrderFields({
            type: SET_DELIVERY_AREA,
            payload: value,
          });
        }

        function setReceiverPhoneNumber(value: string) {
          if (errors.contactPhoneNumber) {
            setFieldError('contactPhoneNumber', '');
          }

          setFieldValue('contactPhoneNumber', value);
          setOrderFields({
            type: SET_THIRD_PARTY_PHONE_NUMBER,
            payload: value,
          });
        }

        function setDeliveryDate(value: string) {
          const parsedDate = new Date(value).toISOString();

          setOrderFields({
            type: SET_DELIVERY_DATE,
            payload: parsedDate,
          });
          setFieldValue('deliveryDate', parsedDate);
        }

        console.log(values.deliveryDate);

        return (
          <View style={styles.container}>
            <View style={styles.wrapper}>
              <WithDescription
                title={t('order:deliveryStep.inputs.country.label')}
                marginBottom={20}
              >
                <Location
                  isDisabled={true}
                  placeholder={t('order:pickupStep.inputs.country.placeholder')}
                  current={values.country}
                  type="country"
                  handleValue={setCountry}
                />
              </WithDescription>

              <WithDescription
                title={t('order:deliveryStep.inputs.city.label')}
                marginBottom={20}
              >
                <Location
                  isDisabled={true}
                  placeholder={t('order:pickupStep.inputs.city.placeholder')}
                  type="city"
                  current={values.city}
                  handleValue={setCity}
                  params={{
                    countryId: values.country ? values.country.id : '',
                  }}
                />
              </WithDescription>

              <WithDescription
                title={t('order:deliveryStep.inputs.area.label')}
                marginBottom={20}
              >
                <TextInput
                  placeholder={t('order:deliveryStep.inputs.area.placeholder')}
                  onChangeText={setArea}
                  value={values.area}
                  error={errors.area}
                />
              </WithDescription>

              <WithDescription
                title={t('order:deliveryStep.inputs.date.label')}
                marginBottom={20}
              >
                <Calendar
                  minDate={moment(generateDateWithOffset(undefined, 2)).format(
                    'yyyy-MM-DD',
                  )}
                  placeholder={t('order:deliveryStep.inputs.date.placeholder')}
                  onPress={setDeliveryDate}
                  value={values.deliveryDate}
                />
              </WithDescription>

              {order.ownerRole === OwnerRole.SENDER && (
                <WithDescription
                  title={t('order:deliveryStep.inputs.contactPhone.label')}
                  description={t(
                    'order:deliveryStep.inputs.contactPhone.description',
                  )}
                  marginBottom={20}
                >
                  <PhoneNumber
                    value={values.contactPhoneNumber.number}
                    defaultCode={values.contactPhoneNumber.countryCode}
                    onChangeText={setReceiverPhoneNumber}
                    error={errors.contactPhoneNumber}
                  />
                </WithDescription>
              )}

              <UgetButton
                type="primary"
                label={t('common:buttons.next')}
                onPress={handleSubmit}
              />
            </View>
          </View>
        );
      }}
    </Formik>
  );
});
