import { COLORS } from '../../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrapper: {
    flexDirection: 'column',
    paddingTop: 27,
  },

  section: {
    paddingVertical: 15,
  },

  sectionWithBorder: {
    borderBottomWidth: 1,
    borderBottomColor: COLORS.GREY3,
  },

  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  rowMarginBottom: {
    marginBottom: 8,
  },

  rowLabel: {
    fontFamily: 'FiraSans-Regular',
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '500',
  },

  rowValue: {
    fontFamily: 'FiraSans-Regular',
  },

  price: {
    height: 30,
    backgroundColor: COLORS.GREEN_LIGHT,
    marginBottom: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
});
