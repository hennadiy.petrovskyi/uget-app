import React, { memo, useContext } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import uuid from 'react-native-uuid';

import { OrderDetails, UgetButton } from '../../../../components';
import { NotificationID } from '../../../../components/ModalNotifications/ModalNotificationsProvider';

import { OrderContext } from '../context';
import { OrderStatus } from '../context/context';
import { NAVIGATION } from '../../../../config/constants';
import { asyncStorage, Keys } from '../../../../utils/asyncStorage';
import { addDraft } from '../../../../utils/store/orders/actions';
import {
  addLocalNotification,
  setLoaderStatus,
} from '../../../../utils/store/general/actions';
import { getMyOrders, publishOrder } from '../../../../api';

import styles from './style';
import { OrderViewType } from '../../../../types';

export default memo(({ saveAsDraft }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { order } = useContext(OrderContext);
  const isAuth = useSelector((state) => state.general.isAuth);

  async function handleSubmitOrder() {
    if (!isAuth) {
      await asyncStorage.save(Keys.ORDER_TO_PUBLISH, order);

      dispatch(
        addLocalNotification({
          id: NotificationID.AUTHORIZE_TO_PUBLISH,
          options: { type: 'order' },
        }),
      );
      return;
    }

    dispatch(setLoaderStatus(true));

    dispatch(publishOrder(order, onSuccess));
  }

  async function onSuccess() {
    await asyncStorage.delete(Keys.SAVED_FORM);

    dispatch(setLoaderStatus(false));

    dispatch(addLocalNotification({ id: NotificationID.ORDER_PUBLISHED }));

    navigation.reset({
      index: 0,
      routes: [{ name: NAVIGATION.ORDER_SCREENS.MY_ORDERS }],
    });
  }

  return (
    <View style={styles.wrapper}>
      <OrderDetails order={order} view={OrderViewType.PREVIEW} />

      <UgetButton
        type="primary"
        label={t('order:buttons.publishOrder')}
        marginBottom={16}
        onPress={handleSubmitOrder}
      />

      <UgetButton
        type="outlined"
        label={t('order:buttons.saveAsDraft')}
        marginBottom={40}
        onPress={saveAsDraft}
      />
    </View>
  );
});
