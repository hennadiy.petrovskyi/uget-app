import {
  SET_ITEM_NAME,
  SET_ITEM_SIZE,
  SET_ITEM_QUANTITY,
  SET_ORDER_DESCRIPTION,
  ADD_ORDER_PHOTO,
  REMOVE_ORDER_PHOTO,
  SET_ORDER_OWNER_ROLE,
  SET_ORDER_TYPE,
  SET_PICKUP_COUNTRY,
  SET_PICKUP_CITY,
  SET_PICKUP_AREA,
  SET_THIRD_PARTY_PHONE_NUMBER,
  SET_DELIVERY_COUNTRY,
  SET_DELIVERY_CITY,
  SET_DELIVERY_AREA,
  SET_DELIVERY_DATE,
  SET_STATE_FROM_SAVED_DATA,
  SET_DELIVERY_PRICE,
} from './actions';
import {
  ItemSize,
  OrderLocationType,
  OrderStatus,
  OrderType,
  OwnerRole,
  PhoneNumberType,
} from './context';

export const orderContextHandler = (state, action) => {
  switch (action.type) {
    case SET_ORDER_OWNER_ROLE:
      return {
        ...state,
        ownerRole: action.payload,
        thirdPartyPhoneNumber: {},
      };

    case SET_ORDER_TYPE:
      return {
        ...state,
        details: {
          ...state.details,
          type: action.payload,
        },
      };

    case SET_ITEM_NAME:
      return {
        ...state,
        details: {
          ...state.details,
          name: action.payload,
        },
      };

    case SET_ITEM_SIZE:
      return {
        ...state,
        details: {
          ...state.details,
          size: action.payload,
        },
      };

    case SET_ITEM_QUANTITY:
      return {
        ...state,
        details: {
          ...state.details,
          quantity: action.payload,
        },
      };

    case SET_ORDER_DESCRIPTION:
      return {
        ...state,
        details: {
          ...state.details,
          description: action.payload,
        },
      };

    case ADD_ORDER_PHOTO:
      return {
        ...state,
        photos: state.photos.concat([action.payload]),
      };

    case REMOVE_ORDER_PHOTO:
      return {
        ...state,
        photos: state.photos.filter((photo) => photo.id !== action.payload),
      };

    case SET_DELIVERY_PRICE:
      return {
        ...state,
        deliveryPrice: action.payload,
      };

    case SET_PICKUP_COUNTRY:
      return {
        ...state,
        locations: {
          ...state.locations,
          pickup: {
            ...state.locations.pickup,
            country: action.payload,
          },
        },
      };

    case SET_PICKUP_CITY:
      return {
        ...state,
        locations: {
          ...state.locations,
          pickup: {
            ...state.locations.pickup,
            city: action.payload,
          },
        },
      };

    case SET_PICKUP_AREA:
      return {
        ...state,
        locations: {
          ...state.locations,
          pickup: {
            ...state.locations.pickup,
            area: action.payload,
          },
        },
      };

    case SET_THIRD_PARTY_PHONE_NUMBER:
      return {
        ...state,
        thirdPartyPhoneNumber: action.payload,
      };

    case SET_DELIVERY_COUNTRY:
      return {
        ...state,
        locations: {
          ...state.locations,
          endpoint: {
            ...state.locations.endpoint,
            country: action.payload,
          },
        },
      };

    case SET_DELIVERY_CITY:
      return {
        ...state,
        locations: {
          ...state.locations,
          endpoint: {
            ...state.locations.endpoint,
            city: action.payload,
          },
        },
      };

    case SET_DELIVERY_AREA:
      return {
        ...state,
        locations: {
          ...state.locations,
          endpoint: {
            ...state.locations.endpoint,
            area: action.payload,
          },
        },
      };

    case SET_DELIVERY_DATE:
      return {
        ...state,
        deliveryDate: action.payload,
      };

    case SET_STATE_FROM_SAVED_DATA:
      return {
        ...state,
        ...action.payload,
      };

    case SET_STATE_FROM_SAVED_DATA:
      return {
        ...state,
        uuid: null,
        ownerRole: OwnerRole.SENDER,
        status: OrderStatus.DRAFT,
        deliveryDate: Date.now(),
        photos: [],
        details: {
          name: '',
          quantity: 1,
          size: ItemSize.A,
          description: '',
          type: OrderType.PARCEL,
        },
        thirdPartyPhoneNumber: {},
        locations: {
          pickup: {
            type: OrderLocationType.PICKUP,
            country: null,
            city: null,
            area: '',
          },
          endpoint: {
            type: OrderLocationType.ENDPOINT,
            country: { id: 'ChIJvRKrsd9IXj4RpwoIwFYv0zM', label: 'UAE' },
            city: { id: 'ChIJRcbZaklDXz4RYlEphFBu5r0', label: 'Dubai' },
            area: '',
          },
        },
        deliveryPrice: null,
      };

    default:
      return state;
  }
};
