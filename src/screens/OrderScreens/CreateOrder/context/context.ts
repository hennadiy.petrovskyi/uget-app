import uuid from 'react-native-uuid';
import React from 'react';
import { generateDateWithOffset } from '../../../../utils/helpers';

export enum PhoneNumberType {
  ACCOUNT_OWNER = 'ACCOUNT_OWNER',
  CONTACT_PERSON = 'CONTACT_PERSON',
}

interface ILocation {
  id: string;
  label: string;
}

type TLocation = {
  type: OrderLocationType;
  country: ILocation | null;
  city: ILocation | null;
  area: string;
};

type thirdPartyPhoneNumber =
  | {
      code: string;
      countryCode: string;
      formatted: string;
      isValid: boolean;
      number: string;
      type: PhoneNumberType.CONTACT_PERSON;
    }
  | {};

export enum ItemSize {
  A = 0.5,
  B = 1,
  C = 1.5,
  D = 2,
  E = 2.5,
  F = 3,
  G = 3.5,
  H = 4,
  I = 4.5,
  J = 5,
  K = 5.5,
  L = 6,
}

export enum OwnerRole {
  SENDER = 'SENDER',
  RECEIVER = 'RECEIVER',
}

export enum OrderType {
  DOCUMENTS = 'document',
  PARCEL = 'parcel',
}

export enum OrderStatus {
  ACCEPTED = 'accepted',
  DRAFT = 'draft',
  EXPIRED = 'expired',
  REQUEST_INCOMING = 'request.incoming',
}

export enum OrderLocationType {
  PICKUP = 'PICKUP',
  ENDPOINT = 'ENDPOINT',
}

const date = new Date();

export class Order {
  uuid: string | null;
  ownerRole: OwnerRole;
  deliveryDate: string | Date;
  status: OrderStatus;
  photos: any[];
  thirdPartyPhoneNumber: thirdPartyPhoneNumber;
  details: {
    name: string;
    quantity: number;
    size: any;
    description: string;
    type: OrderType;
  };
  locations: {
    pickup: TLocation | null;
    endpoint: TLocation;
  };
  deliveryPrice: {
    value: string;
    currency: any;
  } | null;

  constructor() {
    this.uuid = null;
    this.ownerRole = OwnerRole.SENDER;
    this.status = OrderStatus.DRAFT;
    this.deliveryDate = generateDateWithOffset(undefined, 2);
    this.photos = [];
    this.details = {
      name: '',
      quantity: 1,
      size: ItemSize.A,
      description: '',
      type: OrderType.PARCEL,
    };
    this.thirdPartyPhoneNumber = {};
    this.locations = {
      pickup: {
        type: OrderLocationType.PICKUP,
        country: null,
        city: null,
        area: '',
      },
      endpoint: {
        type: OrderLocationType.ENDPOINT,
        country: { id: 'ChIJvRKrsd9IXj4RpwoIwFYv0zM', label: 'UAE' },
        city: { id: 'ChIJRcbZaklDXz4RYlEphFBu5r0', label: 'Dubai' },
        area: '',
      },
    };
    this.deliveryPrice = null;
  }
}

export const defaultOrder = new Order();
export const OrderContext = React.createContext(defaultOrder);
