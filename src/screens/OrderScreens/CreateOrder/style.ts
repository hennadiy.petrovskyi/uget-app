import { StyleSheet } from 'react-native';
import { COLORS } from '../../../config/constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerTxt: {
    fontSize: 16,
    fontFamily: 'FiraSans-SemiBold',
  },
  backBtnWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnText: {
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 16,
    color: COLORS.BLACK,
    marginLeft: 8,
  },
  header: {
    fontSize: 32,
    fontFamily: 'FiraSans-SemiBold',
    paddingTop: 25,
    paddingBottom: 20,
  },
  wrapper: {
    flexDirection: 'column',
  },

  withPadding: {
    paddingHorizontal: 24,
  },
});
