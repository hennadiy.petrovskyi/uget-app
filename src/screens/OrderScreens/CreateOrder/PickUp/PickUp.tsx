import React, { memo, useContext, useState } from 'react';
import { Text, View } from 'react-native';
import { useTranslation } from 'react-i18next';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { UgetButton, WithDescription } from '../../../../components';
import {
  Location,
  PhoneNumber,
  TextInput,
} from '../../../../components/UgetInputs';

import {
  SET_PICKUP_AREA,
  SET_PICKUP_CITY,
  SET_PICKUP_COUNTRY,
  SET_THIRD_PARTY_PHONE_NUMBER,
} from '../context/actions';

import { OrderContext } from '../context';
import styles from './style';
import { OwnerRole } from '../context/context';

export default memo(({ nextScreen }: { nextScreen: any }) => {
  const { t } = useTranslation();
  const { order, setOrderFields } = useContext(OrderContext);

  const createOrderSchema =
    order.ownerRole === OwnerRole.RECEIVER
      ? Yup.object().shape({
          country: Yup.object()
            .shape({
              id: Yup.string().required(),
              label: Yup.string().required(),
            })
            .nullable()
            .required(t('order:pickupStep.inputs.country.errors.required')),
          city: Yup.object()
            .shape({
              id: Yup.string().required(),
              label: Yup.string().required(),
            })
            .nullable()
            .required(t('order:pickupStep.inputs.city.errors.required')),
          area: Yup.string().required(
            t('order:pickupStep.inputs.area.errors.required'),
          ),
          contactPhoneNumber: Yup.object()
            .shape({
              code: Yup.string(),
              countryCode: Yup.string(),
              number: Yup.string().required('Phone number is required'),
              formatted: Yup.string(),
              isValid: Yup.boolean().oneOf([true], 'Invalid number format'),
            })
            .required(),
        })
      : Yup.object().shape({
          country: Yup.object()
            .shape({
              id: Yup.string().required(),
              label: Yup.string().required(),
            })
            .nullable()
            .required(t('order:pickupStep.inputs.country.errors.required')),
          city: Yup.object()
            .shape({
              id: Yup.string().required(),
              label: Yup.string().required(),
            })
            .nullable()
            .required(t('order:pickupStep.inputs.city.errors.required')),
          area: Yup.string().required(
            t('order:pickupStep.inputs.area.errors.required'),
          ),
          contactPhoneNumber: Yup.object().nullable(true),
        });

  function onSubmit(value) {
    nextScreen();
  }

  return (
    <Formik
      initialValues={{
        ...order.locations.pickup,
        contactPhoneNumber: order.thirdPartyPhoneNumber,
      }}
      validationSchema={createOrderSchema}
      validateOnChange={false}
      onSubmit={onSubmit}
    >
      {(props) => {
        const {
          handleSubmit,
          handleChange,
          errors,
          values,
          setFieldError,
          setFieldValue,
        } = props;

        function setCountry(value: string) {
          setFieldValue('country', value);
          setFieldValue('city', null);
          setFieldError('country', null);
          setOrderFields({
            type: SET_PICKUP_COUNTRY,
            payload: value,
          });
        }

        function setCity(value: string) {
          setFieldValue('city', value);
          setFieldError('city', null);
          setOrderFields({
            type: SET_PICKUP_CITY,
            payload: value,
          });
        }

        function setArea(value: string) {
          setFieldValue('area', value);
          setFieldError('area', '');

          setOrderFields({
            type: SET_PICKUP_AREA,
            payload: value,
          });
        }

        function setSenderPhoneNumber(value: string) {
          if (errors.contactPhoneNumber) {
            setFieldError('contactPhoneNumber', '');
          }

          setFieldValue('contactPhoneNumber', value);
          setOrderFields({
            type: SET_THIRD_PARTY_PHONE_NUMBER,
            payload: value,
          });
        }

        console.log(values);

        return (
          <View style={styles.wrapper}>
            <WithDescription
              title={t('order:pickupStep.inputs.country.label')}
              description={t('order:pickupStep.inputs.country.description')}
              marginBottom={20}
            >
              <Location
                placeholder={t('order:pickupStep.inputs.country.description')}
                current={values.country}
                type="country"
                handleValue={setCountry}
                error={errors.country}
              />
            </WithDescription>

            <WithDescription
              title={t('order:pickupStep.inputs.city.label')}
              description={t('order:pickupStep.inputs.city.placeholder')}
              marginBottom={20}
            >
              <Location
                placeholder={t('order:pickupStep.inputs.city.placeholder')}
                type="city"
                isDisabled={Boolean(!values?.country?.id)}
                current={values.city}
                handleValue={setCity}
                error={errors.city}
                params={{
                  countryId: values.country ? values.country.id : '',
                }}
              />
            </WithDescription>

            <WithDescription
              title={t('order:pickupStep.inputs.area.label')}
              description={t('order:pickupStep.inputs.area.description')}
              marginBottom={20}
            >
              <TextInput
                placeholder={t('order:pickupStep.inputs.area.placeholder')}
                onChangeText={setArea}
                value={values.area}
                error={errors.area}
              />
            </WithDescription>

            {order.ownerRole === OwnerRole.RECEIVER && (
              <WithDescription
                title={t('order:pickupStep.inputs.contactPhone.label')}
                description={t(
                  'order:pickupStep.inputs.contactPhone.description',
                )}
                marginBottom={20}
              >
                <PhoneNumber
                  defaultCode={values.contactPhoneNumber?.countryCode}
                  value={values.contactPhoneNumber?.number}
                  onChangeText={setSenderPhoneNumber}
                  error={errors.contactPhoneNumber}
                />
              </WithDescription>
            )}

            <UgetButton
              type="primary"
              label={t('common:buttons.next')}
              onPress={handleSubmit}
            />
          </View>
        );
      }}
    </Formik>
  );
});
