import React, { memo, useContext, useState } from 'react';
import { Modal, View } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { useTranslation } from 'react-i18next';

import { DropDown, Quantity, UgetButton } from '../../../../components';

import {
  RadioButtons,
  TextInput,
  UploadImage,
  WithDescription,
} from '../../../../components/UgetInputs';

import styles from './style';
import { OrderContext } from '../context';
import {
  SET_ITEM_NAME,
  SET_ITEM_QUANTITY,
  SET_ITEM_SIZE,
  SET_ORDER_DESCRIPTION,
  SET_ORDER_OWNER_ROLE,
} from '../context/actions';
import { ItemSize, OwnerRole } from '../context/context';

export default memo(({ nextScreen }: { nextScreen: any }) => {
  const { t } = useTranslation();
  const { order, setOrderFields } = useContext(OrderContext);

  const createOrderSchema = Yup.object().shape({
    name: Yup.string().required(
      t('order:itemStep.inputs.name.errors.required'),
    ),
  });

  const sizeItems = [
    ItemSize.A,
    ItemSize.B,
    ItemSize.C,
    ItemSize.D,
    ItemSize.E,
    ItemSize.F,
    ItemSize.G,
    ItemSize.H,
    ItemSize.I,
    ItemSize.J,
    ItemSize.K,
    ItemSize.L,
  ];

  const senderType = [
    {
      id: OwnerRole.SENDER,
      label: t('order:itemStep.inputs.receiver.options.else'),
    },
    {
      id: OwnerRole.RECEIVER,
      label: t('order:itemStep.inputs.receiver.options.self'),
    },
  ];

  return (
    <Formik
      initialValues={{ ...order.details, photos: order.photos }}
      validationSchema={createOrderSchema}
      validateOnChange={false}
      onSubmit={(values) => {
        nextScreen();
      }}
    >
      {(props) => {
        const { handleSubmit, errors, values, setFieldError, setFieldValue } =
          props;

        function setName(value: string) {
          setFieldValue('name', value);
          setOrderFields({
            type: SET_ITEM_NAME,
            payload: value,
          });
          setFieldError('name', '');
        }

        function setSize(value: string) {
          setFieldValue('size', value);
          setOrderFields({
            type: SET_ITEM_SIZE,
            payload: value,
          });
        }

        function setQuantity(value: number) {
          setFieldValue('quantity', value);

          setOrderFields({
            type: SET_ITEM_QUANTITY,
            payload: value,
          });
        }

        function setDescription(value: string) {
          setFieldValue('description', value);

          setOrderFields({
            type: SET_ORDER_DESCRIPTION,
            payload: value,
          });
        }

        function setPhoto(value: any) {
          setFieldValue('photos', value);
        }

        function setReceiver(value: any) {
          setFieldValue('ownerRole', value);
          setOrderFields({
            type: SET_ORDER_OWNER_ROLE,
            payload: value,
          });
        }

        return (
          <View style={styles.container}>
            <View style={styles.wrapper}>
              <WithDescription
                title={t('order:itemStep.inputs.name.label')}
                marginBottom={20}
              >
                <TextInput
                  placeholder={t('order:itemStep.inputs.name.placeholder')}
                  onChangeText={setName}
                  value={values.name}
                  error={errors.name}
                />
              </WithDescription>

              <WithDescription
                title={t('order:itemStep.inputs.size.label')}
                marginBottom={20}
              >
                <DropDown
                  placeholder={t('order:itemStep.inputs.size.placeholder')}
                  value={values.size}
                  items={sizeItems}
                  setValue={setSize}
                />
              </WithDescription>

              <WithDescription
                flex={true}
                title={t('order:itemStep.quantity')}
                marginBottom={20}
              >
                <Quantity
                  count={values.quantity}
                  handleQuantity={setQuantity}
                />
              </WithDescription>

              <WithDescription
                title={t('order:itemStep.inputs.receiver.label')}
                description={t('order:itemStep.inputs.receiver.description')}
                marginBottom={36}
              >
                <RadioButtons
                  items={senderType}
                  handleSelect={setReceiver}
                  selected={order.ownerRole}
                />
              </WithDescription>

              <WithDescription
                title={t('order:itemStep.inputs.description.label')}
                marginBottom={20}
              >
                <TextInput
                  value={values.description}
                  type="multiline"
                  placeholder={t(
                    'order:itemStep.inputs.description.placeholder',
                  )}
                  onChangeText={setDescription}
                  error={errors.description}
                />
              </WithDescription>

              <WithDescription
                title={t('order:itemStep.inputs.photo.label')}
                description={t('order:itemStep.inputs.photo.placeholder')}
                marginBottom={36}
              >
                <UploadImage photos={values.photos} handlePhotos={setPhoto} />
              </WithDescription>
            </View>

            <UgetButton
              type="primary"
              label={t('common:buttons.next')}
              onPress={handleSubmit}
            />
          </View>
        );
      }}
    </Formik>
  );
});
