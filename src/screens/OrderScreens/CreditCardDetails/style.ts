import { COLORS } from '../../../config/constants';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrapp: {
    paddingHorizontal: 25,
  },
  title: {
    paddingTop: 25,
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 32,
  },
  subTitle: {
    paddingTop: 25,
  },
  subTitleTxt: {
    fontFamily: 'FiraSans-Regular',
    fontSize: 14,
    lineHeight: 22,
    paddingRight: 50,
  },
  inputWrapp: {
    paddingTop: 20,
  },
  inputTitle: {
    paddingTop: 11,
    fontFamily: 'FiraSans-Regular',
  },
  cardDetails: {
    flexDirection: 'row',
  },
  cardDate: {
    width: 121,
  },
  cardCvv: {
    width: 121,
    marginLeft: 16,
  },
  checkboxWrap: {
    paddingTop: 18,
    flexDirection: 'row',
  },
  checkboxTxt: {
    paddingLeft: 13,
    fontFamily: 'FiraSans-Regular',
  },
  errorMessage: {
    paddingTop: 4,
    color: COLORS.RED,
  },
  btn: {
    marginTop: 37,
  },
});
