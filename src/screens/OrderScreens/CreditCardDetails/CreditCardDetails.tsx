import React, { memo, useRef } from 'react';
import { Text, View, SafeAreaView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useTranslation } from 'react-i18next';

import {
  PrimaryButton,
  TextInputComponent,
  Checkbox,
} from '../../../components';
import { NAVIGATION } from '../../../config/constants';
import styles from './style';

export default memo(() => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  const cardNumberRef = useRef(null);
  const cardDateRef = useRef(null);
  const cvvCardRef = useRef(null);

  const signUpSchema = Yup.object().shape({
    cardName: Yup.string()
      .matches(
        /[a-zA-Z]/,
        t('order:creditCard.inputs.cardName.errors.latinLetters'),
      )
      .required(t('order:creditCard.inputs.cardName.errors.required')),
    cardNumber: Yup.string().required(
      t('order:creditCard.inputs.cardNumber.errors.required'),
    ),
    date: Yup.string()
      .min(4, t('order:creditCard.inputs.date.errors.short'))
      .required(t('order:creditCard.inputs.date.errors.required')),
    cvv: Yup.string()
      .min(3, t('order:creditCard.inputs.cvv.errors.short'))
      .required(t('order:creditCard.inputs.cvv.errors.required')),
  });

  return (
    <Formik
      initialValues={{
        cardName: '',
        cardNumber: '',
        date: '',
        cvv: '',
      }}
      validationSchema={signUpSchema}
      validateOnChange={false}
      onSubmit={(values) =>
        navigation.navigate(NAVIGATION.ORDER_SCREENS.CHECKOUT)
      }
    >
      {(props) => {
        const { handleSubmit, handleChange, errors } = props;

        return (
          <SafeAreaView style={styles.container}>
            <View style={styles.wrapp}>
              <Text style={styles.title}>{t('order:creditCard.title')}</Text>
              <View style={styles.subTitle}>
                <Text style={styles.subTitleTxt}>
                  {t('order:creditCard.subTitle')}
                </Text>
              </View>
              <View style={styles.inputWrapp}>
                <Text style={styles.inputTitle}>
                  {t('order:creditCard.inputs.cardName.label')}
                </Text>
                <TextInputComponent
                  placeholder={t(
                    'order:creditCard.inputs.cardName.placeholder',
                  )}
                  onSubmitEditing={() => {
                    cardNumberRef.current.focus();
                  }}
                  onChangeText={handleChange('cardName')}
                  error={errors.cardName}
                />
                <Text style={styles.inputTitle}>
                  {t('order:creditCard.inputs.cardNumber.label')}
                </Text>
                <TextInputComponent
                  customRef={cardNumberRef}
                  onSubmitEditing={() => {
                    cardDateRef.current.focus();
                  }}
                  placeholder="**** **** **** ****"
                  inputMask={true}
                  mask={'[0000] [0000] [0000] [0000]'}
                  onChangeText={handleChange('cardNumber')}
                  error={errors.cardNumber}
                />
                <View style={styles.cardDetails}>
                  <View style={styles.cardDate}>
                    <Text style={styles.inputTitle}>
                      {t('order:creditCard.inputs.date.label')}
                    </Text>
                    <TextInputComponent
                      customRef={cardDateRef}
                      onSubmitEditing={() => {
                        cvvCardRef.current.focus();
                      }}
                      inputMask={true}
                      mask={'[00]{/}[00]'}
                      placeholder={t(
                        'order:creditCard.inputs.date.placeholder',
                      )}
                      textContentType="emailAddress"
                      onChangeText={handleChange('date')}
                      error={errors.date}
                    />
                  </View>
                  <View style={styles.cardCvv}>
                    <Text style={styles.inputTitle}>
                      {t('order:creditCard.inputs.cvv.label')}
                    </Text>
                    <View>
                      <TextInputComponent
                        customRef={cvvCardRef}
                        placeholder="***"
                        inputMask={true}
                        mask={'[000]'}
                        secureTextEntry={true}
                        onChangeText={handleChange('cvv')}
                        error={errors.cvv}
                      />
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.checkboxWrap}>
                <Checkbox />
                <Text style={styles.checkboxTxt}>
                  {t('order:creditCard.checkbox')}
                </Text>
              </View>
              <PrimaryButton
                txt={t('order:buttons.proceedCheckout')}
                style={styles.btn}
                onPress={handleSubmit}
              />
            </View>
          </SafeAreaView>
        );
      }}
    </Formik>
  );
});
