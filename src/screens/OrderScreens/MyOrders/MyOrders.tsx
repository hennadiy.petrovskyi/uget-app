import React, { memo, useEffect, useRef } from 'react';
import { Text, View, SafeAreaView, ScrollView, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import {
  OrderItem,
  ScreenTitle,
  UgetLinkText,
  UgetTabs,
} from '../../../components';

import { NAVIGATION, COLORS } from '../../../config/constants';
import styles from './style';
import { getMyOrders, loadDrafts } from '../../../api/order';
import { setDrafts } from '../../../utils/store/orders/actions';
import { asyncStorage, Keys } from '../../../utils/asyncStorage';
import { OrderViewerType, OrderViewType } from '../../../types';

import Recaptcha, { RecaptchaHandles } from 'react-native-recaptcha-that-works';

export default memo(() => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);
  const ordersUploaded = useSelector((state) => state.orders.uploaded) || [];
  const ordersDrafts = useSelector((state) => state.orders.drafts);

  useEffect(() => {
    getOrders();
  }, []);

  const tabs = [
    {
      label: t('order:tabs.active.label'),
      isActive: isAuth,
      filter: 'published',
      emptyText: t('order:tabs.active.emptyText'),
    },
    {
      label: t('order:tabs.progress.label'),
      isActive: isAuth,
      filter: ['accepted', 'picked-up'],
      emptyText: t('order:tabs.progress.emptyText'),
    },
    {
      label: t('order:tabs.history.label'),
      isActive: isAuth,
      filter: 'delivered',
      emptyText: t('order:tabs.history.emptyText'),
    },
    {
      label: t('order:tabs.drafts.label'),
      isActive: true,
      filter: ['draft', 'inactive'],
      emptyText: t('order:tabs.drafts.emptyText'),
    },
  ];

  const orders = ordersUploaded.concat(ordersDrafts);

  const renderItem = (item, key, setFilter) => {
    return (
      <OrderItem
        key={key}
        order={item}
        view={OrderViewType.PREVIEW}
        viewer={OrderViewerType.ORDER_OWNER}
        setFilter={setFilter}
      />
    );
  };

  function getOrders() {
    dispatch(getMyOrders());

    loadDrafts();
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.titleWrap}>
        <ScreenTitle text={t('order:myOrders.title')} />

        <UgetLinkText
          text={`+ ${t('order:myOrders.createOrder')}`}
          onPress={() =>
            navigation.navigate(NAVIGATION.ORDER_SCREENS.CREATE_ORDER)
          }
        />
      </View>

      <UgetTabs
        tabs={tabs}
        data={orders.length ? orders : []}
        renderItem={renderItem}
        onRefreshControl={getOrders}
      />
    </SafeAreaView>
  );
});
