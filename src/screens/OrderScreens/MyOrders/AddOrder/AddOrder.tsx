import React, { memo } from 'react';
import { View, SafeAreaView } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';

import styles from './style';
import { ScreenTitle, UgetButton, UgetGreyText } from '../../../../components';
import { PLANE_IN_CLOUDS } from '../../../../assets/svg';
import { NAVIGATION } from '../../../../config/constants';

export default memo(() => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  return (
    <>
      <ScreenTitle
        text={t('trips:createOrderThumbnail.title')}
        align="left"
        marginBottom={94}
      />

      <PLANE_IN_CLOUDS style={{ marginBottom: 54.05, alignSelf: 'center' }} />
      <UgetGreyText
        text={t('trips:createOrderThumbnail.description')}
        marginBottom={24}
        outerStyles={styles.description}
      />
      <UgetButton
        label={t('trips:createOrderThumbnail.button')}
        type="primary"
        onPress={() => navigation.navigate(NAVIGATION.TRIPS.CREATE_TRIP)}
      />
    </>
  );
});
