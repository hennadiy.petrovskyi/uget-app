export default {
  cancelOrder: {
    title: 'Cancel ⚠️',
    subtitle: 'We still can save your order to drafts or delete it forever.',
    subtitleNoDraft:
      'We still can save your order for future or delete it forever.',
    btnContinue: 'Continue creating order',
    btnSaveAsDraft: 'Save as draft',
    btnSaveForm: 'Save form for future',
    btnCancel: 'Cancel order creation',
  },
  itemPublished: {
    title: 'Success! 👍',
    trip: {
      subtitle:
        'Your trip is published. Find orders to deliver or wait for delivery request. ',
    },
    order: {
      subtitle:
        'Your order is published. Find travelers to deliver your item or wait for them to reach out to you. ',
    },
    btn: 'Got it',
  },
  orderSenderRequest: {
    title: 'Request sent! 👌',
    subtitle:
      'Thank you. You will be notified once the traveler has accepted your request.',
    btnTrack: 'Track your orders',
    btnCreate: 'Create new order',
  },
  orderSenderRequestAccepted: {
    title: 'Success! 🎉',
    subtitle:
      'The traveler accepted your request to deliver order #{{orderId}}. ',
    btnView: 'View my order',
  },
  authorizeToPublish: {
    title: 'Get your {{type}} published',
    subtitle: 'Please authorize to publish your {{type}}. ',
    btnLogin: 'Log in',
    btnSignUp: 'Sign up',
  },
  editCreditCard: {
    title: 'Enter credit card details',
    subtitle:
      'Please enter your credit card details. Money will be charged only after a traveler picks up your item.',
    btnProceed: 'Proceed',
    btnCancel: 'Cancel',
  },
  orderUnpublished: {
    title: 'Unpublish ⚠️',
    subtitle:
      'If you unpublish your order travelers will not be able to see it. Your order will be saved to drafts.',
    btnUnpublish: 'Unpublish',
    btnCancel: 'Cancel',
  },
  verifyTravelerActions: {
    pickup: {
      title: 'Verify pick-up',
      description:
        'We have sent verification code to the person who passes you the package.',
    },
    delivery: {
      title: 'Verify delivery',
      description:
        'We have sent verification code to the person who receive the package.',
    },
    helper: 'Ask them for a code and enter it here:',
    confirm: 'Confirm',
    cancel: 'Cancel',
  },
  rateUser: {
    title: 'Rate the {{role}} 👌',
    subTitle: 'Please let us know if you’re satisfied',
  },
  resetPasswordRequestSent: {
    title: 'Success 👌',
    subTitle:
      'We sent a reset link to your email address. If you can’t see it in your inbox, please check your spam folder or other filtering tools.',
    button: 'Got it',
  },

  passwordUpdated: {
    title: 'Success 👌',
    subTitle:
      'Password updated successfully! Now you can try to log in with your new credentials',
    button: 'Log in',
  },

  emailVerified: {
    title: 'Success 👌',
    subTitle: 'Your email was successfully verified !',
    button: 'Got it',
  },
};
