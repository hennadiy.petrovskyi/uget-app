export default {
  title: 'Messages',
  emptyText: 'You have no conversations . Add a trip or create new order',
  buttons: {
    createOrder: 'Create new Order',
    addTrip: 'Add trip',
    myOrders: 'view my Orders',
    myTrips: 'view my Trips',
  },
};
