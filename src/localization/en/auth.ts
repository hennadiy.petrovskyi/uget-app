export default {
  login: {
    title: {
      email: 'Log in with email',
      phoneNumber: 'Log in with mobile number',
    },
    btnSubmit: 'Log in',
    btnNext: 'Next',
    bntLoginWith: {
      phone: 'Log in with email',
      email: ' Log in with phone number',
    },
    or: 'or',
    forgotPassword: 'Forgot password',
    account: 'Don’t have an account?',
    signUp: 'Sign up',
    resend: 'Resend code',
  },
  signUp: {
    title: 'Sign up',
    subTitle: 'Please sign up to complete your order.',
    btnSubmit: 'Sign Up',
    haveAccount: 'Already have an account? ',
    linkLogin: 'Log in',
    agreement: 'By clicking on Sign up, I agree to Uget’s',
    terms: 'Terms of Use',
    and: 'and',
    policy: 'Privacy Policy.',
  },
  verifyEmail: {
    title: 'Verify your email',
    subTitle: 'We’ve sent an email with an activation link to',
    openEmail: 'Please open your email and confirm your account information.',
    wrongEmail: 'Wrong email address?',
    changeEmail: 'Change email',
    resend: 'Didn’t get an email? Send it again',
  },
  verifyPhoneNumber: {
    title: 'Verification code',
    subTitle: 'We have sent verification code to',
    btnSubmit: 'Verify',
    wrongNumber: 'Wrong mobile number?',
    linkChangeNumber: 'Change mobile number',
    resend: 'Resend code',
  },
  changeEmail: {
    title: 'Change email',
    subTitle: 'Please enter your new email address',
    btnSubmit: 'Change',
  },
  changePhoneNumber: {
    title: 'Change number',
    subTitle: 'Please enter your new mobile number',
    btnSubmit: 'Change',
  },
  forgotPassword: {
    title: 'Forgot password?',
    subTitle:
      'Not a problem, just enter your email and we’ll send you the instructions.',
    btnSubmit: 'Reset password',
  },
  resetPassword: {
    title: 'Change password',
    btnSubmit: 'Change',
    expired: {
      title: 'Link expired ⌛️',
      description: 'Please click resend link to finish your registration.',
      btnSubmit: 'Resend',
    },
  },
};
