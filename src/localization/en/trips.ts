export default {
  myTrips: {
    title: 'My trips',
    addNewTrip: 'Add new trip',
  },
  addTrip: {
    title: 'My trips',
    description:
      'You have no trips. Add your first trip to start earning money with Uget.',
    button: 'Add your trip',
  },
  createTrip: {
    title: 'Add trip',
    roundTrip: 'Round trip',
    oneWay: 'One way',
    location: 'Location',
    datesAndDetails: 'Dates and details',
    inputs: {
      fromCountry: {
        label: 'From country*',
        placeholder: 'Start typing country name',
        errors: {
          required: 'Country name required',
        },
      },
      fromCity: {
        label: 'From city*',
        placeholder: 'Start typing city name',
        errors: {
          required: 'City name required',
        },
      },
      fromArea: {
        label: 'From area/suburb*',
        placeholder: 'Start typing area/suburb name',
        errors: {
          required: 'Area/Suburb name required',
        },
      },
      toCountry: {
        label: 'To country*',
        placeholder: 'Start typing country name',
        errors: {
          required: 'Country name required',
        },
      },
      toCity: {
        label: 'To city*',
        placeholder: 'Start typing city name',
        errors: {
          required: 'City name required',
        },
      },
      toArea: {
        label: 'To area/suburb*',
        placeholder: 'Start typing area/suburb name',
        errors: {
          required: 'Area/Suburb name required',
        },
      },
      departureDate: {
        label: 'Departure date*',
        placeholder: 'Choose Departure date',
        errors: {
          required: 'Departure date required',
        },
      },
      arrivalDate: {
        label: 'Arrival date*',
        placeholder: 'Choose Arrival date',
        errors: {
          required: 'Arrival date required',
        },
      },
      returnArrivalDate: {
        label: 'Return arrival date*',
        placeholder: 'Choose Return arrival date',
        errors: {
          required: 'Return date required',
        },
      },
      returnDepartureDate: {
        label: 'Return departure date*',
        placeholder: 'Choose Return departure date',
        errors: {
          required: 'Return date required',
        },
      },
      tripDetails: {
        label: 'Trip details',
        placeholder:
          'Enter some trip details here, e.g.: what package weight is appropriate to you or which area you are staying.',
      },
    },
    buttons: {
      addTrip: 'Add trip',
    },
  },
  tripItem: {
    requests: 'Requests {{amount}}',
    accepted: 'Accepted {{amount}}',
    earnings: 'Earnings {{amount}}',
    available: 'Available {{amount}}',
    transit: 'Picked up {{amount}}',
    delivered: 'Delivered {{amount}}',
    buttons: {
      viewOrders: 'View orders',
      delete: 'Delete',
    },
    emptyList: {
      available: 'There is no available orders for your dates',
      requests:
        'You have no delivery requests. Please check your accepted requests or view available orders and start making delivery offers.',
      accepted:
        'You have no accepted requests . Please check your delivery requests or view available orders and start making delivery offers.',
      delivered:
        'You have no delivered items yet. Please check your delivery requests or view available orders and start making delivery offers.',
    },
  },

  tabs: {
    upcoming: {
      label: 'Upcoming',
      emptyText: 'You have no upcoming trips',
    },
    active: {
      label: 'Active',
      emptyText: 'You have no active trips',
    },
    past: {
      label: 'History',
      emptyText: 'You have no completed trips',
    },
  },
  tripOrders: {
    earnings: 'Earnings',
  },
};
