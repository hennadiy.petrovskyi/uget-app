import common from './common';
import auth from './auth';
import home from './home';
import order from './order';
import notifications from './notifications';
import trips from './trips';
import profile from './profile';
import chat from './chat';

export default {
  common,
  auth,
  home,
  order,
  notifications,
  trips,
  profile,
  chat,
};
