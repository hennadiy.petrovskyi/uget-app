export default {
  intro: {
    senderTitle: 'Easy and quick delivery',
    senderSubTitle:
      'Need some documents or personal items to be delivered from your home country to Dubai?',
    senderBtn: 'Place your order',
    travelerTitle: 'Travel and earn money',
    travelerSubTitle:
      'Are you a cabin crew professional? Collect some personal items on request and earn money delivering them.',
    travelerBtn: 'Add your trip',
    tabs: {
      title: 'How it Works',
      receiver: {
        title: 'For receiver',
        p1: {
          title: '1. Place your order',
          text: 'In a few easy steps enter your item details, sender information and delivery details.',
        },
        p2: {
          title: '2. Invite a traveler to deliver',
          text: 'Find a traveler from the list or simply wait until a travelerv reaches you out.',
        },
        p3: {
          title: '3. Get your item',
          text: 'Meet your traveler in Dubai and get your item. Confirm the receiving and rate a traveler.',
        },
      },
      traveler: {
        title: 'For traveler',
        p1: {
          title: '1. Add your trip',
          text: 'In a few easy steps enter your trip details.',
        },
        p2: {
          title: '2. Make delivery offers',
          text: 'Find orders matching your trip dates and location and send a request or receive delivery request and accept it.',
        },
        p3: {
          title: '3. Pick-up item',
          text: 'Meet a sender at an agreed time and place, check and collect a package.',
        },
        p4: {
          title: '4. Deliver and get paid',
          text: 'Meet a receiver at a defined date and time and pass a parcel. A receiver confirms delivery and you get paid.',
        },
      },
    },
  },
  onBoarding: {
    sender: {
      p1: {
        title: 'Easy and quick delivery',
        text: 'Need some documents or personal items to be delivered from your home country to Dubai?',
      },
      p2: {
        title: 'Place your order',
        text: 'In a few easy steps enter your item details, sender information and delivery details.',
      },
      p3: {
        title: 'Invite a traveler to deliver',
        text: 'Find a traveler from the list or simply wait until a traveler reaches you out.',
      },
      p4: {
        title: 'Get your item',
        text: 'Meet your traveler in Dubai and get your item. Confirm the receiving and rate a traveler.',
      },
    },
    traveler: {
      p1: {
        title: 'Add your trip',
        text: 'In a few easy steps enter your trip details.',
      },
      p2: {
        title: 'Make delivery offers',
        text: 'Find orders matching your trip dates and location and send a request or receive delivery request and accept it.',
      },
      p3: {
        title: 'Pick-up item',
        text: 'Meet a sender at an agreed time and place. Check, take a photo, and collect the package.',
      },
      p4: {
        title: 'Deliver and get paid',
        text: 'Meet a sender at a defined date and time. Handover package, take photo to confirm and get paid.',
      },
    },
  },
};
