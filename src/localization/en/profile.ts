export default {
  init: {
    title: 'Profile',
    actions: {
      settings: 'Settings',
      help: 'Get help',
      invite: 'Invite friends',
      rate: 'Rate us on AppStore',
    },
    information: {
      title: 'information',
      faq: 'How does Uget work?',
      policy: 'Privacy policy',
      terms: 'Terms of use',
      safety: 'Safety',
    },
  },
  profile: {
    joined: 'Joined on {{date}}',
    edit: 'Edit',
    sections: {
      information: {
        title: 'Information',
        button: 'verify',
      },
      rating: {
        title: 'Rating',
        traveler: 'Rated as traveler',
        sender: 'Rated as sender',
      },
    },
  },

  edit: {
    save: 'Save',
    cancel: 'Cancel',
  },
  settings: {
    title: 'Settings',
    subTitle: 'Set notifications to stay informed ',
    email: 'Email notifications',
    push: 'Push notifications',
  },
  invite: {
    title: 'Invite friends',
    subTitle:
      'Earn bonuses for each friend who made an order or delivered on Uget',
    button: 'Share a link',
  },
};
