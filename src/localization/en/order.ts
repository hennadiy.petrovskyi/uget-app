export default {
  title: 'Create an order',
  steps: {
    item: 'Item',
    pickUp: 'Pick-up',
    delivery: 'Delivery',
    summary: 'Summary',
  },
  itemStep: {
    inputs: {
      name: {
        label: 'Item name*',
        placeholder: 'Enter an item name...',
        errors: {
          required: 'Name required',
        },
      },
      size: {
        label: 'Package weight',
        placeholder: 'Select package weight',
        upTo: 'Up to',
        kg: 'Kg',
        options: [
          '0.5',
          '1',
          '1.5',
          '2',
          '2.5',
          '3',
          '3.5',
          '4',
          '4.5',
          '5',
          '5.5',
          '6',
        ],
      },
      description: {
        label: 'Description',
        placeholder:
          'Enter some trip details here, e.g.: what package weight is appropriate to you or which area you are staying.',
        errors: {
          required: 'Description is required',
        },
      },
      photo: {
        label: 'Item photos',
        placeholder:
          'We recommend to add a photo. Your order is more likely to convert if you add a photo.',
      },
      receiver: {
        label: 'Role',
        description: 'Choose your role for this order',
        options: {
          self: 'I’m receiving',
          else: 'I’m sending',
        },
      },
    },
    quantity: 'Quantity',
    takePhoto: 'Take a photo',
    uploadPhoto: 'Upload photo from your phone',
  },
  pickupStep: {
    inputs: {
      country: {
        label: 'Country*',
        description: 'A country where a traveler collects your item',
        placeholder: 'Start typing country name',
        errors: {
          required: 'Country is required',
        },
      },
      city: {
        label: 'City*',
        description: 'A city where a traveler collects your item',
        placeholder: 'Start typing city name',
        errors: {
          required: 'City is required',
        },
      },
      area: {
        label: 'Area/suburb*',
        description:
          'An area or a suburb where a traveler meets your contact person to collect your item',
        placeholder: 'Start typing area name',
        errors: {
          required: 'Area is required',
        },
      },
      contactPhone: {
        label: 'Contact person phone number',
        description:
          'This number will not be visible to anyone. We need it for package verification',
        errors: {
          required: 'Phone number is required',
        },
      },
    },
  },
  deliveryStep: {
    inputs: {
      country: {
        label: 'Delivery country',
        placeholder: 'Start typing country name',
        errors: {
          required: 'Delivery country required',
        },
      },
      city: {
        label: 'Delivery city',
        placeholder: 'Start typing city name',
        errors: {
          required: 'Delivery city required',
        },
      },
      date: {
        label: 'Deliver by date',
        placeholder: 'Choose date',
        errors: {
          required: 'Delivery date required',
        },
      },
      area: {
        label: 'Area/suburb*',
        placeholder: 'Start typing area name',
        errors: {
          required: 'Area required',
        },
      },
      contactPhone: {
        label: 'Contact person phone number',
        description:
          'This number will not be visible to anyone. We need it for package verification',
      },
    },
  },
  summaryStep: {
    itemName: 'Item name',
    quantity: 'Quantity',
    packageWeight: {
      key: 'Package weight',
      value: 'Up to {{weight}} Kg',
    },
    pickupCountry: 'Pick-up country',
    orderType: {
      label: 'Order type',
      parcel: 'Parcel',
      document: 'Documents',
    },
    pickupCity: 'Pick-up city',
    pickupArea: 'Pick-up area/suburb',
    senderContactNumber: 'Sender contact number',
    receiverContactNumber: 'Receiver contact number',
    deliveryCountry: 'Delivery country',
    deliveryCity: 'Delivery city',
    deliveryArea: 'Delivery area/suburb',
    deliveryDate: 'Deliver by',
    deliveryPrice: 'Delivery price',
    earning: 'Your earnings',
  },
  typeOfOrder: {
    title: 'Create an order',
    text: 'What would you like to be sent / delivered?',
    parcel: 'Parcel',
    documents: 'Documents',
  },
  myOrders: {
    title: 'My orders',
    createOrder: 'Create order',
    requested: 'Requested',
    accepted: 'Accepted',
    received: 'Received',
    drafts: 'Drafts',
  },
  travelers: {
    title: 'View travelers',
  },
  checkout: {
    title: 'Checkout',
    yourOrder: 'YOUR ORDER',
    paymentDetails: 'PAYMENT DETAILS',
    price: 'Total delivery price',
  },
  creditCard: {
    title: 'Credit card details',
    subTitle: 'In case your order is not accepted, refund will be issued',
    checkbox: 'Save for future use',
    inputs: {
      cardName: {
        label: 'Name on card',
        placeholder: 'Enter name on card',
        errors: {
          required: 'Card name required',
          latinLetters: 'ard name can only contain Latin letters.',
        },
      },
      cardNumber: {
        label: 'Card number',
        errors: {
          required: 'Card number required',
        },
      },
      date: {
        label: 'Exp. date',
        placeholder: 'MM/YY',
        errors: {
          required: 'Exp. date required',
          short: 'Too Short!',
        },
      },
      cvv: {
        label: 'CVV code',
        errors: {
          required: 'CVV required',
          short: 'CVV is too short ',
        },
      },
    },
  },
  buttons: {
    publishOrder: 'Publish order',
    saveAsDraft: 'Save as draft',
    placeOrder: 'Place order',
    proceedCheckout: 'Proceed to checkout',
  },
  signUpModal: {
    title: 'Success!',
    text: 'Thank you for signing up. Your order is saved. To invite travelers to deliver your item, please checkout. ',
    button: 'Got it',
  },
  orderModal: {
    title: 'Success!',
    text1: 'Your order',
    text2:
      'is confirmed. Now invite travelers to deliver your order or wait for them to request delivery.',
    button: 'View my order',
  },
  orderPreview: {
    from: 'From',
    to: 'To',
    by: 'By',

    price: {
      currency: 'USD',
      label: {
        traveler: 'Your earnings',
        owner: 'Total delivery price',
      },
    },

    actions: {
      publish: 'Publish',
      viewTravelers: 'View travelers',
      viewRequests: 'View requests',
      unpublish: 'Unpublish',
      cancel: 'Cancel',
      edit: 'Edit',
      delete: 'Delete',
    },
  },
  requests: {
    title: 'Requests',
  },

  trips: {
    title: 'View travelers',
    by: 'by {{date}}',
    status: {
      outgoing: 'Request was sent by you. Awaits for approval.',
      incoming: 'Incoming request. Awaits for approval.',
      accepted: 'Request was approved.',
      available: '',
    },
    details: {
      from: 'Travels from',
      to: 'Travels to',
      departure: 'Departure date',
      arrival: 'Arrival date',
    },
    actions: {
      chat: 'Chat',
      cancel: 'Cancel',
      request: 'Request to deliver',
    },
  },

  status: {
    draft: 'Draft',
    published: 'Published',
    inactive: 'Inactive',
    available: 'Available',
    incoming: 'Pending incoming request',
    outgoing: 'Pending outgoing request',
    accepted: 'Awaiting for pick-up',
    progress: 'In transit',
    delivered: 'Delivered',
  },

  tabs: {
    active: {
      label: 'Active',
      emptyText: 'You have no published orders.',
    },
    progress: {
      label: 'In Progress',
      emptyText:
        'You have no accepted requests. Please check your accepted requests or view available orders and start making delivery offers.',
    },
    history: {
      label: 'History',
      emptyText: 'You have no delivered or expired orders.',
    },
    drafts: {
      label: 'Drafts',
      emptyText: 'You have no drafts.',
    },
    incoming: {
      label: 'Incoming ({{amount}})',
      emptyText: 'You have no incoming requests.',
    },
    outgoing: {
      label: 'Outgoing ({{amount}})',
      emptyText: 'You have no outgoing requests.',
    },
  },

  createOrderThumbnail: {
    title: 'My orders',
    description:
      'You have no orders. Add your first order and find a travellers .',
    button: '',
  },

  travelersList: {
    emptyText: 'Sorry , there is no available trips on this dates',
  },
};
