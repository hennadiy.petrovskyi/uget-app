export default {
  inputs: {
    phoneNumber: {
      label: 'Phone number',
      placeholder: ' ',
      errors: {
        required: 'Phone number is required',
        404: 'User with this phone number not found',
        exist: 'Phone number already in use',
        same: 'Nothing to change . Number are the same',
      },
    },
    email: {
      label: 'Email address',
      placeholder: 'Enter your email',
      errors: {
        required: 'Email required',
        format: 'Please enter a correct email',
        404: 'User with this email not found',
        exist: 'Email already in use',
        same: 'Nothing to change . Email are the same',
      },
    },
    sms: {
      label: 'Verification code',
      placeholder: '0 0 0 0 0 0',
      errors: {
        required: 'Sms code required',
        404: 'Sms code invalid or expired',
        length: 'Must be exactly 6 digits',
      },
    },
    password: {
      label: 'Password',
      placeholder: '********',
      errors: {
        required: 'Password required',
        match: 'Password does not match',
        min: 'Password is too short - should be 6 chars minimum.',
        max: 'Password is too long - should be 18 chars maximum.',
        numberExist: 'Password should contain minimum 1 number',
        uppercaseExist: 'Password should contain minimum 1 uppercase',
        wrongPassword: 'Wrong password',
      },
    },
    confirmPassword: {
      label: 'Confirm password',
      placeholder: '********',
    },
    firstName: {
      label: 'First Name',
      placeholder: 'Enter your first name',
      errors: {
        required: 'First name required',
        min: 'Too Short!',
        max: 'Too Long!',
        onlyLatin: 'First name can only contain Latin letters.',
      },
    },
    lastName: {
      label: 'Last Name',
      placeholder: 'Enter your last name',
      errors: {
        required: 'Last Name required',
        min: 'Too Short!',
        max: 'Too Long!',
        onlyLatin: 'Last Name can only contain Latin letters.',
      },
    },
    size: {
      value: 'Up to {{weight}} Kg',
      valueKg: '{{weight}} Kg',
    },
  },
  buttons: {
    back: 'Back',
    skip: 'Skip',
    skipForNow: 'Skip for now',
    next: 'Next',
    cancel: 'Cancel',
    ok: 'Ok',
    change: 'Change',
  },
};
