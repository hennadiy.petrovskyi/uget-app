export enum OrderViewerType {
  ORDER_OWNER = 'owner',
  TRAVELER = 'traveler',
  DEFAULT = 'default',
}

export enum OrderOwnerStatus {
  AVAILABLE = 'available',
  ACCEPTED = 'accepted',
  DRAFT = 'draft',
  EXPIRED = 'expired',
  REQUEST_INCOMING = 'request.incoming',
  REQUEST_OUTGOING = 'request.outgoing',
  PICKED_UP = 'picked-up',
  DELIVERED = 'delivered',
}

export enum OrderActionsViewType {
  EXPANDED = 'expanded',
  MINIMIZED = 'minimized',
}

export enum OrderViewType {
  PREVIEW = 'preview',
  MINIMIZED = 'minimized',
}
