export enum TripForOrderStatus {
  REQUEST_INCOMING = 'incoming',
  REQUEST_OUTGOING = 'outgoing',
  ACCEPTED = 'accepted',
  AVAILABLE = 'available',
  DECLINED = 'declined',
}
