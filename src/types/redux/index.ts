import { rootReducer } from '../../utils/store';

export type AppState = ReturnType<typeof rootReducer>;
