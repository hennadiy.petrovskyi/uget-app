import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { HomeScreen } from '../screens';
import { Messages } from './Messages';
import { NAVIGATION, COLORS } from '../config/constants';

import {
  MY_ORDER_ICON,
  HOME_ICON,
  ACTIVE_ORDER_ICON,
  ACTIVE_HOME_ICON,
  ACTIVE_MY_TRIPS_ICON,
  MY_TRIPS_ICON,
  ACTIVE_MESSAGES_ICON,
  MESSAGES_ICON,
  PROFILE_ICON,
  ACTIVE_PROFILE_ICON,
} from '../assets/svg';
import { ProfileStack } from './Profile';
import { TripsStack } from './Trips';
import { OrdersStack } from './Orders';

const Tabs = createBottomTabNavigator();

export const TabsRoute = () => {
  return (
    <Tabs.Navigator
      screenOptions={{
        tabBarStyle: { backgroundColor: COLORS.GREY2 },
        tabBarItemStyle: { paddingVertical: 4 },
        headerShown: false,
      }}
    >
      <Tabs.Screen
        name={NAVIGATION.TABS_ROUTE.HOME_SCREEN}
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarActiveTintColor: COLORS.MAIN_COLOR,
          tabBarInactiveTintColor: COLORS.BLACK,
          tabBarIcon: ({ focused }) =>
            focused ? <ACTIVE_HOME_ICON /> : <HOME_ICON />,
        }}
      />
      <Tabs.Screen
        name={NAVIGATION.TABS_ROUTE.ORDER_SCREENS}
        component={OrdersStack}
        options={{
          tabBarLabel: 'My orders',
          tabBarActiveTintColor: COLORS.MAIN_COLOR,
          tabBarInactiveTintColor: COLORS.BLACK,

          tabBarIcon: ({ focused }) =>
            focused ? <ACTIVE_ORDER_ICON /> : <MY_ORDER_ICON />,
        }}
      />
      <Tabs.Screen
        name={NAVIGATION.TABS_ROUTE.MY_TRIPS}
        component={TripsStack}
        options={{
          tabBarLabel: 'My trips',
          tabBarActiveTintColor: COLORS.MAIN_COLOR,
          tabBarInactiveTintColor: COLORS.BLACK,

          tabBarIcon: ({ focused }) =>
            focused ? <ACTIVE_MY_TRIPS_ICON /> : <MY_TRIPS_ICON />,
        }}
      />
      <Tabs.Screen
        name={NAVIGATION.TABS_ROUTE.MESSAGES}
        component={Messages}
        options={{
          tabBarLabel: 'Messages',
          tabBarActiveTintColor: COLORS.MAIN_COLOR,
          tabBarInactiveTintColor: COLORS.BLACK,

          tabBarIcon: ({ focused }) =>
            focused ? <ACTIVE_MESSAGES_ICON /> : <MESSAGES_ICON />,
        }}
      />
      <Tabs.Screen
        name={NAVIGATION.TABS_ROUTE.PROFILE}
        component={ProfileStack}
        options={{
          tabBarLabel: 'Profile',
          tabBarActiveTintColor: COLORS.MAIN_COLOR,
          tabBarInactiveTintColor: COLORS.BLACK,
          tabBarIcon: ({ focused }) =>
            focused ? <ACTIVE_PROFILE_ICON /> : <PROFILE_ICON />,
        }}
      />
    </Tabs.Navigator>
  );
};
