import { StyleSheet } from 'react-native';
import { COLORS } from '../config/constants';

export default StyleSheet.create({
  backBtnWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnText: {
    fontFamily: 'FiraSans-SemiBold',
    fontSize: 16,
    color: COLORS.BLACK,
    marginLeft: 8,
  },
});
