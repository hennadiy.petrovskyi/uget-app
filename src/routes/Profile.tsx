import React, { useEffect } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import {
  ProfileInitScreen,
  AppSettings,
  EditProfile,
  InviteFriends,
  UserDetails,
} from '../screens';
import {
  NAVIGATION,
  handleStackNavigatorTopHeaderOptions,
} from '../config/constants';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/core';
import { useFocusEffect } from '@react-navigation/native';

import { setNavigationSession } from '../utils/store/general/actions';
import { View } from 'react-native';

const Stack = createNativeStackNavigator();

export const ProfileStack = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);
  const { MAIN, APP_SETTINGS, EDIT_PROFILE, INVITE_FRIENDS, PROFILE } =
    NAVIGATION.PROFILE;

  useFocusEffect(
    React.useCallback(() => {
      if (!isAuth) {
        dispatch(
          setNavigationSession({
            from: {
              stack: NAVIGATION.UGET_ROUTE.TABS_ROUTE,
              screen: NAVIGATION.PROFILE.MAIN,
            },
            to: {
              stack: NAVIGATION.UGET_ROUTE.TABS_ROUTE,
              screen: NAVIGATION.PROFILE.MAIN,
            },
          }),
        );

        navigation.navigate(
          NAVIGATION.UGET_ROUTE.AUTHENTICATION as never,
          {
            screen: NAVIGATION.AUTHENTICATION.LOG_IN_BY_PHONE,
          } as never,
        );
      }
    }, [isAuth]),
  );

  return (
    <Stack.Navigator
      screenOptions={({ route, navigation }) =>
        handleStackNavigatorTopHeaderOptions(route.name, () =>
          navigation.goBack(),
        )
      }
    >
      <Stack.Screen name={MAIN} component={ProfileInitScreen} />
      <Stack.Screen name={PROFILE} component={UserDetails} />
      <Stack.Screen name={EDIT_PROFILE} component={EditProfile} />
      <Stack.Screen name={APP_SETTINGS} component={AppSettings} />
      <Stack.Screen name={INVITE_FRIENDS} component={InviteFriends} />
    </Stack.Navigator>
  );
};
