import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import {
  CreateOrder,
  MyOrders,
  CreditCardDetails,
  Checkout,
  TravelersList,
  Requests,
} from '../screens';
import {
  NAVIGATION,
  handleStackNavigatorTopHeaderOptions,
} from '../config/constants';

const Stack = createNativeStackNavigator();

export const OrdersStack = () => {
  const {
    CREATE_ORDER,
    MY_ORDERS,
    CREDIT_CARD_DETAILS,
    CHECKOUT,
    TRAVELERS_LIST,
    REQUESTS,
  } = NAVIGATION.ORDER_SCREENS;

  return (
    <Stack.Navigator
      screenOptions={({ route, navigation }) =>
        handleStackNavigatorTopHeaderOptions(route.name, () =>
          navigation.goBack(),
        )
      }
    >
      <Stack.Screen name={MY_ORDERS} component={MyOrders} />
      <Stack.Screen name={CREATE_ORDER} component={CreateOrder} />
      <Stack.Screen name={TRAVELERS_LIST} component={TravelersList} />
      <Stack.Screen name={REQUESTS} component={Requests} />
      {/* <Stack.Screen name={CHECKOUT} component={Checkout} /> */}
      {/* <Stack.Screen name={CREDIT_CARD_DETAILS} component={CreditCardDetails} /> */}
    </Stack.Navigator>
  );
};
