import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import {
  SignUp,
  VerifyEmail,
  VerifyPhoneNumber,
  ForgotPassword,
  ChangeEmail,
  ChangePhoneNumber,
  LoginByPhone,
  LoginVerificationCode,
  LoginByEmail,
  ChangePassword,
} from '../screens';
import {
  NAVIGATION,
  handleStackNavigatorTopHeaderOptions,
} from '../config/constants';

const Stack = createNativeStackNavigator();

export const Auth = () => {
  const {
    SIGN_UP,
    LOG_IN_BY_EMAIL,
    LOG_IN_BY_PHONE,
    LOG_IN_VERIFICATION_CODE,
    VERIFY_EMAIL,
    VERIFY_PHONE_NUMBER,
    FORGOT_PASSWORD,
    CHANGE_EMAIL,
    CHANGE_PHONE_NUMBER,
    CHANGE_PASSWORD,
  } = NAVIGATION.AUTHENTICATION;

  return (
    <Stack.Navigator
      screenOptions={({ route, navigation }) =>
        handleStackNavigatorTopHeaderOptions(route.name, () =>
          navigation.goBack(),
        )
      }
    >
      <Stack.Screen name={SIGN_UP} component={SignUp} />
      <Stack.Screen name={LOG_IN_BY_PHONE} component={LoginByPhone} />
      <Stack.Screen
        name={LOG_IN_VERIFICATION_CODE}
        component={LoginVerificationCode}
      />
      <Stack.Screen name={LOG_IN_BY_EMAIL} component={LoginByEmail} />
      <Stack.Screen name={VERIFY_EMAIL} component={VerifyEmail} />
      <Stack.Screen name={VERIFY_PHONE_NUMBER} component={VerifyPhoneNumber} />
      <Stack.Screen name={FORGOT_PASSWORD} component={ForgotPassword} />
      <Stack.Screen name={CHANGE_PHONE_NUMBER} component={ChangePhoneNumber} />
      <Stack.Screen name={CHANGE_EMAIL} component={ChangeEmail} />
      <Stack.Screen name={CHANGE_PASSWORD} component={ChangePassword} />
    </Stack.Navigator>
  );
};
