import React, { useEffect, useState } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useDispatch, useSelector } from 'react-redux';

import { OnBoarding } from '../screens';
import { TabsRoute } from './BottomTab';
import { NAVIGATION } from '../config/constants';
import { Auth } from './Auth';
import { webSocket } from '../api/api';
import {
  addNewConversationMessage,
  conversationsHydrateState,
} from '../utils/store/chat/actions';
import {
  getAllConversations,
  getMyOrders,
  getMyTrips,
  getUserProfile,
} from '../api';
import { headerStyle } from '../config/constants/defaultHeaderStyles';

import { View } from 'react-native';
import { SocketEvents } from '../types/socket';
import { updateTripFilteredByOrder } from '../utils/store/trips/actions';
import { ordersUpdateOrderFilteredByTrip } from '../utils/store/orders/actions';

const Stack = createNativeStackNavigator();
const socket = webSocket();

socket.on('connect', (data) => {
  console.log('SOCKET:  connection established');
});

socket.on('connect_error', (err) => {
  console.log(err instanceof Error);
  console.log(err.message);
});

const ae = socket.listenersAny();

export default () => {
  const dispatch = useDispatch();
  const myUUID = useSelector((state) => state.profile.uuid);
  const isAuth = useSelector((state) => state.general.isAuth);
  const conversations = useSelector((state) => state.chat.conversations);
  const tripsForOrder = useSelector((state) => state.trips.filteredByOrder);
  const ordersForTrip = useSelector((state) => state.orders.filteredByTrip);

  useEffect(() => {
    if (!isAuth) {
      dispatch(conversationsHydrateState([]));

      socket.disconnect();
      return;
    }

    getAllConversations().then((data) => {
      if (data.length) {
        dispatch(conversationsHydrateState(data));
      }

      socket.connect();

      socket.on(myUUID, () => {
        getAllConversations().then((data) =>
          dispatch(conversationsHydrateState(data)),
        );
      });
    });

    return () => socket.disconnect();
  }, [isAuth]);

  useEffect(() => {
    conversations.map((conversation) => {
      socket.once(`get_message_${conversation.id}`, (data) => {
        console.log('WEBSOCKET : message send to CHAT listener', data);

        dispatch(addNewConversationMessage(data));
      });
    });
  }, [conversations]);

  useEffect(() => {
    socket.on(myUUID, (data) => {
      console.log('WEBSOCKET : message send to UUID listener', data);

      const handleActions = (action: SocketEvents) => {
        switch (action) {
          case SocketEvents.UPDATE_PROFILE:
            dispatch(getUserProfile());
            break;

          case SocketEvents.UPDATE_MY_ORDERS:
            dispatch(getMyOrders());
            break;

          case SocketEvents.UPDATE_MY_TRIPS:
            dispatch(getMyTrips());
            break;

          default:
            break;
        }
      };

      if (!data || !data.action) {
        return;
      }

      const isArray = Array.isArray(data.action);

      if (!isArray) {
        handleActions(data.action);
        return;
      }

      data.action.map(async (action) => {
        await handleActions(action);
      });
    });
  }, [myUUID]);

  useEffect(() => {
    tripsForOrder.map((trip) =>
      socket.once(`trip_${trip.uuid}`, ({ data }) => {
        console.log(`TRIP : ${trip.uuid} has new update`, data);

        dispatch(updateTripFilteredByOrder(data));
      }),
    );
  }, [tripsForOrder]);

  useEffect(() => {
    ordersForTrip.map((order) =>
      socket.once(`order_${order.uuid}`, ({ data }) => {
        console.log(`ORDER : ${order.uuid} has new update`, data);

        dispatch(ordersUpdateOrderFilteredByTrip(data));
      }),
    );
  }, [ordersForTrip]);

  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
        headerShown: false,
      }}
    >
      <Stack.Screen
        name={NAVIGATION.UGET_ROUTE.TABS_ROUTE}
        component={TabsRoute}
      />
      <Stack.Screen
        name={NAVIGATION.UGET_ROUTE.ON_BOARDING}
        component={OnBoarding}
        options={{ ...headerStyle, headerLeft: () => <View /> }}
      />
      <Stack.Screen
        name={NAVIGATION.UGET_ROUTE.AUTHENTICATION}
        component={Auth}
      />
    </Stack.Navigator>
  );
};
