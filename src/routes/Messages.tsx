import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { ChatScreen } from '../screens/MessageScreens/ChatScreen';
import { IndividualChatScreen } from '../screens/MessageScreens/IndividualChatScreen';
import {
  handleStackNavigatorTopHeaderOptions,
  NAVIGATION,
} from '../config/constants';

import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { setNavigationSession } from '../utils/store/general/actions';

const Stack = createNativeStackNavigator();

export const Messages = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.general.isAuth);
  const { CHAT, MESSAGES } = NAVIGATION.MESSAGE_SCREENS;

  useFocusEffect(
    React.useCallback(() => {
      if (!isAuth) {
        dispatch(
          setNavigationSession({
            from: {
              stack: NAVIGATION.UGET_ROUTE.TABS_ROUTE,
              screen: NAVIGATION.MESSAGE_SCREENS.MESSAGES,
            },
            to: {
              stack: NAVIGATION.UGET_ROUTE.TABS_ROUTE,
              screen: NAVIGATION.MESSAGE_SCREENS.MESSAGES,
            },
          }),
        );

        navigation.navigate(
          NAVIGATION.UGET_ROUTE.AUTHENTICATION as never,
          {
            screen: NAVIGATION.AUTHENTICATION.LOG_IN_BY_PHONE,
          } as never,
        );
      }
    }, [isAuth]),
  );

  return (
    <Stack.Navigator
      screenOptions={({ route, navigation }) =>
        handleStackNavigatorTopHeaderOptions(route.name, () =>
          navigation.goBack(),
        )
      }
    >
      <Stack.Screen name={MESSAGES} component={ChatScreen} />
      <Stack.Screen name={CHAT} component={IndividualChatScreen} />
    </Stack.Navigator>
  );
};
