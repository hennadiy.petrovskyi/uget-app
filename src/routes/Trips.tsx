import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import {
  MyTrips,
  CreateTrip,
  TripsOrders,
  AddTrip,
  AcceptedOrders,
  DeliveredOrders,
} from '../screens';
import {
  NAVIGATION,
  handleStackNavigatorTopHeaderOptions,
} from '../config/constants';

const Stack = createNativeStackNavigator();

export const TripsStack = () => {
  const {
    MY_TRIPS,
    ADD_TRIP,
    CREATE_TRIP,
    TRIPS_ORDERS,
    ACCEPTED_ORDERS,
    DELIVERED_ORDERS,
  } = NAVIGATION.TRIPS;

  return (
    <Stack.Navigator
      screenOptions={({ route, navigation }) =>
        handleStackNavigatorTopHeaderOptions(route.name, () =>
          navigation.goBack(),
        )
      }
    >
      <Stack.Screen name={MY_TRIPS} component={MyTrips} />
      <Stack.Screen name={ADD_TRIP} component={AddTrip} />
      <Stack.Screen name={CREATE_TRIP} component={CreateTrip} />
      <Stack.Screen name={TRIPS_ORDERS} component={TripsOrders} />
      <Stack.Screen name={ACCEPTED_ORDERS} component={AcceptedOrders} />
      <Stack.Screen name={DELIVERED_ORDERS} component={DeliveredOrders} />
    </Stack.Navigator>
  );
};
