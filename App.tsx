import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';

import { ModalNotificationsProvider } from './src/components/ModalNotifications';
import { ModalLoader, SettingsLoader } from './src/components';

import './i18n';

import { UgetRoute } from './src/routes';
import { store } from './src/utils/store';
import { linking } from './src/config/constants/linking';
import { fcmService } from './src/utils/pushNotifications/FCMservice';

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  useEffect(() => {
    fcmService.register();

    return () => {
      fcmService.unRegister();
    };
  }, []);

  return (
    <NavigationContainer linking={linking}>
      <Provider store={store}>
        <SettingsLoader>
          <StatusBar animated={true} barStyle="dark-content" />
          <UgetRoute />
          <ModalNotificationsProvider />
          <ModalLoader />
        </SettingsLoader>
      </Provider>
    </NavigationContainer>
  );
};

export default App;
