/**
 * @format
 */

import { AppRegistry } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import App from './App';
import { name as appName } from './app.json';

/**  This handler used by IOS to fire push notifications
 from quit state and background mode */
messaging().setBackgroundMessageHandler(async (remoteMessage) => {
  console.log(
    "[MESSAGING] setBackgroundMessageHandler : new message'",
    remoteMessage,
  );

  /**  If incoming remoteMessage from API ,it will be a DATA message only.
     In case of firebase , remoteMessage will include notification payload not in DATA
     To prevent duplication of message for android device when using Firebase console
     Message will be handled only by system UI */
  if (
    (Platform.OS === 'android' && !remoteMessage.notification) ||
    Platform.OS === 'ios'
  ) {
    await fcmService.onNotification(remoteMessage);
  }
});

AppRegistry.registerComponent(appName, () => App);
